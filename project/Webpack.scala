import play.sbt.PlayRunHook
import sbt.File

import scala.sys.process.Process

object Webpack {
  def apply(base: File): PlayRunHook = {
    object WebpackHook extends PlayRunHook {
      var process: Option[Process] = None

      override def beforeStarted(): Unit = {
        (Process("npm ci", base) ### Process("npm run build", base)).!
      }

      override def afterStarted(): Unit = {
        process = Some( Process("npm run build-n-watch", base).run )
      }

      override def afterStopped() = {
        process.foreach(_.destroy())
        process = None
      }
    }

    WebpackHook
  }
}
