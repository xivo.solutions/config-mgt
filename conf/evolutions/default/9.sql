# --- !Ups
ALTER TABLE callback_request ADD COLUMN due_date DATE NOT NULL DEFAULT now();

# --- !Downs
ALTER TABLE callback_request DROP COLUMN due_date;