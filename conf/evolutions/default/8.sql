# --- !Ups
CREATE TABLE preferred_callback_period (
    uuid UUID PRIMARY KEY,
    name VARCHAR(128) NOT NULL DEFAULT '',
    period_start TIME WITHOUT TIME ZONE NOT NULL,
    period_end TIME WITHOUT TIME ZONE NOT NULL,
    "default" BOOLEAN DEFAULT FALSE
);
CREATE TRIGGER preferred_callback_period_uuid_trigger BEFORE INSERT ON preferred_callback_period FOR EACH ROW EXECUTE PROCEDURE uuid_generator();

INSERT INTO preferred_callback_period(name, period_start, period_end, "default") VALUES
    ('Toute la journée', '09:00:00', '17:00:00', true),
    ('Matin', '09:00:00', '12:00:00', false),
    ('Après-midi', '14:00:00', '17:00:00', false);

ALTER TABLE callback_request ADD COLUMN preferred_callback_period_uuid UUID REFERENCES preferred_callback_period(uuid);

UPDATE callback_request SET preferred_callback_period_uuid = (SELECT uuid FROM preferred_callback_period WHERE "default" IS TRUE LIMIT 1);

# --- !Downs
ALTER TABLE callback_request DROP COLUMN preferred_callback_period_uuid;
DROP TABLE preferred_callback_period;