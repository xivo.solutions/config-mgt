# --- !Ups
CREATE TYPE right_type AS ENUM('queue', 'agentgroup', 'incall');

CREATE TABLE rights (
    id SERIAL PRIMARY KEY,
    user_id INTEGER REFERENCES users(id) NOT NULL,
    category right_type NOT NULL,
    category_id INTEGER NOT NULL
);

INSERT INTO rights(user_id, category, category_id)
    SELECT user_id, 'queue'::right_type, queue_id FROM queue_rights;

INSERT INTO rights(user_id, category, category_id)
    SELECT user_id, 'agentgroup'::right_type, group_id FROM group_rights;

DROP TABLE queue_rights;
DROP TABLE group_rights;

# --- !Downs
CREATE TABLE group_rights(
    id SERIAL PRIMARY KEY,
    user_id INTEGER REFERENCES users(id),
    group_id INTEGER
);

CREATE TABLE queue_rights(
    id SERIAL PRIMARY KEY,
    user_id INTEGER REFERENCES users(id),
    queue_id INTEGER
);

INSERT INTO queue_rights(user_id, queue_id) VALUES
    SELECT user_id, category_id FROM rights WHERE category = 'queue'::right_type;

INSERT INTO group_rights(user_id, group_id) VALUES
    SELECT user_id, category_id FROM rights WHERE category = 'agentgroup'::right_type;

DROP TABLE rights;
DROP TYPE right_type;