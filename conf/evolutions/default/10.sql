# --- !Ups
ALTER TABLE callback_request ADD COLUMN reference_number SERIAL NOT NULL;
CREATE INDEX callback_request__idx_reference_number ON callback_request(reference_number);

# --- !Downs
ALTER TABLE callback_request DROP COLUMN reference_number;