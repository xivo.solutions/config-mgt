package xivo.model

import play.api.libs.json.{JsFalse, JsNull, Json}
import ws.model.LineType
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class XivoUserSpecs extends AnyWordSpec with Matchers {

  "XivoUser" should {
    "convert to json with null values" in {
      val x = XivoUser(
        1,
        "My name",
        Some(123456),
        Some(LineType.UA),
        Some("+33612345678"),
        "test",
        None,
        None,
        disabled = false,
        Some(List("blue", "red", "green"))
      )
      Json.toJson(x) shouldEqual (
        Json.obj(
          "id"             -> x.id,
          "fullName"       -> x.fullName,
          "provisioning"   -> x.provisioning,
          "lineType"       -> x.lineType,
          "phoneNumber"    -> x.phoneNumber,
          "entity"         -> x.entity,
          "mdsName"        -> JsNull,
          "mdsDisplayName" -> JsNull,
          "disabled"       -> JsFalse,
          "labels"         -> x.labels
        )
      )
    }
  }
}
