package xivo.model

import play.api.libs.json._
import xivo.model.rabbitmq._
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class MediaServerConfigSpec extends AnyWordSpec with Matchers {

  "MediaServerConfig" should {

    "be serialized to json" in {
      val expected = Json.parse(
        """{"id": 1, "name":"mds1", "display_name":"MDS 1", "voip_ip": "192.168.1.232", "read_only": false}"""
      )
      val mds1 =
        MediaServerConfig(1, "mds1", "MDS 1", Some("192.168.1.232"), false)
      Json.toJson(mds1) shouldBe expected
    }

    "be deserialized from json" in {
      val json = Json.parse(
        """{"id": 1, "name":"mds1", "display_name":"MDS 1", "voip_ip": "192.168.1.232", "read_only": false}"""
      )
      val expected =
        MediaServerConfig(1, "mds1", "MDS 1", Some("192.168.1.232"), false)
      json.as[MediaServerConfig] shouldBe expected
    }

    "have an implicit for RabbitMessage creation" in {
      import ToRabbitMessage.ops._
      val mds =
        MediaServerConfig(1, "mds1", "MDS 1", Some("192.168.1.232"), false)

      mds.toRabbitMessage(ObjectCreated) shouldBe ObjectEvent(
        MediaServerCreated,
        1
      )
      mds.toRabbitMessage(ObjectEdited) shouldBe ObjectEvent(
        MediaServerEdited,
        1
      )
      mds.toRabbitMessage(ObjectDeleted) shouldBe ObjectEvent(
        MediaServerDeleted,
        1
      )
    }
  }

  "MediaServerConfigNoId" should {

    "be serialized to json" in {
      val expected = Json.parse(
        """{"name":"mds1", "display_name":"MDS 1", "voip_ip": "192.168.1.232"}"""
      )
      val mds1 = MediaServerConfigNoId("mds1", "MDS 1", Some("192.168.1.232"))
      Json.toJson(mds1) shouldBe expected
    }

    "be deserialized from json" in {
      val json = Json.parse(
        """{"name":"mds1", "display_name":"MDS 1", "voip_ip": "192.168.1.232"}"""
      )
      val expected =
        MediaServerConfigNoId("mds1", "MDS 1", Some("192.168.1.232"))
      json.as[MediaServerConfigNoId] shouldBe expected
    }
  }

  "MediaServerConfigList" should {
    "be serialized to json" in {}
  }
}
