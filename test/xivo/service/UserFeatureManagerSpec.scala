package xivo.service

import java.util.UUID
import docker.DockerDBTest
import org.scalatest.BeforeAndAfterEach
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Second, Seconds, Span}
import play.api.inject.guice.GuiceApplicationBuilder
import xivo.model.{GenericBsFilter, UserFeature}
import play.api.Application

import java.sql.Connection
import scala.util.{Failure, Success}

class UserFeatureManagerSpec extends DockerDBTest with ScalaFutures with BeforeAndAfterEach {
  implicit val pc: PatienceConfig = PatienceConfig(Span(20, Seconds), Span(1, Second))

  override protected def beforeEach(): Unit = {
      executeSQL(List("TRUNCATE userfeatures, user_line, userpreferences"))(xivoConnection.get)
  }

  class Helper() {

    implicit lazy val app: Application = new GuiceApplicationBuilder()
      .configure(playConfig ++ Map(
        "db.default.url" -> xcjdbcurl.get,
        "db.xivo.url" -> xivojdbcurl.get,
        "db.xc.url" -> xcjdbcurl.get
      ))
      .build()

    implicit val connection: Connection = xivoConnection.get
  }

  "xivo.service.UserFeatureManager" should {
    "create xivo user" in new Helper() {
      val userFeatureManager =
        app.injector.instanceOf(classOf[UserFeatureManager])
      val u = UserFeature(
        None,
        UUID.randomUUID().toString,
        "James",
        "Bond",
        None,
        None,
        None,
        None,
        None,
        30,
        5,
        0,
        "jbond",
        "mypass",
        None,
        1,
        0,
        1,
        0,
        0,
        0,
        0,
        "",
        0,
        "",
        0,
        "",
        "",
        "",
        "",
        "",
        GenericBsFilter.No,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        0,
        "",
        None,
        0,
        None
      )

      val created = userFeatureManager.create(u)

      created.isSuccess shouldBe true
      created.get.id shouldNot be(None)
    }

    "get a xivo user" in new Helper() {
      val userFeatureManager =
        app.injector.instanceOf(classOf[UserFeatureManager])
      val u = UserFeature(
        None,
        UUID.randomUUID().toString,
        "James",
        "Bond",
        None,
        None,
        None,
        None,
        None,
        30,
        5,
        0,
        "jbond",
        "mypass",
        None,
        1,
        0,
        1,
        0,
        0,
        0,
        0,
        "",
        0,
        "",
        0,
        "",
        "",
        "",
        "",
        "",
        GenericBsFilter.No,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        0,
        "",
        None,
        0,
        None
      )

      val createdUser = userFeatureManager.create(u).get

      val fetchedUser = userFeatureManager.get(createdUser.id.get)

      fetchedUser.isSuccess shouldBe true
      fetchedUser.get shouldEqual createdUser
    }

    "update a xivo user" in new Helper() {
      val userFeatureManager =
        app.injector.instanceOf(classOf[UserFeatureManager])
      val u = UserFeature(
        None,
        UUID.randomUUID().toString,
        "James",
        "Bond",
        None,
        None,
        None,
        None,
        None,
        30,
        5,
        0,
        "jbond",
        "mypass",
        None,
        1,
        0,
        1,
        0,
        0,
        0,
        0,
        "",
        0,
        "",
        0,
        "",
        "",
        "",
        "",
        "",
        GenericBsFilter.No,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        0,
        "",
        None,
        0,
        None
      )

      val createdUser =
        userFeatureManager.create(u).get.copy(passwdclient = "xxxx")
      userFeatureManager.update(createdUser) match {
        case Success(_) =>
        case Failure(t) =>
          t.printStackTrace()
          fail(t)
      }

      val fetchedUser = userFeatureManager.get(createdUser.id.get)

      fetchedUser.isSuccess shouldBe true
      fetchedUser.get shouldEqual createdUser
    }

    "fail updating a non existing xivo user" in new Helper() {
      val userFeatureManager =
        app.injector.instanceOf(classOf[UserFeatureManager])
      val u = UserFeature(
        Some(1),
        UUID.randomUUID().toString,
        "James",
        "Bond",
        None,
        None,
        None,
        None,
        None,
        30,
        5,
        0,
        "jbond",
        "mypass",
        None,
        1,
        0,
        1,
        0,
        0,
        0,
        0,
        "",
        0,
        "",
        0,
        "",
        "",
        "",
        "",
        "",
        GenericBsFilter.No,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        0,
        "",
        None,
        0,
        None
      )

      userFeatureManager.delete(1)

      userFeatureManager.update(u).isSuccess shouldBe false
    }

    "delete a xivo user" in new Helper() {
      val userFeatureManager =
        app.injector.instanceOf(classOf[UserFeatureManager])
      val u = UserFeature(
        None,
        UUID.randomUUID().toString,
        "James",
        "Bond",
        None,
        None,
        None,
        None,
        None,
        30,
        5,
        0,
        "jbond",
        "mypass",
        None,
        1,
        0,
        1,
        0,
        0,
        0,
        0,
        "",
        0,
        "",
        0,
        "",
        "",
        "",
        "",
        "",
        GenericBsFilter.No,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        0,
        "",
        None,
        0,
        None
      )

      val createdUser = userFeatureManager.create(u).get

      userFeatureManager.delete(createdUser.id.get)

      val fetchedUser = userFeatureManager.get(createdUser.id.get)

      fetchedUser.isSuccess shouldBe false
    }

    "fail deleting a non existing xivo user" in new Helper() {
      val userFeatureManager =
        app.injector.instanceOf(classOf[UserFeatureManager])

      userFeatureManager.delete(1)

      userFeatureManager.delete(1).isSuccess shouldBe false
    }

    "get all xivo users" in new Helper() {
      val userFeatureManager =
        app.injector.instanceOf(classOf[UserFeatureManager])
      val u1 = UserFeature(
        None,
        UUID.randomUUID().toString,
        "James",
        "Bond",
        None,
        None,
        None,
        None,
        None,
        30,
        5,
        0,
        "jbond",
        "mypass",
        None,
        1,
        0,
        1,
        0,
        0,
        0,
        0,
        "",
        0,
        "",
        0,
        "",
        "",
        "",
        "",
        "",
        GenericBsFilter.No,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        0,
        "",
        None,
        0,
        None
      )

      val u2 = UserFeature(
        None,
        UUID.randomUUID().toString,
        "Jason",
        "Bornes",
        None,
        None,
        None,
        None,
        None,
        30,
        5,
        0,
        "jbornes",
        "mypass",
        None,
        1,
        0,
        1,
        0,
        0,
        0,
        0,
        "",
        0,
        "",
        0,
        "",
        "",
        "",
        "",
        "",
        GenericBsFilter.No,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        0,
        "",
        None,
        0,
        None
      )

      val createdUser1 = userFeatureManager.create(u1).get
      val createdUser2 = userFeatureManager.create(u2).get

      val fetchedUsers = userFeatureManager.all()

      fetchedUsers.isSuccess shouldBe true
      fetchedUsers.get should contain.allOf(createdUser1, createdUser2)
    }
  }

  "get a xivo user by username" in new Helper {
    val userFeatureManager =
      app.injector.instanceOf(classOf[UserFeatureManager])
    val u = UserFeature(
      None,
      UUID.randomUUID().toString,
      "Test",
      "LoginSearch",
      None,
      None,
      None,
      None,
      None,
      30,
      5,
      0,
      "tlogin",
      "mypass",
      None,
      1,
      0,
      1,
      0,
      0,
      0,
      0,
      "",
      0,
      "",
      0,
      "",
      "",
      "",
      "",
      "",
      GenericBsFilter.No,
      None,
      None,
      None,
      None,
      None,
      None,
      None,
      None,
      0,
      "",
      None,
      0,
      None
    )

    val createdUser = userFeatureManager.create(u).get
    val fetchedUser = userFeatureManager.get(createdUser.loginclient)

    fetchedUser.isSuccess shouldBe true
    fetchedUser.get shouldEqual createdUser
  }
}
