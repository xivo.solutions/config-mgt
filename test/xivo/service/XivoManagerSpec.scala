package xivo.service

import model.Utils
import xivo.model.Xivo
import com.dimafeng.testcontainers.lifecycle.and
import docker.DockerDBTest
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.mockito.MockitoSugar
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.Application

import scala.util.{Failure, Success}

class XivoManagerSpec
    extends DockerDBTest
    with BeforeAndAfterEach
    with MockitoSugar {

  "get xivo uuid from database" in {
    withContainers { case xivoDB and xcDB =>
      implicit lazy val app: Application = new GuiceApplicationBuilder()
        .configure(playConfig ++ Map(
          "db.default.url" -> xcDB.jdbcUrl,
          "db.xc.url" -> xcDB.jdbcUrl,
          "db.xivo.url" -> xivoDB.jdbcUrl
        ))
        .build()

      val xivoManager = app.injector.instanceOf(classOf[XivoManager])
      val uuid = java.util.UUID.randomUUID()
      Utils.insertXivoUUID(uuid)(xivoConnection.get)

      xivoManager.get() match {
        case Success(xivo) =>
          xivo shouldBe Xivo(uuid)

        case Failure(f) =>
          fail(f.getMessage)
      }
    }
  }
}
