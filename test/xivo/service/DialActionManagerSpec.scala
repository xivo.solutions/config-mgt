package xivo.service

import model.Utils
import play.api.inject.guice.GuiceApplicationBuilder
import xivo.model.{DialActionAction, DialActionActionType, DialActionEventType}
import docker.DockerDBTest
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.mockito.MockitoSugar
import play.api.Application

import java.sql.Connection


class DialActionManagerSpec extends DockerDBTest with BeforeAndAfterEach with MockitoSugar {

  class Helper() {
    implicit lazy val app: Application = new GuiceApplicationBuilder()
      .configure(playConfig ++ Map(
        "db.default.url" -> xcjdbcurl.get,
        "db.xivo.url" -> xivojdbcurl.get,
        "db.xc.url" -> xcjdbcurl.get
      ))
      .build()
    implicit val connection: Connection = xivoConnection.get
  }

  override protected def beforeEach(): Unit = {
    implicit val connection: Connection = xivoConnection.get
    executeSQL(List("TRUNCATE dialaction"))
    Utils.insertDialAction("noanswer", "group", 1, "none")
    Utils.insertDialAction("congestion", "group", 1, "none")
    Utils.insertDialAction("chanunavail", "group", 1, "none")
    Utils.insertDialAction("noanswer", "group", 2, "user", Some("6"))
    Utils.insertDialAction(
      "congestion",
      "group",
      2,
      "group",
      Some("1"),
      Some("30")
    )
    Utils.insertDialAction(
      "chanunavail",
      "group",
      2,
      "queue",
      Some("3"),
      Some("10")
    )
  }

  "DialActionManager" should {
    "Retrieve all dial actions for all groups from the database" in new Helper {
      val category = "group"
      val expected =
        Map(
          1 -> Map(
            DialActionEventType.noAnswer -> DialActionAction(
              DialActionActionType.none
            ),
            DialActionEventType.congestion -> DialActionAction(
              DialActionActionType.none
            ),
            DialActionEventType.chanUnavailable -> DialActionAction(
              DialActionActionType.none
            )
          ),
          2 -> Map(
            DialActionEventType.noAnswer -> DialActionAction(
              DialActionActionType.user,
              Some("6")
            ),
            DialActionEventType.congestion -> DialActionAction(
              DialActionActionType.group,
              Some("1"),
              Some("30")
            ),
            DialActionEventType.chanUnavailable -> DialActionAction(
              DialActionActionType.queue,
              Some("3"),
              Some("10")
            )
          )
        )
      val dialActionManager = app.injector.instanceOf[DialActionManager]
      dialActionManager.getAllDialActions(category) match {
        case Left(error) =>
          fail(
            s"Error when retrieving dial actions for ${category}s: $error"
          )
        case Right(da) => da shouldEqual expected
      }
    }

    "Retrieve dial actions for a given group from the database" in new Helper {
      val id       = 1
      val category = "group"
      val expected = Map(
        DialActionEventType.noAnswer -> DialActionAction(
          DialActionActionType.none
        ),
        DialActionEventType.congestion -> DialActionAction(
          DialActionActionType.none
        ),
        DialActionEventType.chanUnavailable -> DialActionAction(
          DialActionActionType.none
        )
      )
      val dialActionManager = app.injector.instanceOf[DialActionManager]
      dialActionManager.getDialActions(id, category) match {
        case Left(error) =>
          fail(
            s"Error when retrieving dial actions for $category $id: $error"
          )
        case Right(da) => da shouldEqual expected
      }
    }

    "Insert dial actions of a newly created group into the database" in new Helper {
      val groupId  = 3
      val category = "group"
      val dialActions = Map(
        DialActionEventType.noAnswer -> DialActionAction(
          DialActionActionType.user,
          Some("7")
        ),
        DialActionEventType.congestion -> DialActionAction(
          DialActionActionType.queue,
          Some("1"),
          Some("10")
        ),
        DialActionEventType.chanUnavailable -> DialActionAction(
          DialActionActionType.none
        )
      )
      val expected =
        Map(
          DialActionEventType.noAnswer -> DialActionAction(
            DialActionActionType.user,
            Some("7")
          ),
          DialActionEventType.congestion -> DialActionAction(
            DialActionActionType.queue,
            Some("1"),
            Some("10")
          ),
          DialActionEventType.chanUnavailable -> DialActionAction(
            DialActionActionType.none
          )
        )
      val dialActionManager = app.injector.instanceOf[DialActionManager]
      dialActionManager.createDialActions(
        groupId,
        category,
        dialActions
      ) match {
        case Left(error) =>
          fail(
            s"Error when inserting dial actions for $category $groupId: $error"
          )

        case Right(da) =>
          da shouldEqual expected
      }
    }

    "Delete dial actions when a group is deleted" in new Helper {
      val id                = 1
      val category          = "group"
      val dialActionManager = app.injector.instanceOf[DialActionManager]
      dialActionManager.deleteDialActions(
        id,
        category
      ) match {
        case Left(error) =>
          fail(
            s"Error when deleting dial actions for $category $id: $error"
          )

        case Right(_) =>
          dialActionManager.getDialActions(
            id,
            category
          ) shouldEqual Right(
            Map.empty
          )
      }
    }

    "Update dial actions when a group is updated" in new Helper {
      val groupId  = 1
      val category = "group"
      val dialActions = Map(
        DialActionEventType.noAnswer -> DialActionAction(
          DialActionActionType.user,
          Some("7")
        ),
        DialActionEventType.congestion -> DialActionAction(
          DialActionActionType.queue,
          Some("1"),
          Some("10")
        ),
        DialActionEventType.chanUnavailable -> DialActionAction(
          DialActionActionType.none
        )
      )
      val expected          = dialActions
      val dialActionManager = app.injector.instanceOf[DialActionManager]
      dialActionManager.updateDialActions(
        groupId,
        category,
        dialActions
      ) match {
        case Left(error) =>
          fail(
            s"Error when inserting dial actions for $category $groupId: $error"
          )

        case Right(da) =>
          da shouldEqual expected
      }
    }
  }
}
