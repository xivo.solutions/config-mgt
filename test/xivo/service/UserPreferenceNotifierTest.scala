package xivo.service

import docker.DockerDBTest
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}
import org.mockito.Mockito.verify
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.mockito.MockitoSugar
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import xivo.model.rabbitmq.{ObjectEvent, UserPreferenceCreated, UserPreferenceDeleted, UserPreferenceEdited}
import xivo.service.rabbitmq.XivoRabbitEventsPublisher
import play.api.Application

import java.sql.Connection

class UserPreferenceNotifierTest extends DockerDBTest with BeforeAndAfterEach with MockitoSugar {

  class Helper() {
    val formatter: DateTimeFormatter =
      DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")

    val context: String = playConfig("play.http.context")
    val rabbitPublisher: XivoRabbitEventsPublisher =
      mock[XivoRabbitEventsPublisher]

    implicit lazy val app: Application = new GuiceApplicationBuilder()
      .configure(playConfig ++ Map(
        "db.default.url" -> xcjdbcurl.get,
        "db.xivo.url" -> xivojdbcurl.get,
        "db.xc.url" -> xcjdbcurl.get
      ))
      .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
      .build()
    implicit val connection: Connection = xivoConnection.get
  }

  "UserPreferenceNotifier" should {
    "send message to rabbitmq to notify of preference creation" in new Helper {
      val notifier = app.injector.instanceOf(classOf[UserPreferenceNotifier])
      val userId   = 1

      notifier.onUserPreferenceCreated(userId)
      verify(rabbitPublisher).publish(
        ObjectEvent(UserPreferenceCreated, userId)
      )
    }

    "send message to rabbitmq to notify of preference edition" in new Helper {
      val notifier = app.injector.instanceOf(classOf[UserPreferenceNotifier])
      val userId   = 1

      notifier.onUserPreferenceEdited(userId)
      verify(rabbitPublisher).publish(ObjectEvent(UserPreferenceEdited, userId))
    }

    "send message to rabbitmq to notify of preference deletion" in new Helper {
      val notifier = app.injector.instanceOf(classOf[UserPreferenceNotifier])
      val userId   = 1

      notifier.onUserPreferenceDeleted(userId)
      verify(rabbitPublisher).publish(
        ObjectEvent(UserPreferenceDeleted, userId)
      )
    }
  }
}
