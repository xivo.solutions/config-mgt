package xivo.service

import java.util.UUID
import org.scalatestplus.mockito.MockitoSugar
import pdi.jwt.{JwtAlgorithm, JwtJson}
import play.api.Configuration
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import xivo.model.MeetingRoomTokenContent

import java.sql.Timestamp
import java.time.Instant

class MeetingRoomAuthenticationSpec
    extends AnyWordSpec
    with Matchers
    with MockitoSugar {

  val secret        = "This is a secret !"
  val applicationId = "My App, what else"
  val domain        = "domain.com"

  val config = Map(
    "authentication.secret"        -> secret,
    "authentication.applicationId" -> applicationId,
    "authentication.domain"        -> domain
  )

  val meetingConfig = new MeetingRoomConfiguration(Configuration.from(config))

  "Generate a token with required Jitsi required informations" in {
    val roomDisplayName = "My Room"
    val number          = "1001"
    val uuid: UUID      = UUID.randomUUID()
    val requirepin      = false
    val room = MeetingRoomTokenContent(
      roomDisplayName,
      Some(number),
      uuid,
      requirepin,
      None
    )
    val auth = new MeetingRoomAuthentication(meetingConfig)

    val response = auth.encode(room)

    val json = JwtJson
      .decodeJson(response.token, secret, Seq(JwtAlgorithm.HS256))

    json.isSuccess shouldBe true
    (json.get \ "iss").as[String] shouldBe applicationId
    (json.get \ "sub").as[String] shouldBe domain
    (json.get \ "aud").as[String] shouldBe applicationId
    (json.get \ "room").as[String] shouldBe room.uuid.toString
  }

  "generate a token with meeting room information" in {
    import MeetingRoomTokenContent.timestampReads
    val roomDisplayName = "My Room"
    val number          = "1001"
    val uuid: UUID      = UUID.randomUUID()
    val requirepin      = false
    val timestamp       = Timestamp.from(Instant.now())
    timestamp.setNanos(0)

    val room = MeetingRoomTokenContent(
      roomDisplayName,
      Some(number),
      uuid,
      requirepin,
      Some(timestamp)
    )
    val auth = new MeetingRoomAuthentication(meetingConfig)

    val response = auth.encode(room)

    val json = JwtJson
      .decodeJson(response.token, secret, Seq(JwtAlgorithm.HS256))

    json.isSuccess shouldBe true
    (json.get \ "roomuuid").as[String] shouldBe uuid.toString
    (json.get \ "roomdisplayname").as[String] shouldBe roomDisplayName
    (json.get \ "roomnumber").as[String] shouldBe number
    (json.get \ "requirepin").as[Boolean] shouldBe false
    (json.get \ "timestamp").as[Timestamp] shouldBe timestamp
  }
}
