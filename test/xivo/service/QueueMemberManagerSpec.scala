package xivo.service

import docker.DockerDBTest
import model.Utils
import org.scalatest.BeforeAndAfterEach
import play.api.Application
import play.api.inject.guice.GuiceApplicationBuilder
import xivo.model.{
  Agent,
  QueueCategory,
  QueueMember,
  QueueMemberError,
  QueueMemberUserType
}

import java.sql.Connection

class QueueMemberManagerSpec extends DockerDBTest with BeforeAndAfterEach {
  class Helper() {
    implicit lazy val app: Application = new GuiceApplicationBuilder()
      .configure(
        playConfig ++ Map(
          "db.default.url" -> xcjdbcurl.get,
          "db.xivo.url"    -> xivojdbcurl.get,
          "db.xc.url"      -> xcjdbcurl.get
        )
      )
      .build()

    implicit val connection: Connection = xivoConnection.get
  }

  override protected def beforeEach(): Unit = {
    executeSQL(
      List(
        "TRUNCATE agentfeatures",
        "TRUNCATE queuemember",
        "TRUNCATE userfeatures, user_line, userpreferences",
        "TRUNCATE queuefeatures",
        "TRUNCATE func_key_dest_group, groupfeatures"
      )
    )(xivoConnection.get)
    super.beforeEach()
  }

  "xivo.service.QueueMemberManager" should {
    "get queue member" in new Helper() {
      val queueMemberManager: QueueMemberManager =
        app.injector.instanceOf(classOf[QueueMemberManager])
      val members: List[QueueMember] = List(
        QueueMember(
          "queue1",
          1L,
          "Agent/1011",
          penalty = 1,
          0,
          QueueMemberUserType.Agent,
          1L,
          "Agent",
          QueueCategory.Queue,
          1
        ),
        QueueMember(
          "queue2",
          2L,
          "Agent/1011",
          penalty = 1,
          0,
          QueueMemberUserType.Agent,
          1L,
          "Agent",
          QueueCategory.Queue,
          1
        )
      )

      val agent: Agent =
        Agent(1L, "Agent", "One", "1001", "default", members, 1L, Some(1L))

      Utils.insertAgentFeatures(agent)
      Utils.insertXivoUser(1, "User", "One", Some("login1"), Some(1), Some(1))
      Utils.insertQueueFeature(1, "queue1", "Queue 1")
      Utils.insertQueueFeature(2, "queue2", "Queue 2")
      Utils.insertQueueMember(members.head, "agent", "queue")
      Utils.insertQueueMember(members(1), "agent", "queue")

      val fetchedAgents
          : Either[QueueMemberError, Map[Long, List[QueueMember]]] =
        queueMemberManager.getAllQueueMember(
          QueueMemberUserType.Agent,
          QueueCategory.Queue
        )

      fetchedAgents match {
        case Left(error) =>
          fail(
            s"Error when retrieving queue members for agent on queue: $error"
          )
        case Right(queueMembersMap) =>
          queueMembersMap shouldEqual Map(
            1L -> List(members.head),
            2L -> List(members(1))
          )
      }
    }

    "get all group member" in new Helper() {
      val queueMemberManager: QueueMemberManager =
        app.injector.instanceOf(classOf[QueueMemberManager])
      val members: List[QueueMember] = List(
        QueueMember(
          "group1",
          1L,
          "Local/id-1011@usercallback",
          penalty = 0,
          0,
          QueueMemberUserType.User,
          1L,
          "Local",
          QueueCategory.Group,
          1
        ),
        QueueMember(
          "group2",
          2L,
          "Local/id-1012@usercallback",
          penalty = 0,
          0,
          QueueMemberUserType.User,
          1L,
          "Local",
          QueueCategory.Group,
          1
        )
      )

      Utils.insertXivoUser(1, "User", "One", Some("login1"), Some(1), Some(1))
      Utils.insertGroupFeature(1, "group1", "1000")
      Utils.insertGroupFeature(2, "group2", "1002")
      Utils.insertQueueMember(members.head, "user", "group")
      Utils.insertQueueMember(members(1), "user", "group")

      val fetchedGroups
          : Either[QueueMemberError, Map[Long, List[QueueMember]]] =
        queueMemberManager.getAllQueueMember(
          QueueMemberUserType.User,
          QueueCategory.Group
        )

      fetchedGroups match {
        case Left(error) =>
          fail(
            s"Error when retrieving queue members for user on group: $error"
          )
        case Right(groupMembersMap) =>
          groupMembersMap shouldEqual Map(
            1L -> List(members.head),
            2L -> List(members(1))
          )
      }
    }

    "get a single group member" in new Helper {
      val queueMemberManager: QueueMemberManager =
        app.injector.instanceOf(classOf[QueueMemberManager])
      val group1Member: QueueMember = QueueMember(
        "group1",
        1L,
        "Local/id-1011@usercallback",
        penalty = 0,
        0,
        QueueMemberUserType.User,
        1L,
        "Local",
        QueueCategory.Group,
        1
      )
      val group2Member: QueueMember = QueueMember(
        "group2",
        2L,
        "Local/id-1012@usercallback",
        penalty = 0,
        0,
        QueueMemberUserType.User,
        1L,
        "Local",
        QueueCategory.Group,
        1
      )

      Utils.insertXivoUser(1, "User", "One", Some("login1"), Some(1), Some(1))
      Utils.insertGroupFeature(1, "group1", "1000")
      Utils.insertGroupFeature(2, "group2", "1002")
      Utils.insertQueueMember(group1Member, "user", "group")
      Utils.insertQueueMember(group2Member, "user", "group")

      val fetchedGroup: Either[QueueMemberError, List[QueueMember]] =
        queueMemberManager.getQueueMember(
          QueueMemberUserType.User,
          QueueCategory.Group,
          1L
        )

      fetchedGroup match {
        case Left(error) =>
          fail(
            s"Error when retrieving queue members for user on group: $error"
          )
        case Right(groupMembers) =>
          groupMembers shouldEqual List(group1Member)
      }
    }

    "not fail if retrieving members from an empty group" in new Helper {
      val queueMemberManager: QueueMemberManager =
        app.injector.instanceOf(classOf[QueueMemberManager])

      Utils.insertXivoUser(1, "User", "One", Some("login1"), Some(1), Some(1))
      Utils.insertGroupFeature(1, "group1", "1000")

      val fetchedGroup: Either[QueueMemberError, List[QueueMember]] =
        queueMemberManager.getQueueMember(
          QueueMemberUserType.User,
          QueueCategory.Group,
          1L
        )

      fetchedGroup match {
        case Left(error) =>
          fail(
            s"Error when retrieving queue members for user on group: $error"
          )
        case Right(groupMembers) =>
          groupMembers shouldEqual List.empty
      }
    }
  }
}
