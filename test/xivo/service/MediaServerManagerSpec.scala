package xivo.service

import java.sql.{Connection, SQLException}
import org.mockito.Mockito._
import docker.DockerDBTest
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import xivo.model.rabbitmq._
import xivo.model.{
  MediaServerConfig,
  MediaServerConfigNoId,
  MediaServerListConfig
}
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.mockito.MockitoSugar
import xivo.service.rabbitmq.XivoRabbitEventsPublisher
import play.api.Application

import scala.util.{Failure, Success}

class MediaServerManagerSpec
    extends DockerDBTest
    with BeforeAndAfterEach
    with MockitoSugar {

  val rabbitPublisher: XivoRabbitEventsPublisher =
    mock[XivoRabbitEventsPublisher]

  class Helper() {
    implicit lazy val app: Application = new GuiceApplicationBuilder()
      .configure(playConfig ++ Map(
        "db.default.url" -> xcjdbcurl.get,
        "db.xivo.url" -> xivojdbcurl.get,
        "db.xc.url" -> xcjdbcurl.get
      ))
      .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
      .build()

    implicit val connection: Connection = xivoConnection.get
  }

  override protected def beforeEach(): Unit = {
    reset(rabbitPublisher)
    executeSQL(List("TRUNCATE trunkfeatures, mediaserver, netiface"))(xivoConnection.get)

    super.beforeEach()
  }

  "MediaServerManager" should {
    "create a media server and publish to bus" in new Helper() {

      val mediaServerManager =
        app.injector.instanceOf(classOf[MediaServerManager])

      val mds = MediaServerConfigNoId(
        "mds1",
        "media server",
        Some("10.52.0.10")
      )

      mediaServerManager.create(mds) match {
        case Success(created) =>
          created shouldBe mds.withId(created.id)
          verify(rabbitPublisher).publish(created, ObjectCreated)
          mediaServerManager.delete(created.id)
        case Failure(f) => fail(f.getMessage)
      }

    }

    "not create a duplicated media server" in new Helper() {
      val mediaServerManager =
        app.injector.instanceOf(classOf[MediaServerManager])

      val mds =
        MediaServerConfigNoId("mds1", "media server", Some("10.52.0.10"))

      mediaServerManager.create(mds)

      val f = mediaServerManager.create(mds)
      f shouldBe a[Failure[_]]
      f.failed.get shouldBe a[SQLException]
    }

    "delete an existing media server and publish to bus" in new Helper() {

      val mediaServerManager =
        app.injector.instanceOf(classOf[MediaServerManager])

      val mds = MediaServerConfigNoId(
        "mds1",
        "media server",
        Some("10.52.0.10")
      )
      val res = for {
        mdsCreated <- mediaServerManager.create(mds)
        _ = reset(rabbitPublisher)
        v <- mediaServerManager.delete(mdsCreated.id)
      } yield v

      res match {
        case Success(m) =>
          m shouldBe mds.withId(m.id)
          verify(rabbitPublisher).publish(m, ObjectDeleted)
        case Failure(f) => fail(f.getMessage)
      }

    }

    "delete a non existing media server" in new Helper() {

      val mediaServerManager =
        app.injector.instanceOf(classOf[MediaServerManager])

      val mds = MediaServerConfigNoId(
        "mds1",
        "media server",
        Some("10.52.0.10")
      )

      val res = for {
        mdsCreated <- mediaServerManager.create(mds)
        _          <- mediaServerManager.delete(mdsCreated.id)
        error      <- mediaServerManager.delete(mdsCreated.id)
      } yield error

      res shouldBe a[Failure[_]]
    }

    "get an existing media server" in new Helper() {
      val mediaServerManager =
        app.injector.instanceOf(classOf[MediaServerManager])

      val mds = MediaServerConfigNoId(
        "mds1",
        "media server",
        Some("10.52.0.10")
      )

      val res = for {
        mdsCreated <- mediaServerManager.create(mds)
        value      <- mediaServerManager.get(mdsCreated.id)
      } yield value

      res match {
        case Success(m) =>
          m shouldBe mds.withId(m.id)
          mediaServerManager.delete(m.id)
        case Failure(f) => fail(f.getMessage)
      }
    }

    "get a non existing media server" in new Helper() {
      val mediaServerManager =
        app.injector.instanceOf(classOf[MediaServerManager])

      mediaServerManager.get(2) shouldBe a[Failure[_]]
    }

    "get all media servers" in new Helper() {
      val mediaServerManager =
        app.injector.instanceOf(classOf[MediaServerManager])

      val mds1 =
        MediaServerConfigNoId("mds1", "media server 1", Some("10.52.0.10"))
      val mds2 =
        MediaServerConfigNoId("mds2", "media server 2", Some("10.52.0.20"))

      val res1   = mediaServerManager.create(mds1)
      val res2   = mediaServerManager.create(mds2)
      val resAll = mediaServerManager.all()

      val expected: MediaServerListConfig = MediaServerListConfig(
        List(mds1.withId(res1.get.id), mds2.withId(id = res2.get.id))
      )

      resAll match {
        case Success(response) => response shouldBe expected
        case Failure(f)        => fail(f.getMessage)
      }
    }

    "update an existing media server and publish to bus" in new Helper() {
      val mediaServerManager =
        app.injector.instanceOf(classOf[MediaServerManager])

      val mds = MediaServerConfigNoId(
        "mds1",
        "media server 1",
        Some("10.52.0.10")
      )

      val insertId = mediaServerManager.create(mds).get.id
      val updated = MediaServerConfig(
        insertId,
        "mds2",
        "media server 2",
        Some("10.52.0.22"),
        read_only = false
      )
      val expected: MediaServerConfig = MediaServerConfig(
        insertId,
        "mds1",
        "media server 2",
        Some("10.52.0.22"),
        read_only = false
      )

      reset(rabbitPublisher)
      mediaServerManager.update(updated)
      verify(rabbitPublisher).publish(updated, ObjectEdited)
      mediaServerManager.get(insertId).get shouldBe expected
    }

    "update a non existing media server" in new Helper() {
      val mediaServerManager =
        app.injector.instanceOf(classOf[MediaServerManager])

      val mdsUpdated: MediaServerConfig = MediaServerConfig(
        148,
        "mdsx",
        "media server",
        Some("10.52.0.11"),
        read_only = false
      )

      val res = for {
        value <- mediaServerManager.update(mdsUpdated)
      } yield value

      res match {
        case Success(_) =>
          fail("Return Success for an edition of a non existing media server")
        case Failure(_) => succeed
      }
    }
  }
}
