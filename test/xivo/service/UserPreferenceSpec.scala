package xivo.service

import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import xivo.model.rabbitmq.{
  ObjectCreated,
  ObjectDeleted,
  ObjectEdited,
  ObjectEvent,
  ToRabbitMessage,
  UserPreferenceCreated,
  UserPreferenceDeleted,
  UserPreferenceEdited
}

class UserPreferenceSpec extends AnyWordSpec with Matchers {

  "UserPreference" should {
    "have an implicit for RabbitMessage creation" in {
      import ToRabbitMessage.ops.toAllToRabbitMessageOps
      val userPref =
        UserPreference(1, "some_key", "some_value", "some_value_type")

      userPref.toRabbitMessage(ObjectCreated) shouldBe ObjectEvent(
        UserPreferenceCreated,
        1
      )
      userPref.toRabbitMessage(ObjectEdited) shouldBe ObjectEvent(
        UserPreferenceEdited,
        1
      )
      userPref.toRabbitMessage(ObjectDeleted) shouldBe ObjectEvent(
        UserPreferenceDeleted,
        1
      )
    }
  }

}
