package xivo.service

import docker.DockerDBTest
import model.Utils
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.mockito.MockitoSugar
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.Application

import java.sql.Connection
import scala.util.{Failure, Success}

class StaticSipManagerSpec
    extends DockerDBTest
    with BeforeAndAfterEach
    with MockitoSugar {

  class Helper() {
    implicit lazy val app: Application = new GuiceApplicationBuilder()
      .configure(playConfig ++ Map(
        "db.default.url" -> xcjdbcurl.get,
        "db.xivo.url" -> xivojdbcurl.get,
        "db.xc.url" -> xcjdbcurl.get
      ))
      .build()

    implicit val connection: Connection = xivoConnection.get
  }

  override protected def beforeEach(): Unit = {
    executeSQL(List("TRUNCATE staticsip"))(xivoConnection.get)
    super.beforeEach()
  }

  "get stun address" in new Helper {
    val staticSipManager = app.injector.instanceOf(classOf[StaticSipManager])
    val result           = SipConfig(Some("stun:stun.l.google.com:19302"))

    Utils.insertStunAddress(1, Some(result.stunAddress.get))

    staticSipManager.getStunAddress() match {
      case Success(stunAddr) =>
        stunAddr shouldBe result
      case Failure(f) => fail(f.getMessage)
    }
  }

  "Return None if no stun address is found" in new Helper {
    val staticSipManager = app.injector.instanceOf(classOf[StaticSipManager])

    staticSipManager.getStunAddress() match {
      case Success(stunAddr) =>
        stunAddr shouldBe SipConfig(None)
      case Failure(f) => fail(f.getMessage)
    }
  }

  "Return None if no value is configured" in new Helper {
    val staticSipManager = app.injector.instanceOf(classOf[StaticSipManager])
    Utils.insertStunAddress(2, None)

    staticSipManager.getStunAddress() match {
      case Success(stunAddr) =>
        stunAddr shouldBe SipConfig(None)
      case Failure(f) => fail(f.getMessage)
    }
  }
}
