package xc.service

import docker.DockerDBTest
import model.RecordingModeType
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.inject.guice.GuiceApplicationBuilder
import xc.model.Queue
import xivo.model.QueueFeature
import xivo.service.QueueFeatureManager
import play.api.Application

import java.sql.Connection
import scala.util.{Failure, Success}

class QueueSpec
    extends DockerDBTest
    with GuiceOneAppPerSuite
    with BeforeAndAfterEach {

  class Helper() {
    implicit lazy val app: Application = new GuiceApplicationBuilder()
      .configure(playConfig ++ Map(
        "db.default.url" -> xcjdbcurl.get,
        "db.xc.url" -> xcjdbcurl.get,
        "db.xivo.url" -> xivojdbcurl.get
      ))
      .build()

    implicit val connection: Connection = xcConnection.get
    val manager = app.injector.instanceOf(classOf[QueueManager])
    val xivoQueueFeatureManager =
      app.injector.instanceOf(classOf[QueueFeatureManager])
  }


  override protected def beforeEach(): Unit = {
    executeSQL(List("DELETE FROM xc.queue_members_default", "DELETE FROM xc.queues CASCADE"))(xcConnection.get)
    super.beforeEach()
  }
  "xc.service.QueueManager" should {
    "create a Queue" in new Helper() {
      val q = Queue(1, "queue1", "My First queue", "3000")

      manager.delete(1)

      manager.create(q) match {
        case Success(created) => created shouldEqual (q)
        case Failure(t) => fail(t)
      }

    }

    "get a Queue" in new Helper() {
      val q = Queue(1, "queue1", "My First queue", "3000")

      manager.delete(1)
      manager.create(q)
      manager.get(1) match {
        case Success(fetched) => fetched shouldEqual (q)
        case Failure(t) => fail(t)
      }
    }

    "get a Queue from a xivo QueueFeature if non existent " in new Helper() {
      xivoQueueFeatureManager.deleteByName("queue1")
      val xq = QueueFeature(
        None,
        "queue1",
        "My first Queue",
        "3000",
        None,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        "",
        "",
        None,
        None,
        0,
        None,
        None,
        0,
        RecordingModeType.NotRecorded,
        0
      )

      xivoQueueFeatureManager.create(xq) match {
        case Failure(t) =>
          fail("Cannot create xivo UserFeature as prerequisites", t)
        case Success(createdXivo) =>
          val id = createdXivo.id.get
          manager.delete(id)
          manager.get(id) match {
            case Success(fetched) =>
              fetched.id shouldEqual (id)
              fetched.name shouldEqual (createdXivo.name)
              fetched.displayName shouldEqual (createdXivo.displayname)
              fetched.number shouldEqual (createdXivo.number)
            case Failure(t) => fail(t)
          }
      }
    }

    "get all Queues" in new Helper() {
      val q1 = Queue(1, "queue1", "My First queue", "3000")
      val q2 = Queue(2, "queue2", "My Second queue", "3001")

      manager.delete(1)
      manager.delete(2)
      manager.create(q1)
      manager.create(q2)

      manager.all() match {
        case Success(fetched) => fetched should contain.allOf(q1, q2)
        case Failure(t) => fail(t)
      }

    }

    "update a Queue" in new Helper() {
      val q1 = Queue(1, "queue1", "My First queue", "3000")

      manager.delete(1)
      manager.create(q1)

      val q2 = q1.copy(displayName = "Rename queue")

      manager.update(q2) match {
        case Success(fetched) => fetched shouldEqual (q2)
        case Failure(t) => fail(t)
      }

      manager.get(q2.id) match {
        case Success(fetched) => fetched shouldEqual (q2)
        case Failure(t) => fail(t)
      }
    }

    "delete a Queue" in new Helper() {
      val q = Queue(1, "queue1", "My First queue", "3000")

      manager.delete(1)
      manager.create(q)

      manager.delete(1) match {
        case Success(fetched) => fetched shouldEqual (q)
        case Failure(t) => fail(t)
      }
    }
  }
}
