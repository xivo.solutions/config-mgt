package xc.service

import docker.DockerDBTest
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.inject.guice.GuiceApplicationBuilder
import xc.model.UserQueueMember
import xivo.model.{GenericBsFilter, UserFeature}
import xivo.service.UserFeatureManager
import play.api.Application

import java.util.UUID
import java.sql.Connection
import scala.util.{Failure, Success}

class UserQueueMemberSpec
    extends DockerDBTest
    with GuiceOneAppPerSuite
    with BeforeAndAfterEach {

  class Helper() {
    implicit lazy val app: Application = new GuiceApplicationBuilder()
      .configure(playConfig ++ Map(
        "db.default.url" -> xcjdbcurl.get,
        "db.xc.url" -> xcjdbcurl.get,
        "db.xivo.url" -> xivojdbcurl.get
      ))
      .build()

    implicit val connection: Connection = xcConnection.get
  }

  override protected def beforeEach(): Unit = {
    executeSQL(List("DELETE FROM xc.queue_members_default", "DELETE FROM xc.queues CASCADE", "DELETE FROM xc.users CASCADE"))(xcConnection.get)
    super.beforeEach()
  }

  "xc.service.UserManager" should {
    "create a User" in new Helper {
      val manager = app.injector.instanceOf(classOf[UserQueueManager])
      val u       = UserQueueMember(1, "James", "Bond", "jbond")

      manager.delete(1)

      manager.create(u) match {
        case Success(created) => created shouldEqual (u)
        case Failure(t)       => fail(t)
      }

    }

    "get a User" in new Helper {
      val manager = app.injector.instanceOf(classOf[UserQueueManager])
      val u       = UserQueueMember(1, "James", "Bond", "jbond")

      manager.delete(1)
      manager.create(u)
      manager.get(1) match {
        case Success(fetched) => fetched shouldEqual (u)
        case Failure(t)       => fail(t)
      }
    }

    "get a User from a xivo UserFeature if non existent " in new Helper {
      val manager         = app.injector.instanceOf(classOf[UserQueueManager])
      val xivoUserManager = app.injector.instanceOf(classOf[UserFeatureManager])

      val xu = UserFeature(
        None,
        UUID.randomUUID().toString,
        "James",
        "Bond",
        None,
        None,
        None,
        None,
        None,
        30,
        5,
        0,
        "jbond",
        "mypass",
        None,
        1,
        0,
        1,
        0,
        0,
        0,
        0,
        "",
        0,
        "",
        0,
        "",
        "",
        "",
        "",
        "",
        GenericBsFilter.No,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        0,
        "",
        None,
        0,
        None
      )

      xivoUserManager.create(xu) match {
        case Failure(t) =>
          fail(s"Cannot create xivo UserFeature as prerequisites : $t", t)
        case Success(createdXivo) =>
          val id = createdXivo.id.get
          manager.delete(id)
          manager.get(id) match {
            case Success(fetched) =>
              fetched.id shouldEqual (id)
              fetched.firstName shouldEqual (createdXivo.firstname)
              fetched.lastName shouldEqual (createdXivo.lastname)
              fetched.login shouldEqual (createdXivo.loginclient)
            case Failure(t) => fail(t)
          }
      }
    }

    "get all User" in new Helper {
      val manager = app.injector.instanceOf(classOf[UserQueueManager])
      val u1      = UserQueueMember(1, "James", "Bond", "jbond")
      val u2      = UserQueueMember(2, "Jason", "Bourne", "jbourne")

      manager.delete(1)
      manager.delete(2)
      manager.create(u1)
      manager.create(u2)

      manager.all() match {
        case Success(fetched) => fetched should contain.allOf(u1, u2)
        case Failure(t)       => fail(t)
      }

    }

    "update a User" in new Helper {
      val manager = app.injector.instanceOf(classOf[UserQueueManager])
      val u1      = UserQueueMember(1, "James", "Bond", "jbond")

      manager.delete(1)
      manager.create(u1)

      val u2 =
        u1.copy(firstName = "Jason", lastName = "Bourne", login = "jbourne")

      manager.update(u2) match {
        case Success(fetched) => fetched shouldEqual (u2)
        case Failure(t)       => fail(t)
      }

      manager.get(u2.id) match {
        case Success(fetched) => fetched shouldEqual (u2)
        case Failure(t)       => fail(t)
      }
    }

    "delete a User" in new Helper {
      val manager = app.injector.instanceOf(classOf[UserQueueManager])
      val u1      = UserQueueMember(1, "James", "Bond", "jbond")

      manager.delete(1)
      manager.create(u1)

      manager.delete(1) match {
        case Success(fetched) => fetched shouldEqual (u1)
        case Failure(t)       => fail(t)
      }
    }
  }
}
