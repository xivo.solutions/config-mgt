package xc.service

import docker.DockerDBTest
import org.scalatest.BeforeAndAfterEach
import play.api.inject.guice.GuiceApplicationBuilder
import xc.model.{Queue, QueueMemberDefault, UserQueueMember}
import play.api.Application
import java.sql.Connection
import scala.util.{Failure, Success, Try}

class QueueMemberDefaultSpec extends DockerDBTest with BeforeAndAfterEach {

  class Helper() {
    implicit lazy val app: Application = new GuiceApplicationBuilder()
      .configure(playConfig ++ Map(
        "db.default.url" -> xcjdbcurl.get,
        "db.xc.url" -> xcjdbcurl.get,
        "db.xivo.url" -> xivojdbcurl.get
      ))
      .build()

    implicit val connection: Connection = xcConnection.get
    val userMgr   = app.injector.instanceOf(classOf[UserQueueManager])
    val queueMgr  = app.injector.instanceOf(classOf[QueueManager])
    val memberMgr = app.injector.instanceOf(classOf[QueueMemberDefaultManager])

    def createQueueMember(
     m: QueueMemberDefault
   ): Try[QueueMemberDefault] = {
      userMgr
        .get(m.userId)
        .recoverWith({
          case _ =>
            userMgr.create(
              UserQueueMember(
                m.userId,
                "James",
                "Bond " + m.userId,
                "jbond" + m.userId
              )
            )
        })
      queueMgr
        .get(m.queueId)
        .recoverWith({
          case _ =>
            queueMgr.create(
              Queue(
                m.queueId,
                "queue" + m.queueId,
                "Some queue " + m.queueId,
                "300" + m.queueId
              )
            )
        })

      memberMgr.delete(m.getKey)

      memberMgr.create(m)
    }
  }


  override protected def beforeEach(): Unit = {
    executeSQL(List("DELETE FROM xc.queue_members_default", "DELETE FROM xc.queues CASCADE", "DELETE FROM xc.users CASCADE"))(xcConnection.get)
    super.beforeEach()
  }

  "xc.service.QueueMemberDefault" should {
    "create a QueueMemberDefault" in new Helper {
      val m = QueueMemberDefault(1, 1, 1)

      createQueueMember(m) match {
        case Success(created) => created shouldEqual (m)
        case Failure(t)       => fail(t)
      }
    }

    "get a QueueMemberDefault" in new Helper {
      val manager = app.injector.instanceOf(classOf[QueueMemberDefaultManager])
      val m       = QueueMemberDefault(1, 1, 1)

      createQueueMember(m)

      manager.get(m.getKey) match {
        case Success(fetched) => fetched shouldEqual (m)
        case Failure(t)       => fail(t)
      }
    }

    "get all QueueMemberDefault" in new Helper {
      val manager = app.injector.instanceOf(classOf[QueueMemberDefaultManager])
      val m1      = QueueMemberDefault(1, 1, 1)
      val m2      = QueueMemberDefault(2, 1, 5)
      val m3      = QueueMemberDefault(2, 3, 5)

      createQueueMember(m1)
      createQueueMember(m2)
      createQueueMember(m3)

      manager.all() match {
        case Success(fetched) => fetched should contain.allOf(m1, m2, m3)
        case Failure(t)       => fail(t)
      }

    }

    "update a QueueMemberDefault" in new Helper {
      val manager = app.injector.instanceOf(classOf[QueueMemberDefaultManager])
      val m1      = QueueMemberDefault(1, 1, 1)

      createQueueMember(m1)

      val m2 = m1.copy(penalty = 5)

      manager.update(m2) match {
        case Success(fetched) => fetched shouldEqual (m2)
        case Failure(t)       => fail(t)
      }

      manager.get(m2.getKey) match {
        case Success(fetched) => fetched shouldEqual (m2)
        case Failure(t)       => fail(t)
      }
    }

    "delete a QueueMemberDefault" in new Helper {
      val manager = app.injector.instanceOf(classOf[QueueMemberDefaultManager])
      val m       = QueueMemberDefault(1, 1, 1)

      createQueueMember(m)

      manager.delete(m.getKey) match {
        case Success(fetched) => fetched shouldEqual (m)
        case Failure(t)       => fail(t)
      }
    }

    "get QueueMemberDefault by user id" in new Helper {
      val manager = app.injector.instanceOf(classOf[QueueMemberDefaultManager])
      val m1      = QueueMemberDefault(1, 1, 1)
      val m2      = QueueMemberDefault(2, 1, 8)
      val m3      = QueueMemberDefault(3, 1, 2)

      createQueueMember(m1)
      createQueueMember(m2)
      createQueueMember(m3)

      manager.getByUserId(1) match {
        case Success(fetched) => fetched should contain.allOf(m1, m2, m3)
        case Failure(t)       => fail(t)
      }
    }

    "delete QueueMemberDefault by user id" in new Helper {
      val manager = app.injector.instanceOf(classOf[QueueMemberDefaultManager])
      val m1      = QueueMemberDefault(1, 1, 1)
      val m2      = QueueMemberDefault(2, 1, 8)
      val m3      = QueueMemberDefault(3, 1, 2)

      createQueueMember(m1)
      createQueueMember(m2)
      createQueueMember(m3)

      manager.deleteByUserId(1) match {
        case Success(fetched) => fetched should contain.allOf(m1, m2, m3)
        case Failure(t)       => fail(t)
      }
    }

    "Set all QueueMemberDefault for user id" in new Helper {
      val manager = app.injector.instanceOf(classOf[QueueMemberDefaultManager])
      val m1      = QueueMemberDefault(1, 1, 1)
      val m2      = QueueMemberDefault(2, 1, 8)
      val m3      = QueueMemberDefault(3, 1, 2)

      createQueueMember(m1)
      createQueueMember(m2)
      createQueueMember(m3)

      manager.setForUserId(1, List(m2, m3)) match {
        case Success(fetched) => fetched should contain.allOf(m2, m3)
        case Failure(t)       => fail(t)
      }
    }

    "Reject to set all QueueMemberDefault if the batch contains different users" in new Helper {
      val manager = app.injector.instanceOf(classOf[QueueMemberDefaultManager])
      val m1      = QueueMemberDefault(1, 1, 1)
      val m2      = QueueMemberDefault(2, 2, 8)
      val m3      = QueueMemberDefault(3, 1, 2)

      createQueueMember(m1)
      createQueueMember(m2)
      createQueueMember(m3)

      manager.setForUserId(1, List(m2, m3)) match {
        case Success(fetched) =>
          fail("QueueMemberDefault insert despite beeing for different users")
        case Failure(t) =>
      }
    }
  }
}
