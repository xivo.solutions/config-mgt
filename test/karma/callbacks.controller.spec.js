describe('Callbacks controller', function() {
  var scope;
  var ctrl;

  beforeEach(angular.mock.module('callbacks'));

  beforeEach(angular.mock.inject(function($rootScope, $controller) {
    scope = $rootScope.$new();
    ctrl = $controller('CallbackViewController', { '$scope': scope});
  }));

  it('can instantiate controller', function(){
    expect(ctrl).not.toBeUndefined();
  });

});
