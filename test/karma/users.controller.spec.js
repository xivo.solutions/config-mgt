describe('Users controller', function() {
  var scope;
  var ctrl;

  beforeEach(angular.mock.module('users'));

  beforeEach(angular.mock.inject(function($rootScope, $controller) {
    scope = $rootScope.$new();
    ctrl = $controller('UsersController', { '$scope': scope});
  }));

  it('can instantiate controller', function(){
    expect(ctrl).not.toBeUndefined();
  });

});
