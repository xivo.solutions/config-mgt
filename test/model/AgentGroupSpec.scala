package model

import docker.DockerDBTest
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.mockito.MockitoSugar
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.Application
import java.sql.Connection

class AgentGroupSpec extends DockerDBTest with BeforeAndAfterEach with MockitoSugar {

  class Helper() {
    implicit lazy val app: Application = new GuiceApplicationBuilder()
      .configure(playConfig ++ Map(
        "db.default.url" -> xcjdbcurl.get,
        "db.xivo.url" -> xivojdbcurl.get,
        "db.xc.url" -> xcjdbcurl.get
      ))
      .build()
    implicit val connection: Connection = xivoConnection.get
  }

  override protected def beforeEach(): Unit = {
    executeSQL(List("TRUNCATE agentgroup"))(xivoConnection.get)
    super.beforeEach()
  }

  "The AgentGroup singleton" should {

    "retrieve all agent groups" in new Helper() {
        Utils.insertAgentGroup(1, "default")
        Utils.insertAgentGroup(2, "autre")

        val agentGroupManager =
          app.injector.instanceOf(classOf[AgentGroupManagerImpl])
        agentGroupManager.all() shouldEqual List(
          AgentGroup(Some(1), "default"),
          AgentGroup(Some(2), "autre")
        )
    }

    "do not retrieve deleted agent groups" in new Helper() {
        Utils.insertAgentGroup(1, "default")
        Utils.insertAgentGroup(2, "autre", deleted = true)

        val agentGroupManager =
          app.injector.instanceOf(classOf[AgentGroupManagerImpl])
        agentGroupManager.all() shouldEqual List(AgentGroup(Some(1), "default"))
    }

    "retrieve only a few groups" in new Helper() {
        Utils.insertAgentGroup(1, "default")
        Utils.insertAgentGroup(2, "autre")

        val agentGroupManager =
          app.injector.instanceOf(classOf[AgentGroupManagerImpl])
        agentGroupManager.withIds(List(1)) shouldEqual List(
          AgentGroup(Some(1), "default")
        )
    }

    "not fail with an empty list" in {
      val agentGroupManager =
        app.injector.instanceOf(classOf[AgentGroupManagerImpl])
      agentGroupManager.withIds(List()) shouldEqual List()
    }
  }
}
