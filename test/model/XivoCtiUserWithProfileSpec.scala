package model

import docker.DockerDBTest
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.mockito.MockitoSugar
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.Application
import java.sql.Connection


class XivoCtiUserWithProfileSpec extends DockerDBTest with BeforeAndAfterEach with MockitoSugar {

  class Helper() {
    implicit lazy val app: Application = new GuiceApplicationBuilder()
      .configure(playConfig ++ Map(
        "db.default.url" -> xcjdbcurl.get,
        "db.xivo.url" -> xivojdbcurl.get,
        "db.xc.url" -> xcjdbcurl.get
      ))
      .build()

    implicit val connection: Connection = xivoConnection.get
  }

  override protected def beforeEach(): Unit = {
    executeSQL(List(
      "TRUNCATE users, userfeatures CASCADE"
    ))(xivoConnection.get)
    super.beforeEach()
  }

  "The User singleton" should {
    "retrieve a full User" in new Helper() {
      val user1 = XivoCtiUserWithProfile("Dupond", "Pierre", "pdupond", "admin")
      val user2 =
        XivoCtiUserWithProfile("Dupont", "Jean", "jdupont", "supervisor")
      Utils.insertUser(user1)
      Utils.insertUser(user2)

      val userManager = app.injector.instanceOf(classOf[UserProfileManager])

      userManager.list should contain.only(user1, user2)
    }
  }
}
