package model

import docker.DockerDBTest
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.mockito.MockitoSugar
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.Application
import java.sql.Connection

class IncallSpec extends DockerDBTest with BeforeAndAfterEach with MockitoSugar {

  class Helper() {
    implicit lazy val app: Application = new GuiceApplicationBuilder()
      .configure(playConfig ++ Map(
        "db.default.url" -> xcjdbcurl.get,
        "db.xivo.url" -> xivojdbcurl.get,
        "db.xc.url" -> xcjdbcurl.get
      ))
      .build()
    implicit val connection: Connection = xivoConnection.get
  }

  override protected def beforeEach(): Unit = {
    executeSQL(List(
      "TRUNCATE incall"
    ))(xivoConnection.get)
    super.beforeEach()
  }

  "The Incall singleton" should {
    "retrieve all incalls" in new Helper() {
      Utils.insertIncall(1, "5301")
      Utils.insertIncall(2, "5302")
      val incallManager = app.injector.instanceOf(classOf[IncallManager])

      incallManager.all() should contain.allOf(
        Incall(Some(1), "5301"),
        Incall(Some(2), "5302")
      )
    }

    "retrieve only some incalls" in new Helper() {
      Utils.insertIncall(1, "5301")
      Utils.insertIncall(2, "5302")
      val incallManager = app.injector.instanceOf(classOf[IncallManager])

      incallManager
        .withIds(List(1)) should contain only Incall(Some(1), "5301")
    }

    "not fail with an empty list" in new Helper() {
      val incallManager = app.injector.instanceOf(classOf[IncallManager])
      incallManager.withIds(List()) shouldBe empty
    }
  }
}
