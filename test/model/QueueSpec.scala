package model

import docker.DockerDBTest
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.mockito.MockitoSugar
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.Application
import java.sql.Connection

class QueueSpec extends DockerDBTest with BeforeAndAfterEach with MockitoSugar {

  class Helper() {
    implicit lazy val app: Application = new GuiceApplicationBuilder()
      .configure(playConfig ++ Map(
        "db.default.url" -> xcjdbcurl.get,
        "db.xc.url" -> xcjdbcurl.get,
        "db.xivo.url" -> xivojdbcurl.get
      ))
      .build()
    implicit val connection: Connection = xivoConnection.get
  }

  override protected def beforeEach(): Unit = {
    executeSQL(List(
      "TRUNCATE queuefeatures"
    ))(xivoConnection.get)
    super.beforeEach()
  }

  "The Queue singleton" should {
    "retrieve all queues" in new Helper() {
      Utils.insertQueueFeature(1, "queue1", "Queue 1")
      Utils.insertQueueFeature(2, "queue2", "Queue 2")

      val queueManager = app.injector.instanceOf(classOf[QueueManager])

      queueManager.all() should contain.only(
        Queue(Some(1), "queue1", "Queue 1"),
        Queue(Some(2), "queue2", "Queue 2")
      )
    }

    "retrieve only a few queues" in new Helper() {
      Utils.insertQueueFeature(1, "queue1")
      Utils.insertQueueFeature(2, "queue2")
      val queueManager = app.injector.instanceOf(classOf[QueueManager])

      queueManager.withIds(List(1)) should contain.only(
        Queue(Some(1), "queue1", "")
      )
    }

    "not fail with an empty list" in {

      val queueManager = app.injector.instanceOf(classOf[QueueManager])

      queueManager.withIds(List()) shouldBe empty
    }
  }
}
