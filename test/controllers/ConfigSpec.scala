package controllers

import akka.stream.Materializer
import docker.DockerDBTest
import org.scalatestplus.mockito.MockitoSugar
import org.mockito.Mockito._
import org.scalatest.BeforeAndAfterEach
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.JsValue
import play.api.test.Helpers._
import play.api.test.{FakeRequest, Helpers}
import play.api.{Application, Configuration}
import xivo.service.rabbitmq.XivoRabbitEventsPublisher

import java.io.{BufferedWriter, FileWriter}
import java.nio.file.{Files, Paths}

class ConfigSpec extends DockerDBTest with BeforeAndAfterEach with MockitoSugar {

  val xivoRabbitEventsPublisher: XivoRabbitEventsPublisher =
    mock[XivoRabbitEventsPublisher]
  val fakePath: String = "./fake.json"

  class Helper() {
    implicit lazy val app: Application = new GuiceApplicationBuilder()
      .configure(playConfig ++ Map(
        "db.default.url" -> xcjdbcurl.get,
        "db.xivo.url" -> xivojdbcurl.get,
        "db.xc.url" -> xcjdbcurl.get
      ))
      .overrides(bind[XivoRabbitEventsPublisher].to(xivoRabbitEventsPublisher))
      .build()

    implicit lazy val materializer: Materializer = app.materializer
  }

  def withMocks()(implicit mat: Materializer) = {
    val configuration = mock[Configuration]

    when(configuration.get[String](AndroidConfig.pushConfigKey))
      .thenReturn(fakePath)

    when(configuration.get[String](IosConfig.pushConfigKey))
      .thenReturn(fakePath)

    val controller = new Config(
      app.injector.instanceOf[controllers.Secured],
      configuration
    )
    controller.setControllerComponents(
      Helpers.stubControllerComponents(bodyParser =
        stubPlayBodyParsers.anyContent
      )
    )
    controller
  }

  def mockJsonParse(content: String): JsValue = {
    val jsonMock = mock[JsValue]
    when(jsonMock.toString()).thenReturn(content)
    jsonMock
  }

  override protected def afterEach(): Unit = {
    Files.deleteIfExists(Paths.get(fakePath))
  }

  "ConfigSpec" should {

    "check setup" should {

      "return a 204 if android mobile push notification key file exists on the platform" in new Helper() {
        val controller = withMocks()

        val rq = FakeRequest(GET, "/").withSession(
          "username"     -> "bwillis",
          "isSuperAdmin" -> "true"
        )

        Files.createFile(Paths.get(fakePath))
        val content = """{"key": "value"}"""
        val writer  = new BufferedWriter(new FileWriter(fakePath))
        writer.write(content)
        writer.close()
        val res = call(controller.checkSetup(AndroidConfig), rq)
        status(res) shouldEqual NO_CONTENT
      }

      "return a 204 if ios mobile push notification key file exists on the platform" in new Helper {
        val controller = withMocks()

        val rq = FakeRequest(GET, "/").withSession(
          "username" -> "bwillis",
          "isSuperAdmin" -> "true"
        )

        Files.createFile(Paths.get(fakePath))
        val content = """{"key": "value"}"""
        val writer = new BufferedWriter(new FileWriter(fakePath))
        writer.write(content)
        writer.close()
        val res = call(controller.checkSetup(IosConfig), rq)
        status(res) shouldEqual NO_CONTENT
      }

      "return a 404 if android mobile push notification key file is not installed on the platform" in new Helper {
        val controller = withMocks()

        val rq = FakeRequest(GET, "/").withSession(
          "username"     -> "bwillis",
          "isSuperAdmin" -> "true"
        )

        val res = call(controller.checkSetup(AndroidConfig), rq)
        status(res) shouldEqual NOT_FOUND
      }

      "return a 404 if ios mobile push notification key file is not installed on the platform" in new Helper {
        val controller = withMocks()

        val rq = FakeRequest(GET, "/").withSession(
          "username" -> "bwillis",
          "isSuperAdmin" -> "true"
        )

        val res = call(controller.checkSetup(IosConfig), rq)
        status(res) shouldEqual NOT_FOUND
      }

      "return a 500 if android mobile push notification key file exists on the platform but with wrong json" in new Helper {
        val controller = withMocks()

        val rq = FakeRequest(GET, "/").withSession(
          "username"     -> "bwillis",
          "isSuperAdmin" -> "true"
        )

        Files.createFile(Paths.get(fakePath))
        val content = "key"
        val writer  = new BufferedWriter(new FileWriter(fakePath))
        writer.write(content)
        writer.close()
        val res = call(controller.checkSetup(AndroidConfig), rq)
        status(res) shouldEqual INTERNAL_SERVER_ERROR
      }

      "return a 500 if ios mobile push notification key file exists on the platform but empty" in new Helper {
        val controller = withMocks()

        val rq = FakeRequest(GET, "/").withSession(
          "username" -> "bwillis",
          "isSuperAdmin" -> "true"
        )

        Files.createFile(Paths.get(fakePath))
        val writer = new BufferedWriter(new FileWriter(fakePath))
        writer.close()
        val res = call(controller.checkSetup(IosConfig), rq)
        status(res) shouldEqual INTERNAL_SERVER_ERROR
      }

    }

  }

}
