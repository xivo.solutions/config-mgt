package controllers

import akka.stream.Materializer
import docker.DockerDBTest
import model.{AdminRight, RightManager}
import models.AuthenticatedUser
import models.authentication.AuthenticationProvider
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.mockito.MockitoSugar
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.mvc.{AnyContentAsFormUrlEncoded, Request}
import play.api.test.Helpers.{POST, _}
import play.api.test._
import org.mockito.Mockito.stub
import play.api.Application

class LoginSpec
    extends DockerDBTest
    with BeforeAndAfterEach
    with MockitoSugar {

  class Helper() {
    implicit lazy val app: Application = new GuiceApplicationBuilder()
      .configure(playConfig ++ Map(
        "db.default.url" -> xcjdbcurl.get,
        "db.xivo.url" -> xivojdbcurl.get,
        "db.xc.url" -> xcjdbcurl.get
      ))
      .build()

    implicit val materializer: Materializer = app.materializer
    val rightManagerMock                    = mock[RightManager]
    val authProviderMock                    = mock[AuthenticationProvider]

    val controller = new Login(rightManagerMock, authProviderMock)
    controller.setControllerComponents(
      Helpers.stubControllerComponents(bodyParser =
        stubPlayBodyParsers.anyContent
      )
    )
  }

  "The Login controller" should {
    "should authenticate user" in new Helper {
          val user = AuthenticatedUser("test", false)
          stub(rightManagerMock.forUser("john"))
            .toReturn(Some(mock[AdminRight]))
          stub(authProviderMock.authenticate("john", "doe"))
            .toReturn(Some(user))

          val req: Request[AnyContentAsFormUrlEncoded] =
            FakeRequest(POST, "/login")
              .withFormUrlEncodedBody("login" -> "john", "mdp" -> "doe")

          val res = call(controller.authenticate, req)

          status(res) shouldEqual SEE_OTHER
          session(res).get("isSuperAdmin") shouldBe None
    }

    "should authenticate super admin user" in new Helper() {
          val user = AuthenticatedUser("test", true)
          stub(rightManagerMock.forUser("john"))
            .toReturn(Some(mock[AdminRight]))
          stub(authProviderMock.authenticate("john", "doe"))
            .toReturn(Some(user))

          val req: Request[AnyContentAsFormUrlEncoded] =
            FakeRequest(POST, "/login")
              .withFormUrlEncodedBody("login" -> "john", "mdp" -> "doe")

          val res = call(controller.authenticate, req)

          status(res) shouldEqual SEE_OTHER
          session(res).get("isSuperAdmin").get shouldEqual "true"
    }

    "should not authenticate user" in new Helper {
          stub(rightManagerMock.forUser("john"))
            .toReturn(Some(mock[AdminRight]))
          stub(authProviderMock.authenticate("john", "wrongPass"))
            .toReturn(None)

          val req: Request[AnyContentAsFormUrlEncoded] =
            FakeRequest(POST, "/login")
              .withFormUrlEncodedBody("login" -> "john", "mdp" -> "wrongPass")

          val res = call(controller.authenticate, req)

          status(res) shouldEqual BAD_REQUEST
    }

    "should not authenticate user without rights" in new Helper {
          val user = AuthenticatedUser("test", false)
          stub(rightManagerMock.forUser("john")).toReturn(None)
          stub(authProviderMock.authenticate("john", "doe"))
            .toReturn(Some(user))

          val req: Request[AnyContentAsFormUrlEncoded] =
            FakeRequest(POST, "/login")
              .withFormUrlEncodedBody("login" -> "john", "mdp" -> "doe")

          val res = call(controller.authenticate, req)

          status(res) shouldEqual BAD_REQUEST
    }
  }
}
