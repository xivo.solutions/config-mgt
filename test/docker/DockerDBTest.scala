package docker

import com.dimafeng.testcontainers.PostgreSQLContainer
import com.dimafeng.testcontainers.lifecycle.and
import com.dimafeng.testcontainers.scalatest.TestContainersForAll
import org.testcontainers.utility.DockerImageName
import testutils.PlayShouldSpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite

import java.sql.{Connection, DriverManager}

trait DockerDBTest extends PlayShouldSpec with TestContainersForAll with GuiceOneAppPerSuite {

  override type Containers = PostgreSQLContainer and PostgreSQLContainer

  var xcConnection: Option[Connection] = None
  var xcjdbcurl: Option[String] = None
  var xivoConnection: Option[Connection] = None
  var xivojdbcurl: Option[String] = None

  val playConfig: Map[String, String] = Map(
    "db.default.username"               -> "xc",
    "db.default.password"               -> "xc",
    "db.xc.username"                    -> "xc",
    "db.xc.password"                    -> "xc",
    "db.xivo.username"                  -> "xivo",
    "db.xivo.password"                  -> "xivo",
    "play.evolutions.enabled"           -> "false",
    "authentication.xivo.restapi.token" -> "abcedfghi",
    "play.http.context"                  -> "/configmgt",
    "authentication.confd.restapi.token" -> "abcedfghi",
  )

  override def startContainers(): Containers = {
    val xivoDB = PostgreSQLContainer.Def(
      dockerImageName = DockerImageName.parse("postgres:15.6"),
      databaseName = "xivo",
      username = playConfig("db.xivo.username"),
      password = playConfig("db.xivo.password")
    ).createContainer().configure(_.withCommand("postgres", "-c", "max_connections=2000"))

    val xcDB = PostgreSQLContainer.Def(
      dockerImageName = DockerImageName.parse("postgres:15.6"),
      databaseName = "xc",
      username = playConfig("db.default.username"),
      password = playConfig("db.default.password")
    ).createContainer().configure(_.withCommand("postgres", "-c", "max_connections=2000"))

    xivoDB.start()
    xcDB.start()
    
    xivojdbcurl = Some(xivoDB.jdbcUrl)
    xcjdbcurl = Some(xcDB.jdbcUrl)

    xivoDB and xcDB
  }


  def executeSqlFile(filename: String)(implicit connection: Connection): Unit = {
    val sqlContent = scala.io.Source
      .fromInputStream(
        getClass().getClassLoader().getResourceAsStream(filename)
      )
      .mkString

    val statement = connection.createStatement()
    statement.execute(sqlContent)
    statement.close()
  }

  def executeSQL(sql: List[String])(implicit connection: Connection): Unit = {
    val statement = connection.createStatement()
    sql.foreach(s => {
      statement.execute(s)
    })
    statement.close()
  }

  override def afterContainersStart(container: Containers): Unit = {
    container match {
      case xivoDB and xcDB =>
        xivoConnection = Some(DriverManager.getConnection(xivoDB.jdbcUrl, xivoDB.username, xivoDB.password))
        executeSQL(List(
          "CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\"",
          "select uuid_generate_v4()"
        ))(xivoConnection.get)
        executeSqlFile("docker/create-xivo-db.sql")(xivoConnection.get)

        xcConnection = Some(DriverManager.getConnection(xcDB.jdbcUrl, xcDB.username, xcDB.password))
        executeSQL(List(
          "CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\"",
          "select uuid_generate_v4()"
        ))(xcConnection.get)
        executeSqlFile("docker/create-xc-db.sql")(xcConnection.get)
    }
  }

  /* Example spec

  "PostgreSQL container" should "be started" in {
    withContainers { case xivoDB and xcDB =>
      val xivoConnection = DriverManager.getConnection(xivoDB.jdbcUrl, xivoDB.username, xivoDB.password)
      assert(!connection.isClosed())

      val xcConnection = DriverManager.getConnection(xcDB.jdbcUrl, xcDB.username, xcDB.password)
      assert(!connection.isClosed())
    }
  }

  */

}
