SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

CREATE TYPE generic_bsfilter AS ENUM (
  'no',
  'boss',
  'secretary'
);

CREATE TABLE userfeatures
(
  id serial PRIMARY KEY,
  uuid character varying(38) NOT NULL DEFAULT ''::character varying,
  firstname character varying(128) NOT NULL DEFAULT ''::character varying,
  agentid integer,
  pictureid integer DEFAULT 0,
  lastname character varying(128) NOT NULL DEFAULT ''::character varying,
  voicemailid integer DEFAULT 0,
  loginclient character varying(64) NOT NULL DEFAULT ''::character varying,
  passwdclient character varying(64) NOT NULL DEFAULT ''::character varying,
  cti_profile_id integer DEFAULT 0,
  enablehint integer DEFAULT 1 NOT NULL,
  enablevoicemail integer DEFAULT 0 NOT NULL,
  enablexfer integer DEFAULT 1 NOT NULL,
  enableonlinerec integer DEFAULT 0 NOT NULL,
  entityid integer,
  callerid character varying(160),
  ringseconds integer DEFAULT 30 NOT NULL,
  simultcalls integer DEFAULT 5 NOT NULL,
  enableclient integer DEFAULT 0 NOT NULL,
  callrecord integer DEFAULT 0 NOT NULL,
  incallfilter integer DEFAULT 0 NOT NULL,
  enablednd integer DEFAULT 0 NOT NULL,
  enableunc integer DEFAULT 0 NOT NULL,
  destunc character varying(128) DEFAULT ''::character varying NOT NULL,
  enablerna integer DEFAULT 0 NOT NULL,
  destrna character varying(128) DEFAULT ''::character varying NOT NULL,
  enablebusy integer DEFAULT 0 NOT NULL,
  destbusy character varying(128) DEFAULT ''::character varying NOT NULL,
  musiconhold character varying(128) DEFAULT ''::character varying NOT NULL,
  userfield character varying(128) DEFAULT ''::character varying NOT NULL,
  bsfilter generic_bsfilter DEFAULT 'no'::generic_bsfilter NOT NULL,
  preprocess_subroutine character varying(39) DEFAULT ''::character varying,
  timezone character varying(128) DEFAULT '',
  language character varying(20) DEFAULT '',
  ringintern character varying(64) DEFAULT '',
  ringextern character varying(64) DEFAULT '',
  ringgroup character varying(64) DEFAULT '',
  ringforward character varying(64) DEFAULT '',
  rightcallcode character varying(16) DEFAULT '',
  commented INTEGER NOT NULL DEFAULT 0,
  description text DEFAULT '' NOT NULL,
  func_key_template_id integer,
  func_key_private_template_id integer DEFAULT 0 NOT NULL,
  email character varying(128) COLLATE pg_catalog."default",
  outcallerid character varying(80) DEFAULT ''::character varying NOT NULL,
  mobilephonenumber character varying(128) COLLATE pg_catalog."default" NOT NULL DEFAULT ''::character varying,
  mobile_push_token character varying(255)
);

CREATE TABLE linefeatures (
  id serial PRIMARY KEY,
	name character varying(128),
  protocol character varying(10) NOT NULL,
	protocolid integer,
  configregistrar character varying(128),
  number character varying(40),
	context character varying(39) not null,
	device character varying(32),
  provisioningid integer DEFAULT 0 NOT NULL,
  num integer DEFAULT 1 NOT NULL,
  description text
);

CREATE TABLE entity (
    id integer NOT NULL,
    name character varying(64),
    displayname character varying(128)
);

CREATE TYPE extenumbers_type AS ENUM
    ('extenfeatures', 'featuremap', 'generalfeatures', 'group', 'incall', 'meetme', 'outcall','queue','user','voicemenu');

CREATE TABLE extensions (
  id SERIAL PRIMARY KEY,
  commented integer NOT NULL DEFAULT 0,
  context character varying(39) DEFAULT ''::character varying NOT NULL,
  exten character varying(40) DEFAULT ''::character varying NOT NULL,
  type extenumbers_type NOT NULL,
  typeval character varying(255) DEFAULT ''::character varying NOT NULL,
  CONSTRAINT extensions_exten_context_key UNIQUE (exten, context)
);

CREATE TABLE user_line (
  id serial NOT NULL,
  user_id integer NOT NULL,
  line_id integer,
  extension_id integer,
  main_user boolean,
  main_line boolean,
  CONSTRAINT user_line_pkey PRIMARY KEY (id, line_id),
  CONSTRAINT user_line_user_id_line_id_key UNIQUE (user_id, line_id),
  CONSTRAINT user_line_extension_id_fkey FOREIGN KEY (extension_id)
      REFERENCES extensions (id) MATCH SIMPLE
      ON UPDATE NO ACTION
      ON DELETE NO ACTION,
  CONSTRAINT user_line_line_id_fkey FOREIGN KEY (line_id)
      REFERENCES linefeatures (id) MATCH SIMPLE
      ON UPDATE NO ACTION
      ON DELETE NO ACTION,
  CONSTRAINT user_line_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES userfeatures (id) MATCH SIMPLE
      ON UPDATE NO ACTION
      ON DELETE NO ACTION
);

CREATE TYPE queue_category AS ENUM (
  'group',
  'queue'
);

CREATE TYPE queue_monitor_type AS ENUM (
  'no',
  'mixmonitor'
);

CREATE TYPE queuefeatures_recordingmode AS ENUM (
  'notrecorded',
  'recorded',
  'recordedondemand'
);

CREATE TABLE queue (
  name character varying(128) NOT NULL,
  musicclass character varying(128),
  announce character varying(128),
  context character varying(39),
  timeout integer DEFAULT 0,
  "monitor-type" queue_monitor_type,
  "monitor-format" character varying(128),
  "queue-youarenext" character varying(128),
  "queue-thereare" character varying(128),
  "queue-callswaiting" character varying(128),
  "queue-holdtime" character varying(128),
  "queue-minutes" character varying(128),
  "queue-seconds" character varying(128),
  "queue-thankyou" character varying(128),
  "queue-reporthold" character varying(128),
  "periodic-announce" text,
  "announce-frequency" integer,
  "periodic-announce-frequency" integer,
  "announce-round-seconds" integer,
  "announce-holdtime" character varying(4),
  retry integer,
  wrapuptime integer,
  maxlen integer,
  servicelevel integer,
  strategy character varying(11),
  joinempty character varying(255),
  leavewhenempty character varying(255),
  ringinuse integer DEFAULT 0 NOT NULL,
  reportholdtime integer DEFAULT 0 NOT NULL,
  memberdelay integer,
  weight integer,
  timeoutrestart integer DEFAULT 0 NOT NULL,
  commented integer DEFAULT 0 NOT NULL,
  category queue_category NOT NULL,
  timeoutpriority character varying(10) DEFAULT 'app'::character varying NOT NULL,
  autofill integer DEFAULT 1 NOT NULL,
  autopause character varying(3) DEFAULT 'no'::character varying NOT NULL,
  setinterfacevar integer DEFAULT 0 NOT NULL,
  setqueueentryvar integer DEFAULT 0 NOT NULL,
  setqueuevar integer DEFAULT 0 NOT NULL,
  membermacro character varying(1024),
  "min-announce-frequency" integer DEFAULT 60 NOT NULL,
  "random-periodic-announce" integer DEFAULT 0 NOT NULL,
  "announce-position" character varying(1024) DEFAULT 'yes'::character varying NOT NULL,
  "announce-position-limit" integer DEFAULT 5 NOT NULL,
  defaultrule character varying(1024),
  CONSTRAINT queue_autopause_check CHECK (((autopause)::text = ANY ((ARRAY['no'::character varying, 'yes'::character varying, 'all'::character varying])::text[])))
);

ALTER TABLE ONLY queue
  ADD CONSTRAINT queue_pkey PRIMARY KEY (name);

CREATE TYPE queuemember_usertype AS ENUM (
  'agent',
  'user'
);

CREATE TABLE queuemember
(
  queue_name character varying(128) NOT NULL,
  interface character varying(128) NOT NULL,
  penalty integer NOT NULL DEFAULT 0,
  commented integer NOT NULL DEFAULT 0,
  usertype queuemember_usertype NOT NULL,
  userid integer NOT NULL,
  channel character varying(25) NOT NULL,
  category queue_category NOT NULL,
  "position" integer NOT NULL DEFAULT 0,
  CONSTRAINT queuemember_pkey PRIMARY KEY (queue_name, interface),
  CONSTRAINT queuemember_queue_name_channel_usertype_userid_category_key UNIQUE (queue_name, channel, usertype, userid, category)
);

CREATE TABLE queuefeatures (
  id integer NOT NULL,
  name character varying(128) NOT NULL,
  displayname character varying(128) NOT NULL,
  number character varying(40) DEFAULT ''::character varying NOT NULL,
  context character varying(39),
  data_quality integer DEFAULT 0 NOT NULL,
  hitting_callee integer DEFAULT 0 NOT NULL,
  hitting_caller integer DEFAULT 0 NOT NULL,
  retries integer DEFAULT 0 NOT NULL,
  ring integer DEFAULT 0 NOT NULL,
  transfer_user integer DEFAULT 0 NOT NULL,
  transfer_call integer DEFAULT 0 NOT NULL,
  write_caller integer DEFAULT 0 NOT NULL,
  write_calling integer DEFAULT 0 NOT NULL,
  url character varying(255) DEFAULT ''::character varying NOT NULL,
  announceoverride character varying(128) DEFAULT ''::character varying NOT NULL,
  timeout integer,
  preprocess_subroutine character varying(39),
  announce_holdtime integer DEFAULT 0 NOT NULL,
  waittime integer,
  waitratio double precision,
  ignore_forward  integer DEFAULT 1 NOT NULL,
  recording_mode queuefeatures_recordingmode NOT NULL DEFAULT 'notrecorded'::queuefeatures_recordingmode,
  recording_activated integer NOT NULL DEFAULT 0
);

CREATE SEQUENCE queuefeatures_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;

ALTER SEQUENCE queuefeatures_id_seq OWNED BY queuefeatures.id;

ALTER TABLE ONLY queuefeatures ALTER COLUMN id SET DEFAULT nextval('queuefeatures_id_seq'::regclass);

ALTER TABLE ONLY queuefeatures
  ADD CONSTRAINT queuefeatures_pkey PRIMARY KEY (id);

ALTER TABLE ONLY queuefeatures
  ADD CONSTRAINT queuefeatures_name_key UNIQUE (name);


CREATE TABLE labels (
  id integer NOT NULL,
  display_name CHARACTER varying(128),
  description text
);

CREATE TABLE userlabels (
  id integer NOT NULL,
  user_id integer NOT NULL,
  label_id integer NOT NULL
);


CREATE TABLE qualifications (
  id SERIAL,
  name character varying(128) NOT NULL,
  active integer DEFAULT 1
);

CREATE TABLE subqualifications (
  id SERIAL,
  name character varying(128) NOT NULL,
  qualification_id integer NOT NULL,
  active integer DEFAULT 1
);

CREATE TABLE queue_qualification (
  id SERIAL,
  queue_id integer NOT NULL,
  qualification_id integer NOT NULL
);

CREATE TABLE qualification_answers (
  id SERIAL,
  sub_qualification_id integer NOT NULL,
  time TIMESTAMP,
  callid character varying(128) NOT NULL,
  agent integer NOT NULL,
  queue integer NOT NULL,
  first_name character varying(128) NOT NULL,
  last_name character varying(128) NOT NULL,
  comment character varying(128) NOT NULL,
  custom_data character varying(128) NOT NULL
);

CREATE TABLE agentfeatures
(
  id serial NOT NULL,
  numgroup integer NOT NULL,
  firstname character varying(128) NOT NULL DEFAULT ''::character varying,
  lastname character varying(128) NOT NULL DEFAULT ''::character varying,
  "number" character varying(40) NOT NULL,
  passwd character varying(128) NOT NULL,
  context character varying(39) NOT NULL,
  language character varying(20) NOT NULL,
  autologoff integer,
  "group" character varying(255),
  description text NOT NULL,
  preprocess_subroutine character varying(40),
  CONSTRAINT agentfeatures_pkey PRIMARY KEY (id),
  CONSTRAINT agentfeatures_number_key UNIQUE (number)
);

CREATE TABLE mediaserver (
  id INTEGER NOT NULL,
  name CHARACTER varying(128) NOT NULL,
  display_name CHARACTER varying(128) NOT NULL,
  voip_ip CHARACTER VARYING(39),
  read_only BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE SEQUENCE mediaserver_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;

ALTER SEQUENCE mediaserver_id_seq OWNED BY mediaserver.id;

ALTER TABLE ONLY mediaserver ALTER COLUMN id SET DEFAULT nextval('mediaserver_id_seq'::regclass);

ALTER TABLE ONLY mediaserver
  ADD CONSTRAINT mediaserver_pkey PRIMARY KEY (id);

ALTER TABLE ONLY mediaserver
  ADD CONSTRAINT uq_name UNIQUE (name);

CREATE TABLE netiface (
  id serial NOT NULL,
  networktype CHARACTER varying(128) NOT NULL,
  address CHARACTER varying(39) NOT NULL
);

CREATE OR REPLACE VIEW all_mediaserver AS
  (SELECT 0 as id, 'default' as name, 'MDS Main' as display_name, netiface.address as voip_ip, true as read_only
  FROM netiface where netiface.networktype='voip' LIMIT 1)
UNION ALL
  (SELECT id, name, display_name, voip_ip, false as read_only
  FROM mediaserver);

CREATE TABLE route
(
  id serial NOT NULL,
  subroutine character varying,
  description text,
  internal BOOLEAN NOT NULL DEFAULT FALSE,
  priority integer NOT NULL DEFAULT 1,
  CONSTRAINT route_pkey PRIMARY KEY (id)
);

CREATE TABLE context
(
  name character varying(39) NOT NULL,
  displayname character varying(128) NOT NULL DEFAULT ''::character varying,
  entity character varying(64),
  contexttype character varying(40) NOT NULL DEFAULT 'internal'::character varying,
  commented integer NOT NULL DEFAULT 0,
  description text NOT NULL,
  CONSTRAINT context_pkey PRIMARY KEY (name)
);


CREATE TABLE trunkfeatures
(
  id serial NOT NULL,
  protocol character varying(39) NOT NULL,
  protocolid integer NOT NULL,
  registerid integer NOT NULL DEFAULT 0,
  registercommented integer NOT NULL DEFAULT 0,
  description text,
  mediaserverid integer NOT NULL,
  CONSTRAINT trunkfeatures_pkey PRIMARY KEY (id),
  CONSTRAINT trunkfeatures_mediaserverid_fkey FOREIGN KEY (mediaserverid)
      REFERENCES mediaserver (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE RESTRICT,
  CONSTRAINT trunkfeatures_protocol_protocolid_key UNIQUE (protocol, protocolid)
);

CREATE TYPE useriax_category AS ENUM
    ('trunk','user');

CREATE TYPE useriax_type AS ENUM
    ('friend', 'peer', 'user');

CREATE TABLE usersip
(
  id serial NOT NULL,
  name character varying(40) NOT NULL,
  type useriax_type NOT NULL,
  secret character varying(80) NOT NULL,
  category useriax_category NOT NULL,
  callerid character varying(160),
  options character varying[] NOT NULL DEFAULT array[]::varchar[],
  context character varying(39),
  CONSTRAINT usersip_pkey PRIMARY KEY (id),
  CONSTRAINT usersip_name_key UNIQUE (name)
);

CREATE TYPE trunk_protocol AS ENUM
    ('custom', 'sip', 'sccp');

CREATE TYPE usercustom_category AS ENUM
    ('trunk', 'user');

CREATE TABLE usercustom
(
  id serial NOT NULL,
  name character varying(40) NOT NULL,
  interface character varying(40) NOT NULL,
  intfsuffix character varying(32) NOT NULL DEFAULT ''::character varying,
  protocol trunk_protocol NOT NULL,
  context character varying(39),
  category usercustom_category NOT NULL,
  CONSTRAINT usercustom_pkey PRIMARY KEY (id),
  CONSTRAINT usercustom_interface_intfsuffix_category_key UNIQUE (interface, intfsuffix, category)
);

CREATE TABLE sccpline
(
  id serial NOT NULL PRIMARY KEY,
  name character varying(80) NOT NULL,
  context character varying(80) NOT NULL,
  cid_name character varying(80) NOT NULL,
  cid_num character varying(80) NOT NULL
);

CREATE TABLE sccpdevice
(
  id serial NOT NULL PRIMARY KEY,
  name character varying(80) NOT NULL,
  device character varying(80) NOT NULL,
  line character varying(80) NOT NULL DEFAULT ''::character varying
);

CREATE TABLE routecontext
(
  routeid integer NOT NULL,
  contextname character varying(39) NOT NULL,
  CONSTRAINT routecontext_pkey PRIMARY KEY (routeid, contextname)
);

CREATE TABLE routemediaserver
(
  routeid integer NOT NULL,
  mdsid integer NOT NULL,
  CONSTRAINT routemediaserver_pkey PRIMARY KEY (routeid, mdsid)
);

CREATE TABLE routetrunk
(
  routeid integer NOT NULL,
  trunkid integer NOT NULL,
  priority integer NOT NULL DEFAULT 1,
  CONSTRAINT routetrunk_pkey PRIMARY KEY (routeid, trunkid)
);

CREATE TYPE schedule_path_type AS ENUM
    ('user', 'group', 'queue', 'incall', 'outcall', 'voicemenu', 'route');

CREATE TABLE schedule_path
(
  schedule_id integer NOT NULL,
  path schedule_path_type NOT NULL,
  pathid integer NOT NULL,
  "order" integer NOT NULL,
  CONSTRAINT schedule_path_pkey PRIMARY KEY (schedule_id,path,pathid)
);

CREATE TYPE rightcallmember_type AS ENUM
    ('user', 'group', 'incall', 'outcall', 'route');

CREATE TABLE rightcallmember
(
  id integer NOT NULL ,
  rightcallid integer NOT NULL DEFAULT 0,
  type rightcallmember_type NOT NULL,
  typeval character varying(128) NOT NULL DEFAULT '0'::character varying,
  CONSTRAINT rightcallmember_pkey PRIMARY KEY (id),
  CONSTRAINT rightcallmember_rightcallid_type_typeval_key UNIQUE (rightcallid, type, typeval)
);

CREATE SEQUENCE rightcallmember_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;

ALTER SEQUENCE rightcallmember_id_seq OWNED BY rightcallmember.id;

ALTER TABLE ONLY rightcallmember ALTER COLUMN id SET DEFAULT nextval('rightcallmember_id_seq'::regclass);

CREATE TYPE user_type AS ENUM('admin', 'supervisor', 'teacher');

CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    login VARCHAR(64),
    type user_type,
    validity_start TIMESTAMP WITHOUT TIME ZONE,
    validity_end TIMESTAMP WITHOUT TIME ZONE
);

CREATE TYPE right_type AS ENUM('queue', 'agentgroup', 'incall', 'recording_access', 'dissuasion_access');

CREATE TABLE rights (
    id SERIAL PRIMARY KEY,
    user_id INTEGER REFERENCES users(id) NOT NULL,
    category right_type NOT NULL,
    category_id INTEGER NOT NULL
);

CREATE TABLE routepattern
(
  id serial NOT NULL,
  pattern character varying NOT NULL,
  regexp character varying,
  target character varying,
  callerid character varying,
  routeid integer NOT NULL,
  CONSTRAINT routepattern_pkey PRIMARY KEY (id)
);

CREATE TYPE public.dialaction_event AS ENUM
    ('answer', 'noanswer', 'congestion', 'busy', 'chanunavail', 'qwaittime', 'qwaitratio');

CREATE TYPE public.dialaction_category AS ENUM
    ('callfilter', 'group', 'incall', 'queue', 'schedule', 'user', 'outcall');

CREATE TYPE public.dialaction_action AS ENUM
    ('none', 'endcall:busy', 'endcall:congestion', 'endcall:hangup', 'user', 'group',
        'queue', 'meetme', 'meetingroom', 'voicemail', 'ivr', 'trunk',
        'schedule', 'voicemenu', 'extension', 'outcall', 'application:callbackdisa',
        'application:disa', 'application:directory', 'application:faxtomail',
        'application:voicemailmain', 'application:password', 'sound', 'custom');

CREATE TABLE public.dialaction (
  event public.dialaction_event NOT NULL,
  category public.dialaction_category NOT NULL,
  categoryval character varying(128) DEFAULT ''::character varying NOT NULL,
  action public.dialaction_action NOT NULL,
  actionarg1 character varying(255),
  actionarg2 character varying(255),
  linked integer DEFAULT 0 NOT NULL
);
ALTER TABLE ONLY public.dialaction
    ADD CONSTRAINT dialaction_pkey PRIMARY KEY (event, category, categoryval);

CREATE TABLE infos(uuid character varying(38) NOT NULL PRIMARY KEY);

CREATE TABLE public.userpreferences (
  user_id integer NOT NULL,
  key character varying(255) NOT NULL,
  value character varying(255) NOT NULL,
  value_type character varying(255) NOT NULL,
  CONSTRAINT userpreferences_user_id_key_key UNIQUE (user_id, key),
  CONSTRAINT userpreferences_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES userfeatures(id)
);

CREATE TABLE public.staticsip (
  id integer NOT NULL,
  cat_metric integer DEFAULT 0 NOT NULL,
  var_metric integer DEFAULT 0 NOT NULL,
  commented integer DEFAULT 0 NOT NULL,
  filename character varying(128) NOT NULL,
  category character varying(128) NOT NULL,
  var_name character varying(128) NOT NULL,
  var_val character varying(255)
);

CREATE TABLE meetingroom
(
  id SERIAL NOT NULL,
  display_name character varying(128) NOT NULL,
  "number" character varying(40),
  user_pin character varying(12),
  uuid UUID default uuid_generate_v4(),
  room_type character varying(40) NOT NULL DEFAULT 'static'::character varying,
  user_id integer,
  pin_timestamp TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "alias" character varying(20),
  CONSTRAINT meetingroom_pkey PRIMARY KEY (id),
  CONSTRAINT "number" UNIQUE ("number"),
  CONSTRAINT "alias" UNIQUE ("alias")
);

CREATE SEQUENCE incall_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;

CREATE TABLE incall
(
  id integer NOT NULL DEFAULT nextval('incall_id_seq'::regclass),
  exten character varying(40) COLLATE pg_catalog."default" NOT NULL,
  context character varying(39) COLLATE pg_catalog."default" NOT NULL,
  preprocess_subroutine character varying(39) COLLATE pg_catalog."default",
  commented integer NOT NULL DEFAULT 0,
  description text COLLATE pg_catalog."default" NOT NULL,
  CONSTRAINT incall_pkey PRIMARY KEY (id),
  CONSTRAINT incall_exten_context_key UNIQUE (exten, context)
);

CREATE TABLE agentgroup (
  id SERIAL PRIMARY KEY,
  name VARCHAR(128),
  deleted integer not null default 0
);

CREATE OR REPLACE FUNCTION uuid_generator()
RETURNS TRIGGER
LANGUAGE plpgsql
AS $$
BEGIN
   NEW.uuid := uuid_generate_v4();
   RETURN NEW;
END;
$$;

CREATE TABLE callback_list (
    uuid UUID PRIMARY KEY,
    name VARCHAR(128) NOT NULL,
    queue_id INTEGER NOT NULL
);
CREATE TRIGGER callback_list_uuid_trigger BEFORE INSERT ON callback_list FOR EACH ROW EXECUTE PROCEDURE uuid_generator();

CREATE TABLE preferred_callback_period (
    uuid UUID PRIMARY KEY,
    name VARCHAR(128) NOT NULL DEFAULT '',
    period_start TIME WITHOUT TIME ZONE NOT NULL,
    period_end TIME WITHOUT TIME ZONE NOT NULL,
    "default" BOOLEAN DEFAULT FALSE
);
CREATE TRIGGER preferred_callback_period_uuid_trigger BEFORE INSERT ON preferred_callback_period FOR EACH ROW EXECUTE PROCEDURE uuid_generator();

CREATE TABLE callback_request (
    uuid UUID PRIMARY KEY,
    list_uuid UUID REFERENCES callback_list(uuid) NOT NULL,
    phone_number VARCHAR(40),
    mobile_phone_number VARCHAR(40),
    firstname VARCHAR(128),
    lastname VARCHAR(128),
    company VARCHAR(128),
    description TEXT,
    agent_id INTEGER,
    clotured BOOLEAN DEFAULT FALSE,
    preferred_callback_period_uuid UUID REFERENCES preferred_callback_period(uuid),
    due_date DATE NOT NULL DEFAULT now(),
    reference_number SERIAL NOT NULL,
    voice_message_ref VARCHAR(128)
);
CREATE TRIGGER callback_request_uuid_trigger BEFORE INSERT ON callback_request FOR EACH ROW EXECUTE PROCEDURE uuid_generator();

CREATE TABLE public.groupfeatures (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    number character varying(40) DEFAULT ''::character varying NOT NULL,
    context character varying(39) NOT NULL,
    transfer_user integer DEFAULT 0 NOT NULL,
    transfer_call integer DEFAULT 0 NOT NULL,
    write_caller integer DEFAULT 0 NOT NULL,
    write_calling integer DEFAULT 0 NOT NULL,
    ignore_forward integer DEFAULT 1 NOT NULL,
    timeout integer,
    preprocess_subroutine character varying(39),
    deleted integer DEFAULT 0 NOT NULL,
    mediaserverid integer NOT NULL
);

CREATE SEQUENCE public.groupfeatures_id_seq
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.groupfeatures_id_seq OWNED BY public.groupfeatures.id;
ALTER TABLE ONLY public.groupfeatures ALTER COLUMN id SET DEFAULT nextval('public.groupfeatures_id_seq'::regclass);
ALTER TABLE ONLY public.groupfeatures ADD CONSTRAINT groupfeatures_pkey PRIMARY KEY (id);

CREATE TYPE public.callerid_type AS ENUM
    ('queue', 'group');

CREATE TYPE public.callerid_mode AS ENUM
    ('prepend', 'overwrite', 'append');

CREATE TABLE public.callerid (
    mode public.callerid_mode,
    callerdisplay character varying(80) DEFAULT ''::character varying NOT NULL,
    type public.callerid_type NOT NULL,
    typeval integer NOT NULL
);

ALTER TABLE ONLY public.callerid
    ADD CONSTRAINT callerid_pkey PRIMARY KEY (type, typeval);


CREATE TABLE public.func_key_dest_group (
    func_key_id integer NOT NULL,
    destination_type_id integer DEFAULT 2 NOT NULL,
    group_id integer NOT NULL,
    CONSTRAINT func_key_dest_group_destination_type_id_check CHECK ((destination_type_id = 2))
);

ALTER TABLE ONLY public.func_key_dest_group
    ADD CONSTRAINT func_key_dest_group_group_id_fkey FOREIGN KEY (group_id) REFERENCES public.groupfeatures(id);


CREATE TABLE public.func_key (
    id integer NOT NULL,
    type_id integer NOT NULL,
    destination_type_id integer NOT NULL
);

CREATE SEQUENCE public.func_key_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.func_key_id_seq OWNED BY public.func_key.id;
ALTER TABLE ONLY public.func_key ALTER COLUMN id SET DEFAULT nextval('public.func_key_id_seq'::regclass);
ALTER TABLE ONLY public.func_key
    ADD CONSTRAINT func_key_pkey PRIMARY KEY (id, destination_type_id);

CREATE TABLE public.func_key_type (
    id integer NOT NULL,
    name character varying(128) NOT NULL
);
CREATE SEQUENCE public.func_key_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE ONLY public.func_key_type ALTER COLUMN id SET DEFAULT nextval('public.func_key_type_id_seq'::regclass);
ALTER TABLE ONLY public.func_key_type ADD CONSTRAINT func_key_type_pkey PRIMARY KEY (id);

CREATE TABLE public.func_key_destination_type (
    id integer NOT NULL,
    name character varying(128) NOT NULL
);
CREATE SEQUENCE public.func_key_destination_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE ONLY public.func_key_destination_type ALTER COLUMN id SET DEFAULT nextval('public.func_key_destination_type_id_seq'::regclass);
ALTER TABLE ONLY public.func_key_destination_type ADD CONSTRAINT func_key_destination_type_pkey PRIMARY KEY (id);

create table public.cti_profile
(
    id            serial primary key,
    name          varchar(255) not null,
    presence_id   integer,
    phonehints_id integer
);

create table public.ctistatus
(
    id            serial
        primary key,
    presence_id   integer,
    name          varchar(255) not null,
    display_name  varchar(255),
    actions       varchar(255),
    color         varchar(20),
    access_status varchar(255),
    deletable     integer,
    unique (presence_id, name)
);

create table public.ctipresences
(
    id          serial
        primary key,
    name        varchar(255) not null,
    description varchar(255),
    deletable   integer
);

