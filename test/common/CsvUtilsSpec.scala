package common

import testutils.PlayShouldSpec

class CsvUtilsSpec extends PlayShouldSpec {
  val candidates = List(',', ';', '|')

  "CsvUtils" should {
    "autodetect separator" in {
      CsvUtils.detectCsvSeparator("some;simple;format", candidates) should be(
        Some(';')
      )
      CsvUtils.detectCsvSeparator("other,simple,format", candidates) should be(
        Some(',')
      )
      CsvUtils.detectCsvSeparator(
        """unusual|format,obviously""",
        candidates
      ) should be(Some('|'))
    }

    "autodetect separator even with tricky quotes" in {
      CsvUtils.detectCsvSeparator(
        """"not,so";simple;format""",
        candidates
      ) should be(Some(';'))
    }

    "fails to autodetect if there is no match" in {
      CsvUtils.detectCsvSeparator(
        """no obvious separator""",
        candidates
      ) should be(None)
      CsvUtils.detectCsvSeparator(
        """"no,obvious;separator"""",
        candidates
      ) should be(None)
    }

    "parse csv line" in {
      CsvUtils.parseCsvLine("some,simple,format", ',') should be(
        List("some", "simple", "format")
      )
      CsvUtils.parseCsvLine("other;simple;format", ';') should be(
        List("other", "simple", "format")
      )
    }

    "parse csv line with quotes" in {
      CsvUtils.parseCsvLine("""not,"so,simple",format""", ',') should be(
        List("not", "so,simple", "format")
      )
    }
  }
}
