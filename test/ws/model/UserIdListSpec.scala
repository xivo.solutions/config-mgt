package ws.model

import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}
import testutils.PlayShouldSpec

class UserIdListSpec extends PlayShouldSpec {

  import UserIdListJson._

  "UserIdList" should {
    "convert to json" in {
      val o = UserIdList(List(1, 5, 7))
      Json.toJson(o) shouldEqual (Json.obj(
        "userIds" -> o.userIds
      ))
    }

    "be created from json" in {
      val json: JsValue = Json.parse("""{"userIds": [1,5,7,42]}""")
      json.validate[UserIdList] match {
        case JsSuccess(o, _) => o shouldEqual (UserIdList(List(1, 5, 7, 42)))
        case JsError(errors) => fail()
      }
    }
  }

}
