package ws.controllers.mobile

import akka.stream.Materializer
import anorm.AnormException
import docker.DockerDBTest
import org.mockito.Mockito._
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.mockito.MockitoSugar
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}
import play.api.test.Helpers._
import play.api.test.{FakeRequest, Helpers}
import xivo.model.rabbitmq.{
  MobilePushTokenAdded,
  MobilePushTokenDeleted,
  ObjectEvent
}
import xivo.model.{GenericBsFilter, MobileAppPushToken, UserFeature}
import xivo.service._
import xivo.service.rabbitmq.XivoRabbitEventsPublisher
import java.util.UUID
import scala.util.{Failure, Success}

class MobileAppHelperSpec extends DockerDBTest with BeforeAndAfterEach with MockitoSugar {

  val xivoRabbitEventsPublisher: XivoRabbitEventsPublisher = mock[XivoRabbitEventsPublisher]

  class Helper {
    val baseUser = UserFeature(
      Some(42),
      UUID.randomUUID().toString,
      "Bruce",
      "Wayne",
      None,
      None,
      None,
      None,
      None,
      30,
      5,
      0,
      "bwayne",
      "mypass",
      None,
      1,
      0,
      1,
      0,
      0,
      0,
      0,
      "",
      0,
      "",
      0,
      "",
      "",
      "",
      "",
      "",
      GenericBsFilter.No,
      None,
      None,
      None,
      None,
      None,
      None,
      None,
      None,
      0,
      "",
      None,
      0,
      None
    )

    val fakePath: String = "/tmp/fake.json"
    implicit lazy val app: Application = new GuiceApplicationBuilder()
      .configure(playConfig ++ Map(
        "db.default.url" -> xcjdbcurl.get,
        "db.xc.url" -> xcjdbcurl.get,
        "db.xivo.url" -> xivojdbcurl.get
      ))
      .overrides(bind[XivoRabbitEventsPublisher].to(xivoRabbitEventsPublisher))
      .build()
  }

  implicit lazy val materializer: Materializer = app.materializer

  def withMocks(): (UserFeatureManager, UserPreferenceManager, MobileAppHelper) = {
    val ufmgr    = mock[UserFeatureManager]
    val upMgr = mock[UserPreferenceManager]

    val controller = new MobileAppHelper(
      ufmgr,
      upMgr,
      app.injector.instanceOf[controllers.Secured],
      xivoRabbitEventsPublisher
    )
    controller.setControllerComponents(
      Helpers.stubControllerComponents(bodyParser =
        stubPlayBodyParsers.anyContent
      )
    )
    (ufmgr, upMgr, controller)
  }

  "MobileAppHelper" should {

    "create MobileAppPushToken from json" in {
      val json: JsValue = Json.parse("""{"token": "myToken"}""")
      val token         = MobileAppPushToken(Some("myToken"))

      json.validate[MobileAppPushToken] match {
        case JsSuccess(q, _) => q shouldEqual token
        case JsError(_)      => fail()
      }
    }

    "get mobile push token for a username" in new Helper() {
      val (userFeatureManager, _, controller) = withMocks()

      val token = MobileAppPushToken(Some("myToken"))
      val user  = baseUser.copy(mobile_push_token = Some("myToken"))

      stub(userFeatureManager.get("bwillis"))
        .toReturn(Success(user))

      val rq = FakeRequest(GET, "/").withSession(
        "username"     -> "bwillis",
        "isSuperAdmin" -> "true"
      )
      val res = call(controller.getPushToken("bwillis"), rq)

      status(res) shouldEqual OK
      verify(userFeatureManager).get("bwillis")

      contentAsJson(res) shouldEqual Json.toJson(token)
    }

    "set mobile push token for a username" in new Helper() {
      val (userFeatureManager, userPreferenceManager, controller) = withMocks()

      val token = MobileAppPushToken(Some("myToken"))
      val mobileApp = UserPreference(
        baseUser.id.get,
        UserPreferenceKey.MobileAppInfo,
        "true",
        "Boolean"
      )
      val preferredDevice = UserPreference(
        baseUser.id.get,
        UserPreferenceKey.PreferredDevice,
        UserPreferenceValues.WebAppAndMobileApp,
        "String"
      )

      stub(userFeatureManager.get("bwayne"))
        .toReturn(Success(baseUser))

      stub(
        userFeatureManager.update(
          baseUser.copy(mobile_push_token = Some("myToken"))
        )
      )
        .toReturn(Success(baseUser))

      stub(userPreferenceManager.create(mobileApp))
        .toReturn(Success(1L))
      stub(userPreferenceManager.createOrUpdate(preferredDevice))
        .toReturn(Success(1L))

      val rq = FakeRequest(POST, "/fakereq?username=bwayne")
        .withSession(
          "username"     -> "bwayne",
          "isSuperAdmin" -> "true"
        )
        .withBody(Json.toJson(token))

      val res = call(controller.setPushToken(), rq)
      status(res) shouldEqual CREATED
    }

    "set Android mobile push token for a username" in new Helper() {
      val (userFeatureManager, userPreferenceManager, controller) = withMocks()

      val token = MobileAppPushToken(Some("myToken"))
      val mobileApp = UserPreference(
        baseUser.id.get,
        UserPreferenceKey.MobileAppInfo,
        "true",
        "Boolean"
      )
      val preferredDevice = UserPreference(
        baseUser.id.get,
        UserPreferenceKey.PreferredDevice,
        UserPreferenceValues.WebAppAndMobileApp,
        "String"
      )

      stub(userFeatureManager.get("bwayne"))
        .toReturn(Success(baseUser))

      stub(
        userFeatureManager.update(
          baseUser.copy(mobile_push_token = Some("android-myToken"))
        )
      )
        .toReturn(Success(baseUser))

      stub(userPreferenceManager.create(mobileApp))
        .toReturn(Success(1L))
      stub(userPreferenceManager.createOrUpdate(preferredDevice))
        .toReturn(Success(1L))

      val rq = FakeRequest(POST, "/fakereq?username=bwayne")
        .withSession(
          "username"     -> "bwayne",
          "isSuperAdmin" -> "true"
        )
        .withBody(Json.toJson(token))

      val res = call(controller.setAndroidPushToken(), rq)
      status(res) shouldEqual CREATED
    }

    "set iOS mobile push token for a username" in new Helper {
      val (userFeatureManager, userPreferenceManager, controller) = withMocks()

      val token = MobileAppPushToken(Some("myToken"))
      val mobileApp = UserPreference(
        baseUser.id.get,
        UserPreferenceKey.MobileAppInfo,
        "true",
        "Boolean"
      )
      val preferredDevice = UserPreference(
        baseUser.id.get,
        UserPreferenceKey.PreferredDevice,
        UserPreferenceValues.WebAppAndMobileApp,
        "String"
      )

      stub(userFeatureManager.get("bwayne"))
        .toReturn(Success(baseUser))

      stub(
        userFeatureManager.update(
          baseUser.copy(mobile_push_token = Some("ios-myToken"))
        )
      )
        .toReturn(Success(baseUser))

      stub(userPreferenceManager.create(mobileApp))
        .toReturn(Success(1L))
      stub(userPreferenceManager.createOrUpdate(preferredDevice))
        .toReturn(Success(1L))

      val rq = FakeRequest(POST, "/fakereq?username=bwayne")
        .withSession(
          "username"     -> "bwayne",
          "isSuperAdmin" -> "true"
        )
        .withBody(Json.toJson(token))

      val res = call(controller.setIOSPushToken(), rq)
      status(res) shouldEqual CREATED
    }

    "send message to rabbitmq to notify of mobile push token edited" in new Helper() {
      val (userFeatureManager, userPreferenceManager, controller) = withMocks()

        val token = MobileAppPushToken(Some("myToken"))
        val mobileApp = UserPreference(
          baseUser.id.get,
          UserPreferenceKey.MobileAppInfo,
          "true",
          "Boolean"
        )
        val preferredDevice = UserPreference(
          baseUser.id.get,
          UserPreferenceKey.PreferredDevice,
          UserPreferenceValues.WebAppAndMobileApp,
          "String"
        )

        stub(userFeatureManager.get("bwayne"))
          .toReturn(Success(baseUser))

        stub(
          userFeatureManager.update(
            baseUser.copy(mobile_push_token = Some("myToken"))
          )
        )
          .toReturn(Success(baseUser))

        stub(userPreferenceManager.create(mobileApp))
          .toReturn(Success(1L))
        stub(userPreferenceManager.createOrUpdate(preferredDevice))
          .toReturn(Success(1L))

      val rq = FakeRequest(POST, "/fakereq?username=bwayne")
        .withSession(
          "username"     -> "bwayne",
          "isSuperAdmin" -> "true"
        )
        .withBody(Json.toJson(token))

      reset(xivoRabbitEventsPublisher)
      val res = call(controller.setPushToken(), rq)
      status(res) shouldEqual CREATED
      verify(xivoRabbitEventsPublisher).publish(
        ObjectEvent(MobilePushTokenAdded, 42L)
      )
    }

    "send message to rabbitmq to notify of Android mobile push token edited" in new Helper() {
      val (userFeatureManager, userPreferenceManager, controller) = withMocks()

      val token = MobileAppPushToken(Some("myToken"))
      val mobileApp = UserPreference(
        baseUser.id.get,
        UserPreferenceKey.MobileAppInfo,
        "true",
        "Boolean"
      )
      val preferredDevice = UserPreference(
        baseUser.id.get,
        UserPreferenceKey.PreferredDevice,
        UserPreferenceValues.WebAppAndMobileApp,
        "String"
      )

      stub(userFeatureManager.get("bwayne"))
        .toReturn(Success(baseUser))

      stub(
        userFeatureManager.update(
          baseUser.copy(mobile_push_token = Some("android-myToken"))
        )
      )
        .toReturn(Success(baseUser))

      stub(userPreferenceManager.create(mobileApp))
        .toReturn(Success(1L))
      stub(userPreferenceManager.createOrUpdate(preferredDevice))
        .toReturn(Success(1L))

      val rq = FakeRequest(POST, "/fakereq?username=bwayne")
        .withSession(
          "username"     -> "bwayne",
          "isSuperAdmin" -> "true"
        )
        .withBody(Json.toJson(token))

      reset(xivoRabbitEventsPublisher)
      val res = call(controller.setAndroidPushToken(), rq)
      status(res) shouldEqual CREATED
      verify(xivoRabbitEventsPublisher).publish(
        ObjectEvent(MobilePushTokenAdded, 42L)
      )
    }

    "send message to rabbitmq to notify of iOS mobile push token edited" in new Helper() {
      val (userFeatureManager, userPreferenceManager, controller) = withMocks()

      val token = MobileAppPushToken(Some("myToken"))
      val mobileApp = UserPreference(
        baseUser.id.get,
        UserPreferenceKey.MobileAppInfo,
        "true",
        "Boolean"
      )
      val preferredDevice = UserPreference(
        baseUser.id.get,
        UserPreferenceKey.PreferredDevice,
        UserPreferenceValues.WebAppAndMobileApp,
        "String"
      )

      stub(userFeatureManager.get("bwayne"))
        .toReturn(Success(baseUser))

      stub(
        userFeatureManager.update(
          baseUser.copy(mobile_push_token = Some("ios-myToken"))
        )
      )
        .toReturn(Success(baseUser))

      stub(userPreferenceManager.create(mobileApp))
        .toReturn(Success(1L))
      stub(userPreferenceManager.createOrUpdate(preferredDevice))
        .toReturn(Success(1L))

      val rq = FakeRequest(POST, "/fakereq?username=bwayne")
        .withSession(
          "username"     -> "bwayne",
          "isSuperAdmin" -> "true"
        )
        .withBody(Json.toJson(token))

      reset(xivoRabbitEventsPublisher)
      val res = call(controller.setIOSPushToken(), rq)
      status(res) shouldEqual CREATED
      verify(xivoRabbitEventsPublisher).publish(
        ObjectEvent(MobilePushTokenAdded, 42L)
      )
    }

    "send message to rabbitmq to notify of mobile push token deleted" in new Helper {
      val (userFeatureManager, userPreferenceManager, controller) = withMocks()

      val token = MobileAppPushToken(Some("myToken"))
      val user  = baseUser.copy(mobile_push_token = None)

      val preferredDevice = UserPreference(
        baseUser.id.get,
        UserPreferenceKey.PreferredDevice,
        UserPreferenceValues.TypeDefaultDevice,
        "String"
      )

      stub(userFeatureManager.get("bwayne")).toReturn(Success(baseUser))

      stub(userFeatureManager.update(user)).toReturn(Success(user))

      stub(
        userPreferenceManager.delete(
          baseUser.id.get,
          UserPreferenceKey.MobileAppInfo
        )
      )
        .toReturn(Success(1L))
      stub(userPreferenceManager.createOrUpdate(preferredDevice))
        .toReturn(Success(1L))

      val rq = FakeRequest(DELETE, "/fakereq?username=bwayne")
        .withSession("username" -> "bwayne", "isSuperAdmin" -> "true")
        .withBody(Json.toJson(token))

      reset(xivoRabbitEventsPublisher)
      val res = call(controller.deletePushToken(), rq)
      status(res) shouldEqual NO_CONTENT
      verify(xivoRabbitEventsPublisher).publish(
        ObjectEvent(MobilePushTokenDeleted, 42L)
      )
    }

    "set user preference when adding a push token" in new Helper() {
      val (userFeatureManager, userPreferenceManager, controller) = withMocks()
      val token = MobileAppPushToken(Some("myToken"))

      val mobileApp = UserPreference(
        baseUser.id.get,
        UserPreferenceKey.MobileAppInfo,
        "true",
        "Boolean"
      )
      val preferredDevice = UserPreference(
        baseUser.id.get,
        UserPreferenceKey.PreferredDevice,
        UserPreferenceValues.WebAppAndMobileApp,
        "String"
      )

      stub(userFeatureManager.get("bwayne"))
        .toReturn(Success(baseUser))

      stub(
        userFeatureManager.update(
          baseUser.copy(mobile_push_token = Some("myToken"))
        )
      ).toReturn(Success(baseUser))

      stub(userPreferenceManager.create(mobileApp))
        .toReturn(Success(1L))
      stub(userPreferenceManager.createOrUpdate(preferredDevice))
        .toReturn(Success(1L))

      val rq = FakeRequest(POST, "/fakereq?username=bwayne")
        .withSession(
          "username"     -> "bwayne",
          "isSuperAdmin" -> "true"
        )
        .withBody(Json.toJson(token))

      val res = call(controller.setPushToken(), rq)
      status(res) shouldEqual CREATED
      verify(userPreferenceManager).create(mobileApp)
      verify(userPreferenceManager).createOrUpdate(preferredDevice)
    }

    "set user preference when adding an Android push token" in new Helper() {
      val (userFeatureManager, userPreferenceManager, controller) = withMocks()

      val token = MobileAppPushToken(Some("myToken"))

      val mobileApp = UserPreference(
        baseUser.id.get,
        UserPreferenceKey.MobileAppInfo,
        "true",
        "Boolean"
      )
      val preferredDevice = UserPreference(
        baseUser.id.get,
        UserPreferenceKey.PreferredDevice,
        UserPreferenceValues.WebAppAndMobileApp,
        "String"
      )

      stub(userFeatureManager.get("bwayne"))
        .toReturn(Success(baseUser))

      stub(
        userFeatureManager.update(
          baseUser.copy(mobile_push_token = Some("android-myToken"))
        )
      ).toReturn(Success(baseUser))

      stub(userPreferenceManager.create(mobileApp))
        .toReturn(Success(1L))
      stub(userPreferenceManager.createOrUpdate(preferredDevice))
        .toReturn(Success(1L))

      val rq = FakeRequest(POST, "/fakereq?username=bwayne")
        .withSession(
          "username"     -> "bwayne",
          "isSuperAdmin" -> "true"
        )
        .withBody(Json.toJson(token))

      val res = call(controller.setAndroidPushToken(), rq)
      status(res) shouldEqual CREATED
      verify(userPreferenceManager).create(mobileApp)
      verify(userPreferenceManager).createOrUpdate(preferredDevice)
    }

    "set user preference when adding an iOS push token" in new Helper() {
      val (userFeatureManager, userPreferenceManager, controller) = withMocks()

      val token = MobileAppPushToken(Some("myToken"))

      val mobileApp = UserPreference(
        baseUser.id.get,
        UserPreferenceKey.MobileAppInfo,
        "true",
        "Boolean"
      )
      val preferredDevice = UserPreference(
        baseUser.id.get,
        UserPreferenceKey.PreferredDevice,
        UserPreferenceValues.WebAppAndMobileApp,
        "String"
      )

      stub(userFeatureManager.get("bwayne"))
        .toReturn(Success(baseUser))

      stub(
        userFeatureManager.update(
          baseUser.copy(mobile_push_token = Some("ios-myToken"))
        )
      ).toReturn(Success(baseUser))

      stub(userPreferenceManager.create(mobileApp))
        .toReturn(Success(1L))
      stub(userPreferenceManager.createOrUpdate(preferredDevice))
        .toReturn(Success(1L))

      val rq = FakeRequest(POST, "/fakereq?username=bwayne")
        .withSession(
          "username"     -> "bwayne",
          "isSuperAdmin" -> "true"
        )
        .withBody(Json.toJson(token))

      val res = call(controller.setIOSPushToken(), rq)
      status(res) shouldEqual CREATED
      verify(userPreferenceManager).create(mobileApp)
      verify(userPreferenceManager).createOrUpdate(preferredDevice)
    }

    "delete a push token" in new Helper {
      val (userFeatureManager, userPreferenceManager, controller) = withMocks()

      val token = MobileAppPushToken(Some("myToken"))
      val user  = baseUser.copy(mobile_push_token = None)

      val preferredDevice = UserPreference(
        baseUser.id.get,
        UserPreferenceKey.PreferredDevice,
        UserPreferenceValues.TypeDefaultDevice,
        "String"
      )

      stub(userFeatureManager.get("bwayne")).toReturn(Success(baseUser))

      stub(userFeatureManager.update(user)).toReturn(Success(user))

      stub(
        userPreferenceManager.delete(
          baseUser.id.get,
          UserPreferenceKey.MobileAppInfo
        )
      )
        .toReturn(Success(1L))
      stub(userPreferenceManager.createOrUpdate(preferredDevice))
        .toReturn(Success(1L))

      val rq = FakeRequest(DELETE, "/fakereq?username=bwayne")
        .withSession("username" -> "bwayne", "isSuperAdmin" -> "true")
        .withBody(Json.toJson(token))

      val res = call(controller.deletePushToken(), rq)
      status(res) shouldEqual NO_CONTENT
      verify(userFeatureManager).update(user)
      verify(userPreferenceManager).delete(
        baseUser.id.get,
        UserPreferenceKey.MobileAppInfo
      )
      verify(userPreferenceManager).createOrUpdate(preferredDevice)
    }

    "should return a 404 error if the user does not exist" in new Helper() {
      val (userFeatureManager, userPreferenceManager, controller) = withMocks()

      val token = MobileAppPushToken(Some("myToken"))
      val user  = baseUser.copy(loginclient = "bwaynee")

      val newUserPreference =
        UserPreference(
          user.id.get,
          UserPreferenceKey.MobileAppInfo,
          "true",
          "Boolean"
        )

      stub(userFeatureManager.get("bwaynee"))
        .toReturn(Failure(AnormException("User does not exist !")))

      stub(
        userFeatureManager.update(
          user.copy(mobile_push_token = Some("myToken"))
        )
      ).toReturn(Success(user))

      stub(
        userPreferenceManager.create(
          newUserPreference
        )
      ).toReturn(Success(1L))

      val rq = FakeRequest(POST, "/fakereq?username=bwaynee")
        .withSession(
          "username"     -> "bwayne",
          "isSuperAdmin" -> "true"
        )
        .withBody(Json.toJson(token))

      val res = call(controller.setPushToken(), rq)
      status(res) shouldEqual NOT_FOUND
    }

    "return a 404 error if the Android user does not exist" in new Helper() {
      val (userFeatureManager, userPreferenceManager, controller) = withMocks()

      val token = MobileAppPushToken(Some("myToken"))
      val user  = baseUser.copy(loginclient = "bwaynee")

      val newUserPreference =
        UserPreference(
          user.id.get,
          UserPreferenceKey.MobileAppInfo,
          "true",
          "Boolean"
        )

      stub(userFeatureManager.get("bwaynee"))
        .toReturn(Failure(AnormException("User does not exist !")))

      stub(
        userFeatureManager.update(
          user.copy(mobile_push_token = Some("android-myToken"))
        )
      ).toReturn(Success(user))

      stub(
        userPreferenceManager.create(
          newUserPreference
        )
      ).toReturn(Success(1L))

      val rq = FakeRequest(POST, "/fakereq?username=bwaynee")
        .withSession(
          "username"     -> "bwayne",
          "isSuperAdmin" -> "true"
        )
        .withBody(Json.toJson(token))

      val res = call(controller.setAndroidPushToken(), rq)
      status(res) shouldEqual NOT_FOUND
    }

    "return a 404 error if the iOS user does not exist" in new Helper() {
      val (userFeatureManager, userPreferenceManager, controller) = withMocks()

      val token = MobileAppPushToken(Some("myToken"))
      val user  = baseUser.copy(loginclient = "bwaynee")

      val newUserPreference =
        UserPreference(
          user.id.get,
          UserPreferenceKey.MobileAppInfo,
          "true",
          "Boolean"
        )

      stub(userFeatureManager.get("bwaynee"))
        .toReturn(Failure(AnormException("User does not exist !")))

      stub(
        userFeatureManager.update(
          user.copy(mobile_push_token = Some("ios-myToken"))
        )
      ).toReturn(Success(user))

      stub(
        userPreferenceManager.create(
          newUserPreference
        )
      ).toReturn(Success(1L))

      val rq = FakeRequest(POST, "/fakereq?username=bwaynee")
        .withSession(
          "username"     -> "bwayne",
          "isSuperAdmin" -> "true"
        )
        .withBody(Json.toJson(token))

      val res = call(controller.setIOSPushToken(), rq)
      status(res) shouldEqual NOT_FOUND
    }

    "should return a 500 error if other error happen" in new Helper {
      val (userFeatureManager, userPreferenceManager, controller) = withMocks()
      val token = MobileAppPushToken(Some("myToken"))

      val newUserPreference =
        UserPreference(
          baseUser.id.get,
          UserPreferenceKey.MobileAppInfo,
          "true",
          "Boolean"
        )

      stub(userFeatureManager.get("bwayne"))
        .toReturn(Failure(new Exception("Bdd is not reachable !")))

      stub(
        userFeatureManager.update(
          baseUser.copy(mobile_push_token = Some("myToken"))
        )
      ).toReturn(Success(baseUser))

      stub(
        userPreferenceManager.create(
          newUserPreference
        )
      ).toReturn(Success(1L))

      val rq = FakeRequest(POST, "/")
        .withSession(
          "username"     -> "bwayne",
          "isSuperAdmin" -> "true"
        )
        .withBody(Json.toJson(token))

      val res = call(controller.setPushToken(), rq)
      status(res) shouldEqual INTERNAL_SERVER_ERROR
    }

    "should return a 404 if mobile push notification key file is not installed on the platform" in new Helper {
      val (userFeatureManager, userPreferenceManager, controller) = withMocks()

      val token = MobileAppPushToken(Some("myToken"))

      val newUserPreference =
        UserPreference(
          baseUser.id.get,
          UserPreferenceKey.MobileAppInfo,
          "true",
          "Boolean"
        )

      stub(userFeatureManager.get("bwayne"))
        .toReturn(Failure(new Exception("Bdd is not reachable !")))

      stub(
        userFeatureManager.update(
          baseUser.copy(mobile_push_token = Some("android-myToken"))
        )
      ).toReturn(Success(baseUser))

      stub(
        userPreferenceManager.create(
          newUserPreference
        )
      ).toReturn(Success(1L))

      val rq = FakeRequest(POST, "/")
        .withSession(
          "username"     -> "bwayne",
          "isSuperAdmin" -> "true"
        )
        .withBody(Json.toJson(token))

      val res = call(controller.setAndroidPushToken(), rq)
      status(res) shouldEqual INTERNAL_SERVER_ERROR
    }

    "should return a 204 if mobile push notification key file exists on the platform" in new Helper {
      val (userFeatureManager, userPreferenceManager, controller) = withMocks()
      val token = MobileAppPushToken(Some("myToken"))

      val newUserPreference =
        UserPreference(
          baseUser.id.get,
          UserPreferenceKey.MobileAppInfo,
          "true",
          "Boolean"
        )

      stub(userFeatureManager.get("bwayne"))
        .toReturn(Failure(new Exception("Bdd is not reachable !")))

      stub(
        userFeatureManager.update(
          baseUser.copy(mobile_push_token = Some("ios-myToken"))
        )
      ).toReturn(Success(baseUser))

      stub(
        userPreferenceManager.create(
          newUserPreference
        )
      ).toReturn(Success(1L))

      val rq = FakeRequest(POST, "/")
        .withSession(
          "username"     -> "bwayne",
          "isSuperAdmin" -> "true"
        )
        .withBody(Json.toJson(token))

      val res = call(controller.setIOSPushToken(), rq)
      status(res) shouldEqual INTERNAL_SERVER_ERROR
    }
  }
}
