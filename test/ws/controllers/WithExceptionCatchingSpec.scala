package ws.controllers

import java.security.InvalidParameterException

import play.api.libs.json.Json
import testutils.PlayShouldSpec

class WithExceptionCatchingSpec
    extends PlayShouldSpec
    with WithExceptionCatching {
  "WithExceptionCatching" should {
    "catch InvalidParameterException and return a BadRequest status" in {

      val m = "InvalidParameter"
      withExceptionCatching(throw new InvalidParameterException(m)) should be(
        BadRequest(Json.toJson(m))
      )

    }

    "catch NoSuchElementException and return a NotFound status" in {
      val m = "NoSuchElement"
      withExceptionCatching(throw new NoSuchElementException(m)) should be(
        NotFound(Json.toJson(m))
      )
    }

    "catch Exception and return an InternalServerError" in {
      val m = "Exception"
      withExceptionCatching(throw new Exception(m)) should be(
        InternalServerError(Json.toJson(m))
      )
    }

    "not catch Throwable" in {
      an[Throwable] should be thrownBy withExceptionCatching(
        throw new Throwable("Should be Unhandled")
      )
    }

    "not catch Error" in {
      an[Error] should be thrownBy withExceptionCatching(
        throw new Error("Should be Unhandled")
      )
    }
  }
}
