package ws.controllers

import akka.stream.Materializer
import akka.util.Timeout
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.mockito.MockitoSugar
import docker.DockerDBTest
import model.ws.NotHandledError
import org.mockito.Mockito.{never, verify, when}
import play.api.http.Status._
import play.api.libs.json.Json
import play.api.libs.ws.WSResponse
import play.api.test.FakeRequest
import play.api.test.Helpers.{
  call,
  contentAsJson,
  status,
  writeableOf_AnyContentAsEmpty,
  DELETE,
  GET,
  POST,
  PUT
}
import ws.controllers.Group
import xivo.model
import xivo.model.DialActionEventType.DialActionEventType
import xivo.model._
import xivo.service.{
  DialActionManager,
  GroupManager,
  GroupNotifier,
  QueueMemberManager,
  SysConfdClient,
  SysConfdServiceException
}
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.inject.bind
import xivo.service.rabbitmq.XivoRabbitEventsPublisher

import scala.concurrent.duration.DurationInt
import play.api.Application

import java.sql.Connection
import scala.concurrent.Future

class GroupSpec extends DockerDBTest with BeforeAndAfterEach with MockitoSugar {

  val rabbitPublisher = mock[XivoRabbitEventsPublisher]

  class Helper() {
    implicit lazy val app: Application = new GuiceApplicationBuilder()
      .configure(
        playConfig ++ Map(
          "db.default.url" -> xcjdbcurl.get,
          "db.xivo.url"    -> xivojdbcurl.get,
          "db.xc.url"      -> xcjdbcurl.get
        )
      )
      .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
      .build()

    implicit val materializer: Materializer = app.materializer
    implicit val t: Timeout                 = 5000.millis
    implicit val connection: Connection     = xcConnection.get

    val groupMgr: GroupManager             = mock[GroupManager]
    val dialActionMgr: DialActionManager   = mock[DialActionManager]
    val queueMemberMgr: QueueMemberManager = mock[QueueMemberManager]
    val sysConfdClient: SysConfdClient     = mock[SysConfdClient]
    val notifier: GroupNotifier            = mock[GroupNotifier]
    val defaultDialActions: Map[DialActionEventType, DialActionAction] = Map(
      DialActionEventType.noAnswer ->
        DialActionAction(action = DialActionActionType.none),
      DialActionEventType.congestion ->
        DialActionAction(action = DialActionActionType.none),
      DialActionEventType.chanUnavailable ->
        DialActionAction(action = DialActionActionType.none)
    )
    val customDialActions: Map[DialActionEventType, DialActionAction] = Map(
      DialActionEventType.noAnswer ->
        DialActionAction(
          action = DialActionActionType.group,
          actionArg1 = Some("1"),
          actionArg2 = Some("5")
        ),
      DialActionEventType.congestion ->
        DialActionAction(
          action = DialActionActionType.user,
          actionArg1 = Some("6"),
          actionArg2 = Some("3")
        ),
      DialActionEventType.chanUnavailable ->
        DialActionAction(
          action = DialActionActionType.queue,
          actionArg1 = Some("7"),
          actionArg2 = Some("10")
        )
    )

    val group1: model.Group = Group(
      1L,
      "group_test",
      "default",
      "main",
      "3000",
      allowTransferUser = true,
      allowTransferCall = false,
      allowCallerRecord = false,
      allowCalleeRecord = false,
      ignoreForward = true,
      Some("subroutine"),
      "default",
      Some(10),
      1,
      RingingStrategyType.rRMemory,
      ringInUse = false,
      CallerId(None, "")
    )

    val group1JsonWithoutMembers: String =
      """
      |{
      |   "id":1,
      |   "name":"group_test",
      |   "context":"default",
      |   "mds":"main",
      |   "number":"3000",
      |   "allowTransferUser":true,
      |   "allowTransferCall":false,
      |   "allowCallerRecord":false,
      |   "allowCalleeRecord":false,
      |   "ignoreForward":true,
      |   "preprocessSubroutine":"subroutine",
      |   "onHoldMusic":"default",
      |   "timeout":10,
      |   "retryTime":1,
      |   "ringingStrategy":"rrmemory",
      |   "ringInUse":false,
      |   "callerId":{
      |      "display":""
      |   },
      |   "dialactions":{
      |      "noAnswer":{
      |         "action":"group",
      |         "actionArg1":"1",
      |         "actionArg2":"5"
      |      },
      |      "congestion":{
      |         "action":"user",
      |         "actionArg1":"6",
      |         "actionArg2":"3"
      |      },
      |      "chanUnavailable":{
      |         "action":"queue",
      |         "actionArg1":"7",
      |         "actionArg2":"10"
      |      }
      |   }
      |}""".stripMargin

    val group1Json: String =
      """
      |{
      |   "id":1,
      |   "name":"group_test",
      |   "context":"default",
      |   "mds":"main",
      |   "number":"3000",
      |   "allowTransferUser":true,
      |   "allowTransferCall":false,
      |   "allowCallerRecord":false,
      |   "allowCalleeRecord":false,
      |   "ignoreForward":true,
      |   "preprocessSubroutine":"subroutine",
      |   "onHoldMusic":"default",
      |   "timeout":10,
      |   "retryTime":1,
      |   "ringingStrategy":"rrmemory",
      |   "ringInUse":false,
      |   "callerId":{
      |      "display":""
      |   },
      |   "dialactions":{
      |      "chanUnavailable":{
      |         "action":"queue",
      |         "actionArg1":"7",
      |         "actionArg2":"10"
      |      },
      |      "noAnswer":{
      |         "action":"group",
      |         "actionArg1":"1",
      |         "actionArg2":"5"
      |      },
      |      "congestion":{
      |         "action":"user",
      |         "actionArg1":"6",
      |         "actionArg2":"3"
      |      }
      |   },
      |   "members":[
      |     {
      |       "interface":"Local/id-1011@usercallback",
      |       "penalty":0,
      |       "commented":0,
      |       "usertype":"User",
      |       "userid":1,
      |       "channel":"Local",
      |       "category":"Group",
      |       "position":1
      |     }
      |   ]
      |}""".stripMargin

    val group2: model.Group = Group(
      2L,
      "group_test_second",
      "default",
      "mds1",
      "3001",
      allowTransferUser = false,
      allowTransferCall = true,
      allowCallerRecord = true,
      allowCalleeRecord = true,
      ignoreForward = false,
      None,
      "tt_monkeys",
      Some(10),
      3,
      RingingStrategyType.ringAll,
      ringInUse = true,
      CallerId(Some(CallerIdModeType.prepend), "agent_")
    )

    val group2Json: String =
      """
      |{
      |   "id":2,
      |   "name":"group_test_second",
      |   "context":"default",
      |   "mds":"mds1",
      |   "number":"3001",
      |   "allowTransferUser":false,
      |   "allowTransferCall":true,
      |   "allowCallerRecord":true,
      |   "allowCalleeRecord":true,
      |   "ignoreForward":false,
      |   "onHoldMusic":"tt_monkeys",
      |   "timeout":10,
      |   "retryTime":3,
      |   "ringingStrategy":"ringall",
      |   "ringInUse":true,
      |   "callerId":{
      |      "mode":"prepend",
      |      "display":"agent_"
      |   },
      |   "dialactions":{
      |      "chanUnavailable":{
      |         "action":"none"
      |      },
      |      "noAnswer":{
      |         "action":"none"
      |      },
      |      "congestion":{
      |         "action":"none"
      |      }
      |   },
      |   "members":[
      |     {
      |       "interface":"Local/id-1012@usercallback",
      |       "penalty":0,
      |       "commented":0,
      |       "usertype":"User",
      |       "userid":1,
      |       "channel":"Local",
      |       "category":"Group",
      |       "position":1
      |     }
      |   ]
      |}""".stripMargin

    val groupMembersGroup1: List[QueueMember] = List(
      QueueMember(
        "group_test",
        1L,
        "Local/id-1011@usercallback",
        penalty = 0,
        0,
        QueueMemberUserType.User,
        1L,
        "Local",
        QueueCategory.Group,
        1
      )
    )

    val groupMembers: List[QueueMember] = List(
      QueueMember(
        "group_test",
        1L,
        "Local/id-1011@usercallback",
        penalty = 0,
        0,
        QueueMemberUserType.User,
        1L,
        "Local",
        QueueCategory.Group,
        1
      ),
      QueueMember(
        "group_test_second",
        2L,
        "Local/id-1012@usercallback",
        penalty = 0,
        0,
        QueueMemberUserType.User,
        1L,
        "Local",
        QueueCategory.Group,
        1
      )
    )

    def getController(): Group = {
      new Group(
        app.injector.instanceOf[controllers.Secured],
        groupMgr,
        dialActionMgr,
        queueMemberMgr,
        sysConfdClient,
        notifier
      )
    }
  }

  "Group" should {
    "return '200 OK' when retrieving all groups" in new Helper() {
      val controller = getController()
      val group = List(
        group1,
        group2
      )
      when(groupMgr.getAllGroups)
        .thenReturn(scala.util.Right(group))
      when(dialActionMgr.getAllDialActions("group"))
        .thenReturn(
          scala.util.Right(
            Map(
              group2.id -> defaultDialActions,
              group1.id -> customDialActions
            )
          )
        )
      when(
        queueMemberMgr.getAllQueueMember(
          QueueMemberUserType.User,
          QueueCategory.Group
        )
      )
        .thenReturn(
          scala.util.Right(
            Map(
              1L -> List(groupMembers.head),
              2L -> List(groupMembers.last)
            )
          )
        )
      val rq = FakeRequest(GET, s"/api/2.0/groups").withSession(
        "username"     -> "bwillis",
        "isSuperAdmin" -> "true"
      )
      val res = call(controller.getAllGroups(), rq)
      status(res) shouldEqual OK
      verify(groupMgr).getAllGroups
      verify(dialActionMgr).getAllDialActions("group")
      verify(queueMemberMgr).getAllQueueMember(
        QueueMemberUserType.User,
        QueueCategory.Group
      )
      contentAsJson(res) shouldEqual Json.parse(s"[$group1Json,$group2Json]")
    }

    "return '200 OK' when retrieving an existing group by id" in new Helper() {
      val controller = getController()
      when(groupMgr.getGroupById(group1.id))
        .thenReturn(scala.util.Right(group1))
      when(dialActionMgr.getDialActions(group1.id, "group"))
        .thenReturn(scala.util.Right(customDialActions))
      when(
        queueMemberMgr.getQueueMember(
          QueueMemberUserType.User,
          QueueCategory.Group,
          group1.id
        )
      ).thenReturn(scala.util.Right(groupMembersGroup1))

      val rq = FakeRequest(GET, s"/api/2.0/group/${group1.id}").withSession(
        "username"     -> "bwillis",
        "isSuperAdmin" -> "true"
      )
      val res = call(controller.getGroupById(group1.id), rq)
      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.parse(group1Json)
      verify(groupMgr).getGroupById(group1.id)
      verify(dialActionMgr).getDialActions(group1.id, "group")
      verify(queueMemberMgr).getQueueMember(
        QueueMemberUserType.User,
        QueueCategory.Group,
        group1.id
      )
    }

    "return '404 NOT_FOUND' when fetching a non existing group by id" in new Helper() {
      val groupId    = 78
      val controller = getController()
      when(groupMgr.getGroupById(groupId))
        .thenReturn(Left(GroupError(GroupNotFound, "Group does not exist")))
      val rq = FakeRequest(GET, s"/api/2.0/group/$groupId").withSession(
        "username"     -> "bwillis",
        "isSuperAdmin" -> "true"
      )
      val res = call(controller.getGroupById(groupId), rq)
      status(res) shouldEqual NOT_FOUND
      verify(groupMgr).getGroupById(groupId)
    }

    "return '201 CREATED' when a group is successfully created with minimal properties" in new Helper() {
      val controller: Group = getController()
      val name              = "group_test"
      val context           = "default"
      val mdsName           = "main"
      val number            = "3000"
      val data: CreateGroup = CreateGroup(
        name = name,
        context = context,
        mds = mdsName,
        number = number
      )
      val group: model.Group =
        Group(
          1L,
          name,
          context,
          mdsName,
          number,
          true,
          allowTransferCall = false,
          allowCallerRecord = false,
          allowCalleeRecord = false,
          ignoreForward = true,
          None,
          "default",
          Some(10),
          1,
          RingingStrategyType.rRMemory,
          ringInUse = false,
          CallerId(None, "")
        )
      when(groupMgr.createGroup(data)).thenReturn(scala.util.Right(group))
      when(
        dialActionMgr.createDialActions(
          group.id,
          "group",
          defaultDialActions
        )
      )
        .thenReturn(scala.util.Right(defaultDialActions))

      val rq = FakeRequest(POST, s"/api/2.0/groups")
        .withSession(
          "username"     -> "bwillis",
          "isSuperAdmin" -> "true"
        )
        .withBody(Json.toJson(data))
      val res = call(controller.createGroup(), rq)
      status(res) shouldEqual CREATED
      contentAsJson(res) shouldEqual Json.toJson(
        EnrichedGroup(group, defaultDialActions)
      )
      verify(groupMgr).createGroup(data)
      verify(dialActionMgr).createDialActions(
        group.id,
        "group",
        defaultDialActions
      )
      verify(notifier).onGroupCreated(group.id)
    }

    "return '201 CREATED' when a group is successfully created with all properties" in new Helper() {
      val controller    = getController()
      val name          = "group_test"
      val context       = "default"
      val mdsName       = "main"
      val number        = "3000"
      val strategy      = RingingStrategyType.rRMemory
      val music         = "musique"
      val timeout       = 10
      val retry         = 1
      val ringInUse     = false
      val subroutine    = "subroutine"
      val transferUser  = true
      val transferCall  = false
      val calleeRecord  = false
      val callerRecord  = false
      val ignoreForward = true
      val data = CreateGroup(
        name,
        context,
        mdsName,
        number,
        transferUser,
        transferCall,
        callerRecord,
        calleeRecord,
        ignoreForward,
        Some(subroutine),
        music,
        Some(timeout),
        retry,
        strategy,
        ringInUse,
        CallerId(Some(CallerIdModeType.prepend), "display"),
        customDialActions
      )
      val group =
        Group(
          1L,
          data.name,
          data.context,
          data.mds,
          data.number,
          data.allowTransferUser,
          data.allowTransferCall,
          data.allowCallerRecord,
          data.allowCalleeRecord,
          data.ignoreForward,
          data.preprocessSubroutine,
          data.onHoldMusic,
          data.timeout,
          data.retryTime,
          data.ringingStrategy,
          data.ringInUse,
          data.callerId
        )
      when(groupMgr.createGroup(data)).thenReturn(scala.util.Right(group))
      when(
        dialActionMgr.createDialActions(group.id, "group", data.dialactions)
      ).thenReturn(scala.util.Right(customDialActions))

      val rq = FakeRequest(POST, s"/api/2.0/groups")
        .withSession(
          "username"     -> "bwillis",
          "isSuperAdmin" -> "true"
        )
        .withBody(Json.toJson(data))
      val res = call(controller.createGroup(), rq)
      status(res) shouldEqual CREATED
      contentAsJson(res) shouldEqual Json.toJson(
        EnrichedGroup(group, customDialActions)
      )
      verify(groupMgr).createGroup(data)
      verify(dialActionMgr).createDialActions(
        group.id,
        "group",
        data.dialactions
      )
      verify(notifier).onGroupCreated(group.id)
    }

    "return '200 OK' when updating a group with minimal properties" in new Helper() {
      val controller = getController()
      val groupId    = 26L
      val name       = "new_test"
      val context    = "new_context"
      val mdsName    = "mds1"
      val data       = UpdateGroup(Some(name), Some(context), Some(mdsName))
      val group =
        Group(
          groupId,
          name,
          context,
          mdsName,
          "3000",
          allowTransferUser = true,
          allowTransferCall = false,
          allowCallerRecord = false,
          allowCalleeRecord = false,
          ignoreForward = true,
          None,
          "default",
          Some(10),
          1,
          RingingStrategyType.rRMemory,
          ringInUse = false,
          CallerId(None, "")
        )
      when(groupMgr.updateGroup(groupId, data))
        .thenReturn(scala.util.Right(group))

      when(
        dialActionMgr.getDialActions(group.id, "group")
      ).thenReturn(scala.util.Right(customDialActions))

      val rq = FakeRequest(PUT, s"/api/2.0/groups/$groupId")
        .withSession(
          "username"     -> "bwillis",
          "isSuperAdmin" -> "true"
        )
        .withBody(Json.toJson(data))
      val res =
        call(controller.updateGroup(groupId), rq)
      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(
        EnrichedGroup(
          group.updateModifiedProperties(data),
          customDialActions
        )
      )
      verify(groupMgr).updateGroup(groupId, data)
      verify(dialActionMgr).getDialActions(groupId, "group")
      verify(notifier).onGroupEdited(group.id)
    }

    "return '200 OK' when a group is successfully updated with all properties" in new Helper() {
      val controller    = getController()
      val groupId       = 26L
      val name          = "group_test"
      val context       = "default"
      val mdsName       = "main"
      val number        = Some("3000")
      val strategy      = Some(RingingStrategyType.rRMemory)
      val timeout       = Some(10)
      val retry         = Some(1)
      val ringInUse     = Some(false)
      val subroutine    = Some("subroutine")
      val music         = Some("musique")
      val transferUser  = Some(true)
      val transferCall  = Some(false)
      val calleeRecord  = Some(false)
      val callerRecord  = Some(false)
      val ignoreForward = Some(true)
      val callerId      = Some(CallerId(Some(CallerIdModeType.prepend), "display"))
      val dialactions   = Some(customDialActions)
      val data = UpdateGroup(
        Some(name),
        Some(context),
        Some(mdsName),
        number,
        transferUser,
        transferCall,
        callerRecord,
        calleeRecord,
        ignoreForward,
        subroutine,
        music,
        timeout,
        retry,
        strategy,
        ringInUse,
        callerId,
        dialactions
      )
      val group =
        Group(
          groupId,
          name,
          context,
          mdsName,
          number.getOrElse(""),
          data.allowTransferUser.get,
          data.allowTransferCall.get,
          data.allowCallerRecord.get,
          data.allowCalleeRecord.get,
          data.ignoreForward.get,
          data.preprocessSubroutine,
          data.onHoldMusic.get,
          data.timeout,
          data.retryTime.get,
          data.ringingStrategy.get,
          data.ringInUse.get,
          data.callerId.get
        )

      when(groupMgr.updateGroup(groupId, data))
        .thenReturn(scala.util.Right(group))
      when(
        dialActionMgr.updateDialActions(groupId, "group", dialactions.get)
      )
        .thenReturn(scala.util.Right(dialactions.get))
      val rq = FakeRequest(PUT, s"/api/2.0/groups/$groupId")
        .withSession(
          "username"     -> "bwillis",
          "isSuperAdmin" -> "true"
        )
        .withBody(Json.toJson(data))
      val res = call(
        controller.updateGroup(groupId),
        rq
      )
      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(
        EnrichedGroup(
          group.updateModifiedProperties(data),
          data.dialactions.get
        )
      )
      verify(groupMgr).updateGroup(groupId, data)
      verify(dialActionMgr).updateDialActions(
        groupId,
        "group",
        data.dialactions.get
      )
      verify(notifier).onGroupEdited(group.id)
    }

    "return '204 NO_CONTENT' when deleting a group" in new Helper() {
      val groupId    = 78
      val controller = getController()
      when(groupMgr.deleteGroup(groupId)).thenReturn(scala.util.Right(1))
      when(dialActionMgr.deleteDialActions(groupId, "group"))
        .thenReturn(scala.util.Right(78))
      val rq = FakeRequest(DELETE, s"/api/2.0/group/$groupId").withSession(
        "username"     -> "bwillis",
        "isSuperAdmin" -> "true"
      )
      val res = call(controller.deleteGroup(groupId), rq)
      status(res) shouldEqual NO_CONTENT
      verify(groupMgr).deleteGroup(groupId)
      verify(dialActionMgr).deleteDialActions(groupId, "group")
    }

    "return '201 CREATED' if a user is added successfully in a group" in new Helper() {
      val userId                   = 42
      val groupId                  = 96
      val controller               = getController()
      val fakeResponse: WSResponse = mock[WSResponse]
      when(groupMgr.addUserToGroup(groupId, userId))
        .thenReturn(scala.util.Right(1))
      when(sysConfdClient.reloadAsteriskQueueConfiguration())
        .thenReturn(Future.successful(fakeResponse))
      val rq = FakeRequest(POST, s"/api/2.0/groups/$groupId/members/$userId")
        .withSession(
          "username"     -> "bwillis",
          "isSuperAdmin" -> "true"
        )
      val res = call(controller.addUserToGroup(groupId, userId), rq)
      status(res) shouldEqual CREATED
      verify(groupMgr).addUserToGroup(groupId, userId)
      verify(sysConfdClient).reloadAsteriskQueueConfiguration()
      verify(notifier).onGroupEdited(groupId)
    }

    "return '404 NOT_FOUND' if a user to add to a group is not found" in new Helper() {
      val userId     = 42
      val groupId    = 96
      val controller = getController()
      when(groupMgr.addUserToGroup(groupId, userId))
        .thenReturn(Left(GroupError(UserNotFound, "User does not exist")))
      val rq = FakeRequest(POST, s"/api/2.0/groups/$groupId/members/$userId")
        .withSession(
          "username"     -> "bwillis",
          "isSuperAdmin" -> "true"
        )
      val res = call(controller.addUserToGroup(groupId, userId), rq)
      status(res) shouldEqual NOT_FOUND
      verify(groupMgr).addUserToGroup(groupId, userId)
      verify(groupMgr, never).removeUserFromGroup(groupId, userId)
      verify(notifier, never).onGroupEdited(groupId)
    }

    "return '404 NOT_FOUND' if a user is added to a group not found" in new Helper() {
      val userId     = 42
      val groupId    = 96
      val controller = getController()
      when(groupMgr.addUserToGroup(groupId, userId))
        .thenReturn(Left(GroupError(GroupNotFound, "Group does not exist")))
      val rq = FakeRequest(POST, s"/api/2.0/groups/$groupId/members/$userId")
        .withSession(
          "username"     -> "bwillis",
          "isSuperAdmin" -> "true"
        )
      val res = call(controller.addUserToGroup(groupId, userId), rq)
      status(res) shouldEqual NOT_FOUND
      verify(groupMgr).addUserToGroup(groupId, userId)
      verify(groupMgr, never).removeUserFromGroup(groupId, userId)
      verify(notifier, never).onGroupEdited(groupId)
    }

    "return '409 CONFLICT' if a user is added to a group he's already member of" in new Helper() {
      val userId     = 64
      val groupId    = 1024
      val controller = getController()
      when(groupMgr.addUserToGroup(groupId, userId))
        .thenReturn(
          Left(
            GroupError(
              UserAlreadyInGroup,
              s"The user $userId is already member of the group $groupId"
            )
          )
        )
      val rq = FakeRequest(POST, s"/api/2.0/groups/$groupId/members/$userId")
        .withSession(
          "username"     -> "bwillis",
          "isSuperAdmin" -> "true"
        )
      val res = call(controller.addUserToGroup(groupId, userId), rq)
      status(res) shouldEqual CONFLICT
      verify(groupMgr).addUserToGroup(groupId, userId)
      verify(groupMgr, never).removeUserFromGroup(groupId, userId)
      verify(notifier, never).onGroupEdited(groupId)
    }

    "return '500 INTERNAL_SERVER_ERROR' if an unhandled exception happen when adding a user in a group" in new Helper() {
      val userId     = 64
      val groupId    = 1024
      val controller = getController()
      when(groupMgr.addUserToGroup(groupId, userId))
        .thenReturn(
          Left(
            GroupError(
              NotHandledError,
              s"Unhandled error happen when adding user $userId in group $groupId"
            )
          )
        )
      val rq = FakeRequest(POST, s"/api/2.0/groups/$groupId/members/$userId")
        .withSession(
          "username"     -> "bwillis",
          "isSuperAdmin" -> "true"
        )
      val res = call(controller.addUserToGroup(groupId, userId), rq)
      status(res) shouldEqual INTERNAL_SERVER_ERROR
      verify(groupMgr).addUserToGroup(groupId, userId)
      verify(groupMgr, never).removeUserFromGroup(groupId, userId)
      verify(notifier, never).onGroupEdited(groupId)
    }

    "remove a user from the group if the queue reload failed after adding the user in said group" in new Helper() {
      val userId     = 42
      val groupId    = 96
      val controller = getController()
      when(sysConfdClient.reloadAsteriskQueueConfiguration())
        .thenReturn(
          Future.failed(SysConfdServiceException("Error when reloading queues"))
        )
      when(groupMgr.addUserToGroup(groupId, userId))
        .thenReturn(scala.util.Right(1))
      when(groupMgr.removeUserFromGroup(groupId, userId))
        .thenReturn(scala.util.Right(1))

      val rq = FakeRequest(POST, s"/api/2.0/groups/$groupId/members/$userId")
        .withSession(
          "username"     -> "bwillis",
          "isSuperAdmin" -> "true"
        )
      val res = call(controller.addUserToGroup(groupId, userId), rq)
      status(res) shouldEqual SERVICE_UNAVAILABLE
      verify(groupMgr).addUserToGroup(groupId, userId)
      verify(sysConfdClient).reloadAsteriskQueueConfiguration()
      verify(groupMgr).removeUserFromGroup(groupId, userId)
      verify(notifier, never).onGroupEdited(groupId)
    }

    "return '204 NO_CONTENT' if a user is removed successfully from a group" in new Helper() {
      val userId                   = 63
      val groupId                  = 175
      val fakeResponse: WSResponse = mock[WSResponse]
      val controller               = getController()
      when(groupMgr.removeUserFromGroup(groupId, userId))
        .thenReturn(scala.util.Right(1))
      when(sysConfdClient.reloadAsteriskQueueConfiguration())
        .thenReturn(Future.successful(fakeResponse))
      val rq = FakeRequest(DELETE, s"/api/2.0/groups/$groupId/members/$userId")
        .withSession(
          "username"     -> "bwillis",
          "isSuperAdmin" -> "true"
        )
      val res = call(controller.removeUserFromGroup(groupId, userId), rq)
      status(res) shouldEqual NO_CONTENT
      verify(groupMgr).removeUserFromGroup(groupId, userId)
      verify(sysConfdClient).reloadAsteriskQueueConfiguration()
      verify(notifier).onGroupEdited(groupId)
    }

    "return '404 NOT_FOUND' if a user to remove from a group is not found" in new Helper() {
      val userId     = 42
      val groupId    = 96
      val controller = getController()
      when(groupMgr.removeUserFromGroup(groupId, userId))
        .thenReturn(Left(GroupError(UserNotFound, "User does not exist")))
      val rq = FakeRequest(DELETE, s"/api/2.0/groups/$groupId/members/$userId")
        .withSession(
          "username"     -> "bwillis",
          "isSuperAdmin" -> "true"
        )
      val res = call(controller.removeUserFromGroup(groupId, userId), rq)
      status(res) shouldEqual NOT_FOUND
      verify(groupMgr).removeUserFromGroup(groupId, userId)
      verify(groupMgr, never).addUserToGroup(groupId, userId)
      verify(notifier, never).onGroupDeleted(groupId)
    }

    "return '404 NOT_FOUND' if a user is removed from a group not found" in new Helper() {
      val userId     = 42
      val groupId    = 96
      val controller = getController()
      when(groupMgr.removeUserFromGroup(groupId, userId))
        .thenReturn(Left(GroupError(GroupNotFound, "Group does not exist")))
      val rq = FakeRequest(DELETE, s"/api/2.0/groups/$groupId/members/$userId")
        .withSession(
          "username"     -> "bwillis",
          "isSuperAdmin" -> "true"
        )
      val res = call(controller.removeUserFromGroup(groupId, userId), rq)
      status(res) shouldEqual NOT_FOUND
      verify(groupMgr).removeUserFromGroup(groupId, userId)
      verify(groupMgr, never).addUserToGroup(groupId, userId)
      verify(notifier, never).onGroupDeleted(groupId)
    }

    "return '409 CONFLICT' if a user is removed from a group he's not member of" in new Helper() {
      val userId     = 96
      val groupId    = 3658
      val controller = getController()
      when(groupMgr.removeUserFromGroup(groupId, userId))
        .thenReturn(
          Left(
            GroupError(
              UserNotInGroup,
              s"The user $userId is not member of the group $groupId"
            )
          )
        )
      val rq = FakeRequest(POST, s"/api/2.0/groups/$groupId/members/$userId")
        .withSession(
          "username"     -> "bwillis",
          "isSuperAdmin" -> "true"
        )
      val res = call(controller.removeUserFromGroup(groupId, userId), rq)
      status(res) shouldEqual CONFLICT
      verify(groupMgr).removeUserFromGroup(groupId, userId)
      verify(groupMgr, never).addUserToGroup(groupId, userId)
      verify(notifier, never).onGroupDeleted(groupId)
    }

    "return '500 INTERNAL_SERVER_ERROR' if an unhandled exception happen when removing a user from a group" in new Helper() {
      val userId     = 95
      val groupId    = 41024
      val controller = getController()
      when(groupMgr.removeUserFromGroup(groupId, userId))
        .thenReturn(
          Left(
            GroupError(
              NotHandledError,
              s"Unhandled error happen when removing user $userId from group $groupId"
            )
          )
        )
      val rq = FakeRequest(DELETE, s"/api/2.0/groups/$groupId/members/$userId")
        .withSession(
          "username"     -> "bwillis",
          "isSuperAdmin" -> "true"
        )
      val res = call(controller.removeUserFromGroup(groupId, userId), rq)
      status(res) shouldEqual INTERNAL_SERVER_ERROR
      verify(groupMgr).removeUserFromGroup(groupId, userId)
      verify(groupMgr, never).addUserToGroup(groupId, userId)
      verify(notifier, never).onGroupDeleted(groupId)
    }

    "add a user from the group if the queue reload failed after removing the user in said group" in new Helper() {
      val userId     = 42
      val groupId    = 96
      val controller = getController()
      when(sysConfdClient.reloadAsteriskQueueConfiguration())
        .thenReturn(
          Future.failed(SysConfdServiceException("Error when reloading queues"))
        )
      when(groupMgr.addUserToGroup(groupId, userId))
        .thenReturn(scala.util.Right(1))
      when(groupMgr.removeUserFromGroup(groupId, userId))
        .thenReturn(scala.util.Right(1))

      val rq = FakeRequest(POST, s"/api/2.0/groups/$groupId/members/$userId")
        .withSession(
          "username"     -> "bwillis",
          "isSuperAdmin" -> "true"
        )
      val res = call(controller.removeUserFromGroup(groupId, userId), rq)
      status(res) shouldEqual SERVICE_UNAVAILABLE
      verify(groupMgr).removeUserFromGroup(groupId, userId)
      verify(sysConfdClient).reloadAsteriskQueueConfiguration()
      verify(groupMgr).addUserToGroup(groupId, userId)
      verify(notifier, never).onGroupDeleted(groupId)
    }
  }
}
