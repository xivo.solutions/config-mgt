package integration

import docker.DockerDBTest
import model.{Utils, XivoCtiUserWithProfile}
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.mockito.MockitoSugar
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.test.FakeRequest
import play.api.test.Helpers._
import xivo.service.rabbitmq.XivoRabbitEventsPublisher

import java.sql.Connection

class UsersSpec extends DockerDBTest with BeforeAndAfterEach with MockitoSugar {

  class Helper() {
    val formatter: DateTimeFormatter =
      DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")

    val context: String = playConfig("play.http.context")
    val rabbitPublisher: XivoRabbitEventsPublisher =
      mock[XivoRabbitEventsPublisher]

    implicit lazy val app: Application = new GuiceApplicationBuilder()
      .configure(playConfig ++ Map(
        "db.default.url" -> xcjdbcurl.get,
        "db.xivo.url" -> xivojdbcurl.get
      ))
      .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
      .build()
    implicit val connection: Connection = xivoConnection.get
  }

  override protected def beforeEach(): Unit = {
    executeSQL(List("TRUNCATE userfeatures, users, rights CASCADE"))(xivoConnection.get)
    super.beforeEach()
  }

  "The Users Controller" should {
    "list users" in new Helper() {
      Utils.insertUser(
        XivoCtiUserWithProfile("Dupond", "Pierre", "pdupond", "admin")
      )
      Utils.insertUser(
        XivoCtiUserWithProfile("Dupont", "Jean", "jdupont", "supervisor")
      )

      val res = route(
        app,
        FakeRequest(GET, context + "/api/1.0/users")
          .withSession("username" -> "test")
      ).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(
        List(
          Json.obj(
            "name"      -> "Dupond",
            "firstname" -> "Pierre",
            "login"     -> "pdupond",
            "profile"   -> "admin"
          ),
          Json.obj(
            "name"      -> "Dupont",
            "firstname" -> "Jean",
            "login"     -> "jdupont",
            "profile"   -> "supervisor"
          )
        )
      )
    }
  }
}
