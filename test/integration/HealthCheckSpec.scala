package integration

import docker.DockerDBTest
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.mockito.MockitoSugar
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test.FakeRequest
import play.api.test.Helpers._
import xivo.service.rabbitmq.XivoRabbitEventsPublisher
import java.sql.Connection

class HealthCheckSpec extends DockerDBTest with BeforeAndAfterEach with MockitoSugar {

  class Helper() {
    val context: String = playConfig("play.http.context")
    val rabbitPublisher: XivoRabbitEventsPublisher =
      mock[XivoRabbitEventsPublisher]

    implicit lazy val app: Application = new GuiceApplicationBuilder()
      .configure(playConfig ++ Map(
        "db.default.url" -> xcjdbcurl.get,
        "db.xivo.url" -> xivojdbcurl.get,
        "db.xc.url" -> xcjdbcurl.get
      ))
      .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
      .build()

    implicit val connection: Connection = xcConnection.get
  }

  "The HealthCheck Controller" should {

    "return 204 if configmgt is running" in new Helper {
      val res = route(
        app,
        FakeRequest(GET, context + "/api/2.0/healthcheck/running")
          .withSession()
      ).get

      status(res) shouldEqual NO_CONTENT
    }
  }
}
