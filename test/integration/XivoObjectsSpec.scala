package integration

import docker.DockerDBTest
import model.Utils
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.mockito.MockitoSugar
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.test.FakeRequest
import play.api.test.Helpers._
import xivo.service.rabbitmq.XivoRabbitEventsPublisher
import play.api.Application
import java.sql.Connection

class XivoObjectsSpec extends DockerDBTest with BeforeAndAfterEach with MockitoSugar {

  class Helper() {
    val formatter: DateTimeFormatter =
      DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")

    val context: String = playConfig("play.http.context")
    val rabbitPublisher: XivoRabbitEventsPublisher =
      mock[XivoRabbitEventsPublisher]

    implicit lazy val app: Application = new GuiceApplicationBuilder()
      .configure(playConfig ++ Map(
        "db.default.url" -> xcjdbcurl.get,
        "db.xivo.url" -> xivojdbcurl.get,
        "db.xc.url" -> xcjdbcurl.get
      ))
      .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
      .build()
    implicit val connection: Connection = xivoConnection.get
  }

  override protected def beforeEach(): Unit = {
      executeSQL(List(
        "TRUNCATE userfeatures, users, rights, queuefeatures, agentgroup, incall CASCADE"
      ))(xivoConnection.get)
      super.beforeEach()
  }

  "The XivoObjects Controller" should {
    "return all queues for a superadmin" in new Helper() {
      Utils.insertQueueFeature(1, "queue1", "Queue 1")
      Utils.insertQueueFeature(2, "queue2", "Queue 2")
      val res = route(
        app,
        FakeRequest(GET, context + "/api/1.0/queues")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")
      ).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(
        List(
          Json.obj("id" -> 1, "name" -> "queue1", "displayName" -> "Queue 1"),
          Json.obj("id" -> 2, "name" -> "queue2", "displayName" -> "Queue 2")
        )
      )
    }

    "return only the user's queues for a supervisor" in new Helper() {
        val userid = Utils.insertUser("login1", "supervisor")
        Utils.insertQueueRight(userid, 2)
        Utils.insertQueueFeature(2, "thequeue")
        Utils.insertQueueFeature(3, "anotherqueue")

        val res = route(
          app,
          FakeRequest(GET, context + "/api/1.0/queues")
            .withSession("username" -> "login1")
        ).get

        status(res) shouldEqual OK
        contentAsJson(res) shouldEqual Json.toJson(
          List(Json.obj("id" -> 2, "name" -> "thequeue", "displayName" -> ""))
        )
    }

    "return all agent groups for a superadmin" in new Helper() {
        Utils.insertAgentGroup(1, "group1")
        Utils.insertAgentGroup(2, "group2")
        val res = route(
          app,
          FakeRequest(GET, context + "/api/1.0/groups")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
        ).get

        status(res) shouldEqual OK
        contentAsJson(res) shouldEqual Json.toJson(
          List(
            Json.obj("id" -> 1, "name" -> "group1"),
            Json.obj("id" -> 2, "name" -> "group2")
          )
        )
    }

    "return only the user's groups for a supervisor" in new Helper() {
        val userid = Utils.insertUser("login1", "supervisor")
        Utils.insertGroupRight(userid, 2)
        Utils.insertAgentGroup(2, "thegroup")
        Utils.insertAgentGroup(3, "anothergroup")

        val res = route(
          app,
          FakeRequest(GET, context + "/api/1.0/groups")
            .withSession("username" -> "login1")
        ).get

        status(res) shouldEqual OK
        contentAsJson(res) shouldEqual Json.toJson(
          List(Json.obj("id" -> 2, "name" -> "thegroup"))
        )
    }

    "return all xivo users" in new Helper() {
      Utils.insertXivoUser(
        1,
        "Pierre",
        "Martin",
        Some("pmartin"),
        Some(1),
        Some(1)
      )

      val res = route(
        app,
        FakeRequest(GET, context + "/api/1.0/xivo_users")
          .withSession("username" -> "test")
      ).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(
        List(
          Json.obj(
            "firstname" -> "Pierre",
            "lastname"  -> "Martin",
            "login"     -> "pmartin"
          )
        )
      )
    }

    "return all incalls for a superadmin" in new Helper() {
      Utils.insertIncall(1, "5301")
      Utils.insertIncall(2, "5302")
      val res = route(
        app,
        FakeRequest(GET, context + "/api/1.0/incalls")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")
      ).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(
        List(
          Json.obj("id" -> 1, "number" -> "5301"),
          Json.obj("id" -> 2, "number" -> "5302")
        )
      )
    }

    "return only the user's incalls for a supervisor" in new Helper() {
        val userid = Utils.insertUser("login1", "supervisor")
        Utils.insertIncallRight(userid, 2)
        Utils.insertIncall(2, "5003")
        Utils.insertIncall(3, "5004")

        val res = route(
          app,
          FakeRequest(GET, context + "/api/1.0/incalls")
            .withSession("username" -> "login1")
        ).get

        status(res) shouldEqual OK
        contentAsJson(res) shouldEqual Json.toJson(
          List(Json.obj("id" -> 2, "number" -> "5003"))
        )
    }
  }
}
