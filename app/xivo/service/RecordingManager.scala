package xivo.service

import java.util.NoSuchElementException

import anorm.AnormException
import javax.inject.Inject
import model._
import xivo.model.QueueFeature

import scala.util.{Failure, Success, Try}
import xivo.model.rabbitmq._
import xivo.service.rabbitmq.XivoRabbitEventsPublisher

class RecordingManager @Inject() (
    queueFeatureManager: QueueFeatureManager,
    rabbitPublisher: XivoRabbitEventsPublisher
) {

  def handleError[T](failure: Failure[T]): Try[T] = {
    failure match {
      case Failure(f: AnormException) =>
        Failure(new NoSuchElementException(f.getMessage()))
      case Failure(f) => Failure(f)
    }
  }

  def handleConfigSuccess(
      queueFeature: Success[QueueFeature]
  ): Try[RecordingConfig] = {
    queueFeature match {
      case Success(qf) =>
        Success(RecordingConfig(qf.recording_activated == 1, qf.recording_mode))
    }
  }

  def modeToApply(qf: QueueFeature): RecordingModeType.Type = {
    if (qf.recording_activated == 1) qf.recording_mode
    else RecordingModeType.NotRecorded
  }

  def filterNotRecorded(qf: QueueFeature): Boolean = {
    !qf.recording_mode.equals(RecordingModeType.NotRecorded)
  }

  def existNotRecorded(
      recQMap: Map[Option[Long], RecordingModeType.Type]
  ): Boolean = {
    if (recQMap.isEmpty) false
    else !recQMap.values.exists(_.equals(RecordingModeType.NotRecorded))
  }

  def isRecEnabled(activated: Boolean): Int = {
    if (activated) 1 else 0
  }

  def computeRecordingModeToApply(queueId: Long): Try[RecordingMode] = {
    queueFeatureManager.get(queueId) match {
      case Success(qf) => Success(RecordingMode(modeToApply(qf)))
      case Failure(f)  => handleError(Failure(f))
    }
  }

  def getRecordingQueuesStatus: Try[RecordingQueuesStatus] = {
    queueFeatureManager.getQueueRecorded match {
      case Success(qfl) =>
        val recQMap = qfl
          .map(qf => qf.id -> modeToApply(qf))
          .toMap

        Success(RecordingQueuesStatus(all = existNotRecorded(recQMap), recQMap))

      case Failure(f) => handleError(Failure(f))
    }
  }

  def setRecordingQueuesStatus(
      activate: Boolean
  ): Try[RecordingQueuesStatusPublish] = {
    queueFeatureManager.toggleQueueRecordingStatus(
      isRecEnabled(activate)
    ) match {
      case Success(value) =>
        value.map(queue => {
          rabbitPublisher.publish(queue, ObjectEdited)
          queue.id -> modeToApply(queue)
        })
        Success(
          RecordingQueuesStatusPublish(
            all = activate,
            value.map(qf => qf.id -> modeToApply(qf)).toMap
          )
        )
      case Failure(f) => handleError(Failure(f))
    }
  }

  def getRecordingConfig(queueId: Long): Try[RecordingConfig] = {
    queueFeatureManager.get(queueId) match {
      case Success(qf) => handleConfigSuccess(Success(qf))
      case Failure(f)  => handleError(Failure(f))
    }
  }

  def setRecordingConfig(
      queueId: Long,
      recordingConfig: RecordingConfig
  ): Try[RecordingConfig] = {
    queueFeatureManager
      .get(queueId)
      .map(qf => {
        queueFeatureManager.update(
          qf.copy(
            recording_activated = isRecEnabled(recordingConfig.activated),
            recording_mode = recordingConfig.mode
          )
        )
      })
      .flatMap {
        case Success(qf) =>
          rabbitPublisher.publish(qf, ObjectEdited)
          handleConfigSuccess(Success(qf))
        case Failure(f) => handleError(Failure(f))
      }
  }
}
