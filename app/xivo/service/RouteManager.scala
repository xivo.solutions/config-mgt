package xivo.service

import anorm.SqlParser._
import anorm.{Macro, RowParser, SQL, _}
import javax.inject.Inject
import play.api.db.{Database, NamedDatabase}
import common.refined.anorm._
import xivo.model._

import scala.util.{Failure, Success, Try}

class RouteManager @Inject() (
    mediaMgr: MediaServerManager,
    @NamedDatabase("xivo") dbXivo: Database
) {

  def getContextAll: Try[List[Context]] =
    dbXivo.withConnection { implicit c =>
      val simple: RowParser[Context] = Macro.namedParser[Context]
      Try({
        SQL(s"""SELECT
                 name
          FROM
                 context
          WHERE
              contexttype = 'internal'
            OR
              contexttype = 'outcall';

          """)
          .as(simple.*)
      })
    }

  def getScheduleAll: Try[List[Schedule]] =
    dbXivo.withConnection { implicit c =>
      val simple: RowParser[Schedule] = Macro.namedParser[Schedule]
      Try({
        SQL(s"""SELECT
                 id, name
          FROM
                 schedule""")
          .as(simple.*)
      })
    }

  def getRightAll: Try[List[Right]] =
    dbXivo.withConnection { implicit c =>
      val simple: RowParser[Right] = Macro.namedParser[Right]
      Try({
        SQL(s"""SELECT
                 id, name
          FROM
                 rightcall""")
          .as(simple.*)
      })
    }

  def getSipTrunk(id: Long): Try[Option[TrunkSip]] =
    dbXivo.withConnection { implicit c =>
      val simple: RowParser[TrunkSip] = Macro.namedParser[TrunkSip]
      Try({
        SQL(s"""SELECT
                 id, name
          FROM
                 usersip
          WHERE
                 id = {id}
            AND
                  category = 'trunk'""")
          .on("id" -> id)
          .as(simple.singleOpt)
      })
    }

  def getCustomTrunk(id: Long): Try[Option[TrunkCustom]] =
    dbXivo.withConnection { implicit c =>
      val simple: RowParser[TrunkCustom] = Macro.namedParser[TrunkCustom]
      Try({
        SQL(s"""SELECT
                 id, interface, intfsuffix
          FROM
                 usercustom
          WHERE
                 id = {id}
            AND
                  category = 'trunk'""")
          .on("id" -> id)
          .as(simple.singleOpt)
      })
    }

  def getTrunkAll: Try[List[Trunk]] =
    dbXivo.withConnection { implicit c =>
      val simple: RowParser[TrunkFeature] = Macro.namedParser[TrunkFeature]
      Try({
        SQL(s"""SELECT
                 trunkfeatures.id AS id, trunkfeatures.protocol AS protocol, trunkfeatures.protocolid AS protocolId
          FROM
                 trunkfeatures
          WHERE
                 trunkfeatures.protocol = 'sip'
            OR
                 trunkfeatures.protocol = 'custom'""")
          .as(simple.*)
          .map { tf =>
            tf.protocol match {
              case "sip"    => (getSipTrunk(tf.protocolId), tf.id)
              case "custom" => (getCustomTrunk(tf.protocolId), tf.id)
              case _ =>
                (
                  Failure(
                    new Exception(
                      s"Unexpected trunk type ${tf.protocol} with id ${tf.id}"
                    )
                  ),
                  tf.id
                )
            }
          }
          .flatMap {
            case (Success(t), tfId) => t.map(_.withTrueId(tfId))
            case (Failure(e), _)    => throw new Exception(e)
          }
      })
    }

  def getAll: Try[List[PlainRoute]] =
    dbXivo.withConnection { implicit c =>
      val simple: RowParser[RawRoute] = Macro.namedParser[RawRoute]
      Try({
        SQL(
          s"""SELECT
                 route.id,
                 routepattern.pattern AS pattern, routepattern.regexp, routepattern.target, routepattern.callerid AS callerid,
                 route.subroutine, route.description, route.priority, routecontext.contextname, routemediaserver.mdsid,
                 routetrunk.trunkid, routetrunk.priority as trunkpriority, schedule_path.schedule_id as schedule, rightcallmember.rightcallid AS right,
                 route.internal
          FROM
                 route
          LEFT JOIN
                 routepattern ON route.id = routepattern.routeid
          LEFT JOIN
                 routecontext ON route.id = routecontext.routeid
          LEFT JOIN
                 routemediaserver ON route.id = routemediaserver.routeid
          LEFT JOIN
                 routetrunk ON route.id = routetrunk.routeid
          LEFT JOIN
                 schedule_path ON route.id = schedule_path.pathid AND schedule_path.path='route'
          LEFT JOIN
                  rightcallmember ON route.id = CAST(rightcallmember.typeval AS integer) AND rightcallmember.type='route'
          ORDER BY
                  route.id, routepattern.id"""
        )
          .as(simple.*)
          .groupBy(r =>
            (r.id, r.subroutine, r.description, r.priority, r.internal)
          )
          .view
          .mapValues(r =>
            GrouppedRawRoute(
              r.flatMap(_.contextname),
              r.flatMap(_.mdsid),
              r.flatMap(r => Map(r.trunkid -> r.trunkpriority)),
              r.flatMap(_.schedule),
              r.flatMap(_.right),
              r.map(p =>
                DialPattern(
                  p.pattern.getOrElse(""),
                  p.target,
                  p.regexp,
                  p.callerid
                )
              )
            )
          )
          .map(r =>
            PlainRoute(
              r._1._1,
              r._2.patterns.distinct,
              r._1._2,
              r._1._3,
              r._1._4,
              r._2.context,
              r._2.mdsId,
              r._2.trunkWithPriority
                .collect { case (Some(id), Some(priority)) =>
                  SortedTrunkIds(id, priority)
                }
                .distinct
                .sortBy(_.priority),
              r._2.schedule,
              r._2.right,
              r._1._5
            )
          )
          .toList
          .sortBy(_.priority)
      })
    }

  def get(id: Long): Try[PlainRoute] =
    dbXivo.withConnection { implicit c =>
      val simple: RowParser[RawRoute] = Macro.namedParser[RawRoute]
      Try({
        SQL(s"""SELECT
                 route.id,
                 routepattern.pattern AS pattern, routepattern.regexp, routepattern.target, routepattern.callerid AS callerid,
                 route.subroutine, route.description, route.priority, routecontext.contextname,
                 routemediaserver.mdsid, routetrunk.trunkid, routetrunk.priority as trunkpriority,
                 schedule_path.schedule_id as schedule, rightcallmember.rightcallid as right,
                 route.internal
          FROM
                 route
          LEFT JOIN
                 routepattern ON route.id = routepattern.routeid
          LEFT JOIN
                 routecontext ON route.id = routecontext.routeid
          LEFT JOIN
                 routemediaserver ON route.id = routemediaserver.routeid
          LEFT JOIN
                 routetrunk ON route.id = routetrunk.routeid
          LEFT JOIN
                 schedule_path ON route.id = schedule_path.pathid AND schedule_path.path='route'
          LEFT JOIN
                 rightcallmember ON route.id = CAST(rightcallmember.typeval AS integer) AND rightcallmember.type='route'
          WHERE
                 route.id={id}
          ORDER BY 
                 routepattern.id""")
          .on("id" -> id)
          .as(simple.*)
          .groupBy(r =>
            (r.id, r.subroutine, r.description, r.priority, r.internal)
          )
          .view
          .mapValues(r =>
            GrouppedRawRoute(
              r.flatMap(_.contextname),
              r.flatMap(_.mdsid),
              r.flatMap(r => Map(r.trunkid -> r.trunkpriority)),
              r.flatMap(_.schedule),
              r.flatMap(_.right),
              r.map(p =>
                DialPattern(
                  p.pattern.getOrElse(""),
                  p.target,
                  p.regexp,
                  p.callerid
                )
              )
            )
          )
          .map(r =>
            PlainRoute(
              r._1._1,
              r._2.patterns.distinct,
              r._1._2,
              r._1._3,
              r._1._4,
              r._2.context,
              r._2.mdsId,
              r._2.trunkWithPriority
                .collect { case (Some(tId), Some(priority)) =>
                  SortedTrunkIds(tId, priority)
                }
                .distinct
                .sortBy(_.priority),
              r._2.schedule,
              r._2.right,
              r._1._5
            )
          )
          .head
      })
    }

  def getLastPriority: Try[Long] =
    dbXivo.withConnection { implicit c =>
      val simple = SqlParser.get[Option[Long]]("priority") map {
        case Some(priority) => priority + 1
        case None           => 1
      }
      Try({
        SQL(s"""SELECT MAX(priority) AS priority
          FROM
                 route""")
          .as(simple.single)
      })
    }

  def createRoute(route: RichRouteNoId): Try[RichRoute] =
    dbXivo.withConnection { implicit c =>
      val inserted: Try[Long] = Try({
        SQL(
          s"""INSERT INTO
                 route (subroutine, description, priority, internal)
          VALUES
                 ({subroutine}, {description}, {priority}, {internal})"""
        )
          .on(
            Symbol("subroutine")  -> route.subroutine,
            Symbol("description") -> route.description,
            Symbol("priority")    -> route.priority,
            Symbol("internal")    -> route.internal
          )
          .executeInsert(scalar[Long].single)
      })
      inserted match {
        case Success(r) => Success(route.withId(r))
        case Failure(f) => Failure(new Exception(s"Cannot create route, $f "))
      }
    }

  def createRoutePattern(
      routeId: Long,
      pattern: DialPattern
  ): Try[Option[Long]] =
    dbXivo.withConnection { implicit c =>
      Try({
        SQL(
          s"""INSERT INTO
                 routepattern (routeid, pattern, regexp, target, callerid)
          VALUES
                 ({routeid}, {pattern}, {regexp}, {target}, {callerid})"""
        )
          .on(
            Symbol("routeid")  -> routeId,
            Symbol("pattern")  -> pattern.pattern,
            Symbol("regexp")   -> pattern.regexp,
            Symbol("target")   -> pattern.target,
            Symbol("callerid") -> pattern.callerid
          )
          .executeInsert()
      })
    }

  def createRouteContext(
      routeId: Long,
      contextName: String
  ): Try[Option[Long]] =
    dbXivo.withConnection { implicit c =>
      Try({
        SQL(
          s"""INSERT INTO
                 routecontext (routeid, contextname)
          VALUES
                 ({routeid}, {contextname})"""
        )
          .on("routeid" -> routeId, "contextname" -> contextName)
          .executeInsert()
      })
    }

  def createRouteMediaserver(routeId: Long, mdsId: Long): Try[Option[Long]] =
    dbXivo.withConnection { implicit c =>
      Try({
        SQL(
          s"""INSERT INTO
                 routemediaserver
          VALUES
                 ({routeid}, {mdsid})"""
        )
          .on("routeid" -> routeId, "mdsid" -> mdsId)
          .executeInsert()
      })
    }

  def createRouteTrunk(
      routeId: Long,
      trunkId: Long,
      priority: Long
  ): Try[Option[Long]] =
    dbXivo.withConnection { implicit c =>
      Try({
        SQL(
          s"""INSERT INTO
                 routetrunk
          VALUES
                 ({routeid}, {trunkid}, {priority})"""
        )
          .on(
            "routeid"  -> routeId,
            "trunkid"  -> trunkId,
            "priority" -> priority
          )
          .executeInsert()
      })
    }

  def createRouteSchedule(routeId: Long, scheduleId: Long): Try[Option[Long]] =
    dbXivo.withConnection { implicit c =>
      Try({
        SQL(
          s"""INSERT INTO
                 schedule_path
          VALUES
                 ({scheduleid}, 'route', {routeid}, 0)"""
        )
          .on("scheduleid" -> scheduleId, "routeid" -> routeId)
          .executeInsert()
      })
    }

  def createRouteRight(routeId: Long, rightId: Long): Try[Option[Long]] =
    dbXivo.withConnection { implicit c =>
      Try({
        SQL(
          s"""INSERT INTO
                 rightcallmember(rightcallid, type, typeval)
          VALUES
                 ({rightid}, 'route', {routeid})"""
        )
          .on("rightid" -> rightId, "routeid" -> routeId)
          .executeInsert()
      })
    }

  def delete(id: Long): Try[PlainRoute] =
    dbXivo.withConnection { implicit c =>
      val toDelete = get(id)
      if (toDelete.isFailure) {
        toDelete
      } else {
        val deletedCount =
          SQL("DELETE FROM route WHERE id={id}").on("id" -> id).executeUpdate()
        if (deletedCount == 1) {
          toDelete
        } else {
          Failure(new Exception(s"Cannot delete route with id $id"))
        }
      }
    }

  def deleteRoutePattern(routeId: Long): Try[Long] =
    dbXivo.withConnection { implicit c =>
      Try({
        SQL(
          s"""DELETE FROM
                 routepattern
          WHERE
                 routeid={id}"""
        )
          .on("id" -> routeId)
          .executeUpdate()
          .toLong
      })
    }

  def deleteRouteContext(routeId: Long): Try[Long] =
    dbXivo.withConnection { implicit c =>
      Try({
        SQL(
          s"""DELETE FROM
                 routecontext
          WHERE
                 routeid={id}"""
        )
          .on("id" -> routeId)
          .executeUpdate()
          .toLong
      })
    }

  def deleteRouteMediaserver(routeId: Long): Try[Long] =
    dbXivo.withConnection { implicit c =>
      Try({
        SQL(
          s"""DELETE FROM
                 routemediaserver
          WHERE
                 routeid={id}"""
        )
          .on("id" -> routeId)
          .executeUpdate()
          .toLong
      })
    }

  def deleteRouteTrunk(routeId: Long): Try[Long] =
    dbXivo.withConnection { implicit c =>
      Try({
        SQL(
          s"""DELETE FROM
                 routetrunk
          WHERE
                 routeid={id}"""
        )
          .on("id" -> routeId)
          .executeUpdate()
          .toLong
      })
    }

  def deleteRouteSchedule(routeId: Long): Try[Long] =
    dbXivo.withConnection { implicit c =>
      Try({
        SQL(
          s"""DELETE FROM
                 schedule_path
          WHERE
                 pathid={id}"""
        )
          .on("id" -> routeId)
          .executeUpdate()
          .toLong
      })
    }

  def deleteRouteRight(routeId: Long): Try[Long] =
    dbXivo.withConnection { implicit c =>
      Try({
        SQL(
          s"""DELETE FROM
                 rightcallmember
          WHERE
                 typeval={id}::varchar"""
        )
          .on("id" -> routeId)
          .executeUpdate()
          .toLong
      })
    }
}
