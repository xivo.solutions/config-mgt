package xivo.service

import anorm.{BatchSql, NamedParameter, SQL}
import com.google.inject.Inject
import play.api.db.{Database, NamedDatabase}
import xivo.model.DialAction.DialActionMap
import xivo.model.{DatabaseError, DialAction, DialActionAction, DialActionError}

import java.sql.Connection
import scala.util.{Failure, Right, Success, Try}

class DialActionManager @Inject() (@NamedDatabase("xivo") dbXivo: Database) {

  def getAllDialActions(category: String): Either[
    DialActionError,
    Map[Long, DialActionMap]
  ] =
    dbXivo.withConnection { implicit c =>
      for {
        dialActionsList <- getAllDialActionsFromDatabase(category)
        dialActionsMap = dialActionsListToDialActionsMap(dialActionsList)
      } yield dialActionsMap
    }

  def getDialActions(id: Long, category: String): Either[
    DialActionError,
    DialActionMap
  ] =
    dbXivo.withConnection { implicit c =>
      for {
        dialActionsList <- getDialActionsFromDatabase(id, category)
        dialActionsMap = dialActionsListToDialActionsMap(dialActionsList)
          .getOrElse(id, Map.empty)
      } yield dialActionsMap
    }

  def createDialActions(
      id: Long,
      category: String,
      dialActions: DialActionMap
  ): Either[
    DialActionError,
    DialActionMap
  ] =
    dbXivo.withConnection { implicit c =>
      val dialActionsList =
        dialActionsMapToDialActionsList(id, dialActions)
      if (dialActionsList.isEmpty) {
        Right(Map.empty)
      } else {
        for {
          _ <- insertDialActionIntoDatabase(
            id,
            category,
            dialActionsList
          )
          dialactions <- getDialActions(id, category)
        } yield dialactions
      }
    }

  def deleteDialActions(
      id: Long,
      category: String
  ): Either[DialActionError, Long] =
    dbXivo.withConnection { implicit c =>
      deleteDialActionsFromDatabase(id, category)
    }

  def updateDialActions(
      id: Long,
      category: String,
      dialActions: DialActionMap
  ): Either[DialActionError, DialActionMap] = {
    dbXivo.withConnection { implicit c =>
      val dialActionsList = dialActionsMapToDialActionsList(id, dialActions)
      for {
        _ <- updateDialActionsInDatabase(
          id,
          category,
          dialActionsList
        )
        dialActions <- getDialActions(id, category)
      } yield dialActions
    }
  }

  private def getAllDialActionsFromDatabase(
      category: String
  )(implicit c: Connection): Either[DialActionError, List[DialAction]] =
    Try(
      SQL(s"""
           | SELECT categoryval, event, action, actionarg1, actionarg2
           | FROM dialaction
           | WHERE category = {category}::dialaction_category
           | ORDER BY categoryval
           |""".stripMargin)
        .on("category" -> category)
        .as(DialAction.dialActionMapping.*)
    ).toEither.left.map(f =>
      DialActionError(
        DatabaseError,
        s"Error when retrieving dial actions for ${category}s in database: ${f.getMessage}"
      )
    )

  private def getDialActionsFromDatabase(
      id: Long,
      category: String
  )(implicit c: Connection): Either[DialActionError, List[DialAction]] =
    Try(
      SQL(s"""
           | SELECT categoryval, event, action, actionarg1, actionarg2
           | FROM dialaction
           | WHERE category = {category}::dialaction_category AND categoryval = {id}
           |""".stripMargin)
        .on("id" -> id.toString, "category" -> category)
        .as(DialAction.dialActionMapping.*)
    ).toEither.left.map(f =>
      DialActionError(
        DatabaseError,
        s"Error when retrieving dial actions for $category $id in database: ${f.getMessage}"
      )
    )

  private def insertDialActionIntoDatabase(
      id: Long,
      category: String,
      dialActionsList: List[DialAction]
  )(implicit c: Connection): Either[DialActionError, List[Int]] = {
    val dialActionsToSql = dialActionsList
      .map(dialAction =>
        Seq[NamedParameter](
          "event"      -> dialAction.event.toDatabase,
          "id"         -> id.toString,
          "category"   -> category,
          "action"     -> dialAction.action.toDatabase,
          "actionArg1" -> dialAction.actionArg1,
          "actionArg2" -> dialAction.actionArg2
        )
      )
    Try(
      BatchSql(
        s"""
           | INSERT INTO dialaction (event, category, categoryval,
           | action, actionarg1, actionarg2, linked)
           | VALUES ({event}::dialaction_event, {category}::dialaction_category, {id},
           | {action}::dialaction_action, {actionArg1}, {actionArg2}, 1)
           |""".stripMargin,
        dialActionsToSql.head,
        dialActionsToSql.tail: _*
      )
        .execute()
    ) match {
      case Success(dialActionsIndices) =>
        Right(dialActionsIndices.toList)
      case Failure(f) =>
        Left(
          DialActionError(
            DatabaseError,
            s"Error when inserting dial actions for $category $id in database: ${f.getMessage}"
          )
        )
    }
  }

  private def deleteDialActionsFromDatabase(
      id: Long,
      category: String
  )(implicit c: Connection): Either[DialActionError, Long] = {
    dbXivo.withConnection { implicit c =>
      Try(
        SQL(s"""
             | DELETE FROM dialaction
             | WHERE category = {category}::dialaction_category AND categoryval = {id}
             |""".stripMargin)
          .on("id" -> id.toString, "category" -> category)
          .executeUpdate()
          .toLong
      ).toEither.left.map(f =>
        DialActionError(
          DatabaseError,
          s"Error when deleting dial actions for $category $id in database: ${f.getMessage}"
        )
      )
    }
  }

  private def updateDialActionsInDatabase(
      id: Long,
      category: String,
      dialActionsList: List[DialAction]
  )(implicit c: Connection): Either[DialActionError, List[Int]] = {
    dbXivo.withConnection { implicit c =>
      val dialActionsToSql = dialActionsList
        .map(dialAction =>
          Seq[NamedParameter](
            "event"      -> dialAction.event.toDatabase,
            "id"         -> id.toString,
            "category"   -> category,
            "action"     -> dialAction.action.toDatabase,
            "actionArg1" -> dialAction.actionArg1,
            "actionArg2" -> dialAction.actionArg2
          )
        )
      Try(
        BatchSql(
          s"""
             | UPDATE dialaction
             | SET action = {action}::dialaction_action, actionarg1 = {actionArg1}, actionarg2 = {actionArg2}
             | WHERE category = {category}::dialaction_category and categoryval = {id} and event = {event}::dialaction_event
             |""".stripMargin,
          dialActionsToSql.head,
          dialActionsToSql.tail: _*
        )
          .execute()
      ) match {
        case Success(dialActionsIndices) =>
          Right(dialActionsIndices.toList)
        case Failure(f) =>
          Left(
            DialActionError(
              DatabaseError,
              s"Error when updating dial actions for $category $id in database: ${f.getMessage}"
            )
          )
      }
    }
  }

  private def dialActionsMapToDialActionsList(
      id: Long,
      dialActions: DialActionMap
  ): List[DialAction] =
    dialActions.map { case (event, action) =>
      DialAction(id, event, action.action, action.actionArg1, action.actionArg2)
    }.toList

  private def dialActionsListToDialActionsMap(
      dialActions: List[DialAction]
  ): Map[Long, DialActionMap] = {
    dialActions
      .groupBy(_.categoryId)
      .view
      .mapValues(
        _.groupBy(_.event).view
          .mapValues(dAList => {
            val dA = dAList.head
            DialActionAction(dA.action, dA.actionArg1, dA.actionArg2)
          })
          .toMap
      )
      .toMap
  }
}
