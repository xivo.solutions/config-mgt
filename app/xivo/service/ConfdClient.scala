package xivo.service

import com.google.inject.Inject
import play.api.libs.ws._
import play.api.{Configuration, Logger}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import ws.model.Line

case class ConfdServiceException(message: String)
    extends Exception(message: String)

class ConfdClient @Inject() (configuration: Configuration, ws: WSClient)(
    implicit ec: ExecutionContext
) {

  val log = Logger(getClass.getName)

  val xivoHost: String = configuration.get[String]("xivohost")
  val confdToken: String =
    configuration.get[String]("authentication.confd.restapi.token")
  val confdPort: Int =
    configuration.getOptional[Int]("confdport").getOrElse(9486)
  val confdTimeout: Int =
    configuration.getOptional[Int]("confdtimeout").getOrElse(5000)

  val baseUri = s"https://${xivoHost}:${confdPort}/1.1"

  def request(uri: String) = {
    log.debug(s"Request to ${uri}")
    ws.url(uri)
      .withHttpHeaders(
        ("Content-Type", "application/json"),
        ("Accept", "application/json"),
        ("X-Auth-Token", confdToken)
      )
      .withRequestTimeout(confdTimeout.millis)
  }

  def deviceGet(deviceid: Line.Device): Future[WSResponse] =
    request(s"${baseUri}/devices/${deviceid}")
      .withMethod("GET")
      .execute()
      .map(response => {
        checkError(response)
        log.debug(s"Request successful with response status ${response.status}")
        response
      })

  def deviceUpdate(deviceid: Line.Device): Future[WSResponse] =
    request(s"${baseUri}/devices/${deviceid}/update")
      .withMethod("GET")
      .execute()
      .map(response => {
        checkError(response)
        log.debug(s"Request successful with response status ${response.status}")
        response
      })

  private def checkError(response: WSResponse): Unit = {
    if (response.status >= 400) {
      log.error(
        s"""Confd request failed with code ${response.status} and message "${response.body}""""
      )
      throw ConfdServiceException(
        s"""Confd request failed with code ${response.status} and message "${response.body}""""
      )
    }
  }
}
