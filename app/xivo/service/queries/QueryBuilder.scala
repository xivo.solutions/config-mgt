package xivo.service.queries

import anorm.{Row, RowParser, SQL, SimpleSql}
import model.{AllFieldsString, DynamicFilter, LowerCasing, OperatorContainsAll}

trait QueryBuilder[T] {
  val defaultSelect: String
  val defaultOrder: String
  val defaultGroup: String
  val selectColumns: Map[String, String]
  val havingColumns: Map[String, String] = Map()
  val toCastColumns: List[String]        = List()

  val parser: RowParser[T]

  def columnsIterator(): String =
    selectColumns.map { case (v, k) => k + " as " + v }.mkString(",\n")

  def buildQuery(
      selectClause: String = defaultSelect,
      filters: List[DynamicFilter] = List.empty,
      group: String = defaultGroup,
      paging: Option[String] = None,
      order: String = defaultOrder
  ): SimpleSql[Row] = {
    if (filters.isEmpty)
      SQL(s"$selectClause $group ${paging.getOrElse(order)}").asSimple()
    else {
      val where: String =
        DynamicFilter.toSqlWhereCondition(filters)(LowerCasing)
      val having: String =
        DynamicFilter.toSqlHavingCondition(filters)(LowerCasing)
      SQL(
        s"$selectClause $where $group $having ${paging.getOrElse(order)}"
      )
        .on(DynamicFilter.toNamedParameterList(filters)(AllFieldsString): _*)
    }
  }

  def refineFilters(filters: List[DynamicFilter]): List[DynamicFilter] =
    filters.map(f => {
      if (f.operator.contains(OperatorContainsAll)) {
        f.copy(field = havingColumns.getOrElse(f.field, f.field))
      } else if (toCastColumns.contains(f.field)) {
        f.copy(field = s"${selectColumns.getOrElse(f.field, f.field)}::text")
      } else
        f.copy(field = selectColumns.getOrElse(f.field, f.field))
    })
}
