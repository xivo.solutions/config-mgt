package xivo.service

import anorm.SqlParser._
import anorm._
import com.google.inject.ImplementedBy
import javax.inject.Inject
import play.api.db.{Database, _}
import xivo.model.{Agent, QueueCategory, QueueMember, QueueMemberUserType}

import scala.util.Try

@ImplementedBy(classOf[AgentConfigManagerImp])
trait AgentConfigManager {
  def getById(id: Long): Try[Agent]
  def all(): Try[List[Agent]]
}

class AgentConfigManagerImp @Inject() (
    @NamedDatabase("xivo") dbXivo: Database
) extends AgentConfigManager {

  val simple: RowParser[Agent] =
    get[Long]("agentid") ~
      get[String]("firstname") ~
      get[String]("lastname") ~
      get[String]("number") ~
      get[String]("context") ~
      get[Long]("numgroup") ~
      get[Option[Long]]("userid") map {
        case agentId ~ firstname ~ lastname ~ number ~ context ~ numgroup ~ userid =>
          Agent(
            agentId,
            firstname,
            lastname,
            number,
            context,
            member = getQueueMember(agentId),
            numgroup,
            userid
          )
      }

  def simpleQueueMember: RowParser[QueueMember] = {
    get[String]("queuename") ~
      get[Long]("queueid") ~
      get[Int]("penalty") ~
      get[Int]("commented") ~
      get[String]("interface") ~
      get[QueueMemberUserType.Type]("usertype") ~
      get[Long]("userid") ~
      get[String]("channel") ~
      get[Int]("position") ~
      get[QueueCategory.Type]("category") map {
        case queueName ~ queueId ~ penalty ~ commented ~ interface ~ userType ~ userId ~ channel ~ position ~ category =>
          QueueMember(
            queueName,
            queueId,
            interface,
            penalty,
            commented,
            userType,
            userId,
            channel,
            category,
            position
          )
      }
  }

  private def getQueueMember(agentId: Long): List[QueueMember] =
    dbXivo.withConnection { implicit c =>
      SQL(s"""SELECT
                 queuefeatures.id AS queueid, queuemember.queue_name AS queuename, penalty,
                 commented, interface, usertype, userid, channel, position, category
          FROM
                 queuemember, queuefeatures
          WHERE
                 queuemember.usertype='agent'
          AND
                 queuefeatures.name = queuemember.queue_name
          AND
                 queuemember.userid = {agentId}

     """.stripMargin)
        .on(Symbol("agentId") -> agentId)
        .as(simpleQueueMember.*)
    }

  def getById(id: Long): Try[Agent] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL(s"""SELECT
                 agentfeatures.id AS agentid, agentfeatures.firstname, agentfeatures.lastname,
                 agentfeatures.number AS number, agentfeatures.context AS context, agentfeatures.numgroup,
                 userfeatures.id as userid
          FROM
                 agentfeatures
          LEFT JOIN
                 userfeatures
          ON
                 userfeatures.agentid = agentfeatures.id
          WHERE
                 agentfeatures.id = {id}
          ORDER BY
                 userfeatures.id
          LIMIT  1
      """.stripMargin)
          .on(Symbol("id") -> id)
          .as(simple.single)
      )
    }

  def all(): Try[List[Agent]] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL(s"""SELECT
                 agentfeatures.id AS agentid, agentfeatures.firstname, agentfeatures.lastname,
                 agentfeatures.number AS number, agentfeatures.context AS context, agentfeatures.numgroup, userfeatures.id as userid
          FROM
                 agentfeatures
          LEFT JOIN
                userfeatures
          ON
                userfeatures.agentid = agentfeatures.id
      """.stripMargin)
          .as(simple.*)
      )
    }
}
