package xivo.service.rabbitmq

import com.rabbitmq.client.{AMQP, Channel, Connection, ConnectionFactory}
import javax.inject.Inject
import play.api.{Configuration, Logger}

class XivoRabbitEventsFactory @Inject() (
    configuration: Configuration,
    xivoEventsBusConfiguration: ConnectionFactory = new ConnectionFactory
) {
  val log = Logger(getClass.getName)

  val rabbitUsername: String = configuration.get[String]("RabbitMQ.username")
  val rabbitPassword: String = configuration.get[String]("RabbitMQ.password")
  val rabbitVirtualHost: String =
    configuration.get[String]("RabbitMQ.virtualhost")
  val rabbitHost: String   = configuration.get[String]("RabbitMQ.xivohost")
  val rabbitPort: Int      = configuration.get[Int]("RabbitMQ.xivoport")
  val exchangeName: String = configuration.get[String]("RabbitMQ.exchange")
  val queueName: String    = configuration.get[String]("RabbitMQ.queue")
  val routingKeys: Seq[String] =
    configuration.get[Seq[String]]("RabbitMQ.routingkeys")
  val consumerTag: String = configuration.get[String]("RabbitMQ.consumertag")

  val publishProps: AMQP.BasicProperties = new AMQP.BasicProperties.Builder()
    .contentType("application/json")
    .deliveryMode(2) // persistent(2) vs nonpersistent (1)
    .priority(1)
    .expiration("10000")
    .userId(rabbitUsername)
    .build()

  xivoEventsBusConfiguration.setUsername(rabbitUsername)
  xivoEventsBusConfiguration.setPassword(rabbitPassword)
  xivoEventsBusConfiguration.setVirtualHost(rabbitVirtualHost)
  xivoEventsBusConfiguration.setHost(rabbitHost)
  xivoEventsBusConfiguration.setPort(rabbitPort)

  val xivoEventsConnection: Connection =
    xivoEventsBusConfiguration.newConnection
  val channel: Channel = xivoEventsConnection.createChannel

  def shutdownConnection(): Unit = {
    channel.close()
    xivoEventsConnection.close()
  }
}
