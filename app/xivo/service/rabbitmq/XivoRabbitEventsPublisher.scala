package xivo.service.rabbitmq

import com.rabbitmq.client._
import javax.inject.Inject
import play.api.{Configuration, Logger}
import play.api.libs.json.Json
import xivo.model.rabbitmq._
import xivo.service.XivoManager
import ToRabbitMessage.ops._

class XivoRabbitEventsPublisher @Inject() (
    configuration: Configuration,
    factory: XivoRabbitEventsFactory,
    xivoManager: XivoManager
) {

  val log                                = Logger(getClass.getName)
  val channel: Channel                   = factory.channel
  val exchangeName: String               = factory.exchangeName
  val username: String                   = factory.rabbitUsername
  val publishProps: AMQP.BasicProperties = factory.publishProps
  val xivo                               = xivoManager.get()

  def publish[A: ToRabbitMessage](a: A, updateMode: ObjectUpdateMode): Unit = {
    val msg = a.toRabbitMessage(updateMode)
    publish(msg)
  }

  def publish(msg: BaseRabbitMessage): Unit =
    xivo.foreach { implicit x =>
      val encodedMessage: Array[Byte] = Json.toJson(msg).toString.getBytes()
      channel.basicPublish(
        exchangeName,
        msg.getRabbitMessageType.routingKey,
        publishProps,
        encodedMessage
      )
    }
}
