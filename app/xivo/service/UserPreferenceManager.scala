package xivo.service

import anorm.SqlParser.{long, scalar, str}
import anorm.{RowParser, SQL}
import com.google.inject.Inject
import play.api.db.{Database, NamedDatabase}

import scala.util.{Failure, Success, Try}

class UserPreferenceManager @Inject() (
    @NamedDatabase("xivo") dbXivo: Database,
    notifier: UserPreferenceNotifier
) {

  val userPreferenceParser: RowParser[UserPreference] = for {
    userId    <- long("user_id")
    key       <- str("key")
    value     <- str("value")
    valueType <- str("value_type")
  } yield UserPreference(userId, key, value, valueType)

  def getAll(userId: Long): Try[UserPreferences] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL("""
         SELECT up.user_id, up.key, up.value, up.value_type
         FROM userpreferences up
         WHERE up.user_id={userId}
      """)
          .on("userId" -> userId)
          .as(userPreferenceParser.*)
      )
        .map(UserPreferences(_))
    }

  def get(userId: Long, key: String): Try[UserPreference] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL("""
         SELECT up.user_id, up.key, up.value, up.value_type
         FROM userpreferences up
         WHERE up.user_id={userId}
         AND up.key = {key}
      """)
          .on(
            Symbol("userId") -> userId,
            Symbol("key")    -> key
          )
          .as(userPreferenceParser.single)
      )
    }

  def createOrUpdate(userPreference: UserPreference): Try[Long] = {
    create(userPreference)
      .recoverWith { case _: PreferenceAlreadyExistException =>
        update(userPreference)
      }
  }

  def create(userPreference: UserPreference): Try[Long] = {
    dbXivo
      .withConnection { implicit c =>
        get(userPreference.userId, userPreference.key) match {
          case Success(_) =>
            Failure(
              PreferenceAlreadyExistException(
                s"Preference ${userPreference.key} is already exist for user ${userPreference.userId}"
              )
            )
          case Failure(_) =>
            Try(
              SQL("""
           INSERT INTO userpreferences(user_id, key, value, value_type)
           VALUES ({userId}, {key}, {value}, {value_type})
           """)
                .on(
                  Symbol("userId")     -> userPreference.userId,
                  Symbol("key")        -> userPreference.key,
                  Symbol("value")      -> userPreference.value,
                  Symbol("value_type") -> userPreference.valueType
                )
                .executeInsert(scalar[Long].single)
            )
        }
      }
      .map { id =>
        notifier.onUserPreferenceCreated(userPreference.userId)
        id
      }
  }

  def update(userPreference: UserPreference): Try[Long] = {
    dbXivo
      .withConnection { implicit c =>
        Try(
          SQL("""UPDATE userpreferences up
         SET value = {value}
         WHERE up.user_id = {userId} AND up.key = {key}""")
            .on(
              Symbol("value")  -> userPreference.value,
              Symbol("userId") -> userPreference.userId,
              Symbol("key")    -> userPreference.key
            )
            .executeUpdate()
        )
      }
      .map { id =>
        notifier.onUserPreferenceEdited(userPreference.userId)
        id
      }
  }

  def delete(userId: Long, key: String): Try[Long] = {
    dbXivo
      .withConnection { implicit c =>
        Try(
          SQL("""DELETE FROM userpreferences up
         WHERE up.user_id = {userId} AND up.key = {key}""")
            .on(Symbol("userId") -> userId, Symbol("key") -> key)
            .executeUpdate()
        )
      }
      .map { id =>
        notifier.onUserPreferenceDeleted(userId)
        id
      }
  }
}
