package xivo.service

import play.api.Configuration

import javax.inject.Inject
import scala.util.Random

class MeetingRoomConfiguration @Inject() (configuration: Configuration) {

  lazy val secret: String = configuration
    .getOptional[String]("authentication.secret")
    .getOrElse(Random.alphanumeric.take(32).mkString)

  lazy val applicationId: String = configuration
    .getOptional[String]("authentication.applicationId")
    .getOrElse("xivo")

  lazy val domain: String = configuration
    .getOptional[String]("authentication.domain")
    .getOrElse("*")

  def expiration(): Long =
    configuration
      .getOptional[Long]("authentication.tokenExpiration")
      .getOrElse(28800)

  lazy val minRange = 10000
  lazy val maxRange = 100000
  val intervalRange: List[String] =
    List.range(minRange, maxRange).map(_.toString)
}
