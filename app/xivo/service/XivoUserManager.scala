package xivo.service

import anorm.SqlParser.scalar
import anorm.{Column, Macro, ParameterMetaData, RowParser, SQL, ToStatement}
import com.google.inject.Inject
import common.DBUtil
import model.{DynamicFilter, LowerCasing, OperatorEq}
import play.api.db.{Database, NamedDatabase}
import ws.model.LineType
import ws.model.LineType.Type
import xivo.model.{FindXivoUserResponse, XivoUser, XivoUserContact}
import xivo.service.queries.QueryBuilder

import scala.util.Try

class XivoUserManager @Inject() (@NamedDatabase("xivo") dbXivo: Database)
    extends QueryBuilder[XivoUser] {

  val lineTypeValues: Map[LineType.Type, String] = Map(
    LineType.PHONE  -> "phone",
    LineType.UA     -> "ua",
    LineType.WEBRTC -> "webrtc",
    LineType.SCCP   -> "sccp",
    LineType.CUSTOM -> "custom"
  )

  implicit val lineTypeToStatement: ToStatement[Type] =
    DBUtil.enumToStatement(lineTypeValues)
  implicit val lineTypeMetaData: ParameterMetaData[Type] =
    DBUtil.enumTypeMetaData[LineType.Type]()
  implicit def enumToLineType: Column[Type] =
    DBUtil.columnToEnum(lineTypeValues)

  val parser: RowParser[XivoUser] = Macro.namedParser[XivoUser]
  val contactparser: RowParser[XivoUserContact] =
    Macro.namedParser[XivoUserContact]

  val defaultJoin = {
    """LEFT JOIN entity ON entity.id = userfeatures.entityid
    LEFT JOIN user_line ON user_line.user_id = userfeatures.id
    LEFT JOIN linefeatures ON linefeatures.id = user_line.line_id
    LEFT JOIN userlabels ON  userlabels.user_id = userfeatures.id
    LEFT JOIN labels ON userlabels.label_id = labels.id
    LEFT JOIN mediaserver ON mediaserver.name = linefeatures.configregistrar
    LEFT JOIN (SELECT id, options, generate_subscripts(options, 1) pos FROM usersip) us
      ON us.id=linefeatures.protocolid AND us.options[us.pos][1] = 'webrtc' AND linefeatures.protocol='sip'"""
  }

  val selectColumns: Map[String, String] = Map(
    "id"             -> "userfeatures.id ",
    "provisioning"   -> "linefeatures.provisioningid",
    "phoneNumber"    -> "linefeatures.number",
    "lineType"       -> """CASE
        | WHEN linefeatures.protocol = 'sip' AND us.options[us.pos][2] = 'yes' THEN 'webrtc'
        |	WHEN linefeatures.protocol = 'sip' AND us.options[us.pos][2] = 'ua' THEN 'ua'
        |	WHEN linefeatures.protocol = 'sip' THEN 'phone'
        |	ELSE linefeatures.protocol::text END
        |""".stripMargin,
    "fullName"       -> "userfeatures.firstname || ' ' || userfeatures.lastname",
    "entity"         -> "entity.displayname",
    "mdsName"        -> "linefeatures.configregistrar",
    "mdsDisplayName" -> "mediaserver.display_name"
  )

  override val havingColumns: Map[String, String] = selectColumns ++ Map(
    "labels" -> "labels.display_name"
  )

  override val toCastColumns: List[String] = List("provisioning")

  val defaultSelect =
    s"""SELECT ${columnsIterator()},
             case userfeatures.commented
                   when 0 then false
                   else true END as disabled,
             array_agg(labels.display_name
                ORDER BY labels.display_name)
                FILTER (WHERE labels.display_name IS NOT null) as labels
         FROM userfeatures
         ${defaultJoin}"""

  override val defaultOrder = "ORDER BY fullName"

  override val defaultGroup = "GROUP BY 1,2,3,4,5,6,7,8"

  def getAll: Try[List[XivoUser]] =
    dbXivo.withConnection { implicit c =>
      Try(buildQuery().as(parser.*))
    }

  def getAllAsContact: Try[List[XivoUserContact]] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL("""SELECT u.id, u.firstname, u.lastname, u.email, u.mobilephonenumber, l.number as internalphonenumber, 
             | array(
             |		SELECT labels.display_name 
             |	  FROM userlabels
             |		LEFT JOIN labels on userlabels.label_id=labels.id
             |		WHERE userlabels.user_id=u.id 
             |		) as labels,		
             | array(
             |		SELECT incall.exten 
             |		FROM dialaction 
             |		LEFT JOIN incall ON dialaction.categoryval = incall.id::varchar(255)
             |		WHERE dialaction.actionarg1 = u.id ::varchar(255) AND dialaction.action= 'user' AND dialaction.category= 'incall'
             |		) as externalphonenumber
             | FROM userfeatures u 
             | LEFT JOIN user_line ul on ul.user_id=u.id and ul.main_user=true and ul.main_line=true
             | LEFT JOIN linefeatures l ON ul.line_id = l.id 
             |""".stripMargin).as(contactparser.*)
      )
    }

  def validateUserPassword(login: String, password: String): Try[XivoUser] = {
    dbXivo.withConnection { implicit c =>
      val filters = List(
        DynamicFilter(
          "userfeatures.loginclient",
          Some(OperatorEq),
          Some(login)
        ),
        DynamicFilter(
          "userfeatures.passwdclient",
          Some(OperatorEq),
          Some(password)
        )
      )
      Try(
        buildQuery(defaultSelect, filters = refineFilters(filters))
          .as(parser.single)
      )
    }
  }

  def find(
      filters: List[DynamicFilter],
      offset: Int,
      limit: Int
  ): Try[FindXivoUserResponse] =
    dbXivo.withConnection { implicit c =>
      val orderClause = Option(
        DynamicFilter.toOrderClause(filters)(LowerCasing)
      ) filterNot (_.isEmpty) getOrElse defaultOrder
      val selectCount =
        s"SELECT COUNT(*) FROM (SELECT COUNT(*) FROM userfeatures $defaultJoin"
      val paging = s"$orderClause LIMIT $limit OFFSET $offset"

      val sql = buildQuery(
        defaultSelect,
        refineFilters(filters),
        defaultGroup,
        Some(paging)
      )
      val sqlCount = buildQuery(
        selectCount,
        refineFilters(filters),
        "GROUP BY userfeatures.id",
        Some(") as nb")
      )
      Try({
        val results = sql.as(parser.*)
        val count   = sqlCount.as(scalar[Long].single)
        FindXivoUserResponse(count, results)
      })
    }
}
