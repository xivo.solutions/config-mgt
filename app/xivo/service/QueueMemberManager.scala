package xivo.service

import anorm.SQL
import com.google.inject.Inject
import play.api.db.{Database, NamedDatabase}
import xivo.model.{QueueMemberUserType, _}

import java.sql.Connection
import scala.util.Try

class QueueMemberManager @Inject() (@NamedDatabase("xivo") dbXivo: Database) {

  def getAllQueueMember(
      queueMemberUserType: QueueMemberUserType.Value,
      queueCategory: QueueCategory.Value
  ): Either[QueueMemberError, Map[Long, List[QueueMember]]] = {
    dbXivo.withConnection { implicit c =>
      for {
        queueMemberList <- getGroupMembersFromDatabase(
          queueMemberUserType,
          queueCategory
        )
      } yield queueMemberList.groupBy(_.queue_id)
    }
  }

  def getQueueMember(
      queueMemberUserType: QueueMemberUserType.Value,
      queueCategory: QueueCategory.Value,
      queueId: Long
  ): Either[QueueMemberError, List[QueueMember]] =
    dbXivo.withConnection { implicit c =>
      getGroupMembersFromDatabase(
        queueMemberUserType,
        queueCategory,
        Some(queueId)
      )
    }

  private def getGroupMembersFromDatabase(
      userType: QueueMemberUserType.Value,
      category: QueueCategory.Value,
      queueId: Option[Long] = None
  )(implicit c: Connection): Either[QueueMemberError, List[QueueMember]] = {
    val tableName = s"${category.toString.toLowerCase}features"

    Try(
      SQL(s"""
             | SELECT queue_name, id AS queue_id, interface, penalty, commented, usertype, userid, channel, category, position
             | FROM queuemember
             | INNER JOIN $tableName ON $tableName.name = queuemember.queue_name
             | WHERE category::text = {category}
             | AND usertype::text = {usertype}
             | ${queueId.map(_ => s"AND $tableName.id = {queueId}").getOrElse("")}
             |""".stripMargin)
        .on("category" -> category.toString.toLowerCase)
        .on("usertype" -> userType.toString.toLowerCase)
        .on("queueId" -> queueId.getOrElse(-1L))
        .as(QueueMember.parser.*)
    ).toEither.left.map(f =>
      QueueMemberError(
        DatabaseError,
        s"Error when retrieving queue member for category : $category, with user type: $userType ${queueId
          .map(qid => s"And queue id : $qid")
          .getOrElse("")}: ${f.getMessage}"
      )
    )
  }
}
