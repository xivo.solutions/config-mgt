package xivo.service

import com.google.inject.Inject
import ws.model.{Line, LineType}
import xivo.model.rabbitmq._
import xivo.service.rabbitmq.XivoRabbitEventsPublisher

class LineNotifier @Inject() (
    eventPublisher: XivoRabbitEventsPublisher,
    confd: ConfdClient
) {

  private def notify(line: Line, messageType: RabbitMessageType): Unit = {
    line.id.foreach(id => eventPublisher.publish(ObjectEvent(messageType, id)))
  }

  private def ipbxReload(): Unit = {
    eventPublisher.publish(
      IpbxEvent(List(IpbxSipReload, IpbxDialplanReload, IpbxSccpReload))
    )
  }

  private def associateUserLine(userid: Long, lineid: Long): Unit = {
    eventPublisher.publish(UserLineEvent(UserLineAssociated, userid, lineid))
  }

  private def dissociateUserLine(userid: Long, lineid: Long): Unit = {
    eventPublisher.publish(UserLineEvent(UserLineDissociated, userid, lineid))
  }

  def onUserLineCreated(userid: Long, line: Line, endpointid: Long): Unit = {
    val eventType = line.lineType match {
      case LineType.PHONE | LineType.UA | LineType.WEBRTC => SipEndpointCreated
      case LineType.SCCP                                  => SccpEndpointCreated
      case LineType.CUSTOM                                => CustomEndpointCreated
    }

    eventPublisher.publish(ObjectEvent(eventType, endpointid))
    notify(line, LineCreated)
    line.id.foreach(lineid => associateUserLine(userid, lineid))
    line.device.filterNot(_.toString.isEmpty).foreach(confd.deviceUpdate)
    ipbxReload()
  }

  def onUserLineEdited(
      userid: Long,
      oldline: Line,
      newline: Line,
      endpointid: Long
  ): Unit = {
    val eventType = newline.lineType match {
      case LineType.PHONE | LineType.UA | LineType.WEBRTC => SipEndpointEdited
      case LineType.SCCP                                  => SccpEndpointEdited
      case LineType.CUSTOM                                => CustomEndpointEdited
    }

    eventPublisher.publish(ObjectEvent(eventType, endpointid))
    notify(newline, LineEdited)
    oldline.device.filterNot(_.toString.isEmpty).foreach(confd.deviceUpdate)
    if (newline.device != oldline.device) {
      newline.device.filterNot(_.toString.isEmpty).foreach(confd.deviceUpdate)
    }
    ipbxReload()
  }

  def onUserLineDeleted(userid: Long, line: Line, endpointid: Long): Unit = {
    val eventType = line.lineType match {
      case LineType.PHONE | LineType.UA | LineType.WEBRTC => SipEndpointDeleted
      case LineType.SCCP                                  => SccpEndpointDeleted
      case LineType.CUSTOM                                => CustomEndpointDeleted
    }

    eventPublisher.publish(ObjectEvent(eventType, endpointid))
    notify(line, LineDeleted)
    line.id.foreach(lineid => dissociateUserLine(userid, lineid))
    line.device.filterNot(_.toString.isEmpty).foreach(confd.deviceUpdate)
    ipbxReload()
  }

}
