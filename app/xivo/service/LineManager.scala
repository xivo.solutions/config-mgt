package xivo.service

import java.sql.Connection

import anorm.SqlParser._
import anorm._
import com.google.inject.Inject
import eu.timepit.refined.api.RefType
import eu.timepit.refined.auto.autoUnwrap
import play.api.db.{Database, _}
import common.refined.anorm._
import ws.model.{Line, LineType}

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import scala.util.{Failure, Random, Try, Success => TrySuccess}

class LineManager @Inject() (
    @NamedDatabase("xivo") dbXivo: Database,
    notifier: LineNotifier,
    confd: ConfdClient
) {

  val CONFD_WS_TIMEOUT = 1000.millis

  val lineParser: RowParser[Line] = for {
    id        <- long("id")
    protocol  <- str("protocol")
    device    <- SqlParser.get[Option[Line.Device]]("device")
    site      <- SqlParser.get[Line.Site]("site")
    name      <- SqlParser.get[Option[Line.Name]]("name")
    extension <- SqlParser.get[Line.Extension]("number")
    context   <- SqlParser.get[Line.Context]("context")
    webrtc    <- str("webrtc_options")
    lineType  <- LineType.protocolToType(protocol, webrtc)
    lineNum   <- SqlParser.get[Option[Line.Num]]("num")
    provisioningId <-
      SqlParser.get[Option[Line.ProvisioningId]]("provisioningId")
  } yield Line(
    Some(id),
    lineType,
    context,
    extension,
    site,
    device,
    name,
    lineNum,
    provisioningId
  )

  def getInternal(userId: Long)(implicit c: Connection) = {
    Try(
      SQL("""SELECT l.id, l.protocol, l.device,  l.configregistrar as site,
                      l.name, l.number, l.context, l.num, l.provisioningId, coalesce(us.options[us.pos][2], '') as webrtc_options
               FROM user_line ul
               INNER JOIN linefeatures l ON l.id=ul.line_id
               LEFT JOIN (SELECT id, options, generate_subscripts(options, 1) pos FROM usersip) us
                      ON us.id=l.protocolid AND us.options[us.pos][1] = 'webrtc' AND l.protocol='sip'
               WHERE ul.user_id={user_id} AND ul.main_line='t' AND ul.main_user='t';
      """)
        .on("user_id" -> userId)
        .as(lineParser.single)
    )
  }

  def get(userid: Long): Try[Line] =
    dbXivo.withConnection { implicit c =>
      getInternal(userid)
    }
  private def randomNumber(size: Int): Int =
    LazyList
      .continually((Random.nextInt(9) + 1).toString)
      .take(size)
      .mkString
      .toInt

  private def randomString(size: Int): String =
    Random.alphanumeric.take(size).map(_.toLower).mkString

  private def randomPeerName(): Line.Name =
    RefType.applyRef[Line.Name](randomString(8)) match {
      case Right(name) => name
      case Left(_)     => randomPeerName()
    }

  private def getUniquePeerName()(implicit c: Connection): Try[Line.Name] = {
    val sipName = randomPeerName()
    Try(
      SQL("SELECT count(*) from linefeatures where name={name}")
        .on(Symbol("name") -> sipName)
        .as(scalar[Long].single)
    )
      .flatMap(exists => {
        if (exists == 0)
          TrySuccess(sipName)
        else
          getUniquePeerName()
      })
  }

  private def getMacAddress(
      deviceId: Option[Line.Device],
      lineType: LineType.Type
  )(implicit c: Connection): Option[Line.DeviceMac] = {
    if (
      lineType == LineType.SCCP && deviceId.isDefined && deviceId.forall(
        !_.isEmpty
      )
    ) {
      Try(Await.result(confd.deviceGet(deviceId.get), CONFD_WS_TIMEOUT))
        .map(response => (response.json \ "mac").as[String])
        .flatMap(mac =>
          RefType.applyRef[Line.DeviceMac](mac) match {
            case Right(name) => TrySuccess(name)
            case _           => Failure(new Exception("Can't get MAC address"))
          }
        ) match {
        case TrySuccess(mac) => Some(mac)
        case _               => None
      }
    } else None
  }

  private def getUniqueProvisioningId()(implicit c: Connection): Try[Int] = {
    val provisioningid = randomNumber(6)
    Try(
      SQL(
        "SELECT count(*) from linefeatures where provisioningid={provisioningid}"
      )
        .on(Symbol("provisioningid") -> provisioningid)
        .as(scalar[Long].single)
    )
      .flatMap(exists => {
        if (exists == 0)
          TrySuccess(provisioningid)
        else
          getUniqueProvisioningId()
      })
  }

  def createUserSip(
      userid: Long,
      line: Line,
      peerName: Line.Name,
      options: String = "{}"
  )(implicit c: Connection): Try[Long] = {
    val sipSecret = randomString(8)
    Try(
      SQL("""INSERT INTO usersip(name, type, secret, context, callerid, category, options)
          SELECT
            {name},
            'friend'::useriax_type,
            {secret},
            {context},
            CASE WHEN u.callerid ~ '<.*>\\s*$'
              THEN substring(u.callerid FROM '(.*\\s*<.*>)\\s*$')
              ELSE u.callerid || ' <' || {extension} || '>'
            END,
            'user'::useriax_category, {options}::character varying[]
          FROM userfeatures u where u.id={userid};""")
        .on(
          Symbol("name")      -> peerName,
          Symbol("secret")    -> sipSecret,
          Symbol("context")   -> line.context,
          Symbol("extension") -> line.extension,
          Symbol("userid")    -> userid,
          Symbol("options")   -> options
        )
        .executeInsert(scalar[Long].single)
    )
  }

  def createLineFeature(
      userid: Long,
      line: Line,
      protocolid: Long,
      peerName: Line.Name,
      provisioningid: Int
  )(implicit c: Connection): Try[Long] = {
    val protocol = LineType.typeToProtocol(line.lineType)
    Try(
      SQL(
        """INSERT INTO linefeatures(protocol, protocolid, device, configregistrar, name, number, context, provisioningid, num)
               VALUES({protocol}::trunk_protocol, {protocolid}, {device}, {configregistrar}, {name}, {number}, {context}, {provisioningid}, {num});"""
      )
        .on(
          Symbol("protocol")        -> protocol,
          Symbol("protocolid")      -> protocolid,
          Symbol("device")          -> line.device,
          Symbol("configregistrar") -> line.site,
          Symbol("name")            -> peerName,
          Symbol("number")          -> line.extension,
          Symbol("context")         -> line.context,
          Symbol("provisioningid")  -> provisioningid,
          Symbol("num")             -> line.lineNum.getOrElse(1L)
        )
        .executeInsert(scalar[Long].single)
    )
  }

  def createUserLine(userid: Long, lineid: Long, extensionid: Long)(implicit
      c: Connection
  ): Try[Long] =
    Try(
      SQL("""INSERT INTO user_line(user_id, line_id, extension_id, main_user, main_line)
               VALUES({userid}, {lineid}, {extensionid}, 't', 't')""")
        .on(
          Symbol("userid")      -> userid,
          Symbol("lineid")      -> lineid,
          Symbol("extensionid") -> extensionid
        )
        .executeInsert(scalar[Long].single)
    )

  def createSccpLine(
      userid: Long,
      extension: Line.Extension,
      context: Line.Context
  )(implicit c: Connection): Try[Long] =
    Try(
      SQL("""INSERT INTO sccpline(name, context, cid_name, cid_num)
               SELECT {name}, {context}, u.firstname || ' ' || u.lastname, {cid_num}
               FROM userfeatures u WHERE u.id={userid};""")
        .on(
          Symbol("name")    -> extension,
          Symbol("context") -> context,
          Symbol("cid_num") -> extension,
          Symbol("userid")  -> userid
        )
        .executeInsert(scalar[Long].single)
    )

  def createSccpDevice(mac: Option[Line.DeviceMac], line: Line.Extension)(
      implicit c: Connection
  ): Try[Long] = {
    val device = getSccpDeviceNameFromMac(mac)
    Try(
      SQL("""INSERT INTO sccpdevice(name, device, line)
               VALUES({name}, {device}, {line});""")
        .on(
          Symbol("name")   -> device,
          Symbol("device") -> device,
          Symbol("line")   -> line
        )
        .executeInsert(scalar[Long].single)
    )
  }

  def createCustomLine(context: Line.Context, peerName: Line.Name)(implicit
      c: Connection
  ): Try[Long] =
    Try(
      SQL("""INSERT INTO usercustom(context, interface, category)
               VALUES({context}, {interface}, 'user'::usercustom_category);""")
        .on(
          Symbol("context")   -> context,
          Symbol("interface") -> peerName
        )
        .executeInsert(scalar[Long].single)
    )

  def createExtension(
      userid: Long,
      context: Line.Context,
      extension: Line.Extension
  )(implicit c: Connection): Try[Long] =
    Try(
      SQL(
        """INSERT INTO extensions(context, exten, type, typeval)
               VALUES({context}, {extension}, 'user'::extenumbers_type, {userid})"""
      )
        .on(
          Symbol("context")   -> context,
          Symbol("extension") -> extension,
          Symbol("userid")    -> userid.toString
        )
        .executeInsert(scalar[Long].single)
    )

  def getSccpDeviceNameFromMac(mac: Option[Line.DeviceMac]): String = {
    if (mac.isDefined)
      s"SEP${mac.get.replaceAll(":", "").toUpperCase}"
    else ""
  }

  def deleteSccpDevice(
      mac: Option[Line.DeviceMac]
  )(implicit c: Connection): Try[Long] = {
    val device = getSccpDeviceNameFromMac(mac)

    Try(
      SQL("""DELETE FROM sccpdevice WHERE device={device}""")
        .on(
          Symbol("device") -> device
        )
        .executeUpdate()
    )
  }

  def hasSccpDevice(
      mac: Option[Line.DeviceMac]
  )(implicit c: Connection): Try[Long] = {
    val device = getSccpDeviceNameFromMac(mac)
    Try(
      SQL("""SELECT count(*) FROM sccpdevice WHERE device={device}""")
        .on(
          Symbol("device") -> device
        )
        .as(scalar[Long].single)
    )
  }

  def updateSccpDevice(mac: Option[Line.DeviceMac], exten: Line.Extension)(
      implicit c: Connection
  ): Try[Long] = {
    val device = getSccpDeviceNameFromMac(mac)
    Try(
      SQL("""UPDATE sccpdevice SET name={name}, device={device}, line={line}
        |WHERE device={device};""".stripMargin)
        .on(
          Symbol("name")   -> device,
          Symbol("device") -> device,
          Symbol("line")   -> exten
        )
        .executeUpdate()
    )
  }

  def createOrUpdateSccpDevice(
      mac: Option[Line.DeviceMac],
      oldMac: Option[Line.DeviceMac],
      exten: Line.Extension
  )(implicit c: Connection): Try[Long] = {
    if (mac.isEmpty || oldMac.exists(_ => mac != oldMac)) {
      deleteSccpDevice(oldMac)
    }
    hasSccpDevice(mac).flatMap {
      case 0 if mac.isDefined => createSccpDevice(mac, exten)
      case _                  => updateSccpDevice(mac, exten)
    }
  }

  def updateUserSip(
      userid: Long,
      extension: Line.Extension,
      context: Line.Context,
      protocolId: Long,
      options: Option[String]
  )(implicit c: Connection): Try[Long] = {
    val statementSql = s"""UPDATE usersip us
                         |SET context = {context},
                         |callerid = CASE WHEN u.callerid ~ '<.*>\\s*${"$"}'
                         |  THEN substring(u.callerid FROM '(.*\\s*<.*>)\\s*${"$"}')
                         |  ELSE u.callerid || ' <' || {extension} || '>'
                         |END ${if (options.isDefined)
      ", options = {options}::character varying[]"
    else ""}
                         |FROM userfeatures u WHERE u.id = {userid} AND us.id = {protocolId};""".stripMargin

    val onList = Seq(
      Some(NamedParameter("context", context)),
      Some(NamedParameter("extension", extension)),
      Some(NamedParameter("userid", userid)),
      Some(NamedParameter("protocolId", protocolId)),
      options.map(NamedParameter("options", _))
    ).flatten

    Try(
      SQL(statementSql)
        .on(onList: _*)
        .executeUpdate()
    )
  }

  def updateSccpLine(
      userid: Long,
      extension: Line.Extension,
      context: Line.Context,
      protocolId: Long
  )(implicit c: Connection): Try[Long] =
    Try(
      SQL(
        """UPDATE sccpline sl
        |SET context = {context}, cid_name = u.firstname || ' ' || u.lastname, cid_num = {extension}, name={extension}
        |FROM userfeatures u WHERE u.id={userid} AND sl.id = {protocolId};""".stripMargin
      )
        .on(
          Symbol("extension")  -> extension,
          Symbol("context")    -> context,
          Symbol("userid")     -> userid,
          Symbol("protocolId") -> protocolId
        )
        .executeUpdate()
    )

  def updateCustomLine(context: Line.Context, protocolId: Long)(implicit
      c: Connection
  ): Try[Long] =
    Try(
      SQL(
        """UPDATE usercustom SET context = {context} WHERE id = {protocolId}"""
      )
        .on(
          Symbol("context")    -> context,
          Symbol("protocolId") -> protocolId
        )
        .executeUpdate()
    )

  def updateLineFeature(line: Line, oldLineId: Long)(implicit
      c: Connection
  ): Try[Long] = {
    Try(
      SQL(
        """UPDATE linefeatures SET device={device}, configregistrar={configregistrar}, number={number}, context={context}, num={num} WHERE id={lineId};"""
      )
        .on(
          Symbol("lineId")          -> oldLineId,
          Symbol("device")          -> line.device,
          Symbol("configregistrar") -> line.site,
          Symbol("number")          -> line.extension,
          Symbol("context")         -> line.context,
          Symbol("num")             -> line.lineNum.getOrElse(1L)
        )
        .executeUpdate()
    )
  }

  def updateExtension(
      context: Line.Context,
      extension: Line.Extension,
      extensionId: Long
  )(implicit c: Connection): Try[Long] =
    Try(
      SQL(
        """UPDATE extensions SET context={context}, exten={extension} WHERE id = {extensionId}"""
      )
        .on(
          Symbol("context")     -> context,
          Symbol("extension")   -> extension,
          Symbol("extensionId") -> extensionId
        )
        .executeUpdate()
    )

  def createEndpoint(
      userid: Long,
      line: Line,
      peerName: Line.Name,
      mac: Option[Line.DeviceMac]
  )(implicit c: Connection): Try[Long] = {
    line.lineType match {
      case LineType.PHONE => createUserSip(userid, line, peerName)
      case LineType.UA    => createUserSip(userid, line, peerName, "{{webrtc,ua}}")
      case LineType.WEBRTC =>
        createUserSip(userid, line, peerName, "{{webrtc,yes}}")
      case LineType.SCCP =>
        mac.foreach(m =>
          createOrUpdateSccpDevice(Some(m), None, line.extension)
        )
        createSccpLine(userid, line.extension, line.context)
      case LineType.CUSTOM => createCustomLine(line.context, peerName)
    }
  }

  def updateEndpoint(
      userid: Long,
      line: Line,
      protocolId: Long,
      mac: Option[Line.DeviceMac],
      oldMac: Option[Line.DeviceMac],
      oldLineType: LineType.Type
  )(implicit c: Connection): Try[Long] = {
    line.lineType match {
      case LineType.PHONE | LineType.UA | LineType.WEBRTC =>
        val opt: Try[Option[String]] = if (oldLineType != line.lineType) {
          getOptions(protocolId).map(options =>
            Some(
              convertOptionsToString(
                generationUserSipOption(line.lineType, options)
              )
            )
          )
        } else {
          TrySuccess(None)
        }
        opt match {
          case TrySuccess(value) =>
            updateUserSip(
              userid,
              line.extension,
              line.context,
              protocolId,
              value
            )
          case Failure(f) => Failure(f)
        }
      case LineType.SCCP =>
        createOrUpdateSccpDevice(mac, oldMac, line.extension)
        updateSccpLine(userid, line.`extension`, line.context, protocolId)
      case LineType.CUSTOM => updateCustomLine(line.context, protocolId)
    }
  }

  def generationUserSipOption(
      protocol: LineType.Type,
      options: Array[Array[String]]
  ): Array[Array[String]] = {
    def containsWebRTCKey(options: Array[Array[String]]): Boolean =
      options.exists(option => option.head == "webrtc")

    def addWebRTCKey(
        options: Array[Array[String]],
        value: String
    ): Array[Array[String]] =
      options ++ Array(Array("webrtc", value))

    def updateWebRTCKey(
        options: Array[Array[String]],
        value: String
    ): Array[Array[String]] =
      options.updated(
        options.indexWhere(option => option.head == "webrtc"),
        Array("webrtc", value)
      )

    def removeWebRTCKey(options: Array[Array[String]]): Array[Array[String]] =
      options.filter(option => option.head != "webrtc")

    protocol match {
      case LineType.WEBRTC =>
        if (containsWebRTCKey(options)) updateWebRTCKey(options, "yes")
        else addWebRTCKey(options, "yes")
      case LineType.UA =>
        if (containsWebRTCKey(options)) updateWebRTCKey(options, "ua")
        else addWebRTCKey(options, "ua")
      case _ =>
        if (containsWebRTCKey(options))
          removeWebRTCKey(options)
        else
          options
    }
  }

  def convertOptionsToString(options: Array[Array[String]]): String =
    options
      .map(option => option.mkString("{", ",", "}"))
      .mkString("{", ",", "}")

  def getOptions(
      protocolId: Long
  )(implicit c: Connection): Try[Array[Array[String]]] = {
    Try(
      SQL("SELECT options[0:2] from usersip us WHERE id = {protocolId}")
        .on(Symbol("protocolId") -> protocolId)
        .as(scalar[Array[Array[String]]].single)
    )
  }

  def deleteAll(userid: Long, line: Line, endpointTable: String)(implicit
      c: Connection
  ): Try[Int] = {
    Try(
      SQL(s"""DELETE FROM $endpointTable t USING linefeatures l, user_line ul WHERE ul.line_id=l.id AND t.id=l.protocolid AND ul.user_id={userid};
                 DELETE FROM user_line ul WHERE ul.user_id={userid};
                 DELETE FROM linefeatures l WHERE l.id={lineid};
                 DELETE FROM extensions e WHERE e.type='user'::extenumbers_type and e.typeval={userid}::character varying;
                 DELETE FROM sccpdevice WHERE line={exten};
              """)
        .on(
          Symbol("userid") -> userid,
          Symbol("lineid") -> line.id,
          Symbol("exten")  -> line.extension
        )
        .executeUpdate()
    )
  }

  def getLineExtensionId(userId: Long, lineId: Long)(implicit
      c: Connection
  ): Try[Long] = {
    Try(
      SQL(
        """SELECT extension_id FROM user_line where line_id={lineId} AND user_id={userId}"""
      )
        .on(
          Symbol("lineId") -> lineId,
          Symbol("userId") -> userId
        )
        .as(scalar[Long].single)
    )
  }

  def getLineProtocolId(lineId: Long)(implicit c: Connection): Try[Long] = {
    Try(
      SQL("""SELECT protocolid from linefeatures where id = {lineId}""")
        .on(Symbol("lineId") -> lineId)
        .as(scalar[Long].single)
    )
  }

  def getEndpointTable(lineType: LineType.Type): String =
    lineType match {
      case LineType.PHONE | LineType.UA | LineType.WEBRTC => "usersip"
      case LineType.SCCP                                  => "sccpline"
      case LineType.CUSTOM                                => "usercustom"
    }

  def create(userid: Long, line: Line): Try[Line] = {
    val createTransaction = dbXivo.withTransaction { implicit c =>
      for {
        peerName <- getUniquePeerName()
        mac = getMacAddress(line.device, line.lineType)
        protocolId     <- createEndpoint(userid, line, peerName, mac)
        provisioningId <- getUniqueProvisioningId()
        lineId <-
          createLineFeature(userid, line, protocolId, peerName, provisioningId)
        extenId <- createExtension(userid, line.context, line.extension)
        _       <- createUserLine(userid, lineId, extenId)
        newline <- getInternal(userid)
      } yield (newline, protocolId)
    }

    for {
      (newline, protocolId) <- createTransaction
      _ = notifier.onUserLineCreated(userid, newline, protocolId)
    } yield newline
  }

  def update(userid: Long, line: Line): Try[Line] = {
    val updateTransaction = dbXivo.withTransaction { implicit c =>
      for {
        oldLine   <- getInternal(userid)
        oldLineId <- Try(oldLine.id.get)
        mac    = getMacAddress(line.device, line.lineType)
        oldMac = getMacAddress(oldLine.device, line.lineType)
        protocolId <- getLineProtocolId(oldLineId)
        _ <- updateEndpoint(
          userid,
          line,
          protocolId,
          mac,
          oldMac,
          oldLine.lineType
        )
        _           <- updateLineFeature(line, oldLineId)
        extensionId <- getLineExtensionId(userid, oldLineId)
        _           <- updateExtension(line.context, line.extension, extensionId)
        newline     <- getInternal(userid)
      } yield (oldLine, newline, protocolId)
    }

    for {
      (oldline, newline, protocolId) <- updateTransaction
      _ = notifier.onUserLineEdited(userid, oldline, newline, protocolId)
    } yield newline
  }

  def delete(userid: Long): Try[Line] = {
    val deleteTransaction = dbXivo.withTransaction { implicit c =>
      for {
        line <- getInternal(userid)
        endpointTable = getEndpointTable(line.lineType)
        lineid     <- Try(line.id.get)
        protocolId <- getLineProtocolId(lineid)
        _          <- deleteAll(userid, line, endpointTable)
      } yield (line, protocolId)
    }

    for {
      (line, protocolId) <- deleteTransaction
      _ = notifier.onUserLineDeleted(userid, line, protocolId)
    } yield line
  }

}
