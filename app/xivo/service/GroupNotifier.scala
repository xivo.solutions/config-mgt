package xivo.service

import com.google.inject.Inject
import xivo.model.rabbitmq.{
  GroupCreated,
  GroupDeleted,
  GroupEdited,
  ObjectEvent
}
import xivo.service.rabbitmq.XivoRabbitEventsPublisher

class GroupNotifier @Inject() (
    eventPublisher: XivoRabbitEventsPublisher
) {
  def onGroupEdited(userid: Long): Unit =
    eventPublisher.publish(ObjectEvent(GroupEdited, userid))
  def onGroupCreated(userid: Long): Unit =
    eventPublisher.publish(ObjectEvent(GroupCreated, userid))
  def onGroupDeleted(userid: Long): Unit =
    eventPublisher.publish(ObjectEvent(GroupDeleted, userid))
}
