package xivo.service

import anorm.{Macro, RowParser, SQL}
import com.google.inject.Inject
import play.api.db.{Database, NamedDatabase}
import xivo.model.Xivo

import scala.util.Try

class XivoManager @Inject() (@NamedDatabase("xivo") dbXivo: Database) {

  val xivoParser: RowParser[Xivo] = Macro.namedParser[Xivo]

  def get(): Try[Xivo] =
    dbXivo.withConnection { implicit c =>
      Try(SQL("SELECT uuid from infos LIMIT 1").as(xivoParser.single))
    }
}
