package xivo.service

import anorm.SqlParser.scalar
import anorm._
import com.google.inject.Inject
import common.{CrudMacro, DBUtil}
import model.{DynamicFilter, LowerCasing, OperatorEq}
import play.api.db.{Database, NamedDatabase}
import xivo.model.{
  FindMeetingRoomResult,
  MeetingRoomConfig,
  MeetingRoomError,
  MeetingRoomException,
  RoomTypes
}
import xivo.model.RoomTypes.RoomType
import xivo.service.MeetingRoomManager.createGetFilter
import xivo.service.queries.QueryBuilder

import java.sql.Timestamp
import java.time.Instant
import scala.util.{Failure, Random, Try}

object MeetingRoomManager {
  private val staticFilter =
    DynamicFilter("roomType", Some(OperatorEq), Some(RoomTypes.Static.toString))
  private val personalFilter = DynamicFilter(
    "roomType",
    Some(OperatorEq),
    Some(RoomTypes.Personal.toString)
  )

  val NO_FILTER = List()

  def createGetFilter(field: String, fieldValue: String) =
    List(
      DynamicFilter(
        field,
        Some(OperatorEq),
        Some(fieldValue)
      )
    )

  def createRoomFilter(roomType: RoomType) =
    roomType match {
      case RoomTypes.Static   => List(staticFilter)
      case RoomTypes.Personal => List(personalFilter)
      case RoomTypes.All      => List()
    }

  def createPersonalUserIdFilter(userId: Long) =
    List(DynamicFilter("user_id", Some(OperatorEq), Some(userId.toString)))
}

class MeetingRoomManager @Inject() (
    @NamedDatabase("xivo") dbXivo: Database,
    config: MeetingRoomConfiguration,
    random: Random
) extends QueryBuilder[MeetingRoomConfig] {

  implicit def columnToTimestamp: anorm.Column[Timestamp] =
    anorm.Column.nonNull[Timestamp] { (value, meta) =>
      val MetaDataItem(column, _, _) = meta
      value match {
        case d: java.sql.Timestamp =>
          d.setNanos(0)
          Right(d)
        case _ =>
          Left(
            TypeDoesNotMatch(
              s"Failed to  convert $value: ${value.asInstanceOf[AnyRef].getClass} to Timestamp for column $column"
            )
          )
      }
    }

  val roomTypeValues: Map[RoomTypes.RoomType, String] = Map(
    RoomTypes.Static   -> "static",
    RoomTypes.Personal -> "personal",
    RoomTypes.All      -> "personal"
  )

  implicit val RoomTypeToStatement: ToStatement[RoomType] =
    DBUtil.enumToStatement(roomTypeValues)
  implicit val optionRoomTypeMetaData: ParameterMetaData[RoomType] =
    DBUtil.enumTypeMetaData[RoomTypes.RoomType]()

  implicit def enumToRoomType: Column[RoomType] =
    DBUtil.columnToEnum(roomTypeValues)

  override val toCastColumns: List[String] =
    List("userPin", "number", "id", "uuid", "user_id")

  override val selectColumns: Map[String, String] = Map(
    "id"           -> "meetingroom.id ",
    "displayName"  -> "meetingroom.display_name",
    "number"       -> "meetingroom.number",
    "userPin"      -> "meetingroom.user_pin",
    "uuid"         -> "meetingroom.uuid",
    "roomType"     -> "meetingroom.room_type",
    "userId"       -> "meetingroom.user_id",
    "pinTimestamp" -> "meetingroom.pin_timestamp",
    "alias"        -> "meetingroom.alias"
  )
  override val defaultSelect: String =
    s"""SELECT ${columnsIterator()}
                                         FROM meetingroom"""
  override val defaultOrder = "ORDER BY meetingroom.display_name"
  override val defaultGroup = "GROUP BY 1,2,3,4,5,6,7"

  override val parser: RowParser[MeetingRoomConfig] =
    Macro.namedParser[MeetingRoomConfig]

  val removeQuery: String = s"""DELETE FROM meetingroom"""

  def getAll(
      filters: List[DynamicFilter]
  ): Try[List[MeetingRoomConfig]] = {
    dbXivo.withConnection { implicit c =>
      Try(buildQuery(filters = refineFilters(filters)).as(parser.*))
    }
  }

  def get(filters: List[DynamicFilter]): Try[MeetingRoomConfig] = {
    dbXivo.withConnection { implicit c =>
      Try(
        buildQuery(filters = refineFilters(filters))
          .as(parser.single)
      )
    }
  }

  def create(meetingRoom: MeetingRoomConfig): Try[MeetingRoomConfig] =
    dbXivo.withConnection { implicit c =>
      Try({
        getOrCreateNumberForRoom(meetingRoom)
          .flatMap(enrichRoomWithPinTimestamp)
          .map(enrichRoomWithAlias)
          .map { room =>
            CrudMacro
              .query[MeetingRoomConfig](
                room,
                "meetingroom",
                Set("id"),
                Set("uuid"),
                true,
                CrudMacro.SnakeCase,
                CrudMacro.SQLInsert
              )
              .executeInsert[Option[Long]]()
              .get
          }
      })
        .flatMap(_.flatMap(id => get(createGetFilter("id", id.toString))))
    }

  def update(meetingRoom: MeetingRoomConfig): Try[MeetingRoomConfig] = {
    dbXivo.withConnection { implicit c =>
      Try({
        val roomWithPinTimestamp = enrichRoomWithPinTimestamp(meetingRoom)
        val ids =
          if (meetingRoom.userId.isDefined) {
            Set("id", "userId")
          } else Set("id")
        CrudMacro
          .query[MeetingRoomConfig](
            roomWithPinTimestamp.getOrElse(meetingRoom),
            "meetingroom",
            ids,
            Set("uuid", "alias"),
            true,
            CrudMacro.SnakeCase,
            CrudMacro.SQLUpdate
          )
          .executeUpdate()
      }).map { updateCount =>
        if (updateCount == 1) {
          meetingRoom.id
            .flatMap(id => get(createGetFilter("id", id.toString)).toOption)
            .map(r =>
              meetingRoom.copy(uuid = r.uuid, pinTimestamp = r.pinTimestamp)
            )
            .getOrElse(meetingRoom)
        } else
          throw new Exception(
            s"Cannot update a meeting room with id ${meetingRoom.id}"
          )
      }
    }
  }

  def delete(
      filters: List[DynamicFilter]
  ): Try[MeetingRoomConfig] = {
    dbXivo.withConnection { implicit c =>
      val meetingRoom = get(filters)

      if (meetingRoom.isFailure)
        meetingRoom
      else {
        val deletedCount =
          buildQuery(
            selectClause = removeQuery,
            filters = refineFilters(filters),
            order = "",
            group = ""
          )
            .executeUpdate()

        if (deletedCount == 1) {
          meetingRoom
        } else
          Failure(new Exception(s"Cannot delete a meeting room"))
      }
    }
  }

  def find(
      filters: List[DynamicFilter],
      offset: Int,
      limit: Int
  ): Try[FindMeetingRoomResult] =
    dbXivo.withConnection { implicit c =>
      val orderClause = Option(
        DynamicFilter.toOrderClause(filters)(LowerCasing)
      ) filterNot (_.isEmpty) getOrElse defaultOrder
      val selectCount =
        s"SELECT COUNT(*) as count FROM meetingroom"
      val paging = s"$orderClause LIMIT $limit OFFSET $offset"

      val sql = buildQuery(
        defaultSelect,
        refineFilters(filters),
        defaultGroup,
        Some(paging)
      )
      val sqlCount = buildQuery(
        selectCount,
        refineFilters(filters),
        "",
        None,
        ""
      )
      Try({
        val results = sql.as(parser.*)
        val count   = sqlCount.as(scalar[Long].single)
        FindMeetingRoomResult(count, results)
      })
    }

  def getOrCreateNumberForRoom(
      room: MeetingRoomConfig
  ): Try[MeetingRoomConfig] = {
    room.number match {
      case Some(_) => scala.util.Success(room)
      case None =>
        val takenNumbers: Set[String] =
          getAll(List()).toOption.toList.flatten.flatMap(_.number).toSet
        val availableNumbers: List[String] =
          config.intervalRange.filterNot(takenNumbers)
        Try(availableNumbers(random.nextInt(availableNumbers.length)))
          .map(num => room.copy(number = Some(num)))
          .recoverWith { case _ =>
            Failure(
              MeetingRoomException(
                MeetingRoomError.NumberNotAvailable,
                "Unable to generate the meeting room number"
              )
            )
          }
    }
  }

  def enrichRoomWithPinTimestamp(
      meetingRoom: MeetingRoomConfig
  ): Try[MeetingRoomConfig] = {
    Try {
      val ts = Timestamp.from(Instant.now())
      meetingRoom.id match {
        case Some(id) =>
          get(createGetFilter("id", id.toString)).toOption
            .flatMap {
              case r if meetingRoom.userPin.exists(r.userPin.contains) =>
                r.pinTimestamp
              case _ => Some(ts)
            }
            .map(t => meetingRoom.copy(pinTimestamp = Some(t)))
            .get
        case None =>
          meetingRoom.copy(pinTimestamp = Some(ts))
      }
    }.recoverWith { case _ =>
      Failure(
        MeetingRoomException(
          MeetingRoomError.Timestamp,
          "Unable to set timestamp"
        )
      )
    }
  }

  private def getUniqueAlias(
      meetingRoom: MeetingRoomConfig
  ): Option[String] = {

    def getRand: String = random.alphanumeric.take(4).mkString.toLowerCase
    val alias           = s"$getRand-$getRand"

    Try(
      dbXivo.withConnection { implicit c =>
        SQL(
          "SELECT count(*) from meetingroom where alias={alias}"
        )
          .on(Symbol("alias") -> alias)
          .as(scalar[Long].single)
      }
    ).toOption
      .flatMap(exists => {
        if (exists == 0)
          Some(alias)
        else
          getUniqueAlias(meetingRoom)
      })
  }

  def enrichRoomWithAlias(
      meetingRoom: MeetingRoomConfig
  ): MeetingRoomConfig = {
    meetingRoom.copy(alias = getUniqueAlias(meetingRoom))
  }
}
