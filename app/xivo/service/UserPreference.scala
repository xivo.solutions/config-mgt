package xivo.service

import model.ws.{ErrorResult, ErrorType}
import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import play.api.libs.json._
import play.api.mvc.{Result, Results}
import xivo.model.rabbitmq._

case class UserPreference(
    userId: Long,
    key: String,
    value: String,
    valueType: String
)

object UserPreference {
  implicit val toRabbitMessage: ToRabbitMessage[UserPreference] =
    new ToRabbitMessage[UserPreference] {
      def toRabbitMessage(
          usrPref: UserPreference,
          updateMode: ObjectUpdateMode
      ): BaseRabbitMessage = {
        val mt = updateMode match {
          case ObjectCreated => UserPreferenceCreated
          case ObjectEdited  => UserPreferenceEdited
          case ObjectDeleted => UserPreferenceDeleted
        }
        ObjectEvent(mt, usrPref.userId)
      }
    }
}

case class UserPreferences(preferences: List[UserPreference])
case class UserPreferencePayload(value: String, valueType: String)

case class PreferenceAlreadyExistException(message: String) extends Exception

object UserPreferenceKey {
  val PreferredDevice = "PREFERRED_DEVICE"
  val MobileAppInfo   = "MOBILE_APP_INFO"
  val NbMissedCall    = "NB_MISSED_CALL"
}

object UserPreferenceValues {
  val TypeDefaultDevice  = "TypeDefaultDevice"
  val WebAppAndMobileApp = "WebAppAndMobileApp"
}

object UserPreferences {
  implicit val writesFind: Writes[UserPreferences] =
    new Writes[UserPreferences] {
      def writes(usrPrefs: UserPreferences): JsValue = {
        Json.toJson(
          usrPrefs.preferences
            .map(up =>
              Json.obj(
                "key"        -> up.key,
                "value"      -> up.value,
                "value_type" -> up.valueType
              )
            )
        )
      }
    }
}

object UserPreferencePayload {
  implicit val writes: Writes[UserPreferencePayload] = (
    (JsPath \ "value").write[String] and
      (JsPath \ "value_type").write[String]
  )(unlift(UserPreferencePayload.unapply))

  implicit val reads: Reads[UserPreferencePayload] = (
    (JsPath \ "value").read[String] and
      (JsPath \ "value_type").read[String]
  )(UserPreferencePayload.apply _)
}

trait UserPreferenceErrorType extends ErrorType
case object UserNotFound extends UserPreferenceErrorType {
  val error = "UserNotFound"
}
case object PreferenceAlreadyExists extends UserPreferenceErrorType {
  val error = "PreferenceAlreadyExists"
}
case object PreferenceNotFound extends UserPreferenceErrorType {
  val error = "PreferenceNotFound"
}

case object PreferenceKeyNotExist extends UserPreferenceErrorType {
  val error = "PreferenceKeyNotExist"
}

case class UserPreferenceError(error: ErrorType, message: String)
    extends ErrorResult {
  override def toResult: Result = {
    log.error(this.message)
    val body = Json.toJson(this)
    error match {
      case UserNotFound            => Results.NotFound(body)
      case PreferenceAlreadyExists => Results.Conflict(body)
      case PreferenceNotFound      => Results.NotFound(body)
      case PreferenceKeyNotExist   => Results.BadRequest
      case _                       => Results.InternalServerError(body)
    }
  }
}

object UserPreferenceError {
  implicit val usrPrefErrorWrites: Writes[UserPreferenceError] =
    Json.writes[UserPreferenceError]
}
