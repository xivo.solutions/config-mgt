package xivo.model

import be.venneborg.refined.play.RefinedJsonFormats._
import eu.timepit.refined.W
import eu.timepit.refined.api.Refined
import eu.timepit.refined.collection.MaxSize
import play.api.libs.json.{Json, Reads, Writes}

case class SccpLine(
    id: Option[Long],
    name: String Refined MaxSize[W.`80`.T],
    context: String Refined MaxSize[W.`80`.T],
    cid_name: String Refined MaxSize[W.`80`.T],
    cid_num: String Refined MaxSize[W.`80`.T],
    protocol: ProtocolType.Type = ProtocolType.SCCP,
    commented: Int = 0
)

object SccpLine {
  implicit val reads: Reads[SccpLine]   = Json.reads[SccpLine]
  implicit val writes: Writes[SccpLine] = Json.writes[SccpLine]
}
