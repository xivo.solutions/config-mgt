package xivo.model

import model.DynamicFilter
import play.api.libs.functional.syntax.{toFunctionalBuilderOps, unlift}
import play.api.libs.json.{Format, JsPath, Json, Reads, Writes}
import ws.model.LineType

case class XivoUser(
    id: Long,
    fullName: String,
    provisioning: Option[Int],
    lineType: Option[LineType.Type],
    phoneNumber: Option[String],
    entity: String,
    mdsName: Option[String],
    mdsDisplayName: Option[String],
    disabled: Boolean,
    labels: Option[List[String]]
)

object XivoUser {
  implicit val reads: Reads[XivoUser] = Json.reads[XivoUser]
  implicit val writes: Writes[XivoUser] = (
    (JsPath \ "id").write[Long] and
      (JsPath \ "fullName").write[String] and
      (JsPath \ "provisioning").write(Writes.optionWithNull[Int]) and
      (JsPath \ "lineType").write(Format.optionWithNull[LineType.Type]) and
      (JsPath \ "phoneNumber").write(Writes.optionWithNull[String]) and
      (JsPath \ "entity").write[String] and
      (JsPath \ "mdsName").write(Writes.optionWithNull[String]) and
      (JsPath \ "mdsDisplayName").write(Writes.optionWithNull[String]) and
      (JsPath \ "disabled").write[Boolean] and
      (JsPath \ "labels").write(Writes.optionWithNull[List[String]])
  )(unlift(XivoUser.unapply))
}

case class FindXivoUserRequest(
    filters: List[DynamicFilter],
    offset: Int = 0,
    limit: Int = 100
)
case class FindXivoUserResponse(total: Long, list: List[XivoUser])

object FindXivoUserRequest {
  implicit val reads: Reads[FindXivoUserRequest] =
    Json.reads[FindXivoUserRequest]
  implicit val writes: Writes[FindXivoUserRequest] =
    Json.writes[FindXivoUserRequest]
}

object FindXivoUserResponse {
  implicit val reads: Reads[FindXivoUserResponse] =
    Json.reads[FindXivoUserResponse]
  implicit val writes: Writes[FindXivoUserResponse] =
    Json.writes[FindXivoUserResponse]
}

case class XivoAuthRequest(username: String, password: String)

object XivoAuthRequest {
  implicit val reads: Reads[XivoAuthRequest] = Json.reads[XivoAuthRequest]
}
