package xivo.model

import model.ws.{ErrorResult, ErrorType}
import play.api.libs.json._
import play.api.mvc.{Result, Results}
import xivo.model.rabbitmq._

case class MediaServerConfig(
    id: Long,
    name: String,
    display_name: String,
    voip_ip: Option[String],
    read_only: Boolean
)
case class MediaServerWithLineCount(
    mediaServer: MediaServerConfig,
    line_count: Long
)
case class MediaServerConfigNoId(
    name: String,
    display_name: String,
    voip_ip: Option[String]
) {
  def withId(id: Long): MediaServerConfig =
    MediaServerConfig(id, name, display_name, voip_ip, false)
}

object MediaServerConfig {
  implicit val MediaServerWrites: Writes[MediaServerConfig] =
    Json.writes[MediaServerConfig]

  implicit val mediaServerConfigReads: Reads[MediaServerConfig] =
    Json.reads[MediaServerConfig]

  implicit val toRabbitMessage: ToRabbitMessage[MediaServerConfig] =
    new ToRabbitMessage[MediaServerConfig] {
      def toRabbitMessage(
          mds: MediaServerConfig,
          updateMode: ObjectUpdateMode
      ): BaseRabbitMessage = {
        val mt = updateMode match {
          case ObjectCreated => MediaServerCreated
          case ObjectEdited  => MediaServerEdited
          case ObjectDeleted => MediaServerDeleted
        }
        ObjectEvent(mt, mds.id)
      }
    }
}

object MediaServerWithLineCount {
  implicit val MediaServerWithLinesWrites: Writes[MediaServerWithLineCount] =
    Json.writes[MediaServerWithLineCount]
}

object MediaServerConfigNoId {
  implicit val reads: Reads[MediaServerConfigNoId] =
    Json.reads[MediaServerConfigNoId]
  implicit val writes: Writes[MediaServerConfigNoId] =
    Json.writes[MediaServerConfigNoId]
}

case class MediaServerListConfig(list: List[MediaServerConfig])
case class MediaServerListWithLineCount(list: List[MediaServerWithLineCount])

trait MediaServerErrorType extends ErrorType

case object Invalid extends MediaServerErrorType {
  val error = "Invalid"
}

case object Duplicate extends MediaServerErrorType {
  val error = "Duplicate"
}

case class MediaServerError(error: ErrorType, message: String)
    extends ErrorResult {
  override def toResult: Result = {
    log.error(this.message)
    val body = Json.toJson(this)
    error match {
      case Invalid   => Results.BadRequest(body)
      case Duplicate => Results.BadRequest(body)
      case _         => Results.InternalServerError(body)
    }
  }
}

object MediaServerError {
  implicit val mediaServerErrorWrites: Writes[MediaServerError] =
    Json.writes[MediaServerError]
}
