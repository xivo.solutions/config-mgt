package xivo.model

import model.RecordingModeType
import model.ws.{ErrorResult, ErrorType}
import play.api.libs.json._
import play.api.mvc.{Result, Results}
import xivo.model.rabbitmq._

case class QueueFeature(
    id: Option[Long],
    name: String,
    displayname: String,
    number: String,
    context: Option[String],
    data_quality: Int,
    hitting_callee: Int,
    hitting_caller: Int,
    retries: Int,
    ring: Int,
    transfer_user: Int,
    transfer_call: Int,
    write_caller: Int,
    write_calling: Int,
    url: String,
    announceoverride: String,
    timeout: Option[Int],
    preprocess_subroutine: Option[String],
    announce_holdtime: Int,
    waittime: Option[Int],
    waitratio: Option[Double],
    ignore_forward: Int,
    recording_mode: RecordingModeType.Type,
    recording_activated: Int
)

object QueueFeature {
  implicit val queueFeatureWrites: Writes[QueueFeature] {
    def writes(queueFeature: QueueFeature): JsObject
  } = new Writes[QueueFeature] {
    def writes(queueFeature: QueueFeature): JsObject =
      Json.obj(
        "id"                    -> queueFeature.id,
        "name"                  -> queueFeature.name,
        "displayName"           -> queueFeature.displayname,
        "number"                -> queueFeature.number,
        "context"               -> queueFeature.context,
        "data_quality"          -> queueFeature.data_quality,
        "hitting_callee"        -> queueFeature.hitting_callee,
        "hitting_caller"        -> queueFeature.hitting_caller,
        "retries"               -> queueFeature.retries,
        "ring"                  -> queueFeature.ring,
        "transfer_user"         -> queueFeature.transfer_user,
        "transfer_call"         -> queueFeature.transfer_call,
        "write_caller"          -> queueFeature.write_caller,
        "write_calling"         -> queueFeature.write_calling,
        "url"                   -> queueFeature.url,
        "announceoverride"      -> queueFeature.announceoverride,
        "timeout"               -> queueFeature.timeout,
        "preprocess_subroutine" -> queueFeature.preprocess_subroutine,
        "announce_holdtime"     -> queueFeature.announce_holdtime,
        "waittime"              -> queueFeature.waittime,
        "waitratio"             -> queueFeature.waitratio,
        "ignore_forward"        -> queueFeature.ignore_forward,
        "recording_mode"        -> queueFeature.recording_mode,
        "recording_activated"   -> queueFeature.recording_activated
      )
  }

  implicit val queueFeatureToRabbit: ToRabbitMessage[QueueFeature] =
    new ToRabbitMessage[QueueFeature] {
      def toRabbitMessage(
          q: QueueFeature,
          updateMode: ObjectUpdateMode
      ): BaseRabbitMessage = {
        val mt = updateMode match {
          case ObjectCreated => QueueCreated
          case ObjectEdited  => QueueEdited
          case ObjectDeleted => QueueDeleted
        }
        ObjectEvent(mt, q.id.get)
      }
    }
}

trait QueueFeatureErrorType extends ErrorType
case object QueueFeatureNotFound extends QueueFeatureErrorType {
  val error = "QueueNotFound"
}

case class QueueFeatureError(error: ErrorType, message: String)
    extends ErrorResult {
  override def toResult: Result = {
    log.error(this.message)
    val body = Json.toJson(this)
    error match {
      case QueueFeatureNotFound => Results.NotFound(body)
      case _                    => Results.InternalServerError(body)
    }
  }
}

object QueueFeatureError {
  implicit val queueErrorWrites: Writes[QueueFeatureError] =
    Json.writes[QueueFeatureError]
}
