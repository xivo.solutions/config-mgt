package xivo.model

import anorm.TypeDoesNotMatch

import scala.util.Right

object QueueMonitorType extends Enumeration {
  type Type = Value

  val No, MixMonitor = Value
}

object QueueCategory extends Enumeration {
  type Type = Value

  val Group, Queue = Value

  implicit def rowToQueueCategory
      : anorm.Column[xivo.model.QueueCategory.Type] = {
    anorm.Column[xivo.model.QueueCategory.Type] { (value, meta) =>
      value match {
        case "queue" => Right(xivo.model.QueueCategory.Queue)
        case "group" => Right(xivo.model.QueueCategory.Group)
        case unknown =>
          Left(TypeDoesNotMatch(s"unknown QueueCategory from $unknown"))
      }
    }
  }
}

case class Queue(
    name: String,
    musicclass: Option[String],
    announce: Option[String],
    context: Option[String],
    timeout: Option[Int],
    monitor_type: Option[QueueMonitorType.Type],
    monitor_format: Option[String],
    queue_youarenext: Option[String],
    queue_thereare: Option[String],
    queue_callswaiting: Option[String],
    queue_holdtime: Option[String],
    queue_minutes: Option[String],
    queue_seconds: Option[String],
    queue_thankyou: Option[String],
    queue_reporthold: Option[String],
    periodic_announce: Option[String],
    announce_frequency: Option[Int],
    periodic_announce_frequency: Option[Int],
    announce_round_seconds: Option[Int],
    announce_holdtime: Option[String],
    retry: Option[Int],
    wrapuptime: Option[Int],
    maxlen: Option[Int],
    servicelevel: Option[Int],
    strategy: Option[String],
    joinempty: Option[String],
    leavewhenempty: Option[String],
    ringinuse: Int,
    reportholdtime: Int,
    memberdelay: Option[Int],
    weight: Option[Int],
    timeoutrestart: Int,
    commented: Int,
    category: QueueCategory.Type,
    timeoutpriority: String,
    autofill: Int,
    autopause: String,
    setinterfacevar: Int,
    setqueueentryvar: Int,
    setqueuevar: Int,
    membermacro: Option[String],
    min_announce_frequency: Int,
    random_periodic_announce: Int,
    announce_position: String,
    announce_position_limit: Int,
    defaultrule: Option[String]
)
