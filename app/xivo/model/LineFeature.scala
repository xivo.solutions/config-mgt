package xivo.model

import be.venneborg.refined.play.RefinedJsonFormats._
import eu.timepit.refined._
import eu.timepit.refined.api.Refined
import eu.timepit.refined.collection.MaxSize
import eu.timepit.refined.string.MatchesRegex
import play.api.libs.json.{Json, Reads, Writes}

case class LineFeature(
    id: Option[Long],
    protocol: ProtocolType.Type,
    protocolid: Int,
    device: Option[String Refined MaxSize[W.`32`.T]],
    configregistrar: String Refined MaxSize[W.`128`.T],
    name: String Refined MaxSize[W.`128`.T],
    number: String Refined MaxSize[W.`40`.T],
    context: String Refined MaxSize[W.`39`.T],
    provisioningid: String Refined MatchesRegex[W.`"^[a-z0-9]{6,}$"`.T],
    num: Int = 1,
    ipfrom: String Refined MaxSize[W.`15`.T] = refineMV(""),
    commented: Int = 0
)

object LineFeature {
  implicit val reads: Reads[LineFeature]   = Json.reads[LineFeature]
  implicit val writes: Writes[LineFeature] = Json.writes[LineFeature]
}
