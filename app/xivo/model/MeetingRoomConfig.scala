package xivo.model

import model.DynamicFilter
import model.ws.{ErrorResult, ErrorType}
import play.api.libs.functional.syntax._
import play.api.libs.json._
import play.api.mvc.{Result, Results}
import xivo.model.MeetingRoomError.MeetingRoomErrorType
import xivo.model.RoomTypes.RoomType

import java.sql.Timestamp
import java.time.Instant
import java.util.UUID
import scala.util.{Failure, Success, Try}

object RoomTypes extends Enumeration {
  type RoomType = Value

  val Static   = Value("static")
  val Personal = Value("personal")
  val All      = Value("all")

  implicit val format: Format[RoomType] = new Format[RoomType] {
    def writes(r: RoomType): JsValue = JsString(r.toString)
    def reads(json: JsValue): JsResult[RoomType] =
      JsSuccess(RoomTypes.withName(json.as[String]))
  }

  def validate(
      json: JsValue,
      roomType: RoomType
  ): JsResult[MeetingRoomConfig] = {
    val rType    = roomType.toString
    val jsonRoom = json \ "roomType"
    jsonRoom.as[String] match {
      case `rType` => json.validate[MeetingRoomConfig]
      case _       => JsError(s"Not allowed room type for this endpoint")
    }
  }
}

case class MeetingRoomConfig(
    id: Option[Long],
    displayName: String,
    number: Option[String],
    userPin: Option[String],
    uuid: Option[UUID],
    roomType: RoomType,
    userId: Option[Long],
    pinTimestamp: Option[Timestamp] = None,
    alias: Option[String] = None
)

object MeetingRoomConfig {
  implicit val meetingRoomReads: Reads[MeetingRoomConfig] = (
    (JsPath \ "id").readNullable[Long] and
      (JsPath \ "displayName").read[String] and
      (JsPath \ "number")
        .readNullable[String]
        .filterNot(n => n.isDefined && n.get.isEmpty) and
      (JsPath \ "userPin").readNullable[String] and
      (JsPath \ "roomType")
        .readWithDefault[RoomType](RoomTypes.Static)
        .filterNot(t => t == RoomTypes.All)
  )((id, displayName, number, userPin, roomType) =>
    MeetingRoomConfig.apply(
      id,
      displayName,
      number,
      userPin match {
        case Some("") => None
        case _        => userPin
      },
      None,
      roomType,
      None
    )
  )

  implicit val meetingRoomWrites: Writes[MeetingRoomConfig] =
    Json.writes[MeetingRoomConfig]
}

case class MeetingRoomTokenResponse(uuid: UUID, token: String)
object MeetingRoomTokenResponse {
  implicit val meetingRoomReads: Reads[MeetingRoomTokenResponse] =
    Json.reads[MeetingRoomTokenResponse]
  implicit val meetingRoomWrites: Writes[MeetingRoomTokenResponse] =
    Json.writes[MeetingRoomTokenResponse]
}

case class MeetingRoomAliasResponse(alias: Option[String])
object MeetingRoomAliasResponse {
  implicit val meetingRoomReads: Reads[MeetingRoomAliasResponse] =
    Json.reads[MeetingRoomAliasResponse]
  implicit val meetingRoomWrites: Writes[MeetingRoomAliasResponse] =
    Json.writes[MeetingRoomAliasResponse]
}

case class MeetingRoomTokenContent(
    displayName: String,
    number: Option[String],
    uuid: UUID,
    requirepin: Boolean,
    timestamp: Option[Timestamp],
    expiry: Option[Timestamp] = None,
    temporary: Boolean = false
) {
  def isExpired: Try[MeetingRoomTokenContent] = {
    expiry match {
      case Some(e) =>
        if (e.after(Timestamp.from(Instant.now)))
          Success(this)
        else
          Failure(
            MeetingRoomException(
              MeetingRoomError.TokenTimestampExpired,
              "Unable to validate room access, token timestamp has expired"
            )
          )
      case None => Success(this)
    }
  }
}

object MeetingRoomTokenContent {
  implicit val timestampReads: Reads[Timestamp] = {
    implicitly[Reads[Long]].map { l =>
      val t = new Timestamp(l)
      t.setNanos(0)
      t
    }
  }
  implicit val timestampWrites: Writes[Timestamp] = {
    implicitly[Writes[Long]].contramap(_.getTime)
  }
  implicit val meetingRoomTokenContentReads: Reads[MeetingRoomTokenContent] = (
    (JsPath \ "roomdisplayname").read[String] and
      (JsPath \ "roomnumber").readNullable[String] and
      (JsPath \ "roomuuid").read[String].map(UUID.fromString) and
      (JsPath \ "requirepin").read[Boolean] and
      (JsPath \ "timestamp").readNullable[Timestamp] and
      (JsPath \ "expiry").readNullable[Timestamp] and
      (JsPath \ "temporary").read[Boolean]
  )(MeetingRoomTokenContent.apply _)

  implicit val meetingRoomTokenContentWrites: Writes[MeetingRoomTokenContent] =
    Json.writes[MeetingRoomTokenContent]
}

case class FindMeetingRoom(
    filters: List[DynamicFilter],
    offset: Int = 0,
    limit: Int = 100
)

object FindMeetingRoom {
  implicit val reads: Reads[FindMeetingRoom] =
    Json.reads[FindMeetingRoom]
  implicit val writes: Writes[FindMeetingRoom] =
    Json.writes[FindMeetingRoom]
}

case class FindMeetingRoomResult(total: Long, list: List[MeetingRoomConfig])

object FindMeetingRoomResult {
  implicit val reads: Reads[FindMeetingRoomResult] =
    Json.reads[FindMeetingRoomResult]
  implicit val writes: Writes[FindMeetingRoomResult] =
    Json.writes[FindMeetingRoomResult]
}

case class MeetingRoomError(error: ErrorType, message: String)
    extends ErrorResult {
  override def toResult: Result = {
    log.error(this.message)
    val body = Json.toJson(this)
    error match {
      case MeetingRoomError.Invalid               => Results.BadRequest(body)
      case MeetingRoomError.Duplicate             => Results.BadRequest(body)
      case MeetingRoomError.NotFound              => Results.NotFound(body)
      case MeetingRoomError.InvalidToken          => Results.Unauthorized(body)
      case MeetingRoomError.NotFoundToken         => Results.Unauthorized(body)
      case MeetingRoomError.InvalidPin            => Results.Unauthorized(body)
      case MeetingRoomError.PinTimestampExpired   => Results.Unauthorized(body)
      case MeetingRoomError.TokenTimestampExpired => Results.Unauthorized(body)
      case _                                      => Results.InternalServerError(body)
    }
  }
}

case class MeetingRoomException(error: MeetingRoomErrorType, message: String)
    extends Exception(message)

object MeetingRoomError {
  implicit val meetingRoomErrorWrites: Writes[MeetingRoomError] =
    Json.writes[MeetingRoomError]

  trait MeetingRoomErrorType extends ErrorType

  case object Invalid extends MeetingRoomErrorType {
    val error = "Invalid"
  }

  case object Duplicate extends MeetingRoomErrorType {
    val error = "Duplicate"
  }

  case object NotFound extends MeetingRoomErrorType {
    val error = "NotFound"
  }

  case object InvalidToken extends MeetingRoomErrorType {
    val error = "Invalid Token"
  }

  case object NotFoundToken extends MeetingRoomErrorType {
    val error = "Token Not Found"
  }

  case object NumberNotAvailable extends MeetingRoomErrorType {
    val error = "Number is not available"
  }

  case object InvalidPin extends MeetingRoomErrorType {
    val error = "Invalid Pin"
  }

  case object Timestamp extends MeetingRoomErrorType {
    val error = "Timestamp not set"
  }

  case object PinTimestampExpired extends MeetingRoomErrorType {
    val error = "PIN expired"
  }

  case object TokenTimestampExpired extends MeetingRoomErrorType {
    val error = "Token expired"
  }
}
