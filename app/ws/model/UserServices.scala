package ws.model

import play.api.libs.json.Reads
import play.api.libs.json.Json
import play.api.libs.json.Writes

case class UserForward(enabled: Boolean, destination: String)

case class UserServices(
    dndEnabled: Boolean,
    busy: UserForward,
    noanswer: UserForward,
    unconditional: UserForward
) {
  def update(partial: PartialUserServices): UserServices = {
    this.copy(
      dndEnabled = partial.dndEnabled.getOrElse(dndEnabled),
      busy = partial.busy.getOrElse(busy),
      noanswer = partial.noanswer.getOrElse(noanswer),
      unconditional = partial.unconditional.getOrElse(unconditional)
    )
  }

}

case class PartialUserServices(
    dndEnabled: Option[Boolean],
    busy: Option[UserForward],
    noanswer: Option[UserForward],
    unconditional: Option[UserForward]
)

object UserForward {
  implicit val reads: Reads[UserForward]   = Json.reads[UserForward]
  implicit val writes: Writes[UserForward] = Json.writes[UserForward]
}

object UserServices {
  implicit val reads: Reads[UserServices]   = Json.reads[UserServices]
  implicit val writes: Writes[UserServices] = Json.writes[UserServices]
}

object PartialUserServices {
  implicit val reads: Reads[PartialUserServices] =
    Json.reads[PartialUserServices]
  implicit val writes: Writes[PartialUserServices] =
    Json.writes[PartialUserServices]
}
