package ws.controllers

import java.sql.SQLException

import javax.inject.Inject
import model.ws.{GenericError, JsonParsingError, NotHandledError}
import org.slf4j.{Logger, LoggerFactory}
import play.api.Configuration
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.libs.ws.WSClient
import play.api.mvc.{AbstractController, ControllerComponents}
import xc.service.XivoWebService
import xivo.model._
import xivo.service.MediaServerManager
import scala.util.{Failure, Success}
import scala.util.matching.Regex

import controllers.Secured

class MediaServer @Inject() (
    mediaServerMgr: MediaServerManager,
    cc: ControllerComponents,
    wSClient: WSClient,
    configuration: Configuration,
    secured: Secured
) extends AbstractController(cc)
    with WithExceptionCatching {

  val logger: Logger              = LoggerFactory.getLogger(getClass)
  val xivohost: String            = configuration.get[String]("xivohost")
  val xivoWs                      = new XivoWebService(wSClient, xivohost)
  val mediaServerNameRegex: Regex = """[^a-z0-9\.-]""".r

  def createMediaServer =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"Media Servers - $user - Req : <create>")
            request.body.asJson match {
              case Some(json) =>
                json.validate[MediaServerConfigNoId] match {
                  case JsSuccess(msc, _) =>
                    mediaServerNameRegex.findFirstMatchIn(msc.name) match {
                      case Some(_) =>
                        MediaServerError(
                          Invalid,
                          s"Unable to add Media Server, name contains special characters, only dots and hyphens are allowed"
                        ).toResult
                      case None =>
                        mediaServerMgr.create(msc) match {
                          case Success(mds) => Ok(Json.toJson(mds))
                          case Failure(f: SQLException)
                              if f.getSQLState == "23505" => // unique constraint violation
                            MediaServerError(
                              Duplicate,
                              s"${f.getMessage}"
                            ).toResult
                          case Failure(f) =>
                            GenericError(
                              NotHandledError,
                              s"Unable to add Media Server : ${f.getMessage}"
                            ).toResult
                        }
                    }
                  case JsError(_) =>
                    MediaServerError(
                      Invalid,
                      s"Unable to add Media Server, bad JSON"
                    ).toResult
                }
              case None =>
                GenericError(
                  JsonParsingError,
                  s"Unable to add Media Server, no JSON found"
                ).toResult
            }
          }
    )

  def deleteMediaServer(id: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"Media Servers - $user - Req : <delete>")
            mediaServerMgr.delete(id) match {
              case Success(mds) => Ok(Json.toJson(mds))
              case Failure(f) =>
                GenericError(
                  NotHandledError,
                  s"Unable to delete Media Server : ${f.getMessage}"
                ).toResult
            }
          }
    )

  def getMediaServer(id: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"Media Servers - $user - Req : <get>")
            mediaServerMgr.get(id) match {
              case Success(mds) => Ok(Json.toJson(mds))
              case Failure(f) =>
                GenericError(
                  NotHandledError,
                  s"Unable to get Media Server : ${f.getMessage}"
                ).toResult
            }
          }
    )

  def allMediaServer() =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"Media Servers - $user - Req : <getList>")
            mediaServerMgr.all() match {
              case Success(mdsList) => Ok(Json.toJson(mdsList.list))
              case Failure(f) =>
                GenericError(
                  NotHandledError,
                  s"Unable to get Media Server : ${f.getMessage}"
                ).toResult
            }
          }
    )

  def mediaServerLinesCount() =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(
              s"Media Servers - $user - Req : <getListWithLinesCount>"
            )
            mediaServerMgr.linesCount() match {
              case Success(mdsList) => Ok(Json.toJson(mdsList.list))
              case Failure(f) =>
                GenericError(
                  NotHandledError,
                  s"Unable to get Media Server : ${f.getMessage}"
                ).toResult
            }
          }
    )

  def updateMediaServer() =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"Media Servers - $user - Req : <update>")
            request.body.asJson match {
              case Some(json) =>
                json.validate[MediaServerConfig] match {
                  case JsSuccess(msc, _) =>
                    mediaServerMgr.update(msc) match {
                      case Success(mds) => Ok(Json.toJson(mds))
                      case Failure(f) =>
                        GenericError(
                          NotHandledError,
                          s"Unable to update Media Server : ${f.getMessage}"
                        ).toResult
                    }
                  case JsError(_) =>
                    MediaServerError(
                      Invalid,
                      s"Unable to update Media Server, bad JSON"
                    ).toResult
                }
              case None =>
                GenericError(
                  JsonParsingError,
                  s"Unable to update Media Server, no JSON found"
                ).toResult
            }
          }
    )
}
