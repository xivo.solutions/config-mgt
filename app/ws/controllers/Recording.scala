package ws.controllers

import java.util.NoSuchElementException

import controllers.Secured
import javax.inject.Inject
import model.ws.{GenericError, JsonParsingError, NotHandledError}
import model._
import org.slf4j.{Logger, LoggerFactory}
import play.api.Configuration
import play.api.libs.json.{JsError, JsSuccess}
import play.api.libs.ws.WSClient
import play.api.mvc.{InjectedController, Result}
import xc.service.XivoWebService
import xivo.service.RecordingManager

import scala.util.{Failure, Success, Try}

class Recording @Inject() (
    recordingManager: RecordingManager,
    wSClient: WSClient,
    configuration: Configuration,
    secured: Secured
) extends InjectedController
    with WithExceptionCatching {

  val logger: Logger   = LoggerFactory.getLogger(getClass)
  val xivohost: String = configuration.get[String]("xivohost")

  val xivoWs = new XivoWebService(wSClient, xivohost)

  def handleRecordingResult(
      resultTry: Try[RecordingResult],
      action: String,
      errMsg: String = ""
  ): Result = {
    resultTry match {
      case Success(rec) => rec.toResult
      case Failure(_: NoSuchElementException) =>
        RecordingError(
          QueueNotFound,
          s"Unable to perform $action $errMsg"
        ).toResult
      case Failure(f) =>
        GenericError(
          NotHandledError,
          s"Unable to perform $action $errMsg: ${f.getMessage}"
        ).toResult
    }
  }

  def getRecordingModeToApply(queueId: Long) =
    Action {
      val action = "getRecordingModeToApply"
      logger.info(s"Req : <$action> for queueId $queueId")

      handleRecordingResult(
        recordingManager.computeRecordingModeToApply(queueId),
        action,
        s"for queueId $queueId"
      )
    }

  def getRecordingStatus() =
    secured.WithAuthenticatedUser(
      None,
      user =>
        _ =>
          withExceptionCatching {
            val action = "getRecordingStatus"
            logger.info(s"$user - Req : <$action>")

            handleRecordingResult(
              recordingManager.getRecordingQueuesStatus,
              action
            )
          }
    )

  def setRecordingStatus(state: String) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        _ =>
          withExceptionCatching {
            val action = "setRecordingStatus"
            logger.info(s"$user - Req : <$action>")

            handleRecordingResult(
              recordingManager.setRecordingQueuesStatus(state == "on"),
              action
            )
          }
    )

  def getRecordingConfig(queueId: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        _ =>
          withExceptionCatching {
            val action = "getRecordingConfig"
            logger.info(s"$user - Req : <$action>")

            handleRecordingResult(
              recordingManager.getRecordingConfig(queueId),
              action
            )
          }
    )

  def setRecordingConfig(queueId: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            val action = "setRecordingConfig"
            logger.info(s"$user - Req : <$action>")

            request.body.asJson match {
              case None =>
                GenericError(
                  JsonParsingError,
                  s"Unable to $action for queueId $queueId, no JSON found"
                ).toResult
              case Some(json) =>
                json.validate[RecordingConfig] match {
                  case JsSuccess(res: RecordingConfig, _) =>
                    handleRecordingResult(
                      recordingManager.setRecordingConfig(queueId, res),
                      action,
                      s"for queueId $queueId"
                    )
                  case JsError(e) =>
                    GenericError(
                      JsonParsingError,
                      s"Unable to $action for queueId $queueId, JSON is not formatted properly"
                    ).toResult
                }
            }
          }
    )
}
