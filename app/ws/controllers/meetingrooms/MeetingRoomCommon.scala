package ws.controllers.meetingrooms

import anorm.AnormException
import controllers.Secured
import model.DynamicFilter
import model.ws.{ErrorResult, GenericError, JsonParsingError, NotHandledError}
import org.postgresql.util.PSQLState.UNIQUE_VIOLATION
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc._
import ws.controllers.WithExceptionCatching
import xivo.model.RoomTypes.RoomType
import xivo.model.{FindMeetingRoom, MeetingRoomError, RoomTypes}
import xivo.service.MeetingRoomManager
import xivo.service.MeetingRoomManager.{
  createPersonalUserIdFilter,
  createRoomFilter
}

import java.sql.SQLException
import javax.inject.Inject
import scala.util.{Failure, Success}

class MeetingRoomCommon @Inject() (
    meetingRoomManager: MeetingRoomManager,
    secured: Secured,
    cc: ControllerComponents
) extends AbstractController(cc)
    with WithExceptionCatching {

  val logger: Logger = LoggerFactory.getLogger(getClass)

  def meetingRoomGetAll(filters: List[DynamicFilter]): Result = {
    meetingRoomManager.getAll(filters) match {
      case Failure(f) =>
        GenericError(
          NotHandledError,
          s"Unable to get meeting rooms, ${f.getMessage}"
        ).toResult
      case Success(rooms) =>
        Ok(Json.toJson(rooms))
    }
  }

  def meetingRoomGet(id: Long, filters: List[DynamicFilter]): Result = {
    meetingRoomManager.get(filters) match {
      case Failure(f: AnormException) =>
        MeetingRoomError(
          MeetingRoomError.NotFound,
          s"Meeting room with id $id not found, ${f.getMessage()}"
        ).toResult
      case Failure(f) =>
        GenericError(
          NotHandledError,
          s"Unable to get meeting room with id $id, ${f.getMessage}"
        ).toResult
      case Success(room) =>
        Ok(Json.toJson(room))
    }
  }

  def meetingRoomCreate(request: Request[AnyContent], userId: Option[Long])(
      implicit roomType: RoomType
  ) = {
    request.body.asJson
      .map { json =>
        RoomTypes
          .validate(json, roomType)
          .fold(
            invalid =>
              MeetingRoomError(
                MeetingRoomError.Invalid,
                s"Unable to create meeting room, bad JSON: $invalid"
              ).toResult,
            { room =>
              meetingRoomManager
                .create(room.copy(userId = userId)) match {
                case Success(room) => Ok(Json.toJson(room))
                case Failure(f: SQLException)
                    if f.getSQLState == UNIQUE_VIOLATION.getState =>
                  MeetingRoomError(
                    MeetingRoomError.Duplicate,
                    s"${f.getMessage}"
                  ).toResult
                case Failure(f) =>
                  GenericError(
                    NotHandledError,
                    s"Unable to create meeting room : ${f.getMessage}"
                  ).toResult
              }
            }
          )
      }
      .getOrElse(
        GenericError(
          JsonParsingError,
          s"Unable to create meeting room, no JSON found"
        ).toResult
      )
  }

  def meetingRoomUpdate(request: Request[AnyContent], userId: Option[Long])(
      implicit roomType: RoomType
  ) = {
    request.body.asJson
      .map { json =>
        RoomTypes
          .validate(json, roomType)
          .fold(
            invalid =>
              MeetingRoomError(
                MeetingRoomError.Invalid,
                s"Unable to update meeting room, bad JSON: $invalid"
              ).toResult,
            { room =>
              meetingRoomManager.update(
                room.copy(userId = userId)
              ) match {
                case Success(room) => Ok(Json.toJson(room))
                case Failure(f: SQLException)
                    if f.getSQLState == UNIQUE_VIOLATION.getState =>
                  MeetingRoomError(
                    MeetingRoomError.Duplicate,
                    s"${f.getMessage}"
                  ).toResult
                case Failure(f) =>
                  GenericError(
                    NotHandledError,
                    s"Unable to update meeting room : ${f.getMessage}"
                  ).toResult
              }
            }
          )
      }
      .getOrElse(
        GenericError(
          JsonParsingError,
          s"Unable to update meeting room, no JSON found"
        ).toResult
      )
  }

  def meetingRoomDelete(id: Long, filters: List[DynamicFilter]) = {
    meetingRoomManager.delete(filters) match {
      case Failure(f: AnormException) =>
        MeetingRoomError(
          MeetingRoomError.NotFound,
          s"Meeting room with $id not found, ${f.getMessage()}"
        ).toResult
      case Failure(f) =>
        GenericError(
          NotHandledError,
          s"Unable to delete meeting room with id $id, ${f.getMessage}"
        ).toResult
      case Success(room) =>
        Ok(Json.toJson(room))
    }
  }

  def meetingRoomFind(
      request: Request[AnyContent],
      filters: List[DynamicFilter],
      userId: Option[Long]
  ) = {
    request.body.asJson match {
      case None =>
        GenericError(
          JsonParsingError,
          s"No JSON found in body"
        ).toResult
      case Some(json) =>
        json.validate[FindMeetingRoom] match {
          case JsSuccess(f, _) =>
            val allFilters = f.filters ++ filters
            logger.info(
              s"$userId Meeting rooms Req : <find> with filters $allFilters offset ${f.offset} and limit ${f.limit}"
            )
            meetingRoomManager
              .find(
                allFilters,
                f.offset,
                f.limit
              ) match {
              case Success(rooms) =>
                Ok(Json.toJson(rooms))
              case Failure(f) =>
                GenericError(
                  NotHandledError,
                  s"Unable to find meeting rooms, $f"
                ).toResult
            }
          case JsError(e) =>
            GenericError(
              JsonParsingError,
              s"Unable to parse find meeting rooms request, $e"
            ).toResult
        }
    }
  }

  def genericSimple[T](
      userIdRequired: Boolean,
      actionType: String,
      failure: ErrorResult,
      dbAction: Request[AnyContent] => List[DynamicFilter] => Option[
        Long
      ] => Result,
      extraFilters: List[DynamicFilter]
  )(implicit roomType: RoomType): EssentialAction = {
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(
              s"$user - Meeting rooms ($roomType) Req : <$actionType>"
            )
            withUserId(
              request,
              userIdRequired,
              failure,
              { userId =>
                userId
                  .filter(_ => userIdRequired)
                  .fold[Result]({
                    val filters = createRoomFilter(roomType) ++ extraFilters
                    dbAction(request)(filters)(None)
                  })({ id =>
                    val filters = createRoomFilter(
                      roomType
                    ) ++ createPersonalUserIdFilter(id) ++ extraFilters
                    dbAction(request)(filters)(Some(id))
                  })
              }
            )
          }
    )
  }

  def withUserId(
      request: Request[AnyContent],
      userIdRequired: Boolean,
      failure: ErrorResult,
      f: Option[Long] => Result
  ): Result = {
    val userId: Option[String] =
      request.queryString.get("userId").flatMap(_.headOption)
    if (userIdRequired) {
      userId match {
        case Some(id) => f(Some(id.toLong))
        case None     => failure.toResult
      }
    } else
      f(userId.map(_.toLong))
  }
}
