package ws.controllers

import java.security.InvalidParameterException
import org.slf4j.LoggerFactory
import play.api.libs.json.Json
import play.api.mvc.{Result, Results}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

trait WithExceptionCatching extends Results {
  private val logger = LoggerFactory.getLogger(getClass)

  def withExceptionCatching(f: => Result): Result =
    try {
      f
    } catch {
      case e: InvalidParameterException => BadRequest(Json.toJson(e.getMessage))
      case e: NoSuchElementException    => NotFound(Json.toJson(e.getMessage))
      case e: Exception =>
        logger.error("Unhandled exception caught", e)
        InternalServerError(Json.toJson(e.getMessage))
    }

  def withExceptionCatchingAsync(f: => Future[Result]): Future[Result] = {
    f.map(resolved =>
      try {
        resolved
      } catch {
        case e: InvalidParameterException =>
          BadRequest(Json.toJson(e.getMessage))
        case e: NoSuchElementException => NotFound(Json.toJson(e.getMessage))
        case e: Exception =>
          logger.error("Unhandled exception caught", e)
          InternalServerError(Json.toJson(e.getMessage))
      }
    )
  }
}
