package ws.controllers

import anorm.AnormException
import controllers.Secured
import javax.inject.Inject
import model.ws.{GenericError, JsonParsingError, NotHandledError}
import org.slf4j.{Logger, LoggerFactory}
import play.api.Configuration
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc.{
  AbstractController,
  ControllerComponents,
  EssentialAction,
  Result
}
import xivo.model.{QueueFeatureError, QueueFeatureNotFound, _}
import xivo.service.{
  FileSystemManager,
  QueueDissuasionManager,
  QueueFeatureManager
}

import scala.util.{Failure, Success}

class QueueDissuasion @Inject() (
    secured: Secured,
    cc: ControllerComponents,
    queueDissuasionManager: QueueDissuasionManager,
    queueFeatureManager: QueueFeatureManager,
    fileSystemManager: FileSystemManager,
    configuration: Configuration
) extends AbstractController(cc)
    with WithExceptionCatching {

  val logger: Logger   = LoggerFactory.getLogger(getClass)
  val playbackFilePath = "/var/lib/xivo/sounds/playback/"

  def getQueueDissuasion(id: Long): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      _ =>
        _ =>
          withExceptionCatching {

            val action = "getQueueDissuasion"
            logger.info(s"QueueDissuasion - Req: <$action> - id: <$id>")

            def handleGetQueueDissuasion(name: String): Result = {
              queueDissuasionManager.get(id) match {
                case Success(elem) =>
                  elem match {
                    case Some(qdsf: QueueDissuasionSoundFile) =>
                      Ok(
                        Json.toJson(
                          QueueDissuasionSoundFileSelected(id, name, qdsf)
                        )
                      )
                    case Some(qdq: QueueDissuasionQueue) =>
                      Ok(
                        Json.toJson(QueueDissuasionQueueSelected(id, name, qdq))
                      )
                    case Some(something) =>
                      GenericError(
                        NotHandledError,
                        s"Unable to perform $action on queue $id: $something is not a QueueDissuasionSoundFile nor a QueueDissuasionQueue."
                      ).toResult
                    case None =>
                      Ok(Json.toJson(QueueDissuasionOtherSelected(id, name)))
                  }
                case Failure(f) =>
                  GenericError(
                    NotHandledError,
                    s"Unable to perform $action on queue $id: ${f.getMessage}"
                  ).toResult
              }
            }

            getQueueName(id, handleGetQueueDissuasion, action)
          }
    )

  def setQueueDissuasion(id: Long): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      _ =>
        request =>
          withExceptionCatching {
            val action = "setQueueDissuasion"
            logger.info(s"QueueDissuasion - Req: <$action> - id: <$id>")

            def handleGetQueueName(
                queueName: String,
                queueDissuasionSoundFile: QueueDissuasionSoundFile
            ): Result = {
              if (
                fileSystemManager.doesFileExist(
                  playbackFilePath,
                  queueDissuasionSoundFile.soundFile
                )
              ) {
                queueDissuasionManager.dissuasionAccessToSoundFile(
                  id,
                  playbackFilePath + queueDissuasionSoundFile.soundFile
                ) match {
                  case Success(updatedValue) => Ok(Json.toJson(updatedValue))
                  case Failure(f) =>
                    GenericError(
                      NotHandledError,
                      s"Unable to perform $action on queue $id: ${f.getMessage}"
                    ).toResult
                }
              } else {
                QueueDissuasionError(
                  DissuasionFileNotFound,
                  s"Unable to do $action on queue $id: the file ${queueDissuasionSoundFile.soundFile} was not found."
                ).toResult
              }
            }

            request.body.asJson match {
              case Some(json) =>
                json.validate[QueueDissuasionSoundFile] match {
                  case JsSuccess(queueDissuasionSoundFile, _) =>
                    getQueueName(
                      id,
                      (queueName: String) =>
                        handleGetQueueName(queueName, queueDissuasionSoundFile),
                      action
                    )
                  case JsError(_) =>
                    QueueDissuasionError(
                      InvalidDissuasionParameters,
                      s"Unable to do $action on queue $id, bad JSON : $json"
                    ).toResult
                }
              case None =>
                GenericError(
                  JsonParsingError,
                  s"Unable to to do $action on queue $id, no valid JSON found"
                ).toResult
            }
          }
    )

  def setQueueDissuasionToOtherQueue(id: Long): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      _ =>
        request =>
          withExceptionCatching {
            val action = "setQueueDissuasionToOtherQueue"
            logger.info(s"QueueDissuasion - Req: <$action> - id: <$id>")

            def handleGetQueueName(
                queueName: String,
                queueDissuasionQueue: QueueDissuasionQueue
            ): Result = {
              val otherQueueName = queueDissuasionQueue.queueName
              queueFeatureManager.getQueueIdByName(otherQueueName) match {
                case Some(otherQueueId) =>
                  queueDissuasionManager.dissuasionAccessToOtherQueue(
                    id,
                    otherQueueId,
                    otherQueueName
                  ) match {
                    case Success(updatedValue) => Ok(Json.toJson(updatedValue))
                    case Failure(e: RedirectQueueDissuasionToItselfException) =>
                      QueueFeatureError(
                        RedirectQueueDissuasionToItself,
                        s"Unable to perform $action on queue $id - ${e.getMessage}"
                      ).toResult
                    case Failure(f) =>
                      GenericError(
                        NotHandledError,
                        s"Unable to perform $action on queue $id: ${f.getMessage}"
                      ).toResult
                  }
                case None =>
                  QueueFeatureError(
                    QueueFeatureNotFound,
                    s"Unable to perform $action on queue $id - Could not find the queue $otherQueueName -  ${QueueFeatureNotFound.error}"
                  ).toResult
              }
            }
            request.body.asJson match {
              case Some(json) =>
                json.validate[QueueDissuasionQueue] match {
                  case JsSuccess(queueDissuasionQueue, _) =>
                    getQueueName(
                      id,
                      (queueName: String) =>
                        handleGetQueueName(queueName, queueDissuasionQueue),
                      action
                    )
                  case JsError(_) =>
                    QueueDissuasionError(
                      InvalidDissuasionParameters,
                      s"Unable to do $action on queue $id, bad JSON : $json"
                    ).toResult
                }
              case None =>
                GenericError(
                  JsonParsingError,
                  s"Unable to to do $action on queue $id, no valid JSON found"
                ).toResult
            }
          }
    )

  def getQueueDissuasionList(id: Long): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      _ =>
        _ =>
          withExceptionCatching {

            val action = "getQueueDissuasionList"
            logger.info(s"QueueDissuasionList - Req: <$action> - id: <$id>")

            def handleGetQueueDissuasionList(name: String): Result =
              Ok(
                Json.toJson(
                  getListOfQueueDissuasion(
                    id,
                    fileSystemManager.getListOfFiles(playbackFilePath),
                    name
                  )
                )
              )

            getQueueName(id, handleGetQueueDissuasionList, action)
          }
    )

  def getQueueName(
      id: Long,
      callback: String => Result,
      action: String
  ): Result =
    queueFeatureManager.get(id).map(_.name) match {
      case Success(value) => callback(value)
      case Failure(_: AnormException) =>
        QueueFeatureError(
          QueueFeatureNotFound,
          s"Unable to perform $action on queue $id - ${QueueFeatureNotFound.error}"
        ).toResult
      case Failure(f) =>
        GenericError(
          NotHandledError,
          s"Unable to perform $action on queue $id: ${f.getMessage}"
        ).toResult
    }

  def getListOfQueueDissuasion(
      id: Long,
      files: List[String],
      name: String
  ): QueueDissuasionList = {
    val soundFiles: List[QueueDissuasionSoundFile] = files
      .filter(QueueDissuasion.isQueueSoundFile(_, name))
      .map(fileName => {
        QueueDissuasionSoundFile(
          None,
          QueueDissuasion.removeExtension(fileName)
        )
      })

    val defaultQueues: List[QueueDissuasionQueue] = {
      val defaultQueue = configuration
        .getOptional[String]("defaultQueueDissuasion")
        .getOrElse("")
      if (defaultQueue != "") {
        List(QueueDissuasionQueue(None, defaultQueue))
      } else {
        List()
      }
    }

    QueueDissuasionList(id, name, soundFiles, defaultQueues)
  }

}

object QueueDissuasion {
  def getFileName(path: String): String =
    path.substring(path.lastIndexOf("/") + 1)

  def isQueueSoundFile(fileName: String, queueName: String): Boolean =
    fileName.startsWith(queueName + "_")

  def removeExtension(fileName: String): String = {
    if (fileName.lastIndexOf(".") != -1) {
      fileName.substring(0, fileName.lastIndexOf("."))
    } else {
      fileName
    }
  }
}
