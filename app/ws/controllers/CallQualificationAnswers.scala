package ws.controllers

import controllers.Secured
import javax.inject.Inject
import org.slf4j.LoggerFactory
import play.api.Configuration
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc.InjectedController
import xc.service.{CallQualificationAnswerManager, XivoWebService}
import play.api.libs.ws.WSClient
import xc.model.CallQualificationAnswer

import scala.util.{Failure, Success}

class CallQualificationAnswers @Inject() (
    callQualificationAnswerManager: CallQualificationAnswerManager,
    wSClient: WSClient,
    configuration: Configuration,
    secured: Secured
) extends InjectedController
    with WithExceptionCatching {

  val logger = LoggerFactory.getLogger(getClass)

  val xivohost: String = configuration.get[String]("xivohost")

  val xivoWs = new XivoWebService(wSClient, xivohost)

  def getAll =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            import xc.model.CallQualificationAnswer._
            logger.info(s"$user - Req : <getAll>")

            callQualificationAnswerManager.all() match {
              case Success(qualifList) => Ok(Json.toJson(qualifList))
              case Failure(f) =>
                InternalServerError(s"Unable to get qualifications. $f")
            }
          }
    )

  def get(id: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            import xc.model.CallQualificationAnswer._
            logger.info(s"$user - Req : <get> $id")

            callQualificationAnswerManager.all(id) match {
              case Success(qualifList) => Ok(Json.toJson(qualifList))
              case Failure(f) =>
                InternalServerError(
                  s"Unable to get qualifications for queue id: $id. $f"
                )
            }
          }
    )

  def exportTickets(queueId: Long, from: String, to: String) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Req : <exportTickets> queueId: $queueId")

            callQualificationAnswerManager.allByQueue(queueId, from, to) match {
              case Success(qualifList) =>
                Ok(CallQualificationAnswer.toCsv(qualifList))
                  .as("text/csv")
                  .withHeaders(
                    "Content-Disposition" -> s"attachment;filename=$queueId-$from-$to.csv"
                  )
              case Failure(f) =>
                InternalServerError(
                  s"Unable to get qualifications for queue id: $queueId. Please contact your administrator. \n$f"
                )
            }
          }
    )

  def create =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Req : <create>")

            request.body.asJson match {
              case Some(json) =>
                json.validate[CallQualificationAnswer] match {
                  case JsSuccess(callQualificationAnswer, _) =>
                    callQualificationAnswerManager.create(
                      callQualificationAnswer
                    ) match {
                      case Success(id) => Ok(Json.toJson(id))
                      case Failure(f) =>
                        InternalServerError(
                          s"Unable to create qualification answer. $f"
                        )
                    }
                  case JsError(e) => BadRequest(s"Error decoding JSON: $e")
                }
              case None => BadRequest("No JSON could be decoded")
            }
          }
    )

  def delete(id: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Req : <delete> $id")

            callQualificationAnswerManager.delete(id) match {
              case Success(nb) => Ok(Json.toJson(nb))
              case Failure(f) =>
                InternalServerError(
                  s"Unable to delete qualifications with id: $id. $f"
                )
            }
          }
    )

  def update(id: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Req : <update> $id")

            request.body.asJson match {
              case Some(json) =>
                json.validate[CallQualificationAnswer] match {
                  case JsSuccess(callQualificationAnswer, _) =>
                    callQualificationAnswerManager
                      .update(id, callQualificationAnswer) match {
                      case Success(nb) => Ok(Json.toJson(nb))
                      case Failure(f) =>
                        InternalServerError(
                          s"Unable to update qualifications with id: $id. $f"
                        )
                    }
                  case JsError(e) => BadRequest(s"Error decoding JSON: $e")
                }
              case None => BadRequest("No JSON could be decoded")
            }
          }
    )
}
