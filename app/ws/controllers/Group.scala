package ws.controllers

import com.google.inject.Inject
import controllers.Secured
import model.ws.{ErrorResult, GenericError, JsonParsingError}
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.{Json, Reads}
import play.api.libs.ws.WSResponse
import play.api.mvc._
import xivo.model._
import xivo.service.{
  DialActionManager,
  GroupManager,
  GroupNotifier,
  QueueMemberManager,
  SysConfdClient
}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Left, Right, Success}

class Group @Inject() (
    secured: Secured,
    groupMgr: GroupManager,
    dialActionMgr: DialActionManager,
    queueMemberMgr: QueueMemberManager,
    sysConfd: SysConfdClient,
    notifier: GroupNotifier
) extends InjectedController
    with WithExceptionCatching {

  val log: Logger = LoggerFactory.getLogger(getClass)

  def getAllGroups: EssentialAction = handleCRUDGroupAction(GetAllGroupsAction)

  def getGroupById(groupId: Long): EssentialAction = handleCRUDGroupAction(
    GetGroupAction(groupId)
  )

  def createGroup(): EssentialAction = handleCRUDGroupAction(CreateGroupAction)

  def updateGroup(
      groupId: Long
  ): EssentialAction = handleCRUDGroupAction(UpdateGroupAction(groupId))

  def deleteGroup(groupId: Long): EssentialAction = handleCRUDGroupAction(
    DeleteGroupAction(groupId)
  )

  def addUserToGroup(groupId: Long, userId: Long): EssentialAction =
    addOrRemoveUserFromGroup(AddUser, groupId, userId)

  def removeUserFromGroup(groupId: Long, userId: Long): EssentialAction =
    addOrRemoveUserFromGroup(RemoveUser, groupId, userId)

  private def handleCRUDGroupAction(
      action: GroupAction
  ): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            log.info(
              s"Groups - $user - Req : $action"
            )
            (action match {
              case GetAllGroupsAction      => getEnrichedGroups
              case GetGroupAction(groupId) => getEnrichedGroup(groupId)
              case CreateGroupAction =>
                createGroupAndDialAction(request)
              case UpdateGroupAction(groupId) =>
                updateGroupAndDialAction(groupId, request)
              case DeleteGroupAction(groupId) =>
                deleteGroupAndDialAction(groupId)
            }).fold(_.toResult, identity)
          }
    )

  private def getEnrichedGroups: Either[ErrorResult, Result] = {
    def enrichGroup(
        groups: List[xivo.model.Group],
        dialActions: Map[Long, Map[
          DialActionEventType.DialActionEventType,
          DialActionAction
        ]],
        queueMembersMap: Map[Long, List[QueueMember]]
    ): List[EnrichedGroup] =
      groups.map(group => {
        EnrichedGroup(
          group,
          dialActions.getOrElse(group.id, Map.empty),
          Some(queueMembersMap.getOrElse(group.id, List.empty))
        )
      })

    for {
      groups <- groupMgr.getAllGroups
      dialActions <- dialActionMgr
        .getAllDialActions("group")
      queueMembersMap <- queueMemberMgr.getAllQueueMember(
        QueueMemberUserType.User,
        QueueCategory.Group
      )
      enrichedGroups = enrichGroup(groups, dialActions, queueMembersMap)
    } yield Ok(Json.toJson(enrichedGroups))
  }

  def getEnrichedGroup(groupId: Long): Either[ErrorResult, Result] =
    for {
      group       <- groupMgr.getGroupById(groupId)
      dialActions <- dialActionMgr.getDialActions(groupId, "group")
      groupMembers <- queueMemberMgr.getQueueMember(
        QueueMemberUserType.User,
        QueueCategory.Group,
        groupId
      )
      enrichedGroup = EnrichedGroup(group, dialActions, Some(groupMembers))
    } yield Ok(Json.toJson(enrichedGroup))

  private def createGroupAndDialAction(
      request: Request[AnyContent]
  ): Either[ErrorResult, Result] =
    for {
      createGroup <- validateBody[CreateGroup](request)
      group       <- groupMgr.createGroup(createGroup)
      dialActions <- dialActionMgr.createDialActions(
        group.id,
        "group",
        createGroup.dialactions
      )
      enrichedGroup = EnrichedGroup(group, dialActions)
    } yield {
      notifier.onGroupCreated(group.id)
      Created(
        Json.toJson(enrichedGroup)
      )
    }

  private def updateGroupAndDialAction(
      groupId: Long,
      request: Request[AnyContent]
  ): Either[ErrorResult, Result] = {
    def updateGroupDialActions(
        maybeDAs: Option[
          Map[DialActionEventType.DialActionEventType, DialActionAction]
        ],
        group: xivo.model.Group
    ): Either[ErrorResult, EnrichedGroup] =
      maybeDAs
        .map(dAs =>
          dialActionMgr
            .updateDialActions(groupId, "group", dAs)
        )
        .getOrElse(dialActionMgr.getDialActions(group.id, "group"))
        .map(dAs => EnrichedGroup(group, dAs))

    for {
      updateGroup   <- validateBody[UpdateGroup](request)
      group         <- groupMgr.updateGroup(groupId, updateGroup)
      enrichedGroup <- updateGroupDialActions(updateGroup.dialactions, group)
    } yield {
      notifier.onGroupEdited(group.id)
      Ok(Json.toJson(enrichedGroup))
    }
  }

  private def deleteGroupAndDialAction(
      groupId: Long
  ): Either[ErrorResult, Result] =
    for {
      _ <- groupMgr.deleteGroup(groupId)
      _ <- dialActionMgr.deleteDialActions(groupId, "group")
    } yield {
      notifier.onGroupDeleted(groupId)
      NoContent
    }

  private def addOrRemoveUserFromGroup(
      action: GroupMembershipAction,
      groupId: Long,
      userId: Long
  ): EssentialAction = {

    def notifyGroupChangeOnSuccess(
        maybeGroupUpdate: Either[GroupError, WSResponse]
    ): Either[GroupError, WSResponse] = {
      maybeGroupUpdate.map(groupUpdate => {
        notifier.onGroupEdited(groupId)
        groupUpdate
      })
    }

    secured.WithAuthenticatedUserAsync(
      None,
      user =>
        _ =>
          withExceptionCatchingAsync {
            log.info(
              s"Groups - $user - Req : <$action> | for user $userId in group $groupId"
            )
            (action match {
              case AddUser    => groupMgr.addUserToGroup(groupId, userId)
              case RemoveUser => groupMgr.removeUserFromGroup(groupId, userId)
            }).map(_ => updateConfiguration())
              .fold(
                error => Future.successful(Left(error)),
                identity
              )
              .map(notifyGroupChangeOnSuccess)
              .map {
                case Right(_) if action == AddUser => Created
                case Right(_)                      => NoContent
                case Left(groupError)
                    if groupError.error == ConfigurationReloadError =>
                  rollbackAction(action, groupId, userId)
                  groupError.toResult
                case Left(groupError) => groupError.toResult
              }
          }
    )
  }

  private def rollbackAction(
      action: GroupMembershipAction,
      groupId: Long,
      userId: Long
  ): Either[GroupError, Long] =
    if (action == AddUser) {
      groupMgr.removeUserFromGroup(groupId, userId)
    } else {
      groupMgr.addUserToGroup(groupId, userId)
    }

  private def updateConfiguration(): Future[Either[GroupError, WSResponse]] = {
    sysConfd
      .reloadAsteriskQueueConfiguration()
      .transform {
        case Failure(f) =>
          Success(
            Left(
              GroupError(
                ConfigurationReloadError,
                s"Unhandled error happen when reloading asterisk queue configuration : ${f.getMessage}"
              )
            )
          )
        case Success(value) => Success(Right(value))
      }
  }

  private def validateBody[A](
      request: Request[AnyContent]
  )(implicit r: Reads[A]): Either[ErrorResult, A] =
    request.body.asJson
      .toRight(
        GenericError(JsonParsingError, "No JSON could be decoded")
      )
      .flatMap(
        _.validate[A].asEither.left.map(error =>
          GenericError(
            JsonParsingError,
            s"Error decoding JSON : $error"
          )
        )
      )
}
