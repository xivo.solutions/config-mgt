package ws.controllers

import controllers.Secured
import model.ws.{GenericError, JsonParsingError, NotFoundError, NotHandledError}
import model.{DynamicFilter, OperatorEq}
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc.{EssentialAction, InjectedController}
import xivo.model.{FindXivoUserRequest, FindXivoUserResponse, XivoAuthRequest}
import xivo.service.XivoUserManager

import javax.inject.Inject
import scala.util.{Failure, Success}

class XivoUser @Inject() (xivoUserManager: XivoUserManager, secured: Secured)
    extends InjectedController
    with WithExceptionCatching {

  val logger: Logger = LoggerFactory.getLogger(getClass)

  def validateUser =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Xivo user's Req : <validateUser>")
            request.body.asJson match {
              case Some(json) =>
                json.validate[XivoAuthRequest] match {
                  case JsSuccess(res, _) =>
                    xivoUserManager
                      .validateUserPassword(res.username, res.password) match {
                      case Success(_) => NoContent
                      case Failure(f) =>
                        GenericError(
                          NotFoundError,
                          s"User not found, $f"
                        ).toResult
                    }
                  case JsError(e) =>
                    GenericError(
                      JsonParsingError,
                      s"Error decoding JSON: $e"
                    ).toResult
                }
              case None =>
                GenericError(
                  JsonParsingError,
                  s"No JSON found in body"
                ).toResult
            }
          }
    )

  def getAllAsContact: EssentialAction = {
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Xivo user's Req : <getAllAsContact>")

            xivoUserManager.getAllAsContact match {
              case Success(xivoUsers) => Ok(Json.toJson(xivoUsers))
              case Failure(f) =>
                GenericError(
                  NotHandledError,
                  "Unable to get Xivo users, $f"
                ).toResult
            }
          }
    )
  }

  def getUser(username: String): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(
              s"$user - Xivo user's Req : <getUserByUsername> $username"
            )
            xivoUserManager.find(
              List(
                DynamicFilter(
                  "userfeatures.loginclient",
                  Some(OperatorEq),
                  Some(username)
                )
              ),
              0,
              1
            ) match {
              case Success(
                    FindXivoUserResponse(_, firstUser :: _)
                  ) =>
                Ok(Json.toJson(firstUser))
              case Success(_) =>
                GenericError(
                  NotFoundError,
                  s"No XiVO User with username $username"
                ).toResult
              case Failure(f) =>
                GenericError(
                  NotHandledError,
                  s"Unable to get Xivo user with username $username, $f"
                ).toResult
            }
          }
    )

  def getAll =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Xivo user's Req : <getAll>")

            xivoUserManager.getAll match {
              case Success(xivoUsers) => Ok(Json.toJson(xivoUsers))
              case Failure(f) =>
                GenericError(
                  NotHandledError,
                  s"Unable to get Xivo users, $f"
                ).toResult
            }
          }
    )

  def find =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            request.body.asJson match {
              case None =>
                GenericError(
                  JsonParsingError,
                  s"No JSON found in body"
                ).toResult
              case Some(json) =>
                json.validate[FindXivoUserRequest] match {
                  case JsSuccess(f, _) =>
                    logger.info(
                      s"$user - Xivo user's Req : <find> with filters ${f.filters} offset ${f.offset} and limit ${f.limit}"
                    )
                    xivoUserManager.find(f.filters, f.offset, f.limit) match {
                      case Success(foundXivoUsers) =>
                        Ok(Json.toJson(foundXivoUsers))
                      case Failure(f) =>
                        GenericError(
                          NotHandledError,
                          s"Unable to find Xivo users, $f"
                        ).toResult
                    }
                  case JsError(e) =>
                    GenericError(
                      JsonParsingError,
                      s"Unable to parse find Xivo users request, $e"
                    ).toResult
                }
            }
          }
    )
}
