package ws.controllers

import controllers.Secured
import javax.inject.Inject
import org.slf4j.{Logger, LoggerFactory}
import play.api.mvc.InjectedController
import play.api.libs.json.Json
import xivo.service.UserServicesManager
import model.ws.{GenericError, JsonParsingError, NotFoundError}
import scala.util.{Failure, Success}
import ws.model.PartialUserServices
import play.api.libs.json._

class XivoUserServices @Inject() (
    manager: UserServicesManager,
    secured: Secured
) extends InjectedController
    with WithExceptionCatching {

  val logger: Logger = LoggerFactory.getLogger(getClass)

  def get(id: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - User services req: <get> for user Id $id")
            manager.getServices(id) match {
              case Success(services) => Ok(Json.toJson(services))
              case Failure(f) =>
                GenericError(
                  NotFoundError,
                  s"Unable to get services for user $id, $f"
                ).toResult
            }
          }
    )

  def update(id: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - User services req: <update> for user Id $id")
            request.body.asJson match {
              case Some(json) =>
                json.validate[PartialUserServices] match {
                  case JsSuccess(partialServices, _) =>
                    manager
                      .updatePartialServices(id, partialServices)
                      .map(svc => Ok(Json.toJson(svc)))
                      .getOrElse(
                        GenericError(
                          NotFoundError,
                          s"Unable to find user $id services"
                        ).toResult
                      )
                  case JsError(e) =>
                    GenericError(
                      JsonParsingError,
                      s"Error decoding JSON: $e"
                    ).toResult
                }
              case None =>
                GenericError(
                  JsonParsingError,
                  s"No JSON found"
                ).toResult
            }
          }
    )
}
