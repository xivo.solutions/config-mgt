package ws.controllers.mobile

import anorm.AnormException
import controllers.Secured
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc.{EssentialAction, InjectedController, Result}
import ws.controllers.WithExceptionCatching
import xivo.model.rabbitmq.{
  MobilePushTokenAdded,
  MobilePushTokenDeleted,
  ObjectEvent
}
import xivo.model.{
  MobileAppError,
  MobileAppPushToken,
  UserDoesNotExist,
  UserFeature
}
import xivo.service._
import xivo.service.rabbitmq.XivoRabbitEventsPublisher

import javax.inject.Inject
import scala.util.{Failure, Success, Try}

class MobileAppHelper @Inject() (
    userFeatMgr: UserFeatureManager,
    userPrefMgr: UserPreferenceManager,
    secured: Secured,
    rabbitPublisher: XivoRabbitEventsPublisher
) extends InjectedController
    with WithExceptionCatching {

  val logger: Logger = LoggerFactory.getLogger(getClass)

  def getPushToken(login: String): EssentialAction = {
    secured.WithAuthenticatedUser(
      None,
      user =>
        _ =>
          withExceptionCatching {
            logger.info(s"$user - Req : <getPushToken> $login")
            userFeatMgr.get(login) match {
              case Success(user) =>
                Ok(
                  Json.toJson(
                    MobileAppPushToken(user.mobile_push_token)
                  )
                )
              case Failure(f) =>
                handleError(login, f)
            }
          }
    )
  }

  def setPushToken(
  ): EssentialAction = {
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            import MobileAppPushToken._
            val login =
              request.queryString.get("username").flatMap(_.headOption)
            logger.info(s"$user - Req : <setPushToken> $login")
            request.body.asJson match {
              case None => BadRequest("No JSON could be decoded")
              case Some(json) =>
                json.validate[xivo.model.MobileAppPushToken] match {
                  case JsError(e) => BadRequest(s"Error decoding JSON: $e")
                  case JsSuccess(mobilePush, _) =>
                    (for {
                      userFeature <- getUserByUsername(login.getOrElse(""))
                      userId      <- Try(userFeature.id.get)
                      _ <- userFeatMgr.update(
                        userFeature.copy(
                          mobile_push_token = mobilePush.token
                        )
                      )
                      _ <- addMobileAppUserPreferenceIfNotExist(userId)
                      _ <- setPreferredDeviceIfNoCurrentPushToken(
                        userId,
                        userFeature
                      )
                    } yield {
                      rabbitPublisher.publish(
                        ObjectEvent(MobilePushTokenAdded, userId)
                      )
                      Created
                    }) match {
                      case Success(s) => s
                      case Failure(f) =>
                        handleError(login.getOrElse("<void>"), f)
                    }
                }
            }
          }
    )
  }
  def setAndroidPushToken(): EssentialAction =
    setPushTokenWithPrefix("android")
  def setIOSPushToken(): EssentialAction =
    setPushTokenWithPrefix("ios")
  private def setPushTokenWithPrefix(
      platform: String
  ): EssentialAction = {
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            import MobileAppPushToken._
            val platformPrefix = s"${platform}-"
            val login =
              request.queryString.get("username").flatMap(_.headOption)
            logger.info(s"$user - Req : <platformPushToken> $login")
            request.body.asJson match {
              case None => BadRequest("No JSON could be decoded")
              case Some(json) =>
                json.validate[xivo.model.MobileAppPushToken] match {
                  case JsError(e) => BadRequest(s"Error decoding JSON: $e")
                  case JsSuccess(mobilePush, _) =>
                    (for {
                      userFeature <- getUserByUsername(login.getOrElse(""))
                      userId      <- Try(userFeature.id.get)
                      _ <- userFeatMgr.update(
                        userFeature.copy(
                          mobile_push_token = mobilePush.token.map(token =>
                            s"$platformPrefix$token"
                          )
                        )
                      )
                      _ <- addMobileAppUserPreferenceIfNotExist(userId)
                      _ <- setPreferredDeviceIfNoCurrentPushToken(
                        userId,
                        userFeature
                      )
                    } yield {
                      rabbitPublisher.publish(
                        ObjectEvent(MobilePushTokenAdded, userId)
                      )
                      Created
                    }) match {
                      case Success(s) => s
                      case Failure(f) =>
                        handleError(login.getOrElse("<void>"), f)
                    }
                }
            }
          }
    )
  }

  def deletePushToken(): EssentialAction = {
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            val login =
              request.queryString.get("username").flatMap(_.headOption)
            logger.info(s"$user - Req : <deletePushToken> $login")
            (for {
              user   <- getUserByUsername(login.getOrElse(""))
              userId <- Try(user.id.get)
              _      <- userFeatMgr.update(user.copy(mobile_push_token = None))
              _      <- userPrefMgr.delete(userId, UserPreferenceKey.MobileAppInfo)
              _ <- setUserPreference(
                userId,
                UserPreferenceKey.PreferredDevice,
                UserPreferenceValues.TypeDefaultDevice,
                "String"
              )
            } yield {
              rabbitPublisher.publish(
                ObjectEvent(MobilePushTokenDeleted, userId)
              )
              NoContent
            }) match {
              case Success(s) => s
              case Failure(f) => handleError(login.getOrElse("<void>"), f)
            }
          }
    )
  }

  private def setUserPreference(
      userId: Long,
      key: String,
      value: String,
      valueType: String
  ) = {
    userPrefMgr.createOrUpdate(UserPreference(userId, key, value, valueType))
  }

  private def getUserByUsername(username: String): Try[UserFeature] =
    userFeatMgr.get(username) match {
      case Failure(AnormException(_)) =>
        Failure(
          UserDoesNotExist
        )
      case e => e
    }

  private def handleError(username: String, f: Throwable): Result = {
    f match {
      case UserDoesNotExist =>
        MobileAppError(
          MobileAppError.UserNotFound,
          s"Unable to find user $username in XiVO, no token retrieved"
        ).toResult
      case _ =>
        MobileAppError(
          MobileAppError.UnhandledError,
          f.toString
        ).toResult
    }
  }

  private def addMobileAppUserPreferenceIfNotExist(userId: Long) =
    userPrefMgr.create(
      UserPreference(
        userId,
        UserPreferenceKey.MobileAppInfo,
        "true",
        "Boolean"
      )
    ) recoverWith { case PreferenceAlreadyExistException(_) =>
      Success(0L)
    }

  private def setPreferredDeviceIfNoCurrentPushToken(
      userId: Long,
      user: UserFeature
  ): Try[Long] =
    user.mobile_push_token
      .map(_ => Success(0L))
      .getOrElse(
        setUserPreference(
          userId,
          UserPreferenceKey.PreferredDevice,
          UserPreferenceValues.WebAppAndMobileApp,
          "String"
        )
      )

}
