package ws.controllers.route

import javax.inject.Inject
import org.slf4j.{Logger, LoggerFactory}
import xivo.model._
import xivo.service.{MediaServerManager, RouteManager}

import scala.collection.immutable.SortedMap
import scala.util.{Failure, Success, Try}

object RouteHelper {
  val fromAsteriskMap: Map[String, String] = SortedMap(
    "!" -> """[0-9#\*]*""",
    "X" -> """\d""",
    "Z" -> "[1-9]",
    "N" -> "[2-9]",
    "." -> """[0-9#\*]+""",
    "+" -> """\+"""
  )

  val toAsteriskMap: Map[String, String] =
    fromAsteriskMap.map(_.swap).toSeq.sortWith(_._1.length > _._1.length).toMap

  def fromAsteriskPattern(pattern: String): String = {
    fromAsteriskMap.foldLeft(pattern) { case (p, kv) =>
      p.replace(kv._1, kv._2)
    }
  }

  def toAsteriskPattern(pattern: String): String = {
    toAsteriskMap.foldLeft(pattern) { case (p, kv) =>
      p.replace(kv._1, kv._2)
    }
  }
}

class RouteHelper @Inject() (
    routeManager: RouteManager,
    mdsManager: MediaServerManager
) {

  val logger: Logger = LoggerFactory.getLogger(getClass)

  private def enricher[T, A](
      route: PlainRoute,
      list: List[T],
      action: (PlainRoute, List[T]) => List[A]
  ): List[A] = {
    action(route, list)
  }

  private def context(route: PlainRoute, contextList: List[Context]) = {
    contextList.map {
      case c if route.contextNames.contains(c.name) => RichContext(c.name, true)
      case c                                        => RichContext(c.name)
    }
  }

  private def mediaserver(
      route: PlainRoute,
      mediaserverList: List[MediaServerConfig]
  ) = {
    for {
      mediaserver <- mediaserverList
    } yield {
      mediaserver match {
        case m if route.mediaserverIds.contains(m.id) =>
          RichMediaserverConfig(m.id, m.name, m.display_name, true)
        case m => RichMediaserverConfig(m.id, m.name, m.display_name)
      }
    }
  }

  private def trunk(route: PlainRoute, trunkList: List[Trunk]) = {
    val richTrunkList = trunkList.map {
      case t: TrunkSip if route.trunkIds.map(_.id).contains(t.id) =>
        RichTrunkSip(t.id, t.name, true)
      case t: TrunkCustom if route.trunkIds.map(_.id).contains(t.id) =>
        RichTrunkCustom(t.id, t.interface, true)
      case t: TrunkSip    => RichTrunkSip(t.id, t.name)
      case t: TrunkCustom => RichTrunkCustom(t.id, t.interface)
    }
    val ordered: List[Trunk] =
      route.trunkIds.flatMap(t => richTrunkList.find(_.getId == t.id))
    val notUsedTrunks = richTrunkList.filter(t => !ordered.contains(t))

    ordered ::: notUsedTrunks
  }

  private def schedule(route: PlainRoute, scheduleList: List[Schedule]) = {
    scheduleList.map {
      case s if route.scheduleIds.contains(s.id) =>
        RichSchedule(s.id, s.name, used = true)
      case s => RichSchedule(s.id, s.name)
    }
  }

  private def right(route: PlainRoute, rightList: List[Right]) = {
    rightList.map {
      case r if route.rightIds.contains(r.id) =>
        RichRight(r.id, r.name, used = true)
      case s => RichRight(s.id, s.name)
    }
  }

  def deleteRouteFkRelations(id: Long): Try[Long] = {
    routeManager
      .deleteRoutePattern(id)
      .flatMap(_ => routeManager.deleteRouteContext(id))
      .flatMap(_ => routeManager.deleteRouteMediaserver(id))
      .flatMap(_ => routeManager.deleteRouteTrunk(id))
      .flatMap(_ => routeManager.deleteRouteSchedule(id))
      .flatMap(_ => routeManager.deleteRouteRight(id))
  }

  private def insertOnlyUsedContext(richRoute: RichRoute) = {
    richRoute.context.collect {
      case c if c.used =>
        routeManager.createRouteContext(richRoute.id, c.name)
    }
  }

  private def insertOnlyUsedMds(richRoute: RichRoute) = {
    richRoute.mediaserver.collect {
      case m if m.used =>
        routeManager.createRouteMediaserver(richRoute.id, m.id)
    }
  }

  private def insertOnlyUsedTrunks(richRoute: RichRoute) = {
    richRoute.trunk
      .collect {
        case t if t.isUsed => t
      }
      .zipWithIndex
      .map { case (t, priority) =>
        routeManager.createRouteTrunk(richRoute.id, t.getId, priority.toLong)
      }
  }

  private def insertOnlyUsedSchedule(richRoute: RichRoute) = {
    richRoute.schedule.collect {
      case s if s.used =>
        routeManager.createRouteSchedule(richRoute.id, s.id)
    }
  }

  private def insertOnlyUsedRight(richRoute: RichRoute) = {
    richRoute.right.collect {
      case r if r.used =>
        routeManager.createRouteRight(richRoute.id, r.id)
    }
  }

  private def insertRoutePattern(richRoute: RichRoute) = {
    richRoute.dialPattern.map(dp =>
      routeManager.createRoutePattern(richRoute.id, dp)
    )
  }

  def insertRouteFkRelations(richRoute: RichRoute): List[Try[Option[Long]]] = {
    val maybeRoutePattern = insertRoutePattern(richRoute)
    val maybeContext      = insertOnlyUsedContext(richRoute)
    val maybeMds          = insertOnlyUsedMds(richRoute)
    val maybeTrunk        = insertOnlyUsedTrunks(richRoute)
    val maybeSchedule     = insertOnlyUsedSchedule(richRoute)
    val maybeRight        = insertOnlyUsedRight(richRoute)

    maybeRoutePattern ++ maybeContext ++ maybeMds ++ maybeTrunk ++ maybeSchedule ++ maybeRight
  }

  private def handleEnrichErrors[T](action: Try[T]): T = {
    action match {
      case Success(a) => a
      case Failure(f) =>
        logger.error("An error occurred while fetching data", f)
        throw new Exception(f)
    }
  }

  def enrichRoute(route: PlainRoute): Try[RichRoute] = {
    Try {
      val contextList     = handleEnrichErrors(routeManager.getContextAll)
      val mediaserverList = handleEnrichErrors(mdsManager.all()).list
      val trunkList       = handleEnrichErrors(routeManager.getTrunkAll)
      val scheduleList    = handleEnrichErrors(routeManager.getScheduleAll)
      val rightList       = handleEnrichErrors(routeManager.getRightAll)

      val richContextList     = enricher(route, contextList, context)
      val richMediaserverList = enricher(route, mediaserverList, mediaserver)
      val richTrunkList       = enricher(route, trunkList, trunk)
      val richScheduleList    = enricher(route, scheduleList, schedule)
      val richRightList       = enricher(route, rightList, right)

      RichRoute(
        route.id,
        route.untransformedPattern,
        route.description,
        route.priority,
        richContextList,
        richMediaserverList,
        richTrunkList,
        richScheduleList,
        richRightList,
        route.subroutine,
        route.internal
      )
    }
  }
}
