package ws.controllers

import controllers.Secured
import model.ws.{ErrorResult, ErrorType, NotHandledError}
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.{Json, Writes}
import play.api.mvc.{EssentialAction, InjectedController, Result, Results}
import xivo.service.StaticSipManager

import javax.inject.Inject
import scala.util.{Failure, Success}

class SipConfig @Inject() (staticSipManager: StaticSipManager, secured: Secured)
    extends InjectedController
    with WithExceptionCatching {

  val logger: Logger = LoggerFactory.getLogger(getClass)

  trait StaticSipErrorType extends ErrorType
  case object ValueNotFound extends StaticSipErrorType {
    val error = "ValueNotFound"
  }

  case class StaticSipError(error: ErrorType, message: String)
      extends ErrorResult {
    override def toResult: Result = {
      log.error(this.message)
      val body = Json.toJson(this)
      error match {
        case ValueNotFound => Results.NotFound(body)
        case _             => Results.InternalServerError(body)
      }
    }
  }

  object StaticSipError {
    implicit val writes: Writes[StaticSipError] = Json.writes[StaticSipError]
  }

  def getStunAddress(): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(
              s"$user - StunAddress Req"
            )

            staticSipManager.getStunAddress() match {
              case Success(stunAddr) =>
                Ok(Json.toJson(stunAddr))
              case Failure(f) =>
                StaticSipError(
                  NotHandledError,
                  s"Unable to get stun address, internal error"
                ).toResult
            }
          }
    )
}
