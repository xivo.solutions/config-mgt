package ws.controllers

import controllers.Secured
import javax.inject.Inject
import org.slf4j.LoggerFactory
import play.api.Configuration
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc.InjectedController
import xc.service.{CallQualificationManager, XivoWebService}
import play.api.libs.ws.WSClient
import xc.model.{CallQualification, SimpleCallQualification}

import scala.util.{Failure, Success}

class CallQualifications @Inject() (
    callQualificationManager: CallQualificationManager,
    wSClient: WSClient,
    configuration: Configuration,
    secured: Secured
) extends InjectedController
    with WithExceptionCatching {

  val logger = LoggerFactory.getLogger(getClass)

  val xivohost: String = configuration.get[String]("xivohost")

  val xivoWs = new XivoWebService(wSClient, xivohost)

  def getAll =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            import xc.model.CallQualification._
            logger.info(s"$user - Req : <getAll>")

            callQualificationManager.all() match {
              case Success(qualifList) => Ok(Json.toJson(qualifList))
              case Failure(f) =>
                InternalServerError(s"Unable to get qualifications. $f")
            }
          }
    )

  def get(id: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            import xc.model.CallQualification._
            logger.info(s"$user - Req : <get> $id")

            callQualificationManager.all(id) match {
              case Success(qualifList) => Ok(Json.toJson(qualifList))
              case Failure(f) =>
                InternalServerError(
                  s"Unable to get qualifications for queue id: $id. $f"
                )
            }
          }
    )

  def getByQueue(id: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            import xc.model.CallQualification._
            logger.info(s"$user - Req : <getByQueue> $id")

            callQualificationManager.allByQueue(id) match {
              case Success(qualifList) => Ok(Json.toJson(qualifList))
              case Failure(f) =>
                InternalServerError(
                  s"Unable to get qualifications for queue id: $id. $f"
                )
            }
          }
    )

  def create =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            import xc.model.CallQualification._
            logger.info(s"$user - Req : <create>")

            request.body.asJson match {
              case Some(json) =>
                json.validate[CallQualification] match {
                  case JsSuccess(callQualification, _) =>
                    callQualificationManager.create(callQualification) match {
                      case Success(id) => Ok(Json.toJson(id))
                      case Failure(f) =>
                        InternalServerError(
                          s"Unable to create qualification. $f"
                        )
                    }
                  case JsError(e) => BadRequest(s"Error decoding JSON: $e")
                }
              case None => BadRequest("No JSON could be decoded")
            }
          }
    )

  def deleteQualification(id: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Req : <delete> $id")

            callQualificationManager.deleteQualification(id) match {
              case Success(nb) => Ok(Json.toJson(nb))
              case Failure(f) =>
                InternalServerError(
                  s"Unable to delete qualifications with id: $id. $f"
                )
            }
          }
    )

  def deleteSubQualification(id: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Req : <delete> $id")

            callQualificationManager.deleteSubQualification(id) match {
              case Success(nb) => Ok(Json.toJson(nb))
              case Failure(f) =>
                InternalServerError(
                  s"Unable to delete sub qualifications with id: $id. $f"
                )
            }
          }
    )

  def update(id: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            import xc.model.CallQualification._
            logger.info(s"$user - Req : <update> $id")

            request.body.asJson match {
              case Some(json) =>
                json.validate[CallQualification] match {
                  case JsSuccess(callQualification, _) =>
                    callQualificationManager
                      .update(id, callQualification) match {
                      case Success(nb) =>
                        for (s <- callQualification.subQualification) {
                          callQualificationManager.updateSubQualification(
                            s
                          ) match {
                            case Success(_) =>
                            case Failure(f) =>
                              InternalServerError(
                                s"Unable to update sub qualification: $s. $f"
                              )
                          }
                        }
                        Ok(Json.toJson(nb))
                      case Failure(f) =>
                        InternalServerError(
                          s"Unable to update qualification. $f"
                        )
                    }
                  case JsError(e) => BadRequest(s"Error decoding JSON: $e")
                }
              case None => BadRequest("No JSON could be decoded")
            }
          }
    )

  def assign(id: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Req : <assign> $id")

            request.body.asJson match {
              case Some(json) =>
                json.validate[SimpleCallQualification] match {
                  case JsSuccess(simpleQualification, _) =>
                    callQualificationManager
                      .assign(id, simpleQualification) match {
                      case Success(nb) => Ok(Json.toJson(nb))
                      case Failure(f) =>
                        InternalServerError(
                          s"Unable to assign qualification to queue. $f"
                        )
                    }
                  case JsError(e) => BadRequest(s"Error decoding JSON: $e")
                }
              case None => BadRequest("No JSON could be decoded")
            }
          }
    )

  def unassign(id: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Req : <unassign> $id")

            request.body.asJson match {
              case Some(json) =>
                json.validate[SimpleCallQualification] match {
                  case JsSuccess(simpleQualification, _) =>
                    callQualificationManager
                      .unassign(id, simpleQualification) match {
                      case Success(nb) => Ok(Json.toJson(nb))
                      case Failure(f) =>
                        InternalServerError(
                          s"Unable to assign qualification to queue. $f"
                        )
                    }
                  case JsError(e) => BadRequest(s"Error decoding JSON: $e")
                }
              case None => BadRequest("No JSON could be decoded")
            }
          }
    )
}
