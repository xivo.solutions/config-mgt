package ws.controllers

import controllers.Secured
import javax.inject.Inject
import model.ws.{GenericError, JsonParsingError, NotHandledError}
import org.slf4j.{Logger, LoggerFactory}
import play.api.Configuration
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.libs.ws.WSClient
import play.api.mvc.InjectedController
import ws.controllers.route.RouteHelper
import xivo.model._
import xivo.service.{MediaServerManager, RouteManager}

import scala.util.{Failure, Success, Try}

class Route @Inject() (
    routeManager: RouteManager,
    mdsManager: MediaServerManager,
    helper: RouteHelper,
    wSClient: WSClient,
    configuration: Configuration,
    secured: Secured
) extends InjectedController
    with WithExceptionCatching {

  val logger: Logger = LoggerFactory.getLogger(getClass)

  def create =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Req : <create>")

            request.body.asJson match {
              case Some(json) =>
                json.validate[RichRouteNoId] match {
                  case JsSuccess(richRouteNoId, _) =>
                    routeManager.createRoute(richRouteNoId) match {
                      case Success(richRoute) =>
                        val inserted: List[Try[Option[Long]]] =
                          helper.insertRouteFkRelations(richRoute)
                        val failed = inserted.collect { case Failure(f) => f }
                        if (failed.nonEmpty)
                          GenericError(
                            NotHandledError,
                            s"Unable to create route, ${failed.mkString(", ")}"
                          ).toResult
                        else
                          Created
                      case Failure(f) =>
                        GenericError(
                          NotHandledError,
                          s"Unable to create route, $f"
                        ).toResult
                    }
                  case JsError(e) =>
                    GenericError(
                      JsonParsingError,
                      s"Error decoding JSON: $e"
                    ).toResult
                }
              case None =>
                GenericError(
                  JsonParsingError,
                  s"No JSON could be decoded"
                ).toResult
            }
          }
    )

  def getAll =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Req : <getAll>")

            routeManager.getAll match {
              case Success(routeList) =>
                val tryRichRouteList =
                  Try(routeList.map(helper.enrichRoute).map(_.get))
                tryRichRouteList match {
                  case Success(richRoutes) =>
                    Ok(Json.toJson(richRoutes))
                  case Failure(f) =>
                    GenericError(
                      NotHandledError,
                      s"Unable to get routes, $f"
                    ).toResult
                }
              case Failure(f) =>
                GenericError(
                  NotHandledError,
                  s"Unable to get routes, $f"
                ).toResult
            }
          }
    )

  def get(id: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Req : <get> $id")

            routeManager.get(id) match {
              case Success(route) =>
                val tryRichRoute = helper.enrichRoute(route)
                tryRichRoute match {
                  case Success(richRoute) => Ok(Json.toJson(richRoute))
                  case Failure(f) =>
                    GenericError(
                      NotHandledError,
                      s"Unable to get routes for id: $id. $f"
                    ).toResult
                }
              case Failure(f) =>
                GenericError(
                  NotHandledError,
                  s"Unable to get routes for id: $id. $f"
                ).toResult
            }
          }
    )

  def getNew =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Req : <getNew>")

            val tryRichRoute = for {
              contextList     <- routeManager.getContextAll
              mediaserverList <- mdsManager.all()
              trunkList       <- routeManager.getTrunkAll
              priority        <- routeManager.getLastPriority
              scheduleList    <- routeManager.getScheduleAll
              rightList       <- routeManager.getRightAll
            } yield {
              val richContextList = contextList.map(c => RichContext(c.name))
              val richMdsList = mediaserverList.list.map(m =>
                RichMediaserverConfig(m.id, m.name, m.display_name)
              )
              val richTrunkList = trunkList.map {
                case t: TrunkSip    => RichTrunkSip(t.id, t.name)
                case t: TrunkCustom => RichTrunkCustom(t.id, t.interface)
              }
              val richScheduleList =
                scheduleList.map(s => RichSchedule(s.id, s.name))
              val richRightList = rightList.map(s => RichRight(s.id, s.name))
              RichEmptyRoute(
                priority,
                richContextList,
                richMdsList,
                richTrunkList,
                richScheduleList,
                richRightList
              )
            }

            tryRichRoute match {
              case Success(richRoute) => Ok(Json.toJson(richRoute))
              case Failure(f) =>
                GenericError(
                  NotHandledError,
                  s"Unable to create new route. $f"
                ).toResult
            }
          }
    )

  def delete(id: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Req : <delete>")

            val deletedFks = helper.deleteRouteFkRelations(id)
            deletedFks match {
              case Success(_) =>
                routeManager.delete(id) match {
                  case Success(_) => NoContent
                  case Failure(f) =>
                    GenericError(
                      NotHandledError,
                      s"Unable to delete route with id: $id. $f"
                    ).toResult
                }
              case Failure(f) =>
                GenericError(
                  NotHandledError,
                  s"Unable to delete route with id: $id. $f"
                ).toResult
            }
          }
    )

  def update =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Req : <update>")

            request.body.asJson match {
              case Some(json) =>
                json.validate[RichRoute] match {
                  case JsSuccess(richRoute, _) =>
                    val id         = richRoute.id
                    val deletedFks = helper.deleteRouteFkRelations(id)
                    deletedFks match {
                      case Success(_) =>
                        routeManager.delete(id) match {
                          case Success(_) =>
                            routeManager.createRoute(
                              RichRouteNoId(
                                richRoute.dialPattern,
                                richRoute.description,
                                richRoute.priority,
                                richRoute.context,
                                richRoute.mediaserver,
                                richRoute.trunk,
                                richRoute.schedule,
                                richRoute.right,
                                richRoute.subroutine,
                                richRoute.internal
                              )
                            ) match {
                              case Success(updatedRoute) =>
                                val inserted =
                                  helper.insertRouteFkRelations(updatedRoute)
                                val failed = inserted.collect {
                                  case Failure(f) => f
                                }
                                if (failed.nonEmpty)
                                  GenericError(
                                    NotHandledError,
                                    s"Unable to update route, ${failed.mkString(" ")}"
                                  ).toResult
                                else
                                  Created(Json.toJson(updatedRoute))
                              case Failure(f) =>
                                GenericError(
                                  NotHandledError,
                                  s"Unable to create route, $f"
                                ).toResult
                            }
                          case Failure(f) =>
                            GenericError(
                              NotHandledError,
                              s"Unable to delete route with id: $id. $f"
                            ).toResult
                        }
                      case Failure(f) =>
                        GenericError(
                          NotHandledError,
                          s"Unable to delete route with id: $id. $f"
                        ).toResult
                    }
                  case JsError(e) =>
                    GenericError(
                      JsonParsingError,
                      s"Error decoding JSON: $e"
                    ).toResult
                }
              case None =>
                GenericError(
                  JsonParsingError,
                  s"No JSON could be decoded"
                ).toResult
            }
          }
    )
}
