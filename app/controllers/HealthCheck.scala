package controllers

import com.google.inject.Inject
import play.api.mvc.{
  AbstractController,
  Action,
  AnyContent,
  ControllerComponents
}

class HealthCheck @Inject() (cc: ControllerComponents)
    extends AbstractController(cc) {

  def isRunning: Action[AnyContent] =
    Action {
      NoContent
    }

}
