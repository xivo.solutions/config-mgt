function createReq(type, url) {
    return {
        type: type,
        url: url,
        headers: {
            'X-Auth-Token': authToken
        },
        dataType: 'text'
    };
}

function makeAuthCall(type, url, data) {
    var req = createReq(type, url);

    if(data) {
        req.data = JSON.stringify(data);
        req.contentType = 'application/json;charset=utf-8';
    }

    return $.ajax(req);
}


function makeAuthCallCsv(type, url, data) {
    var req = createReq(type, url);

    var charset = 'utf-8';
    if(window.navigator.platform === 'Win32') {
        charset = 'iso-8859-1';
    }

    if(data) {
        req.data = data;
        req.contentType = "text/plain;charset=" + charset;
        req.processData = false;
    }

    return $.ajax(req);
}
