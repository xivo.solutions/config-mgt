import 'angular-ui-bootstrap';
import angulartranslate from 'angular-translate';
import 'angular-translate-loader-partial';

angular.module('callbacks', ['ui.bootstrap', angulartranslate])
.controller('CallbackViewController', function($scope) {
    $scope.viewName = 'callback_lists';

    $scope.isActive = function(viewName) {
        return ($scope.viewName === viewName);
    };

    $scope.setView = function(viewName) {
        $scope.viewName = viewName;
    };
})
.controller('CallbackListsController', function($scope, $uibModal, $window, $http, $log) {
    $scope.getQueues = function() {
        $http.get('api/1.0/queues').then(function(response) {
            $scope.queues = response.data;
            $scope.getCallbackLists();
        }, function(error) {
            $log.error('Unable to get queues ', error);
        });
    };

    $scope.getCallbackLists = function() {
        $http.get('api/1.0/callback_lists').then(function(response) {
            $scope.callbackLists = response.data;
            $scope.mixQueuesWithCallbacks();
        }, function(error) {
            $log.error('Unable to get callback_lists ', error);
        });
    };

    $scope.mixQueuesWithCallbacks = function() {
        for(var i=0; i<$scope.callbackLists.length; i++) {
            var callback = $scope.callbackLists[i];
            callback.queue = $scope.findQueue(callback.queueId);
        }
    };

    $scope.findQueue = function(id) {
        for(var i=0; i<$scope.queues.length; i++) {
            var queue = $scope.queues[i];
            if(queue.id == id) return queue;
        }
    };

    $scope.createList = function() {
        $uibModal.open({
            templateUrl: 'callbackListModal.html',
            controller: 'ListModalController',
            resolve: {
              queues: function() {
                  return $scope.queues;
              }
            }
        }).result.then($scope.getCallbackLists);
    };

    $scope.deleteList = function(uuid) {
        var doIt = $window.confirm("Ceci supprimera la liste et tous les rappels qui y sont rattachés. Etes-vous sûr de vouloir continuer ?");
        if(!doIt)
            return;
        $http.delete('api/1.0/callback_lists/' + uuid).then($scope.getCallbackLists, function(error) {
            $log.error('Unable to delete callback_list ', uuid, 'error: ', error);
        });
    };

    $scope.getQueues();
})
.controller('ListModalController', function($scope, $uibModalInstance, $http, queues) {
    $scope.queues = queues;
    $scope.callbackList = {
        name: ''
    };
    $scope.close = $uibModalInstance.dismiss;

    $scope.save = function() {
        $http.post('api/1.0/callback_lists', JSON.stringify($scope.callbackList)).then($uibModalInstance.close);
    };
})
.controller('CallbackPeriodsController', function($scope, $uibModal, $window, $http, $log) {
    $scope.getPeriods = function() {
        $http.get('api/1.0/preferred_callback_periods').then(function(response) {
            $scope.callbackPeriods = response.data;
        }, function(error) {
            $log.error('Unable to get preferred callback periods ', error);
        });
    };

    $scope.createPeriod = function() {
        $uibModal.open({
            templateUrl: 'callbackPeriodModal.html',
            controller: 'PeriodModalController',
            resolve: {
                callbackPeriod: function() {
                    return null;
                }
            }
        }).result.then($scope.getPeriods);
    };

    $scope.deletePeriod = function(uuid) {
        var doIt = $window.confirm("Ceci déplacera les rappels de cette période vers la période par défaut. Etes-vous sûr de vouloir continuer ?");
        if(!doIt)
            return;
        $http.delete('api/1.0/preferred_callback_periods/' + uuid).then($scope.getPeriods);
    };

    $scope.editPeriod = function(uuid) {
        $uibModal.open({
            templateUrl: 'callbackPeriodModal.html',
            controller: 'PeriodModalController',
            resolve: {
                callbackPeriod: function() {
                    return $scope.findByUuid(uuid);
                }
            }
        }).result.then($scope.getPeriods);
    };

    $scope.findByUuid = function(uuid) {
        for(var i=0; i<$scope.callbackPeriods.length; i++) {
            var period = $scope.callbackPeriods[i];
            if(period.uuid == uuid)
                return period;
        }
    };

    $scope.getPeriods();
})
.controller('PeriodModalController', function($scope, $uibModalInstance, $http, callbackPeriod) {
    if(callbackPeriod) {
        $scope.callbackPeriod = callbackPeriod;
        $scope.wasDefault = callbackPeriod.default;
        $scope.callbackPeriod._periodStart = new Date('2000-01-01T' + callbackPeriod.periodStart);
        $scope.callbackPeriod._periodEnd = new Date('2000-01-01T' + callbackPeriod.periodEnd);
    } else {
        $scope.callbackPeriod = {
           name: '',
            default: false,
            _periodStart: new Date('2000-01-01T08:00:00'),
            _periodEnd: new Date('2000-01-01T18:00:00')
        };
    }

    function onTwoDigits(i) {
        if(i < 10) return '0' + i;
        else return i;
    }

    function formatTime(time) {
        return onTwoDigits(time.getHours()) + ':' + onTwoDigits(time.getMinutes()) + ':' + onTwoDigits(time.getSeconds());
    }

    $scope.setTimes = function() {
        $scope.callbackPeriod.periodStart = formatTime($scope.callbackPeriod._periodStart);
        $scope.callbackPeriod.periodEnd = formatTime($scope.callbackPeriod._periodEnd);
    };

    $scope.close = $uibModalInstance.dismiss;

    $scope.save = function() {
        $scope.setTimes();
        if($scope.callbackPeriod.uuid) {
            $http.put('api/1.0/preferred_callback_periods/' + $scope.callbackPeriod.uuid,
                      JSON.stringify($scope.callbackPeriod)
                     ).then($uibModalInstance.close);
        } else {
            $http.post('api/1.0/preferred_callback_periods', JSON.stringify($scope.callbackPeriod))
              .then($uibModalInstance.close);
        }
    };
});
