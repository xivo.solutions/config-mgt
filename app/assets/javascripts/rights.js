import 'angular-ui-bootstrap';
import 'checklist-model';
import angulartranslate from 'angular-translate';
import 'angular-translate-loader-partial';

angular.module('users', ['ui.bootstrap', 'checklist-model', angulartranslate])
.controller('UsersController', function($scope, $uibModal, $http, $log) {

    $scope.displayError = function(response) {
        $log.error(response.data.message);
        $scope.error = "ERROR_"+response.data.error;
    };

    $scope.processUsers = function(response) {
        $scope.users = response.data;
    };

    $scope.processXivoUsers = function(response) {
        $scope.xivoUsers = response.data;
    };

    $scope.getUsers = function() {
        $scope.error = undefined;
        $http.get('api/1.0/users').then($scope.processUsers);
    };

    $scope.getXivoUsers = function() {
        $http.get('api/1.0/xivo_users').then($scope.processXivoUsers);
    };

    $scope.editUser = function(user) {
        $uibModal.open({
          templateUrl: 'modal.html',
          controller: 'ModalController',
          resolve: {
            user: function() {
                return user;
            }
          }
        }).result.then($scope.getUsers);
    };

    $scope.deleteUser = function(user) {
        $http.delete('api/1.0/rights/user/' + user.login)
          .then($scope.getUsers, $scope.displayError);
    };

    $scope.fullName = function(user) {
        if(user)
            return user.firstname + ' ' + user.lastname + ' (' + user.login + ')';
    };

    $scope.createUser = function(xivoUser) {
      if (!xivoUser) return;
        var user = {
            name: xivoUser.lastname,
            firstname: xivoUser.firstname,
            login: xivoUser.login,
            profile: 'admin'
        };
        $scope.editUser(user);
    };

    $scope.getUsers();
    $scope.getXivoUsers();
})

.controller('ModalController', function($scope, $uibModalInstance, $http, user, $filter, $log) {

    let today = new Date();
    let todayTxt = $filter('date')(today, $scope.dateFormat);

    $scope.profiles = ['admin', 'supervisor', 'teacher'];
    $scope.user = user;
    $scope.dateFormat = 'yyyy-MM-dd';

    $scope.fermer = $uibModalInstance.dismiss;

    $scope.displayError = function(response) {
      $log.error(response.data.message);
      $scope.error = "ERROR_"+response.data.error;
    };

    $scope.enregistrer = function() {

        $scope.right.data.start = $scope.startDate ? $filter('date')($scope.startDate, $scope.dateFormat) : new Date() ;
        $scope.right.data.end = $scope.endDate ? $filter('date')($scope.endDate, $scope.dateFormat) : new Date();


        $http.post('api/1.0/rights/user/' + $scope.user.login, JSON.stringify($scope.right))
          .then($uibModalInstance.close, $scope.displayError);
    };


    $scope.newRight = function() {
        $scope.right = {
            type: 'admin',
            data: {start: todayTxt, end: todayTxt, recordingAccess: true, dissuasionAccess: false}
        };
        $scope.startDate = today;
        $scope.endDate = today;
    };

    $scope.getRight = function(login) {
        $http.get('api/1.0/rights/user/' + login).then(function(response) {
            $scope.data = response.data;
            $scope.right = response.data;
            if (angular.isUndefined($scope.right.data.recordingAccess)) {
              $scope.right.data.recordingAccess = true;
            }
            if (angular.isUndefined($scope.right.data.dissuasionAccess)) {
              $scope.right.data.dissuasionAccess = false;
            }
            $scope.startDate = $scope.right.data.start ? new Date($scope.right.data.start) : today;
            $scope.endDate = $scope.right.data.end ? new Date($scope.right.data.end) : today;
        },
        (error) => {
            $log.error(error);
            $scope.newRight();
        });

    };


    $scope.getQueues = function () {
        $http.get('api/1.0/queues').then(function(response) {
            $scope.queues = response.data;
            $scope.getGroups();
        });
    };

    $scope.getGroups = function () {
        $http.get('api/1.0/groups').then(function(response) {
            $scope.groups = response.data;
            $scope.getIncalls();
        });
    };

    $scope.getIncalls = function() {
        $http.get('api/1.0/incalls').then(function(response) {
                $scope.incalls = response.data;
                $scope.getRight(user.login);
            });
    };

    $scope.startDateOptions = {
            formatYear: 'yyyy',
            startingDay: 1,
            maxDate: new Date(2030, 5, 22)
        };
    $scope.endDateOptions = {
            formatYear: 'yyyy',
            startingDay: 1,
            maxDate: new Date(2030, 5, 22)
        };

    $scope.startDatePopup = {
      opened: false
    };
    $scope.endDatePopup = {
      opened: false
    };

    $scope.OpenStartDate = function() {
      $scope.startDatePopup.opened = !$scope.startDatePopup.opened;
    };
    $scope.EndStartDate = function() {
      $scope.endDatePopup.opened = !$scope.endDatePopup.opened;
    };

    $scope.getQueues();
});
