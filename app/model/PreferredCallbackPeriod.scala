package model

import java.security.InvalidParameterException
import java.util.{Date, UUID}
import javax.inject.Inject
import org.joda.time.LocalTime
import play.api.db.{Database, _}
import play.api.libs.json.{JsObject, JsPath, Json, Reads, Writes}
import anorm._
import anorm.SqlParser._
import play.api.libs.functional.syntax._
import UUIDUtils._
import com.google.inject.ImplementedBy

case class PreferredCallbackPeriod(
    uuid: Option[UUID],
    name: String,
    periodStart: LocalTime,
    periodEnd: LocalTime,
    default: Boolean
) {}

object PreferredCallbackPeriod {
  implicit val writes: Writes[PreferredCallbackPeriod] =
    new Writes[PreferredCallbackPeriod] {
      def writes(cp: PreferredCallbackPeriod): JsObject =
        Json.obj(
          "uuid"        -> cp.uuid,
          "name"        -> cp.name,
          "periodStart" -> cp.periodStart.toString("HH:mm:ss"),
          "periodEnd"   -> cp.periodEnd.toString("HH:mm:ss"),
          "default"     -> cp.default
        )
    }

  implicit val reads: Reads[PreferredCallbackPeriod] = (
    (JsPath \ "uuid").readNullable[UUID] and
      (JsPath \ "name").read[String] and
      (JsPath \ "periodStart").read[String].map(LocalTime.parse) and
      (JsPath \ "periodEnd").read[String].map(LocalTime.parse) and
      (JsPath \ "default").read[Boolean]
  )(PreferredCallbackPeriod.apply _)
}

@ImplementedBy(classOf[PreferredCallbackPeriodManagerImpl])
trait PreferredCallbackPeriodManager {
  def getPeriods: List[PreferredCallbackPeriod]
  def getPeriodsByName(name: String): List[PreferredCallbackPeriod]
  def createPeriod(period: PreferredCallbackPeriod): PreferredCallbackPeriod
  def deletePeriod(uuid: String): Unit
  def edit(period: PreferredCallbackPeriod): Unit
  def getPeriod(uuid: UUID): Option[PreferredCallbackPeriod]
  def periodExists(uuid: UUID): Boolean
  def getDefault: Option[PreferredCallbackPeriod]
}

class PreferredCallbackPeriodManagerImpl @Inject() (
    @NamedDatabase("xivo") db: Database
) extends PreferredCallbackPeriodManager {

  val simple: RowParser[PreferredCallbackPeriod] = get[Option[UUID]]("uuid") ~
    get[String]("name") ~
    get[Date]("period_start") ~
    get[Date]("period_end") ~
    get[Boolean]("default") map { case uuid ~ name ~ start ~ end ~ default =>
      PreferredCallbackPeriod(
        uuid,
        name,
        new LocalTime(start),
        new LocalTime(end),
        default
      )
    }

  override def getPeriods: List[PreferredCallbackPeriod] =
    db.withConnection { implicit c =>
      SQL(
        "SELECT uuid, name, period_start, period_end, \"default\" FROM preferred_callback_period"
      ).as(simple.*)
    }

  override def getPeriodsByName(name: String): List[PreferredCallbackPeriod] =
    db.withConnection { implicit c =>
      SQL(
        "SELECT uuid, name, period_start, period_end, \"default\" FROM preferred_callback_period WHERE name = {name}"
      )
        .on(Symbol("name") -> name)
        .as(simple.*)
    }

  override def periodExists(uuid: UUID): Boolean =
    db.withConnection { implicit c =>
      SQL("SELECT uuid FROM preferred_callback_period WHERE uuid = {uuid}")
        .on(Symbol("uuid") -> uuid)
        .as((get[UUID]("uuid")).singleOpt)
        .isDefined
    }

  override def getDefault: Option[PreferredCallbackPeriod] =
    db.withConnection { implicit c =>
      SQL(
        "SELECT uuid, name, period_start, period_end, \"default\" FROM preferred_callback_period WHERE \"default\" IS TRUE"
      ).as(simple.*).headOption
    }

  override def getPeriod(uuid: UUID): Option[PreferredCallbackPeriod] =
    db.withConnection { implicit c =>
      SQL(
        "SELECT uuid, name, period_start, period_end, \"default\" FROM preferred_callback_period WHERE uuid = {uuid}"
      )
        .on(Symbol("uuid") -> uuid)
        .as(simple.*)
        .headOption
    }

  override def createPeriod(
      p: PreferredCallbackPeriod
  ): PreferredCallbackPeriod =
    db.withTransaction { implicit c =>
      if (p.default)
        SQL("UPDATE preferred_callback_period SET \"default\" = FALSE")
          .executeUpdate()
      val uuid = SQL(
        "INSERT INTO preferred_callback_period(name, period_start, period_end, \"default\")" +
          " VALUES ({name}, {start}, {end}, {default})"
      )
        .on(
          Symbol("name")    -> p.name,
          Symbol("start")   -> p.periodStart.toDateTimeToday.toDate,
          Symbol("end")     -> p.periodEnd.toDateTimeToday.toDate,
          Symbol("default") -> p.default
        )
        .executeInsert(get[UUID]("uuid").*)
        .head
      p.copy(uuid = Some(uuid))
    }

  override def deletePeriod(uuid: String): Unit =
    db.withTransaction { implicit c =>
      val realUuid = validateUuid(uuid)
      SQL(
        "SELECT \"default\" FROM preferred_callback_period WHERE uuid = {uuid}"
      ).on(Symbol("uuid") -> realUuid)
        .as(get[Boolean]("default").*)
        .headOption match {
        case None =>
          throw new NoSuchElementException(
            s"No callback period found with uuid = $uuid"
          )
        case Some(true) =>
          throw new InvalidParameterException(
            "Cannot delete the default callback period"
          )
        case _ =>
          getDefault match {
            case None =>
              throw new InvalidParameterException(
                "No default period found, aborting"
              )
            case Some(default) =>
              SQL(
                "UPDATE callback_request SET preferred_callback_period_uuid = {defaultUuid} WHERE preferred_callback_period_uuid = {uuid}"
              )
                .on(
                  Symbol("defaultUuid") -> default.uuid.get,
                  Symbol("uuid")        -> realUuid
                )
                .executeUpdate()
              SQL("DELETE FROM preferred_callback_period WHERE uuid = {uuid}")
                .on(Symbol("uuid") -> realUuid)
                .executeUpdate()
          }
      }
      ()
    }

  override def edit(period: PreferredCallbackPeriod): Unit =
    db.withTransaction { implicit c =>
      if (period.uuid.isEmpty)
        throw new InvalidParameterException("The provided period has no uuid")
      getPeriod(period.uuid.get) match {
        case None =>
          throw new NoSuchElementException(
            s"No callback period found with uuid = ${period.uuid.get}"
          )
        case Some(actualPeriod) =>
          if (actualPeriod.default && !period.default)
            throw new InvalidParameterException(
              "Cannot unset the default flag for the current default period"
            )
          SQL(
            "UPDATE preferred_callback_period SET name = {name}, period_start = {start}, period_end = {end}, \"default\" = {default} WHERE uuid = {uuid}"
          )
            .on(
              Symbol("uuid")    -> period.uuid,
              Symbol("name")    -> period.name,
              Symbol("start")   -> period.periodStart.toDateTimeToday.toDate,
              Symbol("end")     -> period.periodEnd.toDateTimeToday.toDate,
              Symbol("default") -> period.default
            )
            .executeUpdate()
          if (period.default)
            SQL(
              "UPDATE preferred_callback_period SET \"default\" = FALSE WHERE uuid != {uuid}"
            ).on(Symbol("uuid") -> period.uuid).executeUpdate()
      }
      ()
    }
}
