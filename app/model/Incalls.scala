package model

import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import com.google.inject.ImplementedBy
import play.api.db._
import play.api.libs.functional.syntax._
import play.api.libs.json._

case class Incall(id: Option[Long], number: String)

object Incall {
  implicit val agGroupWrites: OWrites[Incall] =
    ((__ \ "id").write[Option[Long]] and
      (__ \ "number").write[String])(unlift(Incall.unapply))

}

@ImplementedBy(classOf[IncallManagerImpl])
trait IncallManager {
  def all(): List[Incall]

  def withIds(ids: List[Int]): List[Incall]
}

class IncallManagerImpl @Inject() (@NamedDatabase("xivo") dbXivo: Database)
    extends IncallManager {
  override def withIds(ids: List[Int]): List[Incall] =
    dbXivo.withConnection { implicit c =>
      if (ids.isEmpty) List()
      else
        SQL("SELECT  id, exten FROM incall WHERE id IN ({ids})")
          .on(Symbol("ids") -> ids.asInstanceOf[Seq[Int]])
          .as(simple.*)
    }

  val simple: RowParser[Incall] = get[Long]("id") ~ get[String]("exten") map {
    case id ~ number =>
      Incall(Some(id), number)
  }

  def all(): List[Incall] =
    dbXivo.withConnection { implicit c =>
      SQL("select  id, exten from incall").as(simple.*)
    }

}
