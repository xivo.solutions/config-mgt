package model

import java.security.InvalidParameterException
import java.util.UUID

import anorm.ToStatement

object UUIDUtils {
  implicit def uuidToStatement: ToStatement[UUID] =
    new ToStatement[UUID] {
      def set(s: java.sql.PreparedStatement, index: Int, aValue: UUID): Unit =
        s.setObject(index, aValue)
    }

  def validateUuid(uuid: String): UUID =
    try {
      UUID.fromString(uuid)
    } catch {
      case e: Exception =>
        throw new InvalidParameterException(s"Invalid UUID : $uuid")
    }

}
