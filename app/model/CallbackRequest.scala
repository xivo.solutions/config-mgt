package model

import java.security.InvalidParameterException
import java.util.{Date, UUID}
import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import com.google.inject.ImplementedBy
import common.CsvUtils
import model.UUIDUtils._
import org.joda.time.format.DateTimeFormat
import org.joda.time.{DateTime, LocalDate}
import play.api.db.{Database, NamedDatabase}
import play.api.libs.functional.syntax._
import play.api.libs.json._
import play.api.libs.json.JodaWrites._

import scala.util.Try

case class CallbackRequest(
    uuid: Option[UUID],
    listUuid: UUID,
    phoneNumber: Option[String],
    mobilePhoneNumber: Option[String],
    firstName: Option[String],
    lastName: Option[String],
    company: Option[String],
    description: Option[String],
    preferredPeriodUuid: Option[UUID] = None,
    dueDate: LocalDate = LocalDate.now(),
    voiceMessageRef: Option[String] = None,
    agentId: Option[Long] = None,
    queueId: Option[Long] = None,
    clotured: Boolean = false,
    preferredPeriod: Option[PreferredCallbackPeriod] = None
)

case class FindCallbackRequest(
    filters: List[DynamicFilter],
    offset: Int = 0,
    limit: Int = 100
)
case class FindCallbackResponse(total: Long, list: List[CallbackRequest])

object CallbackRequest {
  import DynamicFilter.{filterReads, filterWrites}
  implicit val writes: OWrites[CallbackRequest] = Json.writes[CallbackRequest]

  implicit val reads: Reads[CallbackRequest] = (
    (JsPath \ "uuid").readNullable[UUID] and
      (JsPath \ "listUuid").read[String].map(validateUuid) and
      (JsPath \ "phoneNumber").readNullable[String] and
      (JsPath \ "mobilePhoneNumber").readNullable[String] and
      (JsPath \ "firstName").readNullable[String] and
      (JsPath \ "lastName").readNullable[String] and
      (JsPath \ "company").readNullable[String] and
      (JsPath \ "description").readNullable[String] and
      (JsPath \ "preferredPeriodUuid")
        .readNullable[String]
        .map(_.map(validateUuid)) and
      (JsPath \ "dueDate")
        .readNullable[String]
        .map(_.map(LocalDate.parse))
        .map(_.getOrElse(LocalDate.now())) and
      (JsPath \ "voiceMessageRef").readNullable[String]
  )(CallbackRequest.apply(_, _, _, _, _, _, _, _, _, _, _))

  implicit val writesFind: Writes[FindCallbackRequest] =
    new Writes[FindCallbackRequest] {
      def writes(f: FindCallbackRequest): JsObject =
        Json.obj(
          "filters" -> f.filters,
          "limit"   -> f.limit,
          "offset"  -> f.offset
        )
    }

  implicit val readsFind: Reads[FindCallbackRequest] = (
    (JsPath \ "filters").read[List[DynamicFilter]] and
      (JsPath \ "offset").read[Int] and
      (JsPath \ "limit").read[Int]
  )(FindCallbackRequest.apply _)

  implicit val readsResp: Reads[FindCallbackResponse] = (
    (JsPath \ "total").read[Long] and
      (JsPath \ "list").read[List[CallbackRequest]]
  )(FindCallbackResponse.apply _)

  implicit val writesResp: Writes[FindCallbackResponse] =
    new Writes[FindCallbackResponse] {
      def writes(f: FindCallbackResponse): JsObject =
        Json.obj(
          "total" -> f.total,
          "list"  -> f.list
        )
    }

}

@ImplementedBy(classOf[CallbackRequestManagerImpl])
trait CallbackRequestManager {
  def create(request: CallbackRequest): CallbackRequest
  def createFromCsv(
      cblManager: CallbackListManager,
      listUuid: String,
      csv: String
  ): Unit
  def takeWithAgent(uuid: String, agentId: Long): Unit
  def release(uuid: String): Unit
  def takenCallbacks(agentId: Long): List[UUID]
  def getCallback(uuid: String): CallbackRequest
  def find(
      filters: List[DynamicFilter],
      offset: Int = 0,
      limit: Int = 100
  ): FindCallbackResponse
  def cloture(uuid: String): Unit
  def uncloture(uuid: String): Unit
  def reschedule(uuid: String, dueDate: DateTime, periodUuid: String): Unit
  def fromJson(listUuid: String, json: JsValue): CallbackRequest

}

class CallbackRequestManagerImpl @Inject() (
    @NamedDatabase("xivo") db: Database,
    pcbManager: PreferredCallbackPeriodManager
) extends CallbackRequestManager {

  implicit val typer: FieldValueTyper = new FieldValueTyper {
    def to(field: String, value: String): ParameterValue =
      field match {
        case "listUuid" | "uuid" =>
          val t = ToParameterValue.apply[UUID]
          t(UUIDUtils.validateUuid(value))
        case "clotured" =>
          val t = ToParameterValue.apply[Boolean]
          t(value.toBoolean)
        case "agentId" | "queueId" =>
          val t = ToParameterValue.apply[Long]
          t(value.toLong)
        case "dueDate" =>
          val t = ToParameterValue.apply[Date]
          t(LocalDate.parse(value).toDate)
        case _ =>
          val t = ToParameterValue.apply[String]
          t(value)
      }

  }

  val LineSeparator = "\\r?\n"
  val CsvSeparators = List('|', ',', ';')

  val simple = get[Option[UUID]]("uuid") ~
    get[UUID]("list_uuid") ~
    get[Option[String]]("phone_number") ~
    get[Option[String]]("mobile_phone_number") ~
    get[Option[String]]("firstname") ~
    get[Option[String]]("lastname") ~
    get[Option[String]]("company") ~
    get[Option[String]]("description") ~
    get[Option[Long]]("agent_id") ~
    get[Option[Long]]("queue_id") ~
    get[Boolean]("clotured") ~
    get[Option[UUID]]("preferred_callback_period_uuid") ~
    get[Date]("due_date") ~
    get[Option[String]]("voice_message_ref") map {
      case uuid ~ lUuid ~ phone ~ mobile ~ firstname ~ lastname ~ company ~ description ~ agentId ~ queueId ~ clotured ~ periodUuid ~ dueDate ~ voiceMessageRef =>
        CallbackRequest(
          uuid,
          lUuid,
          phone,
          mobile,
          firstname,
          lastname,
          company,
          description,
          periodUuid,
          new LocalDate(dueDate),
          voiceMessageRef,
          agentId,
          queueId,
          clotured,
          periodUuid.flatMap(pcbManager.getPeriod)
        )
    }

  def buildFindQuery(
      selectClause: String,
      limitClause: String,
      filters: List[DynamicFilter]
  ): SimpleSql[Row] = {
    implicit val casing: ToSnakeCase.type = ToSnakeCase

    if (filters.isEmpty)
      SQL(selectClause + limitClause).asSimple()
    else
      SQL(
        selectClause + DynamicFilter.toSqlWhereCondition(filters) + limitClause
      ).on(DynamicFilter.toNamedParameterList(filters): _*)
  }

  override def find(
      filters: List[DynamicFilter],
      offset: Int = 0,
      limit: Int = 100
  ): FindCallbackResponse =
    db.withConnection(implicit c => {
      implicit val casing: ToSnakeCase.type = ToSnakeCase
      val select =
        "SELECT r.uuid, r.list_uuid, r.phone_number, r.mobile_phone_number, r.firstname, r.lastname, r.company, r.description, r.agent_id, l.queue_id," +
          "r.clotured, r.preferred_callback_period_uuid, r.due_date, r.voice_message_ref " +
          "FROM callback_request r inner join callback_list l on r.list_uuid = l.uuid"

      val selectCount = "SELECT count(*) FROM callback_request"

      val paging =
        DynamicFilter.toOrderClause(filters) + s" LIMIT $limit OFFSET $offset"

      val sqlSelect = buildFindQuery(select, paging, filters)
      val sqlCount  = buildFindQuery(selectCount, "", filters)

      val count = sqlCount.as(scalar[Long].single)

      val list = sqlSelect.as(simple.*)
      FindCallbackResponse(count, list)
    })

  override def createFromCsv(
      cblManager: CallbackListManager,
      listUuid: String,
      csv: String
  ) = {
    val realUuid = validateUuid(listUuid)
    csv.split(LineSeparator).toList match {
      case head :: tail =>
        CsvUtils.detectCsvSeparator(head, CsvSeparators) match {
          case None =>
            throw new InvalidParameterException("Column separator not found")
          case Some(separator) =>
            val header = CsvUtils.parseCsvLine(head, separator)
            processCsvBody(cblManager, realUuid, header.toList, tail, separator)
        }
      case Nil => throw new InvalidParameterException("File is empty")
    }
  }

  def unquote(s: String): String =
    if (s.startsWith("\"")) {
      s.stripPrefix("\"").stripSuffix("\"").replace("\"\"", "\"")
    } else s

  def processCsvBody(
      cblManager: CallbackListManager,
      listUuid: UUID,
      header: List[String],
      body: List[String],
      separator: Char
  ): Unit = {
    val dateFormat = DateTimeFormat.forPattern("yyyy-MM-dd")
    if (!cblManager.callbackListExists(listUuid))
      throw new InvalidParameterException(
        s"No CallbackList found with uuid $listUuid"
      )
    for (line <- body) {
      val map = header
        .zip(CsvUtils.parseCsvLine(line, separator))
        .toMap
        .filterNot(t => t._2 == "")
      val period =
        map.get("period").flatMap(pcbManager.getPeriodsByName(_).headOption)
      val dueDate = map
        .get("dueDate")
        .flatMap(dateStr => Try(LocalDate.parse(dateStr, dateFormat)).toOption)
        .getOrElse(LocalDate.now())

      val request = CallbackRequest(
        None,
        listUuid,
        map.get("phoneNumber"),
        map.get("mobilePhoneNumber"),
        map.get("firstName"),
        map.get("lastName"),
        map.get("company"),
        map.get("description"),
        period.flatMap(_.uuid),
        dueDate,
        preferredPeriod = period
      )
      create(request)
    }
  }

  override def create(cb: CallbackRequest): CallbackRequest =
    db.withConnection { implicit c =>
      val realCb = validatePeriod(cb)
      val uuid = SQL(
        "INSERT INTO callback_request(" +
          "list_uuid, phone_number, mobile_phone_number, firstname, lastname, company, description, preferred_callback_period_uuid, due_date, voice_message_ref) VALUES (" +
          "{lUuid}, {phone}, {mobile}, {firstname}, {lastname}, {company}, {description}, {periodUuid}, {dueDate}, {voiceMessageRef})"
      )
        .on(
          Symbol("lUuid")           -> realCb.listUuid,
          Symbol("phone")           -> realCb.phoneNumber,
          Symbol("mobile")          -> realCb.mobilePhoneNumber,
          Symbol("firstname")       -> realCb.firstName,
          Symbol("lastname")        -> realCb.lastName,
          Symbol("company")         -> realCb.company,
          Symbol("description")     -> realCb.description,
          Symbol("periodUuid")      -> realCb.preferredPeriodUuid,
          Symbol("dueDate")         -> realCb.dueDate.toDate,
          Symbol("voiceMessageRef") -> realCb.voiceMessageRef
        )
        .executeInsert(get[UUID]("uuid").*)
        .headOption
      realCb.copy(uuid = uuid)
    }

  private def validatePeriod(cb: CallbackRequest): CallbackRequest =
    cb.preferredPeriodUuid match {
      case Some(uuid) if !pcbManager.periodExists(uuid) =>
        throw new InvalidParameterException(
          s"No callback period found with uuid $uuid"
        )
      case Some(uuid) => cb
      case None =>
        cb.copy(preferredPeriodUuid = pcbManager.getDefault.flatMap(_.uuid))
    }

  def takeWithAgent(uuid: String, agentId: Long): Unit =
    db.withTransaction { implicit c =>
      val realUuid = validateUuid(uuid)
      SQL(
        "SELECT agent_id FROM callback_request WHERE uuid = {uuid} FOR UPDATE"
      ).on(Symbol("uuid") -> realUuid)
        .as(get[Option[Long]]("agent_id").*) match {
        case Nil =>
          throw new NoSuchElementException(
            s"No CallbackRequest found with uuid $realUuid"
          )
        case None :: tail =>
          SQL(
            "UPDATE callback_request SET agent_id = {agentId} WHERE uuid = {uuid}"
          ).on(Symbol("agentId") -> agentId, Symbol("uuid") -> realUuid)
            .executeUpdate()
        case Some(currentAgentId) :: tail =>
          throw new InvalidParameterException(
            s"The callback request is already held by the agent $currentAgentId"
          )
      }
      ()
    }

  override def release(uuid: String): Unit =
    db.withConnection { implicit c =>
      val realUuid = validateUuid(uuid)
      SQL("UPDATE callback_request SET agent_id = null WHERE uuid = {uuid}")
        .on(Symbol("uuid") -> realUuid)
        .executeUpdate()
      ()
    }

  override def takenCallbacks(agentId: Long): List[UUID] =
    db.withConnection { implicit c =>
      SQL("SELECT uuid FROM callback_request WHERE agent_id = {agentId}")
        .on(Symbol("agentId") -> agentId)
        .as(get[UUID]("uuid").*)
    }

  override def getCallback(uuid: String): CallbackRequest =
    db.withConnection { implicit c =>
      val realUuid = validateUuid(uuid)
      SQL(
        "SELECT cr.uuid, list_uuid, phone_number, mobile_phone_number, firstname, lastname, company, description, agent_id, cl.queue_id, " +
          "clotured, preferred_callback_period_uuid, due_date, voice_message_ref FROM callback_request cr JOIN callback_list cl ON cr.list_uuid = cl.uuid WHERE cr.uuid = {uuid}"
      )
        .on(Symbol("uuid") -> realUuid)
        .as(simple.*)
        .headOption match {
        case None =>
          throw new NoSuchElementException(
            s"No CallbackRequest found with uuid $uuid"
          )
        case Some(request) => request
      }
    }

  override def cloture(uuid: String): Unit =
    db.withConnection { implicit c =>
      val realUuid = validateUuid(uuid)
      SQL("UPDATE callback_request SET clotured = TRUE WHERE uuid = {uuid}")
        .on(Symbol("uuid") -> realUuid)
        .executeUpdate()
      ()
    }

  override def uncloture(uuid: String): Unit =
    db.withConnection { implicit c =>
      val realUuid = validateUuid(uuid)

      SQL(
        "UPDATE callback_request SET clotured = FALSE, agent_id = null WHERE uuid = {uuid}"
      ).on(Symbol("uuid") -> realUuid).executeUpdate()
      ()
    }

  override def reschedule(
      uuid: String,
      dueDate: DateTime,
      periodUuid: String
  ): Unit =
    db.withConnection { implicit c =>
      val realUuid = validateUuid(uuid)

      SQL("""UPDATE callback_request
        | SET clotured = FALSE,
        | agent_id = null,
        | due_date = {dueDate},
        | preferred_callback_period_uuid = {periodUuid}
        | WHERE uuid = {uuid}""".stripMargin)
        .on(
          Symbol("uuid")       -> realUuid,
          Symbol("dueDate")    -> dueDate.toDate,
          Symbol("periodUuid") -> validateUuid(periodUuid)
        )
        .executeUpdate()
      ()
    }

  override def fromJson(listUuid: String, json: JsValue): CallbackRequest =
    (json.as[JsObject] + ("listUuid" -> Json.toJson(listUuid)))
      .validate[CallbackRequest] match {
      case JsSuccess(req, _) => req
      case JsError(e) =>
        throw new InvalidParameterException(
          "Cannot parse JSON : " + e.toString()
        )
    }
}
