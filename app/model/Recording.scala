package model

import model.ws.{ErrorResult, ErrorType}
import play.api.libs.json._
import play.api.mvc.{Result, Results}

sealed trait RecordingResult {
  def toResult: Result
}

object RecordingModeType extends Enumeration {
  type Type = Value

  val NotRecorded      = Value("notrecorded")
  val Recorded         = Value("recorded")
  val RecordedOnDemand = Value("recordedondemand")

  implicit val format: Format[Type] = new Format[Type] {
    def writes(r: Type): JsValue = JsString(r.toString)
    def reads(json: JsValue): JsResult[Type] =
      JsSuccess(RecordingModeType.withName(json.as[String]))
  }
}

case class RecordingMode(mode: RecordingModeType.Type) extends RecordingResult {
  override def toResult: Result = Results.Ok(Json.toJson(this))
}

object RecordingMode {
  implicit val recordingModeWrites: Writes[RecordingMode] =
    Json.writes[RecordingMode]
}

case class RecordingConfig(activated: Boolean, mode: RecordingModeType.Type)
    extends RecordingResult {
  override def toResult: Result = Results.Ok(Json.toJson(this))
}

object RecordingConfig {
  implicit val RecordingConfigReads: Reads[RecordingConfig] =
    Json.reads[RecordingConfig]
  implicit val RecordingConfigWrites: Writes[RecordingConfig] =
    Json.writes[RecordingConfig]
}

case class RecordingQueuesStatus(
    all: Boolean,
    queues: Map[Option[Long], RecordingModeType.Type]
) extends RecordingResult {
  override def toResult: Result = Results.Ok(Json.toJson(this))
}

object RecordingQueuesStatus {

  implicit val recordingQueuesStatusPublishWrites
      : Writes[RecordingQueuesStatus] {
        def writes(recordingQueuesStatus: RecordingQueuesStatus): JsObject
      } = new Writes[RecordingQueuesStatus] {
    def writes(recordingQueuesStatus: RecordingQueuesStatus) =
      Json.obj(
        "all" -> recordingQueuesStatus.all,
        "queues" -> Json.toJson(recordingQueuesStatus.queues.map {
          case (key, value) => key.getOrElse("").toString -> value
        })
      )
  }
}

case class RecordingQueuesStatusPublish(
    all: Boolean,
    queues: Map[Option[Long], RecordingModeType.Type]
) extends RecordingResult {
  override def toResult: Result = Results.Ok(Json.toJson(this))
}

object RecordingQueuesStatusPublish {
  implicit val recordingQueuesStatusPublishWrites
      : Writes[RecordingQueuesStatusPublish] {
        def writes(recordingQueuesStatus: RecordingQueuesStatusPublish)
            : JsObject
      } = new Writes[RecordingQueuesStatusPublish] {
    def writes(recordingQueuesStatus: RecordingQueuesStatusPublish) =
      Json.obj(
        "all" -> recordingQueuesStatus.all,
        "queues" -> Json.toJson(recordingQueuesStatus.queues.map {
          case (key, value) => key.getOrElse("").toString -> value
        })
      )
  }
}

trait RecordingErrorType extends ErrorType
case object Unreachable  extends RecordingErrorType { val error = "Unreachable" }
case object QueueNotFound extends RecordingErrorType {
  val error = "QueueNotFound"
}

case class RecordingError(error: ErrorType, message: String)
    extends ErrorResult {
  override def toResult: Result = {
    log.error(this.message)
    val body = Json.toJson(this)
    error match {
      case Unreachable   => Results.ServiceUnavailable(body)
      case QueueNotFound => Results.NotFound(body)
      case _             => Results.InternalServerError(body)
    }
  }
}

object RecordingError {
  implicit val recordErrorWrites: Writes[RecordingError] =
    Json.writes[RecordingError]
}
