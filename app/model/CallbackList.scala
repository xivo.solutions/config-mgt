package model

import java.util.{Date, UUID}
import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import com.google.inject.ImplementedBy
import model.UUIDUtils._
import org.joda.time.{LocalDate, LocalTime}
import play.api.db.{Database, _}
import play.api.libs.functional.syntax._
import play.api.libs.json._

case class CallbackList(
    uuid: Option[UUID],
    name: String,
    queueId: Long,
    callbacks: List[CallbackRequest]
) {}

object CallbackList {

  implicit val writes: Writes[CallbackList] = new Writes[CallbackList] {
    def writes(cbList: CallbackList): JsObject =
      Json.obj(
        "uuid"      -> cbList.uuid,
        "name"      -> cbList.name,
        "queueId"   -> cbList.queueId,
        "callbacks" -> cbList.callbacks.map(cbr => cbr)
      )
  }

  implicit val reads: Reads[CallbackList] = (
    (JsPath \ "name").read[String] and
      (JsPath \ "queueId").read[Long]
  )(CallbackList.apply(None, _, _, List()))
}

@ImplementedBy(classOf[CallbackManagerImpl])
trait CallbackListManager {
  def all(withRequests: Boolean): List[CallbackList]

  def create(list: CallbackList): CallbackList

  def delete(uuid: String): Unit

  def callbackListExists(uuid: UUID): Boolean
}

class CallbackManagerImpl @Inject() (@NamedDatabase("xivo") db: Database)
    extends CallbackListManager {

  val simple: RowParser[CallbackList] =
    get[UUID]("uuid") ~
      get[String]("name") ~
      get[Long]("queue_id") map { case uuid ~ name ~ queueId =>
        CallbackList(Some(uuid), name, queueId, List.empty)
      }

  val nested = get[UUID]("list_uuid") ~
    get[String]("list_name") ~
    get[Long]("queue_id") ~
    get[Option[UUID]]("uuid") ~
    get[Option[String]]("phone_number") ~
    get[Option[String]]("mobile_phone_number") ~
    get[Option[String]]("firstname") ~
    get[Option[String]]("lastname") ~
    get[Option[String]]("company") ~
    get[Option[String]]("description") ~
    get[Option[Long]]("agent_id") ~
    get[Option[Boolean]]("clotured") ~
    get[Option[Date]]("due_date") ~
    get[Option[String]]("voice_message_ref") ~
    get[Option[UUID]]("period_uuid") ~
    get[Option[String]]("period_name") ~
    get[Option[Date]]("period_start") ~
    get[Option[Date]]("period_end") ~
    get[Option[Boolean]]("period_isdefault") map {
      case list_uuid ~ list_name ~ queueId ~ uuid ~ phone ~ mobile ~ firstname ~ lastname ~ company ~ description ~ agentId ~ clotured ~ dueDate ~ voiceMessageRef ~ period_uuid ~ period_name ~ period_start ~ period_end ~ period_default =>
        val cbl = CallbackList(Some(list_uuid), list_name, queueId, List.empty)
        val period = for {
          puid       <- period_uuid
          pname      <- period_name
          pstart     <- period_start
          pend       <- period_end
          pisdefault <- period_default
        } yield PreferredCallbackPeriod(
          Some(puid),
          pname,
          new LocalTime(pstart),
          new LocalTime(pend),
          pisdefault
        )

        val request = for {
          ruid      <- uuid
          rclotured <- clotured
          rdue_date <- dueDate
        } yield CallbackRequest(
          Some(ruid),
          list_uuid,
          phone,
          mobile,
          firstname,
          lastname,
          company,
          description,
          period_uuid,
          new LocalDate(rdue_date),
          voiceMessageRef,
          agentId,
          Some(queueId),
          rclotured,
          period
        )

        (cbl, request)
    }

  override def all(withRequests: Boolean): List[CallbackList] =
    db.withConnection(implicit c => {
      if (withRequests) {
        val pair = SQL("""SELECT l.uuid as list_uuid, l.name as list_name, l.queue_id,
                    r.uuid, r.phone_number, r.mobile_phone_number, r.firstname, 
                    r.lastname, r.company, r.description, r.agent_id, r.clotured, 
                    r.preferred_callback_period_uuid, r.due_date, r.voice_message_ref,
                    p.uuid as period_uuid, p.name as period_name, p.period_start, p.period_end, 
                    p."default" as period_isdefault
             FROM callback_list l
             LEFT JOIN callback_request r ON r.list_uuid=l.uuid AND NOT r.clotured
             LEFT JOIN preferred_callback_period p on r.preferred_callback_period_uuid=p.uuid
         """)
          .as(nested.*)

        pair
          .groupBy(_._1)
          .view
          .mapValues(_.map(_._2))
          .toMap
          .map(p => p._1.copy(callbacks = p._2.flatten))
          .toList
      } else {
        SQL("SELECT uuid, name, queue_id FROM callback_list").as(simple.*)
      }
    })

  override def callbackListExists(uuid: UUID): Boolean =
    db.withConnection { implicit c =>
      SQL("SELECT uuid FROM callback_list WHERE uuid = {uuid}")
        .on(Symbol("uuid") -> uuid)
        .as((get[UUID]("uuid")).*)
        .nonEmpty
    }

  override def create(list: CallbackList): CallbackList =
    db.withConnection(implicit c => {
      val uuid = SQL(
        "INSERT INTO callback_list(name, queue_id) VALUES ({name}, {queueId})"
      ).on(Symbol("name") -> list.name, Symbol("queueId") -> list.queueId)
        .executeInsert(get[UUID]("uuid").*)
        .headOption
      list.copy(uuid = uuid)
    })

  override def delete(uuid: String): Unit = {
    val realUuid = validateUuid(uuid)
    if (!callbackListExists(realUuid))
      throw new NoSuchElementException(s"No CallbackList found with uuid $uuid")
    db.withTransaction(implicit c => {
      SQL("DELETE FROM callback_request WHERE list_uuid = {uuid}")
        .on(Symbol("uuid") -> realUuid)
        .executeUpdate()
      SQL("DELETE FROM callback_list WHERE uuid = {uuid}")
        .on(Symbol("uuid") -> realUuid)
        .executeUpdate()
    })
    ()
  }
}
