package model

import javax.inject.Inject

import play.api.db.{Database, _}
import play.api.libs.json._
import play.api.libs.functional.syntax._
import anorm._
import anorm.SqlParser._
import com.google.inject.ImplementedBy

case class XivoCtiUser(
    firstName: String,
    lastName: Option[String],
    login: String
) {
  def toJson: JsValue =
    Json.obj("firstname" -> firstName, "lastname" -> lastName, "login" -> login)
}

object XivoCtiUser {
  implicit val writes: Writes[XivoCtiUser] = new Writes[XivoCtiUser] {
    def writes(u: XivoCtiUser): JsObject =
      Json.obj(
        "firstname" -> u.firstName,
        "lastname"  -> u.lastName,
        "login"     -> u.login
      )
  }

  implicit val reads: Reads[XivoCtiUser] = (
    (JsPath \ "firstname").read[String] and
      (JsPath \ "lastname").readNullable[String] and
      (JsPath \ "login").read[String]
  )(XivoCtiUser.apply(_, _, _))
}

@ImplementedBy(classOf[XivoCtiUserManagerImpl])
trait XivoCtiUserManager {
  def all: List[XivoCtiUser]

}

class XivoCtiUserManagerImpl @Inject() (@NamedDatabase("xivo") dbXivo: Database)
    extends XivoCtiUserManager {

  val simple: RowParser[XivoCtiUser] =
    get[String]("firstname") ~ get[Option[String]]("lastname") ~ get[String](
      "loginclient"
    ) map { case firstName ~ lastName ~ login =>
      XivoCtiUser(firstName, lastName, login)
    }

  override def all: List[XivoCtiUser] =
    dbXivo.withConnection { implicit c =>
      SQL(
        "SELECT firstname, lastname, loginclient FROM userfeatures WHERE loginclient IS NOT NULL AND loginclient != ''"
      ).as(simple.*)
    }
}
