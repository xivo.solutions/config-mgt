package model.ws

import play.api.Logger
import play.api.libs.json.{JsString, JsValue, Json, Writes}
import play.api.mvc.{Result, Results}

trait ErrorResult {
  val log: Logger = Logger("ErrorResult")
  def toResult: Result
}

trait ErrorType { def error: String }
case object JsonParsingError extends ErrorType {
  val error = "JsonParsingError"
}
case object NotHandledError extends ErrorType { val error = "NotHandledError" }
case object NotFoundError   extends ErrorType { val error = "NotFoundError"   }

object ErrorType {
  implicit val writes: Writes[ErrorType] = new Writes[ErrorType] {
    def writes(e: ErrorType): JsValue = JsString(e.error)
  }
}

object GenericError {
  implicit val genericErrorWrites: Writes[GenericError] =
    Json.writes[GenericError]
}

case class GenericError(code: ErrorType, message: String) extends ErrorResult {
  override def toResult: Result = {
    log.error(this.message)
    val body = Json.toJson(this)
    code match {
      case JsonParsingError => Results.BadRequest
      case NotFoundError    => Results.NotFound(body)
      case _                => Results.InternalServerError(body)
    }
  }
}
