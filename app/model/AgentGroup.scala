package model

import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import com.google.inject.ImplementedBy
import play.api.db.{Database, NamedDatabase}
import play.api.libs.functional.syntax._
import play.api.libs.json._

case class AgentGroup(id: Option[Long], name: String)

object AgentGroup {
  implicit val agGroupWrites: OWrites[AgentGroup] =
    ((__ \ "id").write[Option[Long]] and
      (__ \ "name").write[String])(unlift(AgentGroup.unapply))
}

@ImplementedBy(classOf[AgentGroupManagerImpl])
trait AgentGroupManager {
  def all(): List[AgentGroup]
  def withIds(ids: List[Int]): List[AgentGroup]
}

class AgentGroupManagerImpl @Inject() (@NamedDatabase("xivo") dbXivo: Database)
    extends AgentGroupManager {

  val simple: RowParser[AgentGroup] =
    get[Long]("id") ~ get[String]("name") map { case id ~ name =>
      AgentGroup(Some(id), name)
    }

  def all(): List[AgentGroup] =
    dbXivo.withConnection { implicit c =>
      SQL("select  id, name from agentgroup where deleted=0").as(simple.*)
    }

  override def withIds(ids: List[Int]): List[AgentGroup] =
    dbXivo.withConnection { implicit c =>
      if (ids.isEmpty) List()
      else
        SQL("SELECT id, name FROM agentgroup WHERE id IN ({ids})")
          .on(Symbol("ids") -> ids.asInstanceOf[Seq[Int]])
          .as(simple.*)
    }
}
