package model

import javax.inject.Inject

import play.api.db.{Database, NamedDatabase}
import play.api.libs.functional.syntax._
import play.api.libs.json._
import anorm._
import anorm.SqlParser._
import com.google.inject.ImplementedBy

case class XivoCtiUserWithProfile(
    name: String,
    firstName: String,
    login: String,
    profile: String
) {
  def toJson =
    Json.obj(
      "name"      -> name,
      "firstname" -> firstName,
      "login"     -> login,
      "profile"   -> profile
    )
}

object XivoCtiUserWithProfile {
  implicit val writes: Writes[XivoCtiUserWithProfile] =
    new Writes[XivoCtiUserWithProfile] {
      def writes(u: XivoCtiUserWithProfile): JsObject =
        Json.obj(
          "name"      -> u.name,
          "firstname" -> u.firstName,
          "login"     -> u.login,
          "profile"   -> u.profile
        )
    }

  implicit val reads: Reads[XivoCtiUserWithProfile] = (
    (JsPath \ "name").read[String] and
      (JsPath \ "firstname").read[String] and
      (JsPath \ "login").read[String] and
      (JsPath \ "profile").read[String]
  )(XivoCtiUserWithProfile.apply(_, _, _, _))
}

@ImplementedBy(classOf[UserProfileManagerImpl])
trait UserProfileManager {
  def list: List[XivoCtiUserWithProfile]
}

class UserProfileManagerImpl @Inject() (
    @NamedDatabase("xivo") dbXivo: Database
) extends UserProfileManager {
  private val partialUser = get[String]("login") ~ get[String]("type") map {
    case login ~ profile => (login, profile)
  }

  private def fullUser(login: String, profile: String) =
    get[String]("lastname") ~ get[String]("firstname") map {
      case lastname ~ firstname =>
        XivoCtiUserWithProfile(lastname, firstname, login, profile)
    }

  override def list: List[XivoCtiUserWithProfile] =
    dbXivo.withConnection { xivoConn =>
      SQL("SELECT login, type::varchar AS type FROM users")
        .as(partialUser.*)(xivoConn)
        .flatMap(t =>
          SQL(
            "SELECT lastname, firstname FROM userfeatures WHERE loginclient = {login}"
          ).on(Symbol("login") -> t._1)
            .as(fullUser(t._1, t._2).*)(xivoConn)
            .headOption
        )
    }
}
