package xc.service

import play.api.libs.ws.WSClient

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import play.api.mvc._
import play.api.libs.ws._

class XivoWebService(wSClient: WSClient, xivoHost: String) {

  def getWS = wSClient

  def validateCookieAgainstXiVO(cookie: Cookie): Future[XivoCookieResponse] = {
    val ws = getWS

    ws.url(
      s"https://$xivoHost/service/ipbx/auth.php/control_system/authenticate/"
    ).addCookies(DefaultWSCookie(cookie.name, cookie.value))
      .get()
      .map { response =>
        if (response.status == 200) XivoCookieResponse()
        else if (response.status == 403) throw new ForbiddenException
        else
          throw XivoCookieException(
            s"Request to XiVO cookie authentication failed with status ${response.status} and message ${response.body}"
          )
      }
  }
}

case class XivoCookieResponse()
class ForbiddenException extends Exception("This cookie is not valid.")
case class XivoCookieException(message: String)
    extends Exception(message: String)
