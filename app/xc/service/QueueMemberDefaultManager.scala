package xc.service

import anorm._
import com.google.inject.{ImplementedBy, Inject}
import common.{AnormMacro, Crud, CrudMacro}
import play.api.db.{Database, _}
import xc.model.{QueueMemberDefault, QueueMemberDefaultKey}

import scala.util.{Failure, Success, Try}

@ImplementedBy(classOf[QueueMemberDefaultManagerImpl])
trait QueueMemberDefaultManager
    extends Crud[QueueMemberDefault, QueueMemberDefaultKey] {

  def getByUserId(userId: Long): Try[List[QueueMemberDefault]]
  def deleteByUserId(userId: Long): Try[List[QueueMemberDefault]]
  def setForUserId(
      userId: Long,
      l: List[QueueMemberDefault]
  ): Try[List[QueueMemberDefault]]
}

class QueueMemberDefaultManagerImpl @Inject() (
    @NamedDatabase("xc") dbXc: Database,
    userManager: UserQueueManager,
    queueManager: QueueManager
) extends QueueMemberDefaultManager {

  val crudParser: RowParser[QueueMemberDefault] =
    AnormMacro.namedParser[QueueMemberDefault](
      AnormMacro.ColumnNaming.SnakeCase
    )

  override def create(t: QueueMemberDefault): Try[QueueMemberDefault] =
    dbXc.withConnection { implicit c =>
      val query = CrudMacro.query[QueueMemberDefault](
        t,
        "xc.queue_members_default",
        Set("queueId", "userId"),
        Set(),
        false,
        CrudMacro.SnakeCase,
        CrudMacro.SQLInsert
      )

      for {
        u <- userManager.get(t.userId)
        q <- queueManager.get(t.queueId)
        r <- Try({ query.executeInsert(); t })
      } yield (r)
    }

  override def update(t: QueueMemberDefault): Try[QueueMemberDefault] =
    dbXc.withConnection { implicit c =>
      val updateCount = CrudMacro
        .query[QueueMemberDefault](
          t,
          "xc.queue_members_default",
          Set("queueId", "userId"),
          Set(),
          false,
          CrudMacro.SnakeCase,
          CrudMacro.SQLUpdate
        )
        .executeUpdate()

      if (updateCount == 1)
        Success(t)
      else
        Failure(new Exception("Cannot update QueueMemberDefault"))
    }

  override def get(id: QueueMemberDefaultKey): Try[QueueMemberDefault] =
    dbXc.withConnection { implicit c =>
      Try(
        SQL(
          "select  * from xc.queue_members_default where queue_id={queueId} and user_id={userId}"
        ).on("queueId" -> id.queueId, "userId" -> id.userId)
          .as(crudParser.single)
      )
    }

  override def all(): Try[List[QueueMemberDefault]] =
    dbXc.withConnection { implicit c =>
      Try(SQL("select  * from xc.queue_members_default").as(crudParser.*))
    }

  override def delete(id: QueueMemberDefaultKey): Try[QueueMemberDefault] =
    dbXc.withConnection { implicit c =>
      val entry = get(id)
      if (entry.isFailure) entry // already deleted
      else {
        val deletedCount = SQL(
          "delete from xc.queue_members_default where queue_id={queueId} and user_id={userId}"
        ).on("queueId" -> id.queueId, "userId" -> id.userId).executeUpdate()

        if (deletedCount == 1) entry
        else Failure(new Exception("Cannot delete QueueMemberDefault"))
      }
    }

  override def getByUserId(userId: Long): Try[List[QueueMemberDefault]] =
    dbXc.withConnection { implicit c =>
      Try(
        SQL("select  * from xc.queue_members_default where user_id={userId}")
          .on("userId" -> userId)
          .as(crudParser.*)
      )
    }

  override def deleteByUserId(userId: Long): Try[List[QueueMemberDefault]] =
    dbXc.withConnection { implicit c =>
      val entries = getByUserId(userId)
      if (entries.isFailure) entries // already deleted
      else {
        val deletedCount =
          SQL("delete from xc.queue_members_default where user_id={userId}")
            .on("userId" -> userId)
            .executeUpdate()

        if (deletedCount > 0) entries
        else
          Failure(
            new Exception(s"Cannot delete QueueMemberDefault for user $userId")
          )
      }
    }

  override def setForUserId(
      userId: Long,
      l: List[QueueMemberDefault]
  ): Try[List[QueueMemberDefault]] =
    dbXc.withTransaction { implicit c =>
      if (!l.forall(_.userId == userId)) {
        Failure(
          new IllegalArgumentException(
            s"All QueueMemberDefault should be owned by the user $userId"
          )
        )
      }
      deleteByUserId(userId)
      Try(l.map(create(_) match {
        case Success(m) => m
        case Failure(t) => throw t
      }))
    }
}
