package xc.model

case class QueueMemberDefaultKey(queueId: Long, userId: Long)

case class QueueMemberDefault(queueId: Long, userId: Long, penalty: Int) {
  def getKey: QueueMemberDefaultKey = QueueMemberDefaultKey(queueId, userId)
}
