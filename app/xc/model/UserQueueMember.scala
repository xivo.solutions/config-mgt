package xc.model

case class UserQueueMember(
    id: Long,
    firstName: String,
    lastName: String,
    login: String
)
