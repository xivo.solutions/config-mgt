package xc.model

case class Queue(id: Long, name: String, displayName: String, number: String)
