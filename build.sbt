import com.typesafe.sbt.packager.MappingsHelper._
import com.typesafe.sbt.packager.docker._
import com.typesafe.sbt.web.pipeline.Pipeline
import com.typesafe.sbt.web.{CompileProblemsException, GeneralProblem, SbtWeb}
import play.sbt.PlayImport.PlayKeys.playRunHooks
import sbt.Append.appendSeqImplicit

import scala.sys.process._

val appName = "config-mgt"
// Do not change version directly but change TARGET_VERSION file and run `xivocc-build.sh apply-version`
val appVersion = sys.env.getOrElse("TARGET_VERSION", "dev-version")
val appOrganisation = "xivo"

lazy val macros = Project("macro", file("macro"))
  .settings(
    version := "0.1-SNAPSHOT",
    addCompilerPlugin(scalafixSemanticdb),
    scalaCompilerSettings,
    scalaVersion := Dependencies.scalaVersion,
    resolvers ++= Dependencies.resolutionRepos,
    libraryDependencies ++= Seq(
      "org.scala-lang" % "scala-compiler" % Dependencies.scalaVersion,
      "org.scala-lang" % "scala-reflect" % Dependencies.scalaVersion
    ) ++ Dependencies.runDep ++ Dependencies.testDep
  )
  .settings(
    publish := {}
  )

swaggerV3 := true
swaggerPrettyJson := true

lazy val main = Project(appName, file("."))
  .enablePlugins(PlayScala, SbtWeb, DockerPlugin, BuildInfoPlugin, SwaggerPlugin)
  .settings(
    name := appName,
    version := appVersion,
    organization := appOrganisation,
    resolvers ++= Dependencies.resolutionRepos,
    libraryDependencies ++= Dependencies.runDep ++ Dependencies.testDep,
    Assets / LessKeys.less / excludeFilter := new FileFilter{ def accept(f: File) = ".*(/node_modules/.*)".r.pattern.matcher(f.getAbsolutePath).matches },
    dist := (dist dependsOn webpack).value,
    semanticdbEnabled := true,
    semanticdbVersion := scalafixSemanticdb.revision,
    scalaCompilerSettings,
    swaggerDomainNameSpaces := Seq(
      "xivo.model",
      "ws.model",
      "xc.model",
      "model",
      "xivo.service"
    )
  )
  .settings(coverageExcludedPackages := "<empty>;views\\..*;Reverse.*")
  .settings(setVersionSettings: _*)
  .settings(testSettings: _*)
  .settings(dockerSettings: _*)
  .settings(editSourceSettings: _*)
  .settings(buildInfoCustomSettings: _*)
  .settings(docSettings: _*)
  .settings(javaScriptSettings: _*)
  .aggregate(macros)
  .dependsOn(macros)

lazy val setVersionVarTask = taskKey[Unit]("Set version to a env var")
lazy val webpack = taskKey[Unit]("Webpack stage")
lazy val webpackPipeline = taskKey[Pipeline.Stage]("Webpack pipeline")
lazy val npmTest = taskKey[Unit]("Run NPM headless test")

lazy val setVersionSettings = Seq(
  setVersionVarTask := { System.setProperty("APPLI_VERSION", appVersion) },
  EditSource / edit := ((EditSource / edit) dependsOn (EditSource / EditSourcePlugin.autoImport.clean)).value,
  Compile / packageBin := ((Compile / packageBin) dependsOn (EditSource / edit)).value,
  Compile / run := ((Compile / run) dependsOn setVersionVarTask).evaluated
)

lazy val editSourceSettings = Seq(
  EditSource / flatten := true,
  Universal / mappings += file("target/version/appli.version") -> "conf/appli.version",
  EditSource / targetDirectory := baseDirectory(_/"target/version").value,
  EditSource / variables += (version {version => ("SBT_EDIT_APP_VERSION", appVersion)}).value,
  (EditSource / sources) ++= (baseDirectory map { bd =>
    (bd / "src/res" * "appli.version").get
  }).value
)

lazy val javaScriptSettings = Seq(
  PlayKeys.playRunHooks += (baseDirectory.map(base => Webpack(base))).value,
  webpack := {
    val log = new CustomLogger
    if("npm ci".!(log) != 0) throw new CompileProblemsException(Array(new GeneralProblem(log.buf.toString, file("./package.json"))))
    if("npm run build".!(log) != 0) throw new CompileProblemsException(Array(new GeneralProblem(log.buf.toString, file("./webpack.config.js"))))
  },
  npmTest := {
    val log = new CustomLogger
    if("npm ci".!(log) != 0) throw new CompileProblemsException(Array(new GeneralProblem(log.buf.toString, file("./package.json"))))
    if("npm run test-headless".!(log) != 0) throw new RuntimeException("NPM Tests failed")
  },
  webpackPipeline := { mappings =>
    val webpackBaseDir = baseDirectory.value / "target/web/public/main/dist"
    val webpackRebaseDir = baseDirectory.value / "target/web/public/main/"
    val webpackFiles = PathFinder(webpackBaseDir).allPaths --- webpackBaseDir pair relativeTo(webpackRebaseDir)
    mappings ++ webpackFiles
  },
  pipelineStages := Seq(webpackPipeline, digest)
)

lazy val testSettings = Seq(
  Test / parallelExecution := false,
  (Test / test) := ((Test / test) dependsOn (npmTest)).value,
  Test / javaOptions += "-Dlogger.file=test/resources/logback-test.xml"
)

lazy val scalaCompilerSettings = Seq (
  scalaVersion := Dependencies.scalaVersion,
  scalacOptions ++= Seq(
    "-encoding", "UTF-8",
    "-unchecked",
    "-deprecation",
    "-feature",
    "-language:existentials",
    "-language:higherKinds",
    "-language:implicitConversions",
    "-Xlint:adapted-args",
    "-Ymacro-annotations",
    "-Ywarn-dead-code",
    "-Wunused:imports",
    s"-Wconf:src=${target.value}/.*:s"
  )
)

lazy val buildInfoCustomSettings = Seq(
  buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
  buildInfoPackage := "server.info"
)

lazy val dockerSettings = Seq(
  Docker / maintainer := "R&D <randd@xivo.solutions>",
  dockerBaseImage := "openjdk:8u275-jdk-slim-buster",
  dockerExposedPorts := Seq(9000),
  dockerExposedVolumes := Seq("/conf","/logs"),
  dockerRepository := Some("xivoxc"),
  dockerCommands += Cmd("LABEL", s"""version="${appVersion}""""),
  dockerEntrypoint := Seq("bin/config-mgt-docker"),
  Docker / daemonUserUid := None,
  Docker / daemonUser := "daemon",
  dockerChmodType := DockerChmodType.UserGroupWriteExecute,
  (Docker / stage) := ((Docker / stage) dependsOn webpack).value
)


lazy val docSettings = Seq(
  Compile / packageDoc / publishArtifact := false,
  Compile / doc / sources := Seq.empty
)

