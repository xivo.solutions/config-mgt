#!/usr/bin/env bash
set -e

sbt clean 'scalafix --check' 'scalafmtCheck' test docker:publishLocal
